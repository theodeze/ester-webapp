package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Mail;

import java.util.List;

public class MailDAOTest extends ModelDAOTest<Mail> {

    public MailDAOTest() {
        super(MailDAO.getInstance(), Mail.class);
    }

    @Override
    protected List<Mail> getModels() {
        return DataEster.getListMail();
    }
}