package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.Question;
import fr.univangers.esterwebapp.DataEster;

import java.util.List;

public class QuestionDAOTest extends ModelDAOTest<Question> {

    public QuestionDAOTest() {
        super(QuestionDAO.getInstance(), Question.class);
    }

    @Override
    protected List<Question> getModels() {
        return DataEster.getListQuestions();
    }
}