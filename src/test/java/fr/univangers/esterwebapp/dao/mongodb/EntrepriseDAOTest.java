package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Entreprise;
import java.util.List;

public class EntrepriseDAOTest extends ConnexionDAOTest<Entreprise> {

    public EntrepriseDAOTest() {
        super(EntrepriseDAO.getInstance(), Entreprise.class);
    }

    @Override
    protected List<Entreprise> getModels() {
        return DataEster.getListEntreprise();
    }
}