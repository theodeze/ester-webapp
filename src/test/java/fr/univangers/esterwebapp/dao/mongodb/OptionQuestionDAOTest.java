package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.OptionQuestion;

import java.util.List;

public class OptionQuestionDAOTest extends ModelDAOTest<OptionQuestion> {

    public OptionQuestionDAOTest() {
        super(OptionQuestionDAO.getInstance(), OptionQuestion.class);
    }

    @Override
    protected List<OptionQuestion> getModels() {
        return DataEster.getListOptionQuestion();
    }
}