package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Reponse;

import java.util.List;

public class ReponseDAOTest extends ModelDAOTest<Reponse> {

    public ReponseDAOTest() {
        super(ReponseDAO.getInstance(), Reponse.class);
    }

    @Override
    protected List<Reponse> getModels() {
        return DataEster.getListReponse();
    }
}