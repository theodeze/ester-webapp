package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.Resultat;
import fr.univangers.esterwebapp.DataEster;

import java.util.List;

public class ResultatDAOTest extends ModelDAOTest<Resultat> {

    public ResultatDAOTest() {
        super(ResultatDAO.getInstance(), Resultat.class);
    }

    @Override
    protected List<Resultat> getModels() {
        return DataEster.getListResultat();
    }
}