package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.operation.OperationConnexionTest;
import fr.univangers.esterwebapp.model.Connexion;

public abstract class ConnexionDAOTest<T extends Connexion> extends OperationConnexionTest<T> {

    protected ConnexionDAOTest(ConnexionDAO<T> connexionDAO, Class<T> connexionClass) {
        super(connexionDAO, connexionClass);
    }
}