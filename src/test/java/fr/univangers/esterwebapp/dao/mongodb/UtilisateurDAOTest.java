package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.operation.OperationUtilisateurTest;
import fr.univangers.esterwebapp.model.Utilisateur;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;

public abstract class UtilisateurDAOTest<T extends Utilisateur> extends OperationUtilisateurTest<T> {

    protected UtilisateurDAOTest(UtilisateurDAO<T> utilisateurDAO, Class<T> utilisateurClass) {
        super(utilisateurDAO, utilisateurClass);
    }

    /**
     * Sauvegarde d'un modèle qui existe déjà par son identifiant
     */
    @Test
    public void saveExistID() {
        try {
            this.operationModel.save(this.models.get(0));
            //Uniquement l'identifiant
            T model = this.classModel.newInstance();
            model.setIdentifiant(this.models.get(0).getIdentifiant());
            boolean test = this.operationModel.save(model);
            assertFalse(test);
        }
        catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Création d'un modèle qui existe déjà par son identifiant
     */
    @Test
    public void createExistID() {
        try {
            this.operationModel.create(this.models.get(0));
            //Uniquement l'identifiant
            T model = this.classModel.newInstance();
            model.setIdentifiant(this.models.get(0).getIdentifiant());
            boolean test = this.operationModel.create(model);
            assertFalse(test);
        }
        catch (Exception e) {
            fail(e.getMessage());
        }
    }
}