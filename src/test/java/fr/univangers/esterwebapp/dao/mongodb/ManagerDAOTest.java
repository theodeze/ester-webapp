package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.TestEster;
import fr.univangers.esterwebapp.util.scanner.Scanner;
import org.junit.jupiter.api.Test;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class ManagerDAOTest extends TestEster {

    @Test
    public void testGetEntities() {
        int sizeExpected = Scanner.scanPackages(DAO.class, ModelDAO.class.getPackage().getName()).result().size();
        int sizeActual = ManagerDAO.getInstance().getEntities().size();

        assertEquals(sizeExpected, sizeActual);
    }

    @Override
    protected List<?> getModels() {
        return null;
    }
}