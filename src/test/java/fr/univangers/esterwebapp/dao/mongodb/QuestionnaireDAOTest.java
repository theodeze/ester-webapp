package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Questionnaire;
import java.util.List;

public class QuestionnaireDAOTest extends ModelDAOTest<Questionnaire> {

    public QuestionnaireDAOTest() {
        super(QuestionnaireDAO.getInstance(), Questionnaire.class);
    }

    @Override
    protected List<Questionnaire> getModels() {
        return DataEster.getListQuestionnaire();
    }
}