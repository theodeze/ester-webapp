package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Salarie;
import java.util.List;

public class SalarieDAOTest extends UtilisateurDAOTest<Salarie> {

    public SalarieDAOTest() {
        super(SalarieDAO.getInstance(), Salarie.class);
    }

    @Override
    protected List<Salarie> getModels() {
        return DataEster.getListSalarie();
    }
}