package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.ReponseRef;

import java.util.List;

public class ReponseRefDAOTest extends ModelDAOTest<ReponseRef> {

    public ReponseRefDAOTest() {
        super(ReponseRefDAO.getInstance(), ReponseRef.class);
    }

    @Override
    protected List<ReponseRef> getModels() {
        return DataEster.getListReponseRef();
    }
}