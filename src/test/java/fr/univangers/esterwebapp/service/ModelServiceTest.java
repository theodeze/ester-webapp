package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.operation.OperationModelTest;

public abstract class ModelServiceTest<T extends Model> extends OperationModelTest<T> {

    protected ModelServiceTest(ModelService<T> modelService, Class<T> modelClass) {
        super(modelService, modelClass);
    }
}