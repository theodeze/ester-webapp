package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.model.SalarieRef;
import fr.univangers.esterwebapp.DataEster;

import java.util.List;

public class SalarieServiceRefTest extends UtilisateurServiceTest<SalarieRef> {

    public SalarieServiceRefTest() {
        super(SalarieRefService.getInstance(), SalarieRef.class);
    }

    @Override
    protected List<SalarieRef> getModels() {
        return DataEster.getListSalarieRef();
    }
}