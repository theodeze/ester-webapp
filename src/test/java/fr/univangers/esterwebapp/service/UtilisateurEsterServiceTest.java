package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import org.junit.jupiter.api.Test;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class UtilisateurEsterServiceTest extends ConnexionServiceTest<UtilisateurEster> {

    protected UtilisateurEsterServiceTest() {
        super(UtilisateurEsterService.getInstance(), UtilisateurEster.class);
    }

    @Override
    protected List<UtilisateurEster> getModels() {
        return DataEster.getListUtilisateurEster();
    }

    @Test
    protected void connecter() { //                                                                                                         T C L      (Taille Chiffre Lettre)
        assertNull(UtilisateurEsterService.getInstance().connecter("test",""), ""); //                    0 0 0
        assertNull(UtilisateurEsterService.getInstance().connecter("test","p"), ""); //                   0 0 1
        assertNull(UtilisateurEsterService.getInstance().connecter("test","1"), ""); //                   0 1 0
        assertNull(UtilisateurEsterService.getInstance().connecter("test","p1"), ""); //                  0 1 1
        assertNull(UtilisateurEsterService.getInstance().connecter("test","&,;:!;,&::;&"), ""); //        1 0 0
        assertNull(UtilisateurEsterService.getInstance().connecter("test","password"), ""); //            1 0 1
        assertNull(UtilisateurEsterService.getInstance().connecter("test","12345678"), ""); //            1 1 0
        assertNull(UtilisateurEsterService.getInstance().connecter("test","password1"), ""); //           1 1 1
    }
}