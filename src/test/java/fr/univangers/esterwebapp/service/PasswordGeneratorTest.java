package fr.univangers.esterwebapp.service;

import org.junit.jupiter.api.Test;
import static org.springframework.test.util.AssertionErrors.assertTrue;

public class PasswordGeneratorTest {

    @Test
    protected void GeneratePassword() {
        String mdp ="";
        for(int i=0;i<10;i++) {
            mdp = PasswordGeneratorService.getInstance().generatePassword();
            System.out.println(mdp);
            int nbCaracSpec = 0,nbMinusc=0,nbDigit=0,nbMaj=0;
            for (char c : mdp.toCharArray()) {
                if (c >= 33 && c <= 47) {
                    nbCaracSpec++;
                }
                if (c >= 97 && c <= 122) {
                    nbMinusc++;
                }
                if (c >= 48 && c <= 57) {
                    nbDigit++;
                }
                if(c>=64 && c<= 70) {
                    nbMaj++;
                }
            }
            assertTrue("Password validation failed", nbCaracSpec >= 1 || nbMinusc >= 1 || nbDigit  >= 1 | nbMaj >= 1 );
        }
    }
}