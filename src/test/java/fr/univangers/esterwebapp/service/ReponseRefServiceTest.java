package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.model.ReponseRef;
import fr.univangers.esterwebapp.DataEster;

import java.util.List;

public class ReponseRefServiceTest extends ModelServiceTest<ReponseRef> {

    protected ReponseRefServiceTest() {
        super(ReponseRefService.getInstance(), ReponseRef.class);
    }

    @Override
    protected List<ReponseRef> getModels() {
        return DataEster.getListReponseRef();
    }
}
