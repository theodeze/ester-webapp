package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Reponse;

import java.util.List;

public class ReponseServiceTest extends ModelServiceTest<Reponse> {

    protected ReponseServiceTest() {
        super(ReponseService.getInstance(), Reponse.class);
    }

    @Override
    protected List<Reponse> getModels() {
        return DataEster.getListReponse();
    }
}
