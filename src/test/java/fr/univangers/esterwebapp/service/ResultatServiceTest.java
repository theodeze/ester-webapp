package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Resultat;

import java.util.List;

public class ResultatServiceTest extends ModelServiceTest<Resultat> {

    protected ResultatServiceTest() {
        super(ResultatService.getInstance(), Resultat.class);
    }

    @Override
    protected List<Resultat> getModels() {
        return DataEster.getListResultat();
    }
}
