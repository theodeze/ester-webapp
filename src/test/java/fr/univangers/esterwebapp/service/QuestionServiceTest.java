package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Question;

import java.util.List;

public class QuestionServiceTest extends ModelServiceTest<Question> {

    protected QuestionServiceTest() {
        super(QuestionService.getInstance(), Question.class);
    }

    @Override
    protected List<Question> getModels() {
        return DataEster.getListQuestions();
    }
}
