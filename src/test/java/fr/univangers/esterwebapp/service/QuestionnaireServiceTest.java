package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Questionnaire;
import java.util.List;

public class QuestionnaireServiceTest extends ModelServiceTest<Questionnaire> {

    protected QuestionnaireServiceTest() {
        super(QuestionnaireService.getInstance(), Questionnaire.class);
    }

    @Override
    protected List<Questionnaire> getModels() {
        return DataEster.getListQuestionnaire();
    }
}