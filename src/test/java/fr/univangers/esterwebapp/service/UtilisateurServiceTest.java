package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.operation.OperationUtilisateurTest;
import fr.univangers.esterwebapp.model.Utilisateur;

public abstract class UtilisateurServiceTest<T extends Utilisateur> extends OperationUtilisateurTest<T> {

    protected  UtilisateurServiceTest(UtilisateurService<T> utilisateurService, Class<T> utilisateurClass) {
        super(utilisateurService, utilisateurClass);
    }
}