package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.OptionQuestion;

import java.util.List;

public class OptionQuestionServiceTest extends ModelServiceTest<OptionQuestion> {

    protected OptionQuestionServiceTest() {
        super(OptionQuestionService.getInstance(), OptionQuestion.class);
    }

    @Override
    protected List<OptionQuestion> getModels() {
        return DataEster.getListOptionQuestion();
    }
}
