package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Mail;

import java.util.List;

public class MailServiceTest extends ModelServiceTest<Mail> {

    protected MailServiceTest() {
        super(MailService.getInstance(), Mail.class);
    }

    @Override
    protected List<Mail> getModels() {
        return DataEster.getListMail();
    }
}