package fr.univangers.esterwebapp.operation;

import fr.univangers.esterwebapp.TestEster;
import fr.univangers.esterwebapp.model.Model;
import org.junit.jupiter.api.Test;
import java.util.Collection;
import java.util.UUID;
import static org.junit.jupiter.api.Assertions.*;

public abstract class OperationModelTest<T extends Model> extends TestEster<T> {

    protected OperationModel<T> operationModel;
    protected Class<T> classModel;

    protected OperationModelTest(OperationModel<T> operationModel, Class<T> classModel) {
        this.operationModel = operationModel;
        this.classModel = classModel;
    }

    /**
     * Création d'un modèle qui n'existe pas dans la BD
     */
    @Test
    public void createNoExist() {
        boolean test = this.operationModel.create(this.models.get(0));
        assertTrue(test);
    }

    /**
     * Création d'un modèle qui existe déjà par UUID
     */
    @Test
    public void createExistUUID() {
        this.operationModel.create(this.models.get(0));
        boolean test = this.operationModel.create(this.models.get(0));
        assertFalse(test);
    }

    /**
     * Création d'un modèle null
     */
    @Test
    public void createNull() {
        boolean test = this.operationModel.create(null);
        assertFalse(test);
    }

    /**
     * Sauvegarde d'un modèle qui n'existe pas dans la BD
     */
    @Test
    public void saveNoExist() {
        boolean test = this.operationModel.save(this.models.get(0));
        assertTrue(test);
    }

    /**
     * Sauvegarde d'un modèle qui existe déjà par UUID = maj
     */
    @Test
    public void saveExistUUID() {
        this.operationModel.save(this.models.get(0));
        boolean test = this.operationModel.save(this.models.get(0));
        assertTrue(test);
    }

    /**
     * Sauvegarde d'un modèle null
     */
    @Test
    public void saveNull() {
        boolean test = this.operationModel.save(null);
        assertFalse(test);
    }

    /**
     * Récupération d'un modele par son identifiant qui existe
     */
    @Test
    public void readByIdFind() {
        this.populateDataBase(operationModel);
        T model = this.operationModel.readById(this.models.get(0).getIdentifiant());
        assertNotNull(model);
        assertTrue(model.equals(this.models.get(0)));
    }

    /**
     * Récupération d'un modele par son identifiant qui n'existe pas
     */
    @Test
    public void readByIdNotFind() {
        this.populateDataBase(operationModel);
        T model = this.operationModel.readById("bidon");
        assertNull(model);
    }

    /**
     * Récupération d'un modele par son identifiant qui est null
     */
    @Test
    public void readByIdNotFindNull() {
        this.populateDataBase(operationModel);
        T model = this.operationModel.readById((String)null);
        assertNull(model);
    }

    /**
     * Récupération d'un modele par son identifiant qui est vide
     */
    @Test
    public void readByIdNotFindEmpty() {
        this.populateDataBase(operationModel);
        T model = this.operationModel.readById("");
        assertNull(model);
    }

    /**
     * Mise à jour d'un modele qui existe
     */
    @Test
    public void updateExist() {
        this.populateDataBase(operationModel);
        this.models.get(0).setIdentifiant("bidon");

        boolean test = this.operationModel.update(this.models.get(0));
        assertTrue(test);
        T model = this.operationModel.readById("bidon");
        assertNotNull(model);
        assertTrue(this.models.get(0).equals(model));
    }

    /**
     * Mise à jour d'un modele qui n'existe pas
     */
    @Test
    public void updateNoExist() {
        this.models.get(0).setIdentifiant("bidon");

        boolean test = this.operationModel.update(this.models.get(0));
        assertFalse(test);
    }

    /**
     * Sauvegarde d'un modele qui existe
     */
    @Test
    public void saveExist() {
        this.populateDataBase(operationModel);
        this.models.get(0).setIdentifiant("bidon");

        boolean test = this.operationModel.save(this.models.get(0));
        assertTrue(test);
        T model = this.operationModel.readById("bidon");
        assertNotNull(model);
        assertTrue(this.models.get(0).equals(model));
    }

    /**
     * Récupération des modeles d'une table vide
     */
    @Test
    public void readEmpty() {
        boolean test = this.operationModel.read().isEmpty();
        assertTrue(test);
    }

    /**
     * Récupération des modeles d'une table non vide
     */
    @Test
    public void readNoEmpty() {
        this.populateDataBase(this.operationModel);
        Collection<T> list = this.operationModel.read();
        boolean test = list.isEmpty();
        int count = list.size();
        assertFalse(test);
        assertEquals(this.models.size(), count);
    }

    /**
     * Récupération d'un modele par son UUID qui existe
     */
    @Test
    public void readByUUIDFind() {
        this.populateDataBase(this.operationModel);
        T model = this.operationModel.readById(this.models.get(0).getUuid());
        assertNotNull(model);
        assertTrue(model.equals(this.models.get(0)));
    }

    /**
     * Récupération d'un modele par son UUID qui n'existe pas
     */
    @Test
    public void readByUUIDNotFind() {
        this.populateDataBase(this.operationModel);
        T model = this.operationModel.readById(UUID.randomUUID());
        assertNull(model);
    }

    /**
     * Récupération d'un modele par son UUID qui est null
     */
    @Test
    public void readByUUIDNotFindNull() {
        this.populateDataBase(this.operationModel);
        T model = this.operationModel.readById((UUID) null);
        assertNull(model);
    }

    /**
     * Récupération du dernier modele enregistré dans la BD qui est null
     */
    @Test
    public void readLastNull() {
        T model = this.operationModel.readLast();
        assertNull(model);
    }

    /**
     * Récupération du dernier modele enregistré dans la BD qui n'est pas null
     */
    @Test
    public void readLastExist() {
        this.populateDataBase(this.operationModel);
        T model = this.operationModel.readLast();
        assertNotNull(model);
        assertEquals(models.get(models.size()-1), model);
    }

    /**
     * Récupération du premier modele enregistré dans la BD qui est null
     */
    @Test
    public void readFirstNull() {
        T model = this.operationModel.readFirst();
        assertNull(model);
    }

    /**
     * Récupération du premier modele enregistré dans la BD qui n'est pas null
     */
    @Test
    public void readFirstExist() {
        this.populateDataBase(this.operationModel);
        T model = this.operationModel.readFirst();
        assertNotNull(model);
        assertEquals(model, models.get(0));
    }

    /**
     * Mise à jour d'un modele qui est null
     */
    @Test
    public void updateNull() {
        boolean test = this.operationModel.update(null);
        assertFalse(test);
    }

    /**
     * Suppression d'un modele qui existe
     */
    @Test
    public void deleteExist() {
        this.populateDataBase(this.operationModel);
        boolean test = this.operationModel.delete(this.models.get(0));
        assertTrue(test);
        int count =  this.operationModel.read().size();
        assertEquals(this.models.size()-1, count);
    }

    /**
     * Suppression d'un modele qui n'existe pas
     */
    @Test
    public void deleteNoExist() {
        boolean test = this.operationModel.delete(this.models.get(0));
        assertFalse(test);
    }

    /**
     * Suppression d'un modele qui est null
     */
    @Test
    public void deleteNull() {
        boolean test = this.operationModel.delete(null);
        assertFalse(test);
    }

    /**
     * Suppression de tous les modeles d'une table vide
     */
    @Test
    public void deleteAllNothing() {
        boolean test = this.operationModel.delete();
        assertFalse(test);
    }

    /**
     * Suppression de tous les modeles d'une table non vide
     */
    @Test
    public void deleteAll() {
        this.populateDataBase(this.operationModel);
        boolean test = this.operationModel.delete();
        assertTrue(test);
        assertEquals(0,  this.operationModel.read().size());
    }
}