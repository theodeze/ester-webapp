package fr.univangers.esterwebapp.operation;

import fr.univangers.esterwebapp.model.Connexion;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public abstract class OperationConnexionTest<T extends Connexion> extends OperationUtilisateurTest<T> {

    protected OperationConnexion<T> operationConnexion;

    protected OperationConnexionTest(OperationConnexion<T> operationConnexion, Class<T> classModel) {
        super(operationConnexion, classModel);
        this.operationConnexion = operationConnexion;
    }

    /**
     * Création d'un modèle qui existe déjà par son adresse mail
     */
    @Test
    public void createExistMail() {
        try {
            this.operationConnexion.create(this.models.get(0));
            //Uniquement l'identifiant
            T model = this.classModel.newInstance();
            model.setEmail(this.models.get(0).getEmail());
            boolean test = this.operationConnexion.create(model);
            assertFalse(test);
        }
        catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Récupération d'un modele par son adresse mail qui existe
     */
    @Test
    public void readByEmailFind() {
        this.populateDataBase(operationConnexion);
        T utilisateur = this.operationConnexion.readByEmail(this.models.get(0).getEmail());
        assertNotNull(utilisateur);
        assertTrue(utilisateur.equals(this.models.get(0)));
    }

    /**
     * Récupération d'un modele par son adresse mail qui n'existe pas
     */
    @Test
    public void readByEmailNotFind() {
        this.populateDataBase(operationConnexion);
        T utilisateur = this.operationConnexion.readByEmail("bidon");
        assertNull(utilisateur);
    }

    /**
     * Récupération d'un modele par son adresse mail qui est null
     */
    @Test
    public void readByEmailNotFindNull() {
        this.populateDataBase(operationConnexion);
        T utilisateur = this.operationConnexion.readByEmail(null);
        assertNull(utilisateur);
    }

    /**
     * Récupération d'un modele par son adresse mail ou identifiant qui existe (email)
     */
    @Test
    public void readByEmailOrIdFindWithEmail() {
        this.populateDataBase(operationConnexion);
        T utilisateur = this.operationConnexion.readByEmailOrId(this.models.get(0).getEmail());
        assertNotNull(utilisateur);
        assertTrue(utilisateur.equals(this.models.get(0)));
    }

    /**
     * Récupération d'un modele par son adresse mail ou identifiant qui existe (id)
     */
    @Test
    public void readByEmailOrIdFindWithId() {
        this.populateDataBase(operationConnexion);
        T utilisateur = this.operationConnexion.readByEmailOrId(this.models.get(0).getIdentifiant());
        assertNotNull(utilisateur);
        assertTrue(utilisateur.equals(this.models.get(0)));
    }

    /**
     * Récupération d'un modele par son adresse mail ou identifiant qui n'existe pas
     */
    @Test
    public void readByEmailOrIdNotFind() {
        this.populateDataBase(operationConnexion);
        T utilisateur = this.operationConnexion.readByEmailOrId("bidon");
        assertNull(utilisateur);
    }

    /**
     * Récupération d'un modele par son adresse mail ou identifiant qui est null
     */
    @Test
    public void readByEmailOrIdNotFindNull() {
        this.populateDataBase(operationConnexion);
        T utilisateur = this.operationConnexion.readByEmailOrId(null);
        assertNull(utilisateur);
    }
}