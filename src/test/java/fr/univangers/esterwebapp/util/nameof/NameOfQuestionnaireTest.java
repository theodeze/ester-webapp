package fr.univangers.esterwebapp.util.nameof;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Questionnaire;
import java.util.List;

public class NameOfQuestionnaireTest extends NameOfModelTest<Questionnaire> {

    protected NameOfQuestionnaireTest() {
        super(Questionnaire.class);
    }

    @Override
    protected List<Questionnaire> getModels() {
        return DataEster.getListQuestionnaire();
    }
}
