package fr.univangers.esterwebapp.util.nameof;

import fr.univangers.esterwebapp.TestEster;
import fr.univangers.esterwebapp.model.Model;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public abstract class NameOfModelTest<T extends Model> extends TestEster<T> {

    private Class<T> clazz;

    protected NameOfModelTest(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Test
    public void testNameOfProperty() {
        assertEquals("created", LangUtils.nameOfProperty(this.clazz, Model::getCreated));
        assertEquals("updated", LangUtils.nameOfProperty(this.clazz, Model::getUpdated));
        assertEquals("uuid", LangUtils.nameOfProperty(this.clazz, Model::getUuid));
    }
}