package fr.univangers.esterwebapp.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SecurityUtilTest {
    private String password;

    @BeforeEach
    public void setUp()
    {
        this.password = "tmahauda";
    }
    

    @Test
    public void verifierMotPasseTrue() {
        String chiffre = SecurityUtil.chiffrerMotPasse(this.password);
        boolean test = SecurityUtil.verifierMotPasse(this.password, chiffre);
        assertTrue(test);
    }

    @Test
    public void verifierMotPasseFalse() {
        String chiffre = SecurityUtil.chiffrerMotPasse(this.password);
        boolean test = SecurityUtil.verifierMotPasse("toto", chiffre);
        assertFalse(test);
    }
}