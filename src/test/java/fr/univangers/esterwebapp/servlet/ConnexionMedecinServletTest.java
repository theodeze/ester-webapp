package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.service.UtilisateurEsterService;

import java.util.List;

public class ConnexionMedecinServletTest extends ConnexionUtilisateurServletTest<UtilisateurEster> {

    private static final String TYPE = ConnexionServlet.TYPE_VALUE_UTILISATEUR;
    private static final Role ROLE = Role.MEDECIN;
    private static final String ID = "MedecinTrueESTER17";
    private static final String EMAIL = "medecintrue.ester17@gmail.com";
    private static final String PASSWORD = "Medecin@True!ESTER17";

    public ConnexionMedecinServletTest() {
        super(TYPE, ROLE, ID, EMAIL, PASSWORD, UtilisateurEsterService.getInstance());
    }

    @Override
    protected List<UtilisateurEster> getModels() {
        return DataEster.getListUtilisateurEster();
    }
}
