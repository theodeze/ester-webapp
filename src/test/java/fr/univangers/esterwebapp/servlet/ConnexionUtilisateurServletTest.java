package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.model.Utilisateur;
import fr.univangers.esterwebapp.operation.OperationUtilisateur;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public abstract class ConnexionUtilisateurServletTest<T extends Utilisateur> extends ConnexionServletTest<T> {

    private String typeUser;
    private Role role;
    private String id;
    private String email;
    private String password;
    private OperationUtilisateur<T> operationUtilisateur;

    public ConnexionUtilisateurServletTest(String typeUser, Role role, String id, String email, String password, OperationUtilisateur<T> operationUtilisateur) {
        this.typeUser = typeUser;
        this.role = role;
        this.id = id;
        this.email = email;
        this.password = password;
        this.operationUtilisateur = operationUtilisateur;
    }

    @BeforeEach
    protected void setUp() {
        super.setUp();
        this.populateDataBase(this.operationUtilisateur);
    }

    @AfterEach
    protected void tearDown() {
        super.tearDown();
    }

    //Utilisateur avec connection via id

    /**
     * Test avec
     * - Type incorrect
     * - Identifiant incorrect
     * - mot de passe incorrect
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeKOIdKOPasswordKO() {
        assertNotNull(this);
        this.testDoPost("bidon", "bidon", "bidon",
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }

    /**
     * Test avec
     * - Type incorrect
     * - Identifiant incorrect
     * - mot de passe correct
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeKOIdKOPasswordOK() {
        assertNotNull(this);
        this.testDoPost("bidon", "bidon", this.password,
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }

    /**
     * Test avec
     * - Type incorrect
     * - Identifiant correct
     * - mot de passe incorrect
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeKOIdOKPasswordKO() {
        assertNotNull(this);
        this.testDoPost("bidon", this.id, "bidon",
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }

    /**
     * Test avec
     * - Type incorrect (se connecte depuis l'entreprise)
     * - Identifiant correct
     * - mot de passe correct
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeKOIdOKPasswordOK() {
        assertNotNull(this);
        this.testDoPost("bidon", this.id, this.password,
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }

    /**
     * Test avec
     * - Type correct
     * - Identifiant incorrect
     * - mot de passe incorrect
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeOKIdKOPasswordKO() {
        assertNotNull(this);
        this.testDoPost(this.typeUser, "bidon", "bidon",
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }

    /**
     * Test avec
     * - Type correct
     * - Identifiant incorrect
     * - mot de passe correct
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeOKIdKOPasswordOK() {
        assertNotNull(this);
        this.testDoPost(this.typeUser, "bidon", this.password,
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }


    /**
     * Test avec
     * - Type correct
     * - Identifiant correct
     * - mot de passe incorrect
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeOKIdOKPasswordKO() {
        assertNotNull(this);
        this.testDoPost(this.typeUser, this.id, "bidon",
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }

    /**
     * Test avec
     * - Type correct
     * - Identifiant correct
     * - mot de passe correct
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeOKIdOKPasswordOK() {
        Utilisateur user = this.operationUtilisateur.readByEmailOrId(this.id);
        assertNotNull(user);
        this.testDoPost(this.typeUser, this.id, this.password,
                Servlet.ATT_MSG_SUCCESS.getType(), user, this.role);
    }

    //Assistant avec connection via Email

    /**
     * Test avec
     * - Type incorrect
     * - Identifiant incorrect
     * - mot de passe incorrect
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeKOEmailKOPasswordKO() {
        assertNotNull(this);
        this.testDoPost("bidon", "bidon", "bidon",
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }

    /**
     * Test avec
     * - Type incorrect
     * - Identifiant incorrect
     * - mot de passe correct
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeKOEmailKOPasswordOK() {
        assertNotNull(this);
        this.testDoPost("bidon", "bidon", this.password,
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }

    /**
     * Test avec
     * - Type incorrect
     * - Identifiant correct
     * - mot de passe incorrect
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeKOEmailOKPasswordKO() {
        assertNotNull(this);
        this.testDoPost("bidon", this.email, "bidon",
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }

    /**
     * Test avec
     * - Type incorrect (se connecte depuis l'entreprise)
     * - Identifiant correct
     * - mot de passe correct
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeKOEmailOKPasswordOK() {
        assertNotNull(this);
        this.testDoPost("bidon", this.email, this.password,
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }

    /**
     * Test avec
     * - Type correct
     * - Identifiant incorrect
     * - mot de passe incorrect
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeOKEmailKOPasswordKO() {
        assertNotNull(this);
        this.testDoPost(this.typeUser, "bidon", "bidon",
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }

    /**
     * Test avec
     * - Type correct
     * - Identifiant incorrect
     * - mot de passe correct
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeOKEmailKOPasswordOK() {
        assertNotNull(this);
        this.testDoPost(this.typeUser, "bidon", this.password,
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }


    /**
     * Test avec
     * - Type correct
     * - Identifiant correct
     * - mot de passe incorrect
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeOKEmailOKPasswordKO() {
        assertNotNull(this);
        this.testDoPost(this.typeUser, this.email, "bidon",
                Servlet.ATT_MSG_WARNING.getType(), null, null);
    }

    /**
     * Test avec
     * - Type correct
     * - Identifiant correct
     * - mot de passe correct
     */
    @Test
    protected void testDoPostUtilisateurEsterTypeOKEmailOKPasswordOK() {
        Utilisateur user = this.operationUtilisateur.readByEmailOrId(this.email);
        assertNotNull(user);
        this.testDoPost(this.typeUser, this.email, this.password,
                Servlet.ATT_MSG_SUCCESS.getType(), user, this.role);
    }
}
