package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.service.UtilisateurEsterService;

import java.util.List;

public class ConnexionInfirmierServletTest extends ConnexionUtilisateurServletTest<UtilisateurEster> {

    private static final String TYPE = ConnexionServlet.TYPE_VALUE_UTILISATEUR;
    private static final Role ROLE = Role.INFIRMIER;
    private static final String ID = "InfirmierTrueESTER25";
    private static final String EMAIL = "infirmiertrue.ester25@outlook.fr";
    private static final String PASSWORD = "Infirmier@True!ESTER25";

    public ConnexionInfirmierServletTest() {
        super(TYPE, ROLE, ID, EMAIL, PASSWORD, UtilisateurEsterService.getInstance());
    }

    @Override
    protected List<UtilisateurEster> getModels() {
        return DataEster.getListUtilisateurEster();
    }
}
