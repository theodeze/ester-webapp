package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.service.UtilisateurEsterService;

import java.util.List;

public class ConnexionAdminServletTest extends ConnexionUtilisateurServletTest<UtilisateurEster> {

    private static final String TYPE = ConnexionServlet.TYPE_VALUE_UTILISATEUR;
    private static final Role ROLE = Role.ADMIN;
    private static final String ID = "AdminTrueESTER21";
    private static final String EMAIL = "admintrue.ester21@free.fr";
    private static final String PASSWORD = "Admin@True!ESTER21";

    public ConnexionAdminServletTest() {
        super(TYPE, ROLE, ID, EMAIL, PASSWORD, UtilisateurEsterService.getInstance());
    }

    @Override
    protected List<UtilisateurEster> getModels() {
        return DataEster.getListUtilisateurEster();
    }
}
