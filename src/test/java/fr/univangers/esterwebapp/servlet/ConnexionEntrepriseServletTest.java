package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Entreprise;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.service.EntrepriseService;

import java.util.List;

public class ConnexionEntrepriseServletTest extends ConnexionUtilisateurServletTest<Entreprise> {

    private static final String TYPE = ConnexionServlet.TYPE_VALUE_ENTREPRISE;
    private static final Role ROLE = Role.ENTREPRISE;
    private static final String ID = "EntrepriseTrueENTREPRISE05";
    private static final String EMAIL = "entreprisetrue.ester05@yahoo.com";
    private static final String PASSWORD = "Entreprise@True!ESTER05";

    public ConnexionEntrepriseServletTest() {
        super(TYPE, ROLE, ID, EMAIL, PASSWORD, EntrepriseService.getInstance());
    }

    @Override
    protected List<Entreprise> getModels() {
        return DataEster.getListEntreprise();
    }
}
