package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.TestEster;
import fr.univangers.esterwebapp.model.Model;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.*;
import javax.servlet.http.HttpServlet;

public abstract class ServletTest<T extends Model, S extends HttpServlet> extends TestEster<T> {

    protected MockHttpServletRequest request;
    protected MockHttpServletResponse response;
    protected S modelServlet;
    protected Class<? extends S> classServlet;

    protected ServletTest(Class<? extends S> classServlet) {
        this.classServlet = classServlet;
    }

    /**
     * Méthode qui va nettoyer la base de test au début de chaque cas de test
     * et initialiser la requete et la réponse
     */
    @BeforeEach
    protected void setUp() {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        try {
            this.modelServlet = this.classServlet.newInstance();
            this.request = new MockHttpServletRequest();
            this.response = new MockHttpServletResponse();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui va nettoyer la base de test à la fin de chaque cas de test
     */
    @AfterEach
    protected void tearDown() {
        super.tearDown();
        this.request = null;
        this.response = null;
        this.modelServlet = null;
    }
}
