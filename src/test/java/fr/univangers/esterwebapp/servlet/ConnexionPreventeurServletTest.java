package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.service.UtilisateurEsterService;

import java.util.List;

public class ConnexionPreventeurServletTest extends ConnexionUtilisateurServletTest<UtilisateurEster> {

    private static final String TYPE = ConnexionServlet.TYPE_VALUE_UTILISATEUR;
    private static final Role ROLE = Role.PREVENTEUR;
    private static final String ID = "PreventeurTrueESTER29";
    private static final String EMAIL = "preventeurtrue.ester29@yahoo.com";
    private static final String PASSWORD = "Preventeur@True!ESTER29";

    public ConnexionPreventeurServletTest() {
        super(TYPE, ROLE, ID, EMAIL, PASSWORD, UtilisateurEsterService.getInstance());
    }

    @Override
    protected List<UtilisateurEster> getModels() {
        return DataEster.getListUtilisateurEster();
    }
}
