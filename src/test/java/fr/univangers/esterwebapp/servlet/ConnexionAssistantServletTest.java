package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.service.UtilisateurEsterService;

import java.util.List;

public class ConnexionAssistantServletTest extends ConnexionUtilisateurServletTest<UtilisateurEster> {

    private static final String TYPE = ConnexionServlet.TYPE_VALUE_UTILISATEUR;
    private static final Role ROLE = Role.ASSISTANT;
    private static final String ID = "AssistantTrueESTER13";
    private static final String EMAIL = "assistanttrue.ester13@laposte.net";
    private static final String PASSWORD = "Assistant@True!ESTER13";

    public ConnexionAssistantServletTest() {
        super(TYPE, ROLE, ID, EMAIL, PASSWORD, UtilisateurEsterService.getInstance());
    }

    @Override
    protected List<UtilisateurEster> getModels() {
        return DataEster.getListUtilisateurEster();
    }
}
