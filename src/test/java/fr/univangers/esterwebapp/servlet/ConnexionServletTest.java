package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.model.Utilisateur;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;

import javax.servlet.http.HttpSession;
import static org.junit.jupiter.api.Assertions.*;

public abstract class ConnexionServletTest<T extends Utilisateur> extends ServletTest<T, ConnexionServlet> {

    protected ConnexionServletTest() {
        super(ConnexionServlet.class);
    }

    protected void testDoPost(String typeUser, String id, String password, String typeMess, Utilisateur userOracle, Role roleOracle) {
        this.request.addParameter(ConnexionServlet.TYPE_INPUT, typeUser);
        this.request.addParameter(ConnexionServlet.IDENTIFIANT_INPUT, id);
        this.request.addParameter(ConnexionServlet.MOTPASSE_INPUT, password);

        this.modelServlet.doPost(this.request, this.response);

        String message = (String) this.request.getAttribute(typeMess);
        assertNotNull(message);

        HttpSession session = this.request.getSession(false);
        assertNotNull(session);

        Utilisateur userFind = (Utilisateur) session.getAttribute(Servlet.USER);
        assertEquals(userOracle, userFind);

        if(userFind == null) return;
        assertEquals(roleOracle, userFind.getRole());
    }
}