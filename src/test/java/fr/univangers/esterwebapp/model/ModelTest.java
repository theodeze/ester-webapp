package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.TestEster;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public abstract class ModelTest<T extends Model> extends TestEster<T> {

    private static Logger log = Logger.getLogger(ModelTest.class);

    @Test
    protected void testEqualsTrue() {
        assertTrue(this.models.get(0).equals(this.models.get(0)));
    }

    @Test
    protected void testEqualsFalse() {
        assertFalse(this.models.get(0).equals(this.models.get(1)));
    }

    @Test
    protected void testToString() {
        assertEquals(this.models.get(0).toString(), this.models.get(0).toString());
    }

    @Test
    protected void testHashCode() {
        assertEquals(this.models.get(0).hashCode(), this.models.get(0).hashCode());
    }
}