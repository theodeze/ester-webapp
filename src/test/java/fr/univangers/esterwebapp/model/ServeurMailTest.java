package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.DataEster;

import java.util.List;

public class ServeurMailTest extends ModelTest<ServeurMail> {

    @Override
    protected List<ServeurMail> getModels() {
        return DataEster.getListServeursMail();
    }
}
