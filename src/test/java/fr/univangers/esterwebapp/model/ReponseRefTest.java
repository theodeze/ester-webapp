package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.DataEster;
import java.util.List;

public class ReponseRefTest extends ModelTest<ReponseRef> {

    @Override
    protected List<ReponseRef> getModels() {
        return DataEster.getListReponseRef();
    }
}
