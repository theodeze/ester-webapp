package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.DataEster;
import java.util.List;

public class QuestionTest extends ModelTest<Question> {

    @Override
    protected List<Question> getModels() {
        return DataEster.getListQuestions();
    }
}
