package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.DataEster;
import java.util.List;

public class ReponseTest extends ModelTest<Reponse> {

    @Override
    protected List<Reponse> getModels() {
        return DataEster.getListReponse();
    }
}
