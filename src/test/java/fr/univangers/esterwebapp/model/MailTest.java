package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.DataEster;
import java.util.List;

public class MailTest extends ModelTest<Mail> {

    @Override
    protected List<Mail> getModels() {
        return DataEster.getListMail();
    }
}
