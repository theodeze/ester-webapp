package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.DataEster;

import java.util.List;

public class OptionQuestionTest extends ModelTest<OptionQuestion> {

    @Override
    protected List<OptionQuestion> getModels() {
        return DataEster.getListOptionQuestion();
    }
}
