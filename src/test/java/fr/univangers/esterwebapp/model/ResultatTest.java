package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.DataEster;
import java.util.List;

public class ResultatTest extends ModelTest<Resultat> {

    @Override
    protected List<Resultat> getModels() {
        return DataEster.getListResultat();
    }
}