package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.Question;
import org.apache.log4j.Logger;

/**
 * DAO Question qui fait le lien entre l'objet Question et la table Question dans une BD MongoDB
 */
@DAO
public class QuestionDAO extends ModelDAO<Question> {

    /**
     * Log les traitements de la DAO Question
     */
    private static Logger log = Logger.getLogger(QuestionDAO.class);

    /**
     * Instance unique de la DAO Question
     */
    private static QuestionDAO questionDAO = null;

    /**
     * Constructeur qui prend en paramétre la description d'une Question
     */
    private QuestionDAO() {
        super(Question.class);
    }

    /**
     * Singleton qui assure l'unicité de la DAO
     * On veut eviter plusieurs DAO pour une entité
     * Une entité = Une DAO
     * @return l'instance de la DAO Question
     */
    public static synchronized QuestionDAO getInstance() {
        if(questionDAO == null)
            questionDAO = new QuestionDAO();

        log.info("Get singleton QuestionDAO : " + questionDAO);
        return questionDAO;
    }

    public Question readByIntitule(String intitule) {
        return this.read("intituleQuestion",intitule);
    }
}
