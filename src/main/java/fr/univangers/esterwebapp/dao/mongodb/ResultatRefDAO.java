package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.ResultatRef;
import org.apache.log4j.Logger;

/**
 * DAO Resultat réf qui fait le lien entre l'objet Resultat ref et la table Resultat ref dans une BD MongoDB
 */
@DAO
public class ResultatRefDAO extends ModelDAO<ResultatRef> {

    /**
     * Log les traitements de la DAO Resultat ref
     */
    private static Logger log = Logger.getLogger(ResultatRefDAO.class);

    /**
     * Instance unique de la DAO Resultat ref
     */
    private static ResultatRefDAO resultatRefDAO = null;

    /**
     * Constructeur qui prend en paramétre la description d'un Resultat ref
     */
    private ResultatRefDAO() {
        super(ResultatRef.class);
    }

    /**
     * Singleton qui assure l'unicité de la DAO
     * On veut eviter plusieurs DAO pour une entité
     * Une entité = Une DAO
     * @return l'instance de la DAO Resultat ref
     */
    public static synchronized ResultatRefDAO getInstance() {
        if(resultatRefDAO == null)
            resultatRefDAO = new ResultatRefDAO();

        log.info("Get singleton ResultatRefDAO : " + resultatRefDAO);
        return resultatRefDAO;
    }
}
