package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.util.scanner.Manager;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;

/**
 * Manager qui gère toutes les DAO de MongoDB
 * Elle permet de récupérer toutes les DAO pour effectuer un traitement commun à toutes les DAOs
 */
public class ManagerDAO extends Manager<ModelDAO<? extends Model>> {

    /**
     * Log les traitements du Manager DAO
     */
    private static Logger log = Logger.getLogger(ManagerDAO.class);

    /**
     * Instance unique du DAO Manager
     */
    private static ManagerDAO managerDAO = null;

    /**
     * Constructeur qui permet de recupérer toutes les classes qui ont le tag @DAO et qui hérite de la super classe ModelDAO
     */
    private ManagerDAO() {
        super(DAO.class, ModelDAO.class);
    }

    /**
     * Singleton qui assure l'unicité du Manager
     * On veut eviter plusieurs Manager
     * Un manager DAO = Un ensemble de DAO
     * @return l'instance de la DAO Manager
     */
    public static synchronized ManagerDAO getInstance() {
        if(managerDAO == null)
            managerDAO = new ManagerDAO();

        log.info("Get singleton ManagerDAO " + managerDAO);
        return managerDAO;
    }

    /**
     * Méthode qui récupère toutes les DAOs en lien avec des models utilisateurs qui peuvent se connecter par mail et mot de passe
     * @return une liste de DAO qui gère les utilisateurs (ester et entreprise)
     */
    public synchronized List<ConnexionDAO> getUtilisateursConnexionDAO() {
        List<ConnexionDAO> utilisateursDAO = new ArrayList<>();

        for(ModelDAO<? extends Model> modelDAO : this.entities) {
            if(modelDAO instanceof ConnexionDAO) {
                utilisateursDAO.add((ConnexionDAO) modelDAO);
                log.info("Add utilisateurConnexionDAO : " + modelDAO);
            }
        }

        return utilisateursDAO;
    }
}