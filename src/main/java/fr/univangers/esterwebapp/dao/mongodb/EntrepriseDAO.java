package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.Entreprise;
import org.apache.log4j.Logger;

/**
 * DAO Entreprise qui fait le lien entre l'objet Entreprise et la table Entreprise dans une BD MongoDB
 */
@DAO
public class EntrepriseDAO extends ConnexionDAO<Entreprise> {

    /**
     * Log les traitements de la DAO Entreprise
     */
    private static Logger log = Logger.getLogger(EntrepriseDAO.class);

    /**
     * Instance unique de la DAO Entreprise
     */
    private static EntrepriseDAO entrepriseDAO = null;

    /**
     * Constructeur qui prend en paramétre la description d'une entreprise
     */
    private EntrepriseDAO() {
        super(Entreprise.class);
    }

    /**
     * Singleton qui assure l'unicité de la DAO
     * On veut eviter plusieurs DAO pour une entité
     * Une entité = Une DAO
     * @return l'instance de la DAO Entreprise
     */
    public static synchronized EntrepriseDAO getInstance() {
        if(entrepriseDAO == null)
            entrepriseDAO = new EntrepriseDAO();

        log.info("Get singleton EntrepriseDAO : " + entrepriseDAO);
        return entrepriseDAO;
    }
}