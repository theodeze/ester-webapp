package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.util.nameof.LangUtils;
import fr.univangers.esterwebapp.model.Connexion;
import fr.univangers.esterwebapp.operation.OperationConnexion;
import org.apache.log4j.Logger;

/**
 * DAO Connexion abstraite qui permet de retrouver un utilisateur par email ou identifiant
 * @param <T> l'utilisateur qui peut se connecter par identifiant ou email
 */
public abstract class ConnexionDAO<T extends Connexion> extends UtilisateurDAO<T> implements OperationConnexion<T> {

    /**
     * Log les traitements de la DAO Connexion
     */
    private static Logger log = Logger.getLogger(ConnexionDAO.class);

    /**
     * Constructeur qui prend en paramétre le modele de classe d'un utilisateur
     * @param clazz le modele de classe d'un utilisateur
     */
    public ConnexionDAO(Class<T> clazz) {
        super(clazz);
    }

    @Override
    public synchronized T readByEmail(String email) {
        String property = LangUtils.nameOfProperty(this.clazz, Connexion::getEmail);
        log.info("Propriété à interroger : " + property);
        log.info("Valeur : " + email);
        return this.read(property, email);
    }

    @Override
    public synchronized T readByEmailOrId(String emailOrID) {
        T utilisateur = this.readById(emailOrID);
        log.info("Utilisateur trouvé par identifiant " + emailOrID + " : " + utilisateur);
        if(utilisateur != null) return utilisateur;
        utilisateur = this.readByEmail(emailOrID);
        log.info("Utilisateur trouvé par email " + emailOrID + " : " + utilisateur);
        return utilisateur;
    }
}