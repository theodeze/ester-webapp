package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.Reponse;
import org.apache.log4j.Logger;

/**
 * DAO Reponse qui fait le lien entre l'objet Reponse et la table Reponse dans une BD MongoDB
 */
@DAO
public class ReponseDAO extends ModelDAO<Reponse> {

    /**
     * Log les traitements de la DAO Reponse
     */
    private static Logger log = Logger.getLogger(ReponseDAO.class);

    /**
     * Instance unique de la DAO Reponse
     */
    private static ReponseDAO reponseDAO = null;

    /**
     * Constructeur qui prend en paramétre la description d'une Reponse
     */
    private ReponseDAO() {
        super(Reponse.class);
    }

    /**
     * Singleton qui assure l'unicité de la DAO
     * On veut eviter plusieurs DAO pour une entité
     * Une entité = Une DAO
     * @return l'instance de la DAO Reponse
     */
    public static synchronized ReponseDAO getInstance() {
        if(reponseDAO == null)
            reponseDAO = new ReponseDAO();

        log.info("Get singleton ReponseDAO : " + reponseDAO);
        return reponseDAO;
    }
}