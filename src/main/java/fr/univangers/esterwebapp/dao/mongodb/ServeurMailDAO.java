package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.ServeurMail;
import org.apache.log4j.Logger;

/**
 * DAO Mail qui fait le lien entre l'objet Mail et la table Mail dans une BD MongoDB
 */
@DAO
public class ServeurMailDAO extends ModelDAO<ServeurMail> {

    /**
     * Log les traitements de la DAO Mail
     */
    private static Logger log = Logger.getLogger(ServeurMailDAO.class);

    /**
     * Instance unique de la DAO Mail
     */
    private static ServeurMailDAO serveurMailDAO = null;

    /**
     * Constructeur qui prend en paramétre la description d'un mail
     */
    private ServeurMailDAO() {
        super(ServeurMail.class);
    }

    /**
     * Singleton qui assure l'unicité de la DAO
     * On veut eviter plusieurs DAO pour une entité
     * Une entité = Une DAO
     * @return l'instance de la DAO Mail
     */
    public static synchronized ServeurMailDAO getInstance() {
        if(serveurMailDAO == null)
            serveurMailDAO = new ServeurMailDAO();

        log.info("Get singleton MailDAO " + serveurMailDAO);
        return serveurMailDAO;
    }
}