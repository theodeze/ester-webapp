package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.Mail;
import org.apache.log4j.Logger;

/**
 * DAO Mail qui fait le lien entre l'objet Mail et la table Mail dans une BD MongoDB
 */
@DAO
public class MailDAO extends ModelDAO<Mail> {

    /**
     * Log les traitements de la DAO Mail
     */
    private static Logger log = Logger.getLogger(MailDAO.class);

    /**
     * Instance unique de la DAO Mail
     */
    private static MailDAO mailDAO = null;

    /**
     * Constructeur qui prend en paramétre la description d'un mail
     */
    private MailDAO() {
        super(Mail.class);
    }

    /**
     * Singleton qui assure l'unicité de la DAO
     * On veut eviter plusieurs DAO pour une entité
     * Une entité = Une DAO
     * @return l'instance de la DAO Mail
     */
    public static synchronized MailDAO getInstance() {
        if(mailDAO == null)
            mailDAO = new MailDAO();

        log.info("Get singleton MailDAO " + mailDAO);
        return mailDAO;
    }
}