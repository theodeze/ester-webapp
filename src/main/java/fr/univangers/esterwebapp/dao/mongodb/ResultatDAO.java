package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.Resultat;
import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.Salarie;
import fr.univangers.esterwebapp.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.Transaction;
import org.hibernate.ogm.OgmSession;
import java.util.List;
import java.util.Optional;

/**
 * DAO Resultat qui fait le lien entre l'objet Resultat et la table Resultat dans une BD MongoDB
 */
@DAO
public class ResultatDAO extends ModelDAO<Resultat> {

    /**
     * Log les traitements de la DAO Resultat
     */
    private static Logger log = Logger.getLogger(ResultatDAO.class);

    /**
     * Instance unique de la DAO Resultat
     */
    private static ResultatDAO resultatDAO = null;

    /**
     * Constructeur qui prend en paramétre la description d'un Resultat
     */
    private ResultatDAO() {
        super(Resultat.class);
    }

    /**
     * Singleton qui assure l'unicité de la DAO
     * On veut eviter plusieurs DAO pour une entité
     * Une entité = Une DAO
     *
     * @return l'instance de la DAO Resultat
     */
    public static synchronized ResultatDAO getInstance() {
        if (resultatDAO == null)
            resultatDAO = new ResultatDAO();

        log.info("Get singleton ResultatDAO : " + resultatDAO);
        return resultatDAO;
    }


    /**
     * Recupère le résultat d'un questionnaire répondu par un salarié
     *
     * @param questionnaireRepondu le questionnaire répondu
     * @param personneRepondant    le salarié ayant répondu à ce questionnaire
     * @return le résultat
     */
    public synchronized Resultat readByQuestionnaireReponduSalarieRepondant(Questionnaire questionnaireRepondu, Salarie personneRepondant) {
        List<Resultat> resultats;
        Resultat resultat = null;
        Transaction transaction = null;

        try {
            OgmSession session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
           // String sql = String.format("db.%s.find({ $and: [ {'%s':'%s'},{'%s':'%s'}]})", this.clazz.getSimpleName(),"questionnaireRepondu_uuid",questionnaireRepondu.getUuid(),"personneRepondant_uuid",personneRepondant.getUuid());
            String sql = String.format("db.%s.find({ $and: [ {'%s':'%s'},{'%s':'%s'}]})", this.clazz.getSimpleName(), "questionnaireRepondu_uuid", questionnaireRepondu.getUuid(), "personneRepondant_uuid", personneRepondant.getUuid());
            resultats = session.createNativeQuery(sql, this.clazz).getResultList();
            Optional<Resultat> optionalResultat = resultats.stream().findFirst();
            if (optionalResultat.isPresent())
                resultat = optionalResultat.get();
            transaction.commit();
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
            if(transaction != null) transaction.rollback();
        }

        return resultat;
    }
}
