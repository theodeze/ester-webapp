package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.ReponseRef;
import fr.univangers.esterwebapp.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.ogm.OgmSession;

import java.util.Collection;

/**
 * DAO Reponse qui fait le lien entre l'objet Reponse ref et la table Reponse ref dans une BD MongoDB
 */
@DAO
public class ReponseRefDAO extends ModelDAO<ReponseRef> {

    /**
     * Log les traitements de la DAO Reponse ref
     */
    private static Logger log = Logger.getLogger(ReponseRefDAO.class);

    /**
     * Instance unique de la DAO Reponse ref
     */
    private static ReponseRefDAO reponseRefDAO = null;

    /**
     * Constructeur qui prend en paramétre la description d'un Reponse ref
     */
    private ReponseRefDAO() {
        super(ReponseRef.class);
    }

    /**
     * Singleton qui assure l'unicité de la DAO
     * On veut eviter plusieurs DAO pour une entité
     * Une entité = Une DAO
     * @return l'instance de la DAO Reponse ref
     */
    public static synchronized ReponseRefDAO getInstance() {
        if(reponseRefDAO == null)
            reponseRefDAO = new ReponseRefDAO();

        log.info("Get singleton ReponseRefDAO : " + reponseRefDAO);
        return reponseRefDAO;
    }

    public Collection<ReponseRef> readByQuestion(String questionUUID)
    {
        Collection<ReponseRef> reponseRefs = null;
        try {
            OgmSession session = HibernateUtil.getSessionFactory().getCurrentSession();
            String sql = String.format("SELECT t FROM %s t WHERE %s = '%s'", this.clazz.getSimpleName(), "question_uuid", questionUUID);
            reponseRefs = session.createQuery(sql, this.clazz).getResultList();
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
        }

        return reponseRefs;
    }

}
