package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.Utilisateur;
import fr.univangers.esterwebapp.operation.OperationUtilisateur;
import org.apache.log4j.Logger;

/**
 * DAO Utilisateur abstraite qui permet de retrouver un utilisateur par identifiant
 * @param <T> l'utilisateur qui possède un identifiant
 */
public abstract class UtilisateurDAO<T extends Utilisateur> extends ModelDAO<T> implements OperationUtilisateur<T> {

    /**
     * Log les traitements de la DAO Utilisateur
     */
    private static Logger log = Logger.getLogger(UtilisateurDAO.class);

    /**
     * Constructeur qui prend en paramétre le modele de classe d'un utilisateur
     * @param clazz le modele de classe d'un utilisateur
     */
    public UtilisateurDAO(Class<T> clazz) {
        super(clazz);
    }

    @Override
    public synchronized boolean create(T entity) {
        log.info("Entité : " + entity);
        if(entity == null) return false;
        if(this.readById(entity.getIdentifiant()) != null) return false;

        return super.create(entity);
    }

    @Override
    public synchronized T readByEmailOrId(String emailOrID) {
        return this.readById(emailOrID);
    }
}