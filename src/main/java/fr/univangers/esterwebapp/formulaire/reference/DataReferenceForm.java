package fr.univangers.esterwebapp.formulaire.reference;

import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.servlet.reference.DataReferenceServlet;
import fr.univangers.esterwebapp.servlet.reference.ExportDataReferenceServlet;
import fr.univangers.esterwebapp.servlet.reference.ImportDataReferenceServlet;
import fr.univangers.esterwebapp.formulaire.Form;
import fr.univangers.esterwebapp.formulaire.ModelForm;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;

@Form
public class DataReferenceForm<T extends Model> extends ModelForm<DataReferenceServlet<T>> {

    /**
     * Logger pour tracer les traitements du formulaire
     */
    private static Logger log = Logger.getLogger(DataReferenceForm.class);

    /**
     * Nom du champ import ou export dans le formulaire (mappage entre la back et le front)
     */
    public static final String IMPORT_EXPORT_INPUT = "importExportInput";

    /**
     * S'agit-il d'un import ?
     */
    public static final String IMPORT = "Importer";

    /**
     * S'agit-il d'un export ?
     */
    public static final String EXPORT = "Exporter";

    /**
     * Le model dont on doit importer ou export des données
     */
    private T model;

    public DataReferenceForm(HttpServletRequest request, T model) {
        super(request);
        this.model = model;
    }

    @Override
    public DataReferenceServlet<T> createModelFromRequest() {

        //Import ou export
        String importOrExport = this.getValueInput(IMPORT_EXPORT_INPUT);
        log.info("importOrExport : " + importOrExport);
        if(Util.isNullOrEmpty(importOrExport)) {
            this.erreurs.put(IMPORT_EXPORT_INPUT, "Veuillez choisir l'action a effectuer : import ou export");
        }

        //Switch vers l'opération d'import ou d'export
        if(importOrExport.equals(IMPORT)) {
            return new ImportDataReferenceServlet<>(this.model);
        } else if(importOrExport.equals(EXPORT)) {
            return new ExportDataReferenceServlet<>(this.model);
        } else {
            return null;
        }
    }

    /**
     * Getter model
     *
     * @return model
     */
    public T getModel() {
        return model;
    }

    /**
     * Setter model
     *
     * @param model to set
     */
    public void setModel(T model) {
        this.model = model;
    }
}
