package fr.univangers.esterwebapp.formulaire;

import fr.univangers.esterwebapp.util.Util;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public abstract class ModelForm<T> {

    /**
     * La requête émise par l'utilisateur
     */
    protected HttpServletRequest request;

    /**
     * Le résultat du formulaire : échec ou correct
     */
    protected String resultat;

    /**
     * Liste d'erreurs sous forme clé valeur
     * Clé = le champ qui pose problème
     * Valeur = ce qui ne va pas
     * Ex : Clé = Champ mail ; Valeur = Le format n'est pas respecté
     */
    protected Map<String, String> erreurs = new HashMap<>();

    public ModelForm(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * Méthode qui permet de créer un model à partir des données
     * contenus dans la requête
     * @return le nouveau model crée
     */
    public T createModelFromRequest() {
        return null;
    }

    /**
     * Méthode qui permet de mettre à jour un model à partir des données
     * contenus dans la requête
     * @return vrai si mise à jour. Faux dans le cas contraire
     */
    public boolean updateModelFromRequest(T model) {
        return model != null;
    }

    /**
     * Récupère la valeur d'un champ dans la requete
     * @param input le nom du champ
     * @return la valeur contenu dans le champ
     */
    protected String getValueInput(String input) {
        String value = this.request.getParameter(input);
        if (Util.isNullOrEmpty(value)) {
            return null;
        } else {
            return value.trim();
        }
    }

    /**
     * Un formulaire est valide si aucune erreur n'est détectée
     * @return
     */
    protected boolean isValid() {
        return this.erreurs.isEmpty();
    }

    /**
     * Getter resultat
     *
     * @return resultat
     */
    public String getResultat() {
        return resultat;
    }

    /**
     * Setter resultat
     *
     * @param resultat to set
     */
    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    /**
     * Getter erreurs
     *
     * @return erreurs
     */
    public Map<String, String> getErreurs() {
        return erreurs;
    }

    /**
     * Setter erreurs
     *
     * @param erreurs to set
     */
    public void setErreurs(Map<String, String> erreurs) {
        this.erreurs = erreurs;
    }
}
