package fr.univangers.esterwebapp.formulaire.Questionnaire;

import fr.univangers.esterwebapp.model.*;
import fr.univangers.esterwebapp.model.modelenum.question.QuestionType;
import fr.univangers.esterwebapp.service.QuestionnaireService;
import fr.univangers.esterwebapp.service.ResultatService;
import fr.univangers.esterwebapp.service.SalarieService;
import fr.univangers.esterwebapp.servlet.Servlet;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class ReponseQuestionnaireForm {

    private HttpServletRequest request;
    private Questionnaire questionnaire;
    private Salarie salarie;
    private Resultat resultat;
    private int scoreGlobal = 0;

    private static final Logger log = Logger.getLogger(ReponseQuestionnaireForm.class);

    private static final String ERROR_REPONSE = "La réponse n'est pas reconnu";
    public static final String REPONSE_INPUT = "idReponse";
    public static final String QUESTION_INPUT = "Question";

    public static final String MSG_ERREUR = "Problème lors de la soumission du questionnaire. Rééesayer ultérieurement";


    public ReponseQuestionnaireForm(HttpServletRequest request, Questionnaire questionnaire, Salarie salarie, Resultat resultat){
        this.request = request;
        this.questionnaire = questionnaire;
        this.salarie = salarie;
        this.resultat = resultat;
    }

    public boolean errorMessages(String msgError){
        Servlet.ATT_MSG_ERROR.write(this.MSG_ERREUR, request);
        log.error(msgError);
        return false;
    }

    public void saveUpdating(){
        boolean saveSalarie = false;
        boolean saveQuestionnaire = false;

        //On supprime le questionnaire de la liste non répondu du salarié
        salarie.getQuestionnairesNonRepondus().remove(questionnaire);
        log.info("Suppression du questionnaire non répondu coté salarié");
        questionnaire.getSalariesNonRepondus().remove(salarie);
        log.info("Suppression du salarié coté questionnaire non répondu");

        //On ajoute le questionnaire dans la liste des répondu du salarié
        salarie.getQuestionnairesRepondus().add(questionnaire);
        log.info("Ajout du questionnaire répondu coté salarié");
        questionnaire.getSalariesRepondus().add(salarie);
        log.info("Suppression du salarié coté questionnaire répondu");

        //On enregistre le tout
        saveQuestionnaire = QuestionnaireService.getInstance().save(questionnaire);
        log.info("Questionnaire maj : " + saveQuestionnaire);
        saveSalarie = SalarieService.getInstance().update(salarie);
        log.info("Salarié maj : " + saveSalarie);

        if (saveQuestionnaire && saveSalarie) {
            Servlet.ATT_MSG_SUCCESS.write("Merci d'avoir répondu au questionnaire", request);
        } else {
            Servlet.ATT_MSG_ERROR.write("Problèmes autres...", request);
        }
    }

    public void saveAllUpdating() {

        boolean saveResultat = false;
        resultat.setScoreGlobal(scoreGlobal);
        log.info("Resultat scoreGlobal :" + resultat.getScoreGlobal());
        saveResultat = ResultatService.getInstance().merge(resultat);
        log.info("Resultat maj : " + saveResultat);
        if (saveResultat) {
            this.saveUpdating();
        } else {
            Servlet.ATT_MSG_ERROR.write("Problèmes à l'enregistrement final", request);
        }
    }

    public boolean getQuestionnaryElements(int nbQuestions) {

        boolean valid = true;

        //Pour chaque question
        for (int index = 0; index < nbQuestions; index++) {

            //On récupère l'id de la question coté front
            String intituleQuestion = request.getParameter(QUESTION_INPUT + (index + 1));
            log.info("Intitulé question : " + intituleQuestion);
            if (Util.isNullOrEmpty(intituleQuestion)) {
                valid = errorMessages( "Aucun intitulé trouvé dans la base de données");
            } else {
                //On récupère la question coté back
                Question question = returnQuestion(intituleQuestion);

                log.info("Question : " + question);
                if (question == null) {
                    valid = this.errorMessages( "La question n'est pas reconnu");
                } else {

                    if (question.getTypeQuestion() == QuestionType.RADIO || question.getTypeQuestion() == QuestionType.CHECKBOX) {
                        valid = setReponseForQuestionTypeRadio(question,index, valid);

                    } else {
                        // On récupére la réponse
                        valid = setReponseForWrittenQuestions(question, index, valid );

                    }
                }
            }
        }
        return valid;
    }

    private boolean setReponseForQuestionTypeRadio(Question question,int nbElement, boolean valid){
        // On récupére la réponse
        String codeEtReponseEtScoreString = request.getParameter(REPONSE_INPUT + (nbElement + 1));
        log.info("Code et reponse et score string : " + codeEtReponseEtScoreString);
        if (Util.isNullOrEmpty(codeEtReponseEtScoreString)) {
            valid = this.errorMessages( "3_" + ERROR_REPONSE);
        } else {

            List<OptionQuestion> listReponse = new ArrayList<>(question.getOptionsQuestion());
            OptionQuestion optionReponse = null;
            for (OptionQuestion element : listReponse) {
                if (element.getValeur().trim().equals(codeEtReponseEtScoreString.trim())) {
                    optionReponse = element;
                }
            }
            if (optionReponse == null) {
                valid = this.errorMessages("2_" + ERROR_REPONSE);
            } else {

                //On crée la réponse pour cette question
                Reponse reponse = new Reponse(null, optionReponse.getValeur(), optionReponse.getScore(), salarie, resultat, question);
                log.info("Réponse crée : " + reponse);
                scoreGlobal = scoreGlobal + optionReponse.getScore();
            }
        }
        return valid;
    }

    private boolean setReponseForWrittenQuestions(Question question, int nbElement, boolean valid){
        String codeEtReponseEtScoreString = request.getParameter("q_" + (nbElement + 1));
        log.info("Code et reponse et score string : " + codeEtReponseEtScoreString + "  | ID théorique : q_" + (nbElement + 1));
        if (Util.isNullOrEmpty(codeEtReponseEtScoreString)) {
            valid = this.errorMessages( "1_" + ERROR_REPONSE);
        } else {
            //On crée la réponse pour cette question
            Reponse reponse = new Reponse(null, codeEtReponseEtScoreString, 0, salarie, resultat, question);
            log.info("Réponse crée : " + reponse);
        }
        return valid;
    }

    private Question returnQuestion(String intituleQuestion) {
        List<Question> questionList = new ArrayList<>(questionnaire.getQuestions());
        Question question = null;
        for (Question element : questionList) {
            if (element.getIntituleQuestion().equals(intituleQuestion)) {
                question = element;
            }
        }
        return question;
    }
}
