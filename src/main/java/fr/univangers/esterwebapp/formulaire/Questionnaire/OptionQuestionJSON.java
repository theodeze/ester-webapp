package fr.univangers.esterwebapp.formulaire.Questionnaire;

public class OptionQuestionJSON {

    private Integer score;
    private String valeur;

    public OptionQuestionJSON(Integer score, String valeur) {
        this.score = score;
        this.valeur = valeur;
    }

    public Integer getScore() {
        return score;
    }

    public String getValeur() {
        return valeur;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }
}
