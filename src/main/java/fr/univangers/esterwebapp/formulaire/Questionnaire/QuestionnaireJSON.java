package fr.univangers.esterwebapp.formulaire.Questionnaire;

import fr.univangers.esterwebapp.model.OptionQuestion;
import fr.univangers.esterwebapp.model.Question;
import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class QuestionnaireJSON {
    private String nom;
    private Integer version;
    private String emailCreateur;
    private Date dateDernierModif;
    private Date dateSoumission;
    private String emailDernierModificateur;
    private List<String> emailModificateurs;
    private List<QuestionJSON> questions ;

    public QuestionnaireJSON(String nom, Integer version, String emailCreateur, Date dateDernierModif, Date dateSoumission, String emailDernierModificateur) {
        this.nom = nom;
        this.version = version;
        this.emailCreateur = emailCreateur;
        this.dateDernierModif = dateDernierModif;
        this.dateSoumission = dateSoumission;
        this.emailDernierModificateur = emailDernierModificateur;
        this.emailModificateurs = new ArrayList<>();
        this.questions = new ArrayList<>();
    }

    public String getNom() {
        return nom;
    }

    public Integer getVersion() {
        return version;
    }

    public String getEmailCreateur() {
        return emailCreateur;
    }

    public Date getDateDernierModif() {
        return dateDernierModif;
    }

    public Date getDateSoumission() {
        return dateSoumission;
    }

    public String getEmailDernierModificateur() {
        return emailDernierModificateur;
    }

    public List<String> getEmailModificateurs() {
        return emailModificateurs;
    }

    public List<QuestionJSON> getQuestionJSONS() {
        return questions;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public void setEmailCreateur(String emailCreateur) {
        this.emailCreateur = emailCreateur;
    }

    public void setDateDernierModif(Date dateDernierModif) {
        this.dateDernierModif = dateDernierModif;
    }

    public void setDateSoumission(Date dateSoumission) {
        this.dateSoumission = dateSoumission;
    }

    public void setEmailDernierModificateur(String emailDernierModificateur) {
        this.emailDernierModificateur = emailDernierModificateur;
    }

    public void setEmailModificateurs(List<String> emailModificateurs) {
        this.emailModificateurs = emailModificateurs;
    }

    public void setQuestionJSONS(List<QuestionJSON> questionJSONS) {
        this.questions = questionJSONS;
    }

    public void addEmailModificateur(String email){
        this.emailModificateurs.add(email);
    }

    public void addQuestion(QuestionJSON questionJSON){
        this.questions.add(questionJSON);
    }

    public void initQuestionnaireJSON(Questionnaire questionnaireToSet, UtilisateurEster membreESTER){
        List<UtilisateurEster> listForEmail = new ArrayList<>(questionnaireToSet.getModificateursQuestion());
        for(UtilisateurEster utilisateurEster : listForEmail){
            if(utilisateurEster.getRole()!= Role.ADMIN && !membreESTER.getEmail().equals(utilisateurEster.getEmail()))
                this.addEmailModificateur(utilisateurEster.getEmail());
        }

        List<Question> listQuestions = new ArrayList<>(questionnaireToSet.getQuestions());
        for(int index =0 ; index < listQuestions.size();++index){
            QuestionJSON questionJSON = new QuestionJSON(listQuestions.get(index).getIntituleQuestion(),listQuestions.get(index).getScoreMaxQuestion(), listQuestions.get(index).getTypeQuestion() );
            List<OptionQuestion> listOptions = new ArrayList<>(listQuestions.get(index).getOptionsQuestion());
            for(int subIndex =0 ; subIndex < listOptions.size();++subIndex){
                OptionQuestionJSON option = new OptionQuestionJSON(listOptions.get(subIndex).getScore(), listOptions.get(subIndex).getValeur());
                questionJSON.addInOptionsQuestions(option);
            }
            this.addQuestion(questionJSON);
        }
    }

}
