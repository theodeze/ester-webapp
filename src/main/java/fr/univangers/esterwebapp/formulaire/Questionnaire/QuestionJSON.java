package fr.univangers.esterwebapp.formulaire.Questionnaire;

import fr.univangers.esterwebapp.model.OptionQuestion;
import fr.univangers.esterwebapp.model.modelenum.question.QuestionType;
import org.directwebremoting.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class QuestionJSON {
    private String intitule;
    private Integer scoreMax;
    private QuestionType typeQuestion;
    private List<OptionQuestionJSON> optionsQuestions;

    public QuestionJSON(String intitule, Integer scoreMax, QuestionType typeQuestion) {
        this.intitule = intitule;
        this.scoreMax = scoreMax;
        this.typeQuestion = typeQuestion;
        this.optionsQuestions = new ArrayList<>();
    }

    public String getIntitule() {
        return this.intitule;
    }

    public Integer getScoreMax() {
        return this.scoreMax;
    }

    public QuestionType getTypeQuestion() {
        return this.typeQuestion;
    }

    public List<OptionQuestionJSON> getOptionsQuestions() {
        return this.optionsQuestions;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public void setScoreMax(Integer scoreMax) {
        this.scoreMax = scoreMax;
    }

    public void setTypeQuestion(QuestionType typeQuestion) {
        this.typeQuestion = typeQuestion;
    }

    public void setOptionsQuestions(List<OptionQuestionJSON> optionsQuestions) {
        this.optionsQuestions = optionsQuestions;
    }

    public void addInOptionsQuestions(OptionQuestionJSON option){
        this.optionsQuestions.add(option);
    }


}
