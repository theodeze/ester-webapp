package fr.univangers.esterwebapp.formulaire.Questionnaire;

import fr.univangers.esterwebapp.model.Question;
import fr.univangers.esterwebapp.model.modelenum.question.QuestionType;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.service.QuestionnaireService;
import fr.univangers.esterwebapp.service.UtilisateurEsterService;
import fr.univangers.esterwebapp.model.OptionQuestion;
import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.servlet.Servlet;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class GenerateQuestionnaireForm {

    private static final String INTITULE_QUESTION = "intituleQuestion";
    private static final String OPTION_QUESTION = "optionsQuestion";
    private static final String TYPE_QUESTION = "typeQuestion";
    private static final String SCORE_MAX_QUESTION = "scoreMaxQuestion";
    private static final String OPTION = "option";
    private static final String SCORE = "score";
    private static final String ERROR_SAVE_QUESTIONNARY = "Problème lors de l'enregistrement du questionnaire";
    private static final String ERROR_QUESTIONNAIRE_DB = "ERROR QUESTIONNAIRE DB";


    private static Logger log = Logger.getLogger(GenerateQuestionnaireForm.class);

    // Ajout des emails pour les droits de modification des questionnaires
    public void addQuestionnaireAcces(JSONObject questionnaireIntermediaire, Questionnaire thisNewQuestionnaire, UtilisateurEster createur) {
        JSONArray listUsersESTER = questionnaireIntermediaire.getJSONArray("droitsDeModification");
        Collection<UtilisateurEster> listUserTempo = new ArrayList<>() ;

        for (int index = 0; index < listUsersESTER.length(); index++) {
            UtilisateurEster user = UtilisateurEsterService.getInstance().readByEmail(listUsersESTER.getString(index));
            log.info("User : " + user);
            if (user != null) {
                Collection<Questionnaire> questionnairesAcces = user.getQuestionnairesAcces();
                questionnairesAcces.add(thisNewQuestionnaire);
                log.info("questionnairesAcces : " + questionnairesAcces);
                user.setQuestionnairesAcces(questionnairesAcces);
                listUserTempo.add(user);
            }
        }
        listUserTempo.add(createur);
        Collection<UtilisateurEster> listUser = thisNewQuestionnaire.getModificateursQuestion();

        if(!listUser.isEmpty()) {
            listUser.removeAll(listUser);
            listUser.addAll(listUserTempo);
        }

        List<UtilisateurEster> pourAdmin = new ArrayList<>(UtilisateurEsterService.getInstance().read());
        for(UtilisateurEster admin : pourAdmin){
            if(!listUser.contains(admin) && admin.getRole()== Role.ADMIN){
                listUser.add(admin);
            }
        }
    }

    // Ajout ou modification des options des questions
    public void createNewOptions(JSONObject questionIntermediaire, Question thisNewQuestion, String identifiantQuestion){
        JSONArray thisAnswerCollection = questionIntermediaire.getJSONArray(OPTION_QUESTION);
        log.info("Réponses collection : " + thisAnswerCollection);

        //Pour chaque réponse de la question
        for (int j = 0; j < thisAnswerCollection.length(); j++) {
            JSONObject answerIntermediaire = thisAnswerCollection.getJSONObject(j);
            log.info("Réponse intermédiaire : " + answerIntermediaire);

            String newId = identifiantQuestion+"_R_"+j;
                OptionQuestion thisNewOptionQuestion = new OptionQuestion(newId,
                        answerIntermediaire.getString(OPTION), answerIntermediaire.getInt(SCORE), thisNewQuestion);
                log.info("Réponse objet : " + thisNewOptionQuestion);
        }
    }

    // Creation d'un nouveau questionnaire (absent de la base de données)
    public void createNewQuestions(Questionnaire thisNewQuestionnaire, JSONObject jsonObject){

        log.info("Questionnaire objet : " + thisNewQuestionnaire);

        JSONArray thisQuestionCollection = jsonObject.getJSONArray("questionsCollection");
        log.info("Questions collection : " + thisQuestionCollection);

        //Pour chaque question du questionnaire
        for (int i = 0; i < thisQuestionCollection.length(); i++) {
            JSONObject questionIntermediaire = thisQuestionCollection.getJSONObject(i);
            log.info("Question intermédiaire : " + questionIntermediaire);

            String identifiantQuestion = thisNewQuestionnaire.getNom() + "Q_" + (i+1);
            Question thisNewQuestion = new Question(identifiantQuestion, questionIntermediaire.getString(GenerateQuestionnaireForm.INTITULE_QUESTION),
                    QuestionType.getProposition(questionIntermediaire.getString(GenerateQuestionnaireForm.TYPE_QUESTION)), questionIntermediaire.getInt(GenerateQuestionnaireForm.SCORE_MAX_QUESTION), thisNewQuestionnaire);
            log.info("Question objet : " + thisNewQuestion);

            this.createNewOptions(questionIntermediaire, thisNewQuestion, identifiantQuestion);
        }
        this.addQuestionnaireAcces(jsonObject, thisNewQuestionnaire, thisNewQuestionnaire.getCreateurQuestion());
    }

    // Création ou mise à jour d'un questionnaire dans la Base de données
    public void createOrUpdateQuestionnary(HttpServletRequest request, HttpServletResponse response, String myQuestionnary, String createurParam, Date creationDateParam) throws IOException {

        boolean allIsOk = false;
        UtilisateurEster createur = createurParam == null ? Servlet.readUserInSession(request):UtilisateurEsterService.getInstance().readById(createurParam);
        log.info("Créateur : " + createur);

        Date creationDate = creationDateParam == null? new Date():creationDateParam;

        //Pour chaque questionnaire
        JSONObject questionnaireIntermediaire = new JSONObject(myQuestionnary);
        log.info("Questionnaire intermédiaire : " + questionnaireIntermediaire);

        int versionQuestionnaire = questionnaireIntermediaire.getInt("version");
        String nomQuestionnaire = questionnaireIntermediaire.getString("nom");
        log.info("Nom : " + nomQuestionnaire);

        Questionnaire thisNewQuestionnaire = new Questionnaire(versionQuestionnaire, null, nomQuestionnaire,
                    creationDate, new Date(), createur);
        this.createNewQuestions(thisNewQuestionnaire, questionnaireIntermediaire);

        //On enregistre le créateur qui va crée en cascade les questionnaires puis les questions etc

        if (thisNewQuestionnaire != null) {

            log.info("New Createur : "+createur.getQuestionnairesCrees());
            boolean save = UtilisateurEsterService.getInstance().save(createur);
            log.info("New Createur 2 : "+createur.getQuestionnairesCrees());
            log.info("Questionnaires list : "+QuestionnaireService.getInstance().read());

            if (save) {
                log.info("SUCCESS QUESTIONNAIRE DB");
                Servlet.ATT_MSG_SUCCESS.write("Questionnaire enregistré avec succès", request);
                Servlet.writeUserInSession(createur, false, request);
                response.setStatus(200);
            }
            else {
                log.info(ERROR_QUESTIONNAIRE_DB);
                Servlet.ATT_MSG_ERROR.write(ERROR_SAVE_QUESTIONNARY, request);
                response.setStatus(400);

            }
        }
        else {
            log.info(ERROR_QUESTIONNAIRE_DB);
            Servlet.ATT_MSG_ERROR.write(ERROR_SAVE_QUESTIONNARY, request);
            response.setStatus(400);
        }

        String saveString = String.valueOf(allIsOk);
        response.setContentType("text/plaintext");
        response.getWriter().write(saveString);
    }

    // Suppression d'un questionnaire dans la base de données
    public void deleteQuestionnary(HttpServletRequest request, HttpServletResponse response, String nameQuestionnary) throws IOException {

        boolean allIsOK = true;

        Questionnaire questionnaireToDelete = QuestionnaireService.getInstance().readByName(nameQuestionnary);
        if(questionnaireToDelete == null){
            allIsOK = false;
        }
        else{
            QuestionnaireService.getInstance().delete(questionnaireToDelete);
            Questionnaire toHaveBedeleted = QuestionnaireService.getInstance().readByName(nameQuestionnary);
            log.info("Test Suppression : "+toHaveBedeleted);
            if(toHaveBedeleted != null){
                allIsOK = false;
            }
        }

        if (allIsOK) {
            log.info("SUCCESS QUESTIONNAIRE DB");
            Servlet.ATT_MSG_SUCCESS.write("Questionnaire supprimé avec succès", request);
            response.setStatus(200);
        }
        else {
            log.info(ERROR_QUESTIONNAIRE_DB);
            Servlet.ATT_MSG_ERROR.write("Erreur lors de la suppression", request);
            response.setStatus(400);

        }

        String saveString = String.valueOf(allIsOK);
        response.setContentType("text/plaintext");
        response.getWriter().write(saveString);
    }
}
