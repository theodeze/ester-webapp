package fr.univangers.esterwebapp.formulaire.connexion;

import fr.univangers.esterwebapp.service.EntrepriseService;
import fr.univangers.esterwebapp.service.SalarieService;
import fr.univangers.esterwebapp.service.UtilisateurEsterService;
import fr.univangers.esterwebapp.formulaire.Form;
import fr.univangers.esterwebapp.formulaire.ModelForm;
import fr.univangers.esterwebapp.model.Utilisateur;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Form
public class ParametresConnexionCollectionForm extends ModelForm<Collection<Utilisateur>> {

    /**
     * Logger pour tracer les traitements du formulaire
     */
    private static Logger log = Logger.getLogger(ParametresConnexionCollectionForm.class);

    /**
     * L'utilisateur est-il un salarié, une entreprise ou un membre ester
     */
    public static final String TYPE_UTILISATEUR = "typeUtilisateur";

    public ParametresConnexionCollectionForm(HttpServletRequest request) {
        super(request);
    }

    @Override
    public Collection<Utilisateur> createModelFromRequest() {

        String typeUtilisateur = this.getValueInput(TYPE_UTILISATEUR);
        log.info("Type utilisateur : " + typeUtilisateur);
        if(Util.isNullOrEmpty(typeUtilisateur)) {
            this.erreurs.put(TYPE_UTILISATEUR, "L'utilisateur n'est pas reconnu");
        }

        Role roleUtilisateur = Role.getProposition(typeUtilisateur);
        log.info("Role utilisateur : " + typeUtilisateur);
        if(roleUtilisateur == null) {
            this.erreurs.put(TYPE_UTILISATEUR, "L'utilisateur n'est pas reconnu");
        }

        if(roleUtilisateur != null) {
            switch(roleUtilisateur) {
                case SALARIE:
                    return SalarieService.getInstance().read().stream().filter(s -> s.getRole() == Role.SALARIE).map(s -> (Utilisateur)s).collect(Collectors.toList());
                case ENTREPRISE:
                    return EntrepriseService.getInstance().read().stream().filter(e -> e.getRole() == Role.ENTREPRISE).map(e -> (Utilisateur)e).collect(Collectors.toList());
                case ADMIN:
                    return UtilisateurEsterService.getInstance().read().stream().filter(u -> u.getRole() == Role.ADMIN).map(u -> (Utilisateur)u).collect(Collectors.toList());
                case MEDECIN:
                    return UtilisateurEsterService.getInstance().read().stream().filter(u -> u.getRole() == Role.MEDECIN).map(u -> (Utilisateur)u).collect(Collectors.toList());
                case PREVENTEUR:
                    return UtilisateurEsterService.getInstance().read().stream().filter(u -> u.getRole() == Role.PREVENTEUR).map(u -> (Utilisateur)u).collect(Collectors.toList());
                case INFIRMIER:
                    return UtilisateurEsterService.getInstance().read().stream().filter(u -> u.getRole() == Role.INFIRMIER).map(u -> (Utilisateur)u).collect(Collectors.toList());
                case ASSISTANT:
                    return UtilisateurEsterService.getInstance().read().stream().filter(u -> u.getRole() == Role.ASSISTANT).map(u -> (Utilisateur)u).collect(Collectors.toList());
                default:
                    return Collections.emptyList();
            }
        } else return Collections.emptyList();
    }
}
