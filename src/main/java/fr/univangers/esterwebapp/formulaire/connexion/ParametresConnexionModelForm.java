package fr.univangers.esterwebapp.formulaire.connexion;

import fr.univangers.esterwebapp.formulaire.Form;
import fr.univangers.esterwebapp.formulaire.ModelForm;
import fr.univangers.esterwebapp.formulaire.SalarieForm;
import fr.univangers.esterwebapp.model.Connexion;
import fr.univangers.esterwebapp.model.Salarie;
import fr.univangers.esterwebapp.model.Utilisateur;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.service.ManagerService;
import fr.univangers.esterwebapp.util.SecurityUtil;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

@Form
public class ParametresConnexionModelForm extends ModelForm<Utilisateur> {

    /**
     * Logger pour tracer les traitements du formulaire
     */
    private static Logger log = Logger.getLogger(ParametresConnexionModelForm.class);

    /**
     * Identifiant de l'utilisateur sélectionné dans le select (mappage entre la back et le front)
     */
    public static final String UTILISATEUR_SELECT = "utilisateurSelect";

    /**
     * Nom du champ identifiant dans le formulaire (mappage entre la back et le front)
     */
    public static final String NEW_IDENTIFIANT_INPUT = "NewIdInput";

    /**
     * Nom du champ email dans le formulaire (mappage entre la back et le front)
     */
    public static final String EMAIL_INPUT = "emailInput";

    /**
     * Nom du champ ancien mot de passe dans le formulaire (mappage entre la back et le front)
     */
    public static final String OLD_MOTPASSE_INPUT = "oldPasswordInput";

    /**
     * Nom du champ nouveau mot de passe dans le formulaire (mappage entre la back et le front)
     */
    public static final String NEW_MOTPASSE_INPUT = "newPasswordInput";

    public ParametresConnexionModelForm(HttpServletRequest request) {
        super(request);
    }

    @Override
    public Utilisateur createModelFromRequest() {
       String idUser = this.getValueInput(UTILISATEUR_SELECT);
       log.info("Identifiant select : " + idUser);
       if(Util.isNullOrEmpty(idUser)) {
           this.erreurs.put(UTILISATEUR_SELECT, "Aucun utilisateur de sélectionné");
       }

        return ManagerService.getInstance().getUtilisateurByIdentifiant(idUser);
    }

    @Override
    public boolean updateModelFromRequest(Utilisateur utilisateur) {
        if(utilisateur.getRole() == Role.SALARIE) {
            return updateSalarieFromRequest((Salarie)utilisateur);
        } else return updateUserFromRequest(utilisateur);
    }

    private boolean updateSalarieFromRequest(Salarie salarie) {
        return new SalarieForm(this.request).updateModelFromRequest(salarie);
    }

    private boolean updateUserFromRequest(Utilisateur utilisateur) {
        //On récupère le nouveau identifiant de l'utilisateur
        String newIdentifiant = request.getParameter(NEW_IDENTIFIANT_INPUT);
        log.info("Nouveau identifiant : " + newIdentifiant);
        if (Util.isNullOrEmpty(newIdentifiant)) {
            this.erreurs.put(NEW_IDENTIFIANT_INPUT, "Vous devez renseigner un identifiant !");
        }

        String email = request.getParameter(EMAIL_INPUT);
        log.info("Email : " + email);
        if (Util.isNullOrEmpty(email)) {
            this.erreurs.put(EMAIL_INPUT, "Vous devez renseigner un email !");
        }

        //On récupére éventuellement le nouveau mot de passe si renseigné
        String newPassword = request.getParameter(NEW_MOTPASSE_INPUT);
        log.info("Nouveau mot de passe : " + newPassword);

        //Si le nouveau mot de passe est renseigné alors il faut vérifier :
        //- Si l'ancien mot de passe est renseigné
        //- Si l'ancien mot de passe est correct
        //- Si l'ancien et le nouveau mot de passe sont identiques
        if(!Util.isNullOrEmpty(newPassword)) {

            //On récupére l'ancien mot de passe
            String oldPassword = request.getParameter(OLD_MOTPASSE_INPUT);
            log.info("Ancien mot de passe : " + oldPassword);

            //Si l'ancien mot de passe est renseigné
            if(!Util.isNullOrEmpty(oldPassword)) {

                //Si l'ancien mot de passe est correct
                if(SecurityUtil.verifierMotPasse(oldPassword, ((Connexion)utilisateur).getPassword())) {

                    //Si l'ancien et le nouveau mot de passe sont identiques
                    if(oldPassword.equals(newPassword)) {
                        this.erreurs.put(OLD_MOTPASSE_INPUT, "L'ancien et le nouveau mot de passe sont identiques !");
                    }

                } else {
                    this.erreurs.put(OLD_MOTPASSE_INPUT, "Le mot de passe actuel est incorrect");
                }

            } else {
                this.erreurs.put(OLD_MOTPASSE_INPUT, "L'ancien mot de passe n'est pas renseigné");
            }
        }

        //Si le formulaire est valide
        if(this.isValid()) {
            //On modifie les attributs de l'utilisateur
            utilisateur.setIdentifiant(newIdentifiant);
            if(!Util.isNullOrEmpty(newPassword)) {
                ((Connexion)utilisateur).setEmail(email);
            }
            if(!Util.isNullOrEmpty(newPassword)) {
                ((Connexion)utilisateur).setPassword(SecurityUtil.chiffrerMotPasse(newPassword));
            }
            return true;
        } else return false;
    }
}
