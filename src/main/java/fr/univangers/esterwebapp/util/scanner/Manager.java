package fr.univangers.esterwebapp.util.scanner;

import org.apache.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public abstract class Manager<T> {

    private static Logger log = Logger.getLogger(Manager.class);

    private static final String METHOD = "getInstance";
    protected List<T> entities;

    public Manager(Class<? extends Annotation> annotation, Class<?> clazzEntity) {
        List<Class<?>> clazzs = Scanner.scanPackages(annotation, clazzEntity.getPackage().getName()).result();
        this.entities = new ArrayList<>();

        for(Class<?> clazz : clazzs) {
            this.entities.add(this.getInstance(clazz));
        }
    }

    public List<T> getEntities() {
        return this.entities;
    }

    private T getInstance(Class<?> clazz) {
        try {
            Method method = clazz.getMethod(METHOD);
            return (T) method.invoke(null);
        }
        catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
