package fr.univangers.esterwebapp.util.scanner;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;

/**
 * Source : https://github.com/v-ladynev/fluent-hibernate
 * Classe modifié afin de rechercher des classes avec n'importe quelle annotation
 * Dans la version original on ne pouvait rechercher uniquement des Entity
 * Avec cette modification je peux rechercher des Entity mais aussi des DAO et des Service
 */
public final class Scanner {

    private AnnotationChecker checker;

    private List<ClassLoader> loaders;

    private final String[] packagesToScan;

    private List<Class<?>> result = InternalUtils.CollectionUtils.newArrayList();

    private Scanner(String[] packagesToScan) {
        this.packagesToScan = packagesToScan;
    }

    /**
     * Scan all the class path for the @Entity annotation. It scans JRE libraries too, so this
     * method recommended to use only for the test purposes.
     *
     * @return EntityScanner for fluent calls
     */
    public static Scanner scanPackages(Class<? extends Annotation> annotation) {
        return scanPackages(null, null, annotation);
    }

    /**
     * Scan packages for the @Entity annotation.
     *
     * @param packages one or more Java package names
     *
     * @return EntityScanner for fluent calls
     */
    public static Scanner scanPackages(Class<? extends Annotation> annotation, String... packages) {
        InternalUtils.Asserts.isTrue(!InternalUtils.CollectionUtils.isEmptyEllipsis(packages),
                "You should to specify at least one package to scan.");
        return scanPackages(packages, null, annotation);
    }

    /**
     * Scan packages for the @Entity annotation.
     *
     * @param loaders  one or more class loaders to find classes in
     * @param packages one or more Java package names
     *
     * @return EntityScanner for fluent calls
     */
    public static Scanner scanPackages(List<ClassLoader> loaders, Class<? extends Annotation> annotation, String... packages) {
        InternalUtils.Asserts.isTrue(!InternalUtils.CollectionUtils.isEmptyEllipsis(packages),
                "You should to specify at least one package to scan.");
        InternalUtils.Asserts.isTrue(!InternalUtils.CollectionUtils.isEmpty(loaders),
                "You should to specify at least one class loader.");
        return scanPackages(packages, loaders, annotation);
    }

    private static Scanner scanPackages(String[] packages, List<ClassLoader> loaders,
                                Class<? extends Annotation> annotation) {
        try {
            return scanPackagesInternal(packages, loaders, annotation);
        } catch (Exception ex) {
            throw InternalUtils.toRuntimeException(ex);
        }
    }

    private static Scanner scanPackagesInternal(String[] packages, List<ClassLoader> loaders,
                                                Class<? extends Annotation> annotation) throws Exception {
        Scanner scanner = new Scanner(packages);
        scanner.loaders = loaders;
        scanner.scan(annotation);
        return scanner;
    }

    private void scan(Class<? extends Annotation> annotation) throws Exception {
        checker = new AnnotationChecker(annotation);

        ClasspathScanner scanner = new ClasspathScanner(new ClasspathScanner.IResourceAcceptor() {
            @Override
            public void accept(String resource, ClassLoader loader) throws Exception {
                addClassToResult(resource, loader);
            }
        });

        if (packagesToScan == null) {
            scanner.allPackagesToScan();
        } else {
            scanner.setPackagesToScan(Arrays.asList(packagesToScan));
        }

        scanner.setLoaders(loaders);

        scanner.scan();
    }

    private void addClassToResult(String resource, ClassLoader loader) throws IOException {
        if (!ResourceUtils.hasClassExtension(resource)) {
            return;
        }

        // in JDK 8 getResourceAsStream() returns null for version.rc
        if (!checker.hasAnnotation(loader.getResourceAsStream(resource))) {
            return;
        }

        Class<?> clazz = InternalUtils.ClassUtils.classForName(ResourceUtils.getClassNameFromPath(resource),
                loader);
        result.add(clazz);
    }


    public List<Class<?>> result() {
        return result;
    }

}
