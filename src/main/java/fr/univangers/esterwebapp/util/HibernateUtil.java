package fr.univangers.esterwebapp.util;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.service.MailService;
import fr.univangers.esterwebapp.service.QuestionnaireService;
import fr.univangers.esterwebapp.operation.OperationModel;
import fr.univangers.esterwebapp.util.scanner.Scanner;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.ogm.OgmSession;
import org.hibernate.ogm.OgmSessionFactory;
import org.hibernate.ogm.boot.OgmSessionFactoryBuilder;
import org.hibernate.ogm.cfg.OgmProperties;
import javax.persistence.Entity;
import java.io.*;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class HibernateUtil {

    private static Logger log = Logger.getLogger(HibernateUtil.class);

    private static StandardServiceRegistry registry;
    private static OgmSessionFactory sessionFactory;
    private static List<Class<?>> modelsClass = Scanner.scanPackages(Entity.class, Model.class.getPackage().getName()).result();

    private static final boolean CREATE_DATABASE = true;
    private static final String DATABASE_PROD = "ESTER_PROD";
    private static final String DATABASE_TEST = "ESTER_TEST";
    private static String DATABASE;
    private static final String PROVIDER = "mongodb";
    private static final String HOST = "127.0.0.1";
    private static final String PORT = "27017";
    private static final String USERNAME = "";
    private static final String PASSWORD = "";

    private HibernateUtil() { }

    private static void buildOgmSessionFactory() {
        registry = new StandardServiceRegistryBuilder()
                .applySetting(OgmProperties.ENABLED, true)
                .applySetting(AvailableSettings.CURRENT_SESSION_CONTEXT_CLASS, "thread") //Can access that session anywhere in application by using the SessionFactory.getCurrentSession()
                .applySetting(AvailableSettings.MERGE_ENTITY_COPY_OBSERVER, "allow")
                .applySetting(AvailableSettings.ENABLE_LAZY_LOAD_NO_TRANS, true)
                .applySetting(OgmProperties.DATASTORE_PROVIDER, PROVIDER)
                .applySetting(OgmProperties.HOST, HOST)
                .applySetting(OgmProperties.PORT, PORT)
                .applySetting(OgmProperties.DATABASE, DATABASE)
                .applySetting(OgmProperties.CREATE_DATABASE, CREATE_DATABASE)
                .applySetting(OgmProperties.USERNAME, USERNAME)
                .applySetting(OgmProperties.PASSWORD, PASSWORD)
                .build();

        log.info("Registry hibernate : " + registry);

        MetadataSources metadataSources = new MetadataSources(registry);
        for(Class<?> modelClazz : modelsClass) {
            metadataSources.addAnnotatedClass(modelClazz);
        }
        sessionFactory = metadataSources
                .buildMetadata()
                .getSessionFactoryBuilder()
                .unwrap(OgmSessionFactoryBuilder.class)
                .build();

        log.info("Session hibernate : " + sessionFactory);
    }

    public static synchronized OgmSessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                buildOgmSessionFactory();
            }
            catch(Exception e) {
                log.error(e.getMessage());
                close();
            }
        }

        return sessionFactory;
    }

    public static void activateLog(boolean activate) {
        List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
        loggers.add(LogManager.getRootLogger());
        for (Logger logger : loggers) {
            if(activate)
                logger.setLevel(Level.ALL);
            else
                logger.setLevel(Level.OFF);
        }
    }

    public static void open() {
        openProd();
    }

    public static void openProd() {
        close();
        DATABASE = DATABASE_PROD;
        getSessionFactory();
    }

    public static void openTest() {
        close();
        DATABASE = DATABASE_TEST;
        getSessionFactory();
    }

    public static void close() {
        if(sessionFactory != null) {
            sessionFactory.getCurrentSession().close();
            sessionFactory.close();
            sessionFactory = null;
        }

        if (registry != null) {
            StandardServiceRegistryBuilder.destroy(registry);
            registry = null;
        }
    }

    public static boolean populateDataBaseProd() {
        if(sessionFactory == null) return false;

        //Création en cascade des models (questionnaire --> entreprise --> salarie --> membre ester)
        return //populateDataBase(DataEster.getListUtilisateurEsterDemo(), UtilisateurEsterService.getInstance()) &&
                //populateDataBase(Arrays.asList(DataEster.getEntrepriseDemo()), EntrepriseService.getInstance()) &&
                //populateDataBase(DataEster.getListSalarieDemo(), SalarieService.getInstance()) &&
                populateDataBase(DataEster.getListQuestionnairesDemo(), QuestionnaireService.getInstance()) &&
                populateDataBase(DataEster.getListMail(), MailService.getInstance());
    }

    public static <T extends Model> boolean populateDataBase(Collection<T> models, OperationModel<T> operationModel) {
        if(sessionFactory == null) return false;

        for(T model : models) {
            if(!operationModel.save(model))
                return false;
        }
        return true;
    }

    public static boolean deleteDataBase() {
        return deleteDataBase(DATABASE_PROD) && deleteDataBase(DATABASE_TEST);
    }

    public static boolean deleteDataBaseProd() {
        return deleteDataBase(DATABASE_PROD);
    }

    public static boolean deleteDataBaseTest() {
        return deleteDataBase(DATABASE_TEST);
    }

    public static boolean deleteDataBase(String dataBase) {
        close();
        String command = String.format("mongo %s --host %s --port %s --eval 'db.dropDatabase()'", dataBase, HOST, PORT);
        log.info("Commande drop database : " + command);

        //Attention ! Cette commande peut s'éxécuter sur un environnement linux
        //https://www.mkyong.com/java/java-processbuilder-examples/
        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command("bash", "-c", command);

        try {
            Process process = processBuilder.start();
            StringBuilder output = new StringBuilder();

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }
            log.info("Résultat commande : " + output);

            int resultat = process.waitFor();
            log.info("Code résultat : " + resultat);

            return resultat == 0;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }

    }

    public static boolean deleteCollection() {
        for(Class<?> modelClass : modelsClass) {
            if(!deleteCollection(modelClass.getSimpleName()))
                return false;
        }
        return true;
    }

    public static boolean deleteCollection(String collection) {
        if(sessionFactory == null) return false;

        Transaction transaction = null;
        boolean delete = false;

        try {
            OgmSession session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            String sql = String.format("db.%s.drop()", collection);
            session.createNativeQuery(sql).executeUpdate();
            transaction.commit();
            delete = true;
        }
        catch(Exception e) {
            log.error(e.getMessage());
            if(transaction != null) transaction.rollback();
        }

        return delete;
    }
}