package fr.univangers.esterwebapp.util.nameof.impl;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.ClassFileVersion;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.dynamic.loading.ClassLoadingStrategy;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatchers;

public final class PropertyNames {

    private PropertyNames() {}

    public static <T> T getPropertyNameExtractor(Class<T> type) {
        DynamicType.Builder<?> builder = new ByteBuddy(ClassFileVersion.JAVA_V8)
                .subclass(type.isInterface() ? Object.class : type);

        if (type.isInterface()) {
            builder = builder.implement(type);
        }

        Class<?> proxyType = builder
                .method(ElementMatchers.any())
                .intercept(MethodDelegation.to(PropertyNameExtractorInterceptor.class))
                .make()
                .load(
                        PropertyNames.class.getClassLoader(),
                        ClassLoadingStrategy.Default.WRAPPER
                )
                .getLoaded();

        try {
            @SuppressWarnings("unchecked")
            Class<T> typed = (Class<T>) proxyType;
            return typed.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(
                    "Couldn't instantiate proxy for method name retrieval", e
            );
        }
    }
}