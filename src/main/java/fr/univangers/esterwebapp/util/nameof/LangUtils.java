package fr.univangers.esterwebapp.util.nameof;

import fr.univangers.esterwebapp.util.nameof.impl.MethodNameExtractorInterceptor;
import fr.univangers.esterwebapp.util.nameof.impl.MethodNames;
import fr.univangers.esterwebapp.util.nameof.impl.PropertyNameExtractorInterceptor;
import fr.univangers.esterwebapp.util.nameof.impl.PropertyNames;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * Utility class that can provide name of bean property using method reference of a getter.
 */
public final class LangUtils {

    private LangUtils() {}

    // cached extractors instances
    private static final Map<Class, Object> extractors = new ConcurrentHashMap<>();

    @SuppressWarnings("unchecked")
    public static final <T> String nameOfProperty(Class<T> clazz, Function<? super T, ?> bridge) {
        T extractor = (T) extractors.computeIfAbsent(clazz, PropertyNames::getPropertyNameExtractor);

        bridge.apply(extractor);

        return PropertyNameExtractorInterceptor.extractMethodName();
    }

    @SuppressWarnings("unchecked")
    public static final <T> String nameOfMethod(Class<T> clazz, Function<? super T, ?> bridge) {
        T extractor = (T) extractors.computeIfAbsent(clazz, MethodNames::getMethodNameExtractor);

        bridge.apply(extractor);

        return MethodNameExtractorInterceptor.extractMethodName();
    }
}