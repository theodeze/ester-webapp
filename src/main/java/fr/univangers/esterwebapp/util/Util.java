package fr.univangers.esterwebapp.util;

public class Util {

    /**
     * Programme pour tester des programmes rapidement (programme bac à sable)
     * @param args
     */
    public static void main (String[] args) {
        HibernateUtil.deleteDataBaseProd();
        HibernateUtil.open();
        HibernateUtil.populateDataBaseProd();

        //ConverterEnum.writeJSONArray(Departement.values());
        //ConverterEnum.writeCSVArray(Departement.values());
    }

    public static boolean isNullOrEmpty(String string) {
        return string == null || string.trim().length() == 0;
    }
}