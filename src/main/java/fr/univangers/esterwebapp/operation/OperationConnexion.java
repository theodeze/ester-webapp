package fr.univangers.esterwebapp.operation;

import fr.univangers.esterwebapp.model.Connexion;

/**
 * Opération qui permet de gérer les utilisateurs qui peuvent se connecter par email
 * @param <T> l'utilisateur qui peut se connecter par email
 */
public interface OperationConnexion<T extends Connexion> extends OperationUtilisateur<T> {
    /**
     * Rechercher un utilisateur T par email dans la BD
     * @param email à rechercher
     * @return un utilisateur qui correspond à l'email
     */
    public T readByEmail(String email);
}