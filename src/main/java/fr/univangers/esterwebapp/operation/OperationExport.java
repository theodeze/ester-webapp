package fr.univangers.esterwebapp.operation;

public interface OperationExport {

    /**
     * Exporte les résultats d'une entité dans un fichier JSON
     * @param pathFileJSON le chemin du fichier où enregistrer le JSON
     * @return vrai si enregistré. Faux dans le cas contraire
     */
    public boolean exportJSON(String pathFileJSON);

    /**
     * Exporte les résultats d'une entité dans un fichier CSV
     * @param pathFileCSV le chemin du fichier où enregistrer le CSV
     * @return vrai si enregistré. Faux dans le cas contraire
     */
    public boolean exportCSV(String pathFileCSV);

    /**
     * Exporter une entité trouvé par l'id dans un fichier JSON
     * @param id pour retrouver l'entité
     * @param pathFileJSON le chemin du fichier où enregistrer le JSON
     * @return vrai si enregistré. Faux dans le cas contraire
     */
    public boolean exportJSONById(String id, String pathFileJSON);

    /**
     * Exporter une entité trouvé par l'id dans un fichier CSV
     * @param id pour retrouver l'entité
     * @param pathFileCSV le chemin du fichier où enregistrer le CSV
     * @return vrai si enregistré. Faux dans le cas contraire
     */
    public boolean exportCSVById(String id, String pathFileJSON);
}
