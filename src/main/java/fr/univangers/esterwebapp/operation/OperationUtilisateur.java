package fr.univangers.esterwebapp.operation;

import fr.univangers.esterwebapp.model.Utilisateur;

/**
 * Opération qui permet de gérer les utilisateurs qui peuvent se connecter par identifiant
 * @param <T> l'utilisateur qui peut se connecter par identifiant
 */
public interface OperationUtilisateur<T extends Utilisateur> extends OperationModel<T> {

    /**
     * Rechercher un utilisateur par son adresse mail ou son identifiant dans la BD
     * @param emailOrID à rechercher
     * @return un utilisateur qui correspond l'identifiant uniquement
     */
    public T readByEmailOrId(String emailOrID);
}