package fr.univangers.esterwebapp.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import fr.univangers.esterwebapp.model.modelenum.question.QuestionnaireEnum;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.SalarieEnum;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import org.apache.log4j.Logger;
import javax.persistence.*;
import java.util.*;
import java.util.Collection;

/**
 * Modele qui représente un Questionnaire
 */
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Entity
public class Questionnaire extends Model {

    /**
     * Log les traitements d'un Questionnaire
     */
    private static Logger log = Logger.getLogger(Questionnaire.class);

    /**
     * Version du questionnaire
     */
    @Column(name= "version")
    private int version;

    /**
     * Nom du questionnaire
     */
    @Column(name= "nom")
    private String nom;

    /**
     * Date de soumission du questionnaire
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "dateSoumission")
    private Date dateSoumission;

    /**
     * Date de dernière modification du questionnaire
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "dateDerniereModif")
    private Date dateDerniereModif;

    /**
     * Ensemble de questions
     */
    @OneToMany(mappedBy = "questionnaire", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Collection<Question> questions;

    /**
     * Ensemble de résultats
     */
    @OneToMany(mappedBy = "questionnaireRepondu", fetch = FetchType.EAGER,  cascade = CascadeType.ALL)
    private Collection<Resultat> resultats;

    /**
     * Ensemble de résultats références
     * Chargé uniquement lors de l'appel au getter => LAZY
     */
    @OneToMany(mappedBy = "questionnaireRepondu", fetch = FetchType.LAZY,  cascade = CascadeType.ALL)
    private Collection<ResultatRef> resultatsRef;

    /**
     * Ensemble de salariés qui ont répondus au questionnaire
     */
    @ManyToMany(mappedBy = "questionnairesRepondus", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Collection<Salarie> salariesRepondus;

    /**
     * Ensemble de salariés qui n'ont pas encore répondus au questionnaire
     */
    @ManyToMany(mappedBy = "questionnairesNonRepondus", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Collection<Salarie> salariesNonRepondus;

    /**
     * Ensemble de salariés références qui ont répondus au questionnaire
     * Chargé uniquement lors de l'appel au getter => LAZY
     */
    @ManyToMany(mappedBy = "questionnairesRepondus", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<SalarieRef> salariesRefRepondus;

    /**
     * Ensemble de salariés références qui n'ont pas encore répondus au questionnaire
     * Chargé uniquement lors de l'appel au getter => LAZY
     */
    @ManyToMany(mappedBy = "questionnairesNonRepondus", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<SalarieRef> salariesRefNonRepondus;

    /**
     * Ensemble des utilisateur qui ont le droit de modifier le questionnaire
     */
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
    private Collection<UtilisateurEster> modificateursQuestion;

    /**
     * L'utilisateur qui a crée le questionnaire
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
    private UtilisateurEster createurQuestion;

    /**
     * L'utilisateur qui a modifié en dernier le questionnaire
     */
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
    private UtilisateurEster dernierModificateurQuestion;

    /**
     * Constructeur par défaut pour Hibernate
     */
    public Questionnaire() {
        this(0, "", "", new Date(), new Date());
    }

    public Questionnaire(QuestionnaireEnum questionnaireEnum, Date dateSoumission, Date dateDerniereModif, UtilisateurEster createurQuestion) {
        this(questionnaireEnum.getVersion(), questionnaireEnum.getCode(), questionnaireEnum.getNom(), dateSoumission, dateDerniereModif, createurQuestion);
    }

    public Questionnaire(QuestionnaireEnum questionnaireEnum, Date dateSoumission, Date dateDerniereModif) {
        this(questionnaireEnum.getVersion(), questionnaireEnum.getCode(), questionnaireEnum.getNom(), dateSoumission, dateDerniereModif, null);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs sans utilisateur qui a crée la question à rattacher
     * @param version
     * @param identifiant
     * @param nom
     * @param dateSoumission
     * @param dateDerniereModif
     */
    public Questionnaire(int version, String identifiant, String nom, Date dateSoumission, Date dateDerniereModif) {
        this(version, identifiant, nom, dateSoumission, dateDerniereModif, null);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs avec l'utilisateur qui a crée la question à rattacher
     * @param version
     * @param identifiant
     * @param nom
     * @param dateSoumission
     * @param dateDerniereModif
     * @param createurQuestion
     */
    public Questionnaire(int version, String identifiant, String nom, Date dateSoumission, Date dateDerniereModif, UtilisateurEster createurQuestion) {
        super(identifiant);
        this.version = version;
        log.info("Version : " + this.version);
        this.nom = nom;
        log.info("Nom : " + this.nom);
        this.dateSoumission = dateSoumission;
        log.info("Date de soumission : " + this.dateSoumission);
        this.dateDerniereModif = dateDerniereModif;
        log.info("Date de dernière modification : " + this.dateDerniereModif);
        this.createurQuestion = createurQuestion;
        log.info("Créateur de question : " + this.createurQuestion);
        this.dernierModificateurQuestion = createurQuestion;
        log.info("Dernier modificateur de question : " + this.modificateursQuestion);
        this.modificateursQuestion = new ArrayList<>();
        log.info("Modificateurs de question : " + this.modificateursQuestion);

        if(!this.modificateursQuestion.contains(createurQuestion)) {
            this.modificateursQuestion.add(createurQuestion);
            log.info("Ajout du créateur dans la liste des ayants droits de modifiés : " + this.modificateursQuestion);
        }
        if(this.createurQuestion != null && !this.createurQuestion.getQuestionnairesAcces().contains(this)){
            this.createurQuestion.getQuestionnairesAcces().add(this);
            log.info("Ajout du questionnaire dans la liste d'accès du créateur : " + this.createurQuestion.getQuestionnairesAcces());
        }
        if(this.createurQuestion != null && !this.createurQuestion.getQuestionnairesCrees().contains(this)) {
            this.createurQuestion.getQuestionnairesCrees().add(this);
            log.info("Ajout du questionnaire dans la liste de création du créateur : " + this.createurQuestion.getQuestionnairesCrees());
        }

        this.questions = new ArrayList<>();
        this.resultats = new ArrayList<>();
        this.resultatsRef = new ArrayList<>();
        this.salariesRepondus = new ArrayList<>();
        this.salariesRefRepondus = new ArrayList<>();
        this.salariesNonRepondus = new ArrayList<>();
        this.salariesRefNonRepondus = new ArrayList<>();
    }

    /**
     * Récupère une question à partir de l'identifiant
     * @param id permettant de retrouver la question
     * @return la question qui correspond à l'identifiant
     */
    public Question getQuestion(String id) {
        Question question = null;

        Optional<Question> optionalQuestion = this.questions.stream().filter(q -> q.getIdentifiant().equals(id)).findFirst();
        if(optionalQuestion.isPresent())
            question = optionalQuestion.get();

        return question;
    }

    /**
     * Getter version
     *
     * @return version
     */
    public int getVersion() {
        return version;
    }

    /**
     * Setter version
     *
     * @param version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * Getter nom
     *
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Setter nom
     *
     * @param nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Getter dateSoumission
     *
     * @return dateSoumission
     */
    public Date getDateSoumission() {
        return dateSoumission;
    }

    /**
     * Setter dateSoumission
     *
     * @param dateSoumission to set
     */
    public void setDateSoumission(Date dateSoumission) {
        this.dateSoumission = dateSoumission;
    }

    /**
     * Getter dateDerniereModif
     *
     * @return dateDerniereModif
     */
    public Date getDateDerniereModif() {
        return dateDerniereModif;
    }

    /**
     * Setter dateDerniereModif
     *
     * @param dateDerniereModif to set
     */
    public void setDateDerniereModif(Date dateDerniereModif) {
        this.dateDerniereModif = dateDerniereModif;
    }

    /**
     * Getter questions
     *
     * @return questions
     */
    public Collection<Question> getQuestions() {
        return questions;
    }

    /**
     * Setter questions
     *
     * @param questions to set
     */
    public void setQuestions(Collection<Question> questions) {
        this.questions = questions;
    }

    /**
     * Getter resultats
     *
     * @return resultats
     */
    public Collection<Resultat> getResultats() {
        return resultats;
    }

    /**
     * Setter resultats
     *
     * @param resultats to set
     */
    public void setResultats(Collection<Resultat> resultats) {
        this.resultats = resultats;
    }

    /**
     * Getter resultatsRef
     *
     * @return resultatsRef
     */
    public Collection<ResultatRef> getResultatsRef() {
        return resultatsRef;
    }

    /**
     * Setter resultatsRef
     *
     * @param resultatsRef to set
     */
    public void setResultatsRef(Collection<ResultatRef> resultatsRef) {
        this.resultatsRef = resultatsRef;
    }

    /**
     * Getter salariesRepondus
     *
     * @return salariesRepondus
     */
    public Collection<Salarie> getSalariesRepondus() {
        return salariesRepondus;
    }

    /**
     * Setter salariesRepondus
     *
     * @param salariesRepondus to set
     */
    public void setSalariesRepondus(Collection<Salarie> salariesRepondus) {
        this.salariesRepondus = salariesRepondus;
    }

    /**
     * Getter salariesNonRepondus
     *
     * @return salariesNonRepondus
     */
    public Collection<Salarie> getSalariesNonRepondus() {
        return salariesNonRepondus;
    }

    /**
     * Setter salariesNonRepondus
     *
     * @param salariesNonRepondus to set
     */
    public void setSalariesNonRepondus(Collection<Salarie> salariesNonRepondus) {
        this.salariesNonRepondus = salariesNonRepondus;
    }

    /**
     * Getter modificateursQuestion
     *
     * @return modificateursQuestion
     */
    public Collection<UtilisateurEster> getModificateursQuestion() {
        return modificateursQuestion;
    }

    /**
     * Setter modificateursQuestion
     *
     * @param modificateursQuestion to set
     */
    public void setModificateursQuestion(Collection<UtilisateurEster> modificateursQuestion) {
        this.modificateursQuestion = modificateursQuestion;
    }

    /**
     * Getter createurQuestion
     *
     * @return createurQuestion
     */
    public UtilisateurEster getCreateurQuestion() {
        return createurQuestion;
    }

    /**
     * Setter createurQuestion
     *
     * @param createurQuestion to set
     */
    public void setCreateurQuestion(UtilisateurEster createurQuestion) {
        this.createurQuestion = createurQuestion;
    }

    /**
     * Getter dernierModificateurQuestion
     *
     * @return dernierModificateurQuestion
     */
    public UtilisateurEster getDernierModificateurQuestion() {
        return dernierModificateurQuestion;
    }

    /**
     * Setter dernierModificateurQuestion
     *
     * @param dernierModificateurQuestion to set
     */
    public void setDernierModificateurQuestion(UtilisateurEster dernierModificateurQuestion) {
        this.dernierModificateurQuestion = dernierModificateurQuestion;
    }

    /**
     * Getter salariesRefRepondus
     *
     * @return salariesRefRepondus
     */
    public Collection<SalarieRef> getSalariesRefRepondus() {
        return salariesRefRepondus;
    }

    /**
     * Setter salariesRefRepondus
     *
     * @param salariesRefRepondus to set
     */
    public void setSalariesRefRepondus(Collection<SalarieRef> salariesRefRepondus) {
        this.salariesRefRepondus = salariesRefRepondus;
    }

    /**
     * Getter salariesRefNonRepondus
     *
     * @return salariesRefNonRepondus
     */
    public Collection<SalarieRef> getSalariesRefNonRepondus() {
        return salariesRefNonRepondus;
    }

    /**
     * Setter salariesRefNonRepondus
     *
     * @param salariesRefNonRepondus to set
     */
    public void setSalariesRefNonRepondus(Collection<SalarieRef> salariesRefNonRepondus) {
        this.salariesRefNonRepondus = salariesRefNonRepondus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Questionnaire that = (Questionnaire) o;
        return version == that.version &&
                Objects.equals(nom, that.nom) &&
                Objects.equals(dateSoumission, that.dateSoumission) &&
                Objects.equals(dateDerniereModif, that.dateDerniereModif);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), version, nom, dateSoumission, dateDerniereModif);
    }

    @Override
    public String toString() {
        return "Questionnaire{" +
                "version=" + version +
                ", identifiant='" + identifiant + '\'' +
                ", nom='" + nom + '\'' +
                ", dateSoumission=" + dateSoumission +
                ", dateDerniereModif=" + dateDerniereModif +
                ", uuid=" + uuid +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }

    @Override
    protected String toJSON(List<Class<? extends Model>> clazzFrom, String indentation) {
        String json = "";

        json += indentation + "{\n";

        json += super.toJSON(clazzFrom, indentation);

        json = this.toJSON(clazzFrom, indentation, json, UtilisateurEster.class, LangUtils.nameOfProperty(this.getClass(), Questionnaire::getCreateurQuestion), this.getCreateurQuestion());
        json = this.toJSON(clazzFrom, indentation, json, UtilisateurEster.class, LangUtils.nameOfProperty(this.getClass(), Questionnaire::getDernierModificateurQuestion), this.getDernierModificateurQuestion());

        json = this.toJSON(clazzFrom, indentation, json, Question.class, LangUtils.nameOfProperty(this.getClass(), Questionnaire::getQuestions), this.getQuestions());
        json = this.toJSON(clazzFrom, indentation, json, Resultat.class, LangUtils.nameOfProperty(this.getClass(), Questionnaire::getResultats), this.getResultats());
        json = this.toJSON(clazzFrom, indentation, json, Salarie.class, LangUtils.nameOfProperty(this.getClass(), Questionnaire::getSalariesRepondus), this.getSalariesRepondus());
        json = this.toJSON(clazzFrom, indentation, json, Salarie.class, LangUtils.nameOfProperty(this.getClass(), Questionnaire::getSalariesNonRepondus), this.getSalariesNonRepondus());
        json = this.toJSON(clazzFrom, indentation, json, UtilisateurEster.class, LangUtils.nameOfProperty(this.getClass(), Questionnaire::getModificateursQuestion), this.getModificateursQuestion());

        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Questionnaire::getVersion) + "\":" + this.getVersion() + ",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Questionnaire::getNom) + "\":\"" + this.getNom() + "\",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Questionnaire::getDateSoumission) + "\":\"" + this.getDateSoumission() + "\",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Questionnaire::getDateDerniereModif) + "\":\"" + this.getDateDerniereModif() + "\"\n";

        json += indentation + "}";

        return json;
    }

    @Override
    public String toCSV(String separator) {
        StringBuilder sb = new StringBuilder();

        //On affiche d'abord les entetes
        this.displayHeaders(separator, sb);

        //On ajoute un saut de ligne
        sb.append(System.lineSeparator());

        //On affiche les données
        this.displayData(separator, sb);

        return sb.toString();
    }

    private void displayHeaders(String separator, StringBuilder sb) {
        //On affiche d'abord les infos questions
        for(Question question : this.questions) {
            //On affiche en entete les codes questions (AQ48, etc.)
            sb.append(question.getIdentifiant());
            log.info("Identifiant question : " + question.getIdentifiant());

            //On affiche le séparateur entre chaque question
            sb.append(separator);
        }

        //Puis les infos du salarié
        for(SalarieEnum salarieEnum : SalarieEnum.values()) {
            //On affiche en entete les codes salariés (AGENUM, ...)
            sb.append(salarieEnum.getCode());
            log.info("Identifiant salarié : " + salarieEnum.getCode());

            //On affiche le séparateur entre chaque salarié
            sb.append(separator);
        }

        //On supprime le dernier séparateur de la question à la fin
        sb.deleteCharAt(sb.length()-1);
    }

    private void displayData(String separator, StringBuilder sb) {
        //Pour chaque salarié, on affiche les réponses aux questions
        for(Salarie salarie : this.salariesRepondus) {
            log.info("Salarie : " + salarie);
            for(Question question : this.questions) {
                log.info("Question : " + question);
                for(Resultat resultat : this.resultats) {
                    log.info("Résultat : " + resultat);
                    for(Reponse reponse : resultat.getReponseParQuestion()) {
                        log.info("Reponse : " + reponse);
                        if(reponse.getRepondant().equals(salarie) && reponse.getQuestion().equals(question) && reponse.getResultat().equals(resultat)) {
                            sb.append(reponse.getValeurReponse());
                            sb.append(separator);
                            log.info("Valeur question : " + reponse.identifiant);
                        }
                    }
                }
            }

            //A la fin on ajoute les infos relatives aux salariés
            for(SalarieEnum salarieEnum : SalarieEnum.values()) {
                //On affiche en entete les codes salariés (AGENUM, ...)
                String value = salarieEnum.getValue(salarie);
                sb.append(value);
                log.info("Valeur salarié : " + value);

                //On affiche le séparateur entre chaque salarié
                sb.append(separator);
            }

            //On supprime le dernier séparateur de la réponse à la fin
            sb.deleteCharAt(sb.length()-1);

            //On ajoute un saut de ligne entre chaque salarié
            sb.append(System.lineSeparator());
        }

        //On supprime le dernier séparateur du salarié à la fin
        sb.deleteCharAt(sb.length()-1);
    }
}