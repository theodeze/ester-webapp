package fr.univangers.esterwebapp.model;

import org.apache.log4j.Logger;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.*;
import java.util.*;

/**
 * Modele qui représente un Mail permettant d'envoyer un mail aux utilisateurs qui ont oubliés leurs mot de passes ou la création d'un nouveau compte
 */
@Entity
public class Mail extends Model {

    /**
     * Log les traitements d'un Mail
     */
    private static Logger log = Logger.getLogger(Mail.class);

    /**
     * L'objet du mail
     */
    @Column(name = "subject")
    private String subject;

    /**
     * Les destinataires du mail.
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @Column(name = "recipients")
    private List<String> recipients;

    /**
     * Le format du message (html, txt, json, etc.)
     */
    @Column(name = "typeBody")
    private String typeBody;

    /**
     * Le contenu du mail
     */
    @Column(name = "body")
    private String body;

    /**
     * L'heure limite d'expiration du mail
     */
    @Column(name = "expireHour")
    private int expireHour;

    /**
     * La date d'expiration du mail
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "dateExpire")
    private Date dateExpire;

    /**
     * Le serveur de mail
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private ServeurMail serveurMail;

    /**
     * Constructeur par défaut pour Hibernate
     */
    public Mail() {
        this("", new ArrayList<>(), "", "", 0, null);
    }

    /**
     * Constructeur avec arguments pour intialiser les attributs
     * @param subject
     * @param recipients
     * @param typeBody
     * @param body
     * @param expireHour
     * @param serveurMail
     */
    public Mail(String subject, List<String> recipients, String typeBody, String body, int expireHour, ServeurMail serveurMail) {
        super(String.join(", ", recipients));
        this.subject = subject;
        log.info("Objet mail : " + this.subject);
        this.recipients = recipients;
        log.info("Destinataires : " + this.recipients);
        this.typeBody = typeBody;
        log.info("Type du message : " + this.typeBody);
        this.body = body;
        log.info("Corps du message : " + this.body);
        this.expireHour = expireHour;
        log.info("Mail valable : " + this.expireHour + " heure");
        this.dateExpire = new Date();
        log.info("Date expéritation : " + this.dateExpire);
        this.serveurMail = serveurMail;
        log.info("Serveur mail : " + this.serveurMail);

        this.nom= "";

        if(this.serveurMail != null)
            this.serveurMail.getMails().add(this);
    }

    /**
     * Méthode qui permet d'envoyer un mail à un ou plusieurs destinataires
     * @return vrai si envoyé. Faux dans le cas contraire
     */
    public boolean send() {
        try {
            log.info("Get session : " + this.serveurMail.getSession());
            Message message = new MimeMessage(this.serveurMail.getSession());
            log.info("Message : " + message);
            message.setFrom(new InternetAddress(this.serveurMail.identifiant, this.serveurMail.getAlias()));
            log.info("Set from : " + new InternetAddress(this.serveurMail.identifiant, this.serveurMail.getAlias()));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(this.identifiant));
            log.info("Set recipients : " + Arrays.toString(InternetAddress.parse(this.identifiant)));
            message.setSubject(this.subject);
            log.info("Set subject : " + this.subject);
            message.setContent(this.body, this.typeBody);
            log.info("Set type : " + this.typeBody);
            log.info("Set content : " + this.body);

            Transport.send(message);
            log.info("Message envoyé : " + message);
            this.dateExpire = this.createDateExpire();
            log.info("Date expiration : " + this.dateExpire);
            return true;
        }
        catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    /**
     * Méthode qui permet de créer une date d'expiration du mail
     * @return la date d'expiration
     */
    private Date createDateExpire() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, this.expireHour);
        return calendar.getTime();
    }

    /**
     * Getter subject
     *
     * @return subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Setter subject
     *
     * @param subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Getter recipients
     *
     * @return recipients
     */
    public List<String> getRecipients() {
        return recipients;
    }

    /**
     * Setter recipients
     *
     * @param recipients to set
     */
    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    /**
     * Getter typeBody
     *
     * @return typeBody
     */
    public String getTypeBody() {
        return typeBody;
    }

    /**
     * Setter typeBody
     *
     * @param typeBody to set
     */
    public void setTypeBody(String typeBody) {
        this.typeBody = typeBody;
    }

    /**
     * Getter body
     *
     * @return body
     */
    public String getBody() {
        return body;
    }

    /**
     * Setter body
     *
     * @param body to set
     */
    public void setBody(String body) {
        this.body = body;
    }

    /**
     * Getter expireHour
     *
     * @return expireHour
     */
    public int getExpireHour() {
        return expireHour;
    }

    /**
     * Setter expireHour
     *
     * @param expireHour to set
     */
    public void setExpireHour(int expireHour) {
        this.expireHour = expireHour;
    }

    /**
     * Getter dateExpire
     *
     * @return dateExpire
     */
    public Date getDateExpire() {
        return dateExpire;
    }

    /**
     * Setter dateExpire
     *
     * @param dateExpire to set
     */
    public void setDateExpire(Date dateExpire) {
        this.dateExpire = dateExpire;
    }

    /**
     * Getter serveurMail
     *
     * @return serveurMail
     */
    public ServeurMail getServeurMail() {
        return serveurMail;
    }

    /**
     * Setter serveurMail
     *
     * @param serveurMail to set
     */
    public void setServeurMail(ServeurMail serveurMail) {
        this.serveurMail = serveurMail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Mail mail = (Mail) o;
        return expireHour == mail.expireHour &&
                Objects.equals(subject, mail.subject) &&
                Objects.equals(typeBody, mail.typeBody) &&
                Objects.equals(body, mail.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), subject, recipients, typeBody, body, expireHour, dateExpire);
    }

    @Override
    public String toString() {
        return "Mail{" +
                "subject='" + subject + '\'' +
                ", recipients=" + recipients +
                ", typeBody='" + typeBody + '\'' +
                ", body='" + body + '\'' +
                ", expireHour=" + expireHour +
                ", dateExpire=" + dateExpire +
                ", uuid=" + uuid +
                ", identifiant='" + identifiant + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }
}