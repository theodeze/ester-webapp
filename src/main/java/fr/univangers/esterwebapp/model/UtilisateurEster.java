package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import org.apache.log4j.Logger;
import javax.persistence.*;
import java.util.*;

/**
 * Modele qui représente un Utilisateur Ester
 */
@Entity
public class UtilisateurEster extends Connexion {

    /**
     * Log les traitements d'un utilisateur ester
     */
    private static Logger log = Logger.getLogger(UtilisateurEster.class);


    /**
     * Les questionnaires dont il a accées
     */
    @ManyToMany(mappedBy = "modificateursQuestion", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Collection<Questionnaire> questionnairesAcces;

    /**
     * Les questionnaires qu'il a crées
     */
    @OneToMany(mappedBy = "createurQuestion", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Collection<Questionnaire> questionnairesCrees;

    /**
     * Constructeur par défaut pour Hibernate
     */
    public UtilisateurEster() {
        this("", "", "", "", Role.ASSISTANT, true);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs
     * @param identifiant
     * @param nom
     * @param email
     * @param password
     * @param role
     * @param isFirstConnexion
     */
    public UtilisateurEster(String identifiant, String nom, String email, String password, Role role, boolean isFirstConnexion) {
        super(identifiant, email, password, role, isFirstConnexion);
        this.nom = nom;
        this.questionnairesCrees = new ArrayList<>();
        this.questionnairesAcces = new ArrayList<>();
    }


    /**
     * Getter questionnairesAcces
     *
     * @return questionnairesAcces
     */
    public Collection<Questionnaire> getQuestionnairesAcces() {
        return questionnairesAcces;
    }

    /**
     * Setter questionnairesAcces
     *
     * @param questionnairesAcces to set
     */
    public void setQuestionnairesAcces(Collection<Questionnaire> questionnairesAcces) {
        this.questionnairesAcces = questionnairesAcces;
    }

    /**
     * Getter questionnairesCrees
     *
     * @return questionnairesCrees
     */
    public Collection<Questionnaire> getQuestionnairesCrees() {
        return questionnairesCrees;
    }

    /**
     * Setter questionnairesCrees
     *
     * @param questionnairesCrees to set
     */
    public void setQuestionnairesCrees(Collection<Questionnaire> questionnairesCrees) {
        this.questionnairesCrees = questionnairesCrees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UtilisateurEster that = (UtilisateurEster) o;
        return Objects.equals(email, that.email) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), nom);
    }

    @Override
    public String toString() {
        return "UtilisateurEster{" +
                "nom='" + nom + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", identifiant='" + identifiant + '\'' +
                ", role=" + role +
                ", firstConnexion=" + firstConnexion +
                ", uuid=" + uuid +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }
    @Override
    protected String toJSON(List<Class<? extends Model>> clazzFrom, String indentation) {
        String json = "";

        json += indentation + "{\n";

        json += super.toJSON(clazzFrom, indentation);

        json = this.toJSON(clazzFrom, indentation, json, Questionnaire.class, LangUtils.nameOfProperty(this.getClass(), UtilisateurEster::getQuestionnairesAcces), this.getQuestionnairesAcces());
        json = this.toJSON(clazzFrom, indentation, json, Questionnaire.class, LangUtils.nameOfProperty(this.getClass(), UtilisateurEster::getQuestionnairesCrees), this.getQuestionnairesCrees());

        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), UtilisateurEster::getNom) + "\":\"" + this.getNom() + "\"\n";
        json += indentation + "}";

        return json;
    }
}