package fr.univangers.esterwebapp.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import fr.univangers.esterwebapp.model.modelenum.proposition.Proposition;
import fr.univangers.esterwebapp.model.modelenum.question.QuestionEnum;
import fr.univangers.esterwebapp.model.modelenum.question.QuestionType;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import org.apache.log4j.Logger;
import javax.persistence.*;
import java.util.*;

/**
 * Modele qui représente une Question
 */
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Entity
public class Question extends Model {

    /**
     * Log les traitements d'une Question
     */
    private static Logger log = Logger.getLogger(Question.class);

    /**
     * L'intitulé de la question
     */
    @Column(name = "intituleQuestion")
    private String intituleQuestion;

    /**
     * Le type de la question sous forme d'énumération
     */
    @Column(name = "typeQuestion")
    @Enumerated(EnumType.STRING)
    private QuestionType typeQuestion;

    /**
     * Le score maximum que l'on peut atteindre pour cette question
     */
    @Column(name = "scoreMaxQuestion")
    private int scoreMaxQuestion;

    /**
     * Les propositions que l'on peut choisir pour répondre à la question
     */
    @OneToMany(mappedBy = "question", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Collection<OptionQuestion> optionsQuestion;

    /**
     * Le questionnaire qui référence la question
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
    private Questionnaire questionnaire;

    /**
     * Constructeur par défaut pour Hibernate
     */
    public Question() {
        this("", "", QuestionType.CHECKBOX, 0);
    }

    /**
     * Constructeur avec question prédéfinie sans questionnaire à rattacher
     * @param questionEnum la question prédéfnie
     */
    public Question(QuestionEnum questionEnum) {
        this(questionEnum.getCode(), questionEnum.getNom(), questionEnum.getQuestionType(), questionEnum.getScore(), (Questionnaire)null, questionEnum.getPropositions());
    }

    /**
     * Constructeur avec question prédéfinie avec questionnaire à rattacher
     * @param questionEnum la question prédéfnie
     * @param questionnaire le questionnaire à rattacher
     */
    public Question(QuestionEnum questionEnum, Questionnaire questionnaire) {
        this(questionEnum.getCode(), questionEnum.getNom(), questionEnum.getQuestionType(), questionEnum.getScore(), questionnaire, questionEnum.getPropositions());
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs, dont une liste de proposition sans questionnaire à rattacher
     * @param identifiantQuestion
     * @param intituleQuestion
     * @param typeQuestion
     * @param scoreMaxQuestion
     * @param propositions
     */
    public Question(String identifiantQuestion, String intituleQuestion, QuestionType typeQuestion, int scoreMaxQuestion, Proposition[] propositions) {
        this(identifiantQuestion, intituleQuestion, typeQuestion, scoreMaxQuestion, (Questionnaire)null, propositions);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs, dont une liste de proposition avec questionnaire à rattacher
     * @param identifiantQuestion
     * @param intituleQuestion
     * @param typeQuestion
     * @param scoreMaxQuestion
     * @param questionnaire
     * @param propositions
     */
    public Question(String identifiantQuestion, String intituleQuestion, QuestionType typeQuestion, int scoreMaxQuestion, Questionnaire questionnaire, Proposition[] propositions) {
        this(identifiantQuestion, intituleQuestion, typeQuestion, scoreMaxQuestion, questionnaire);
        for(Proposition proposition : propositions) {
            OptionQuestion optionQuestion = new OptionQuestion(proposition, this);
            this.optionsQuestion.add(optionQuestion);
            log.info("Ajout d'une proposition : " + optionQuestion);
        }
        log.info("Propositions : " + this.optionsQuestion);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs sans questionnaire à rattacher
     * @param identifiantQuestion
     * @param intituleQuestion
     * @param typeQuestion
     * @param scoreMaxQuestion
     */
    public Question(String identifiantQuestion, String intituleQuestion, QuestionType typeQuestion, int scoreMaxQuestion) {
        this(identifiantQuestion, intituleQuestion, typeQuestion, scoreMaxQuestion, (Questionnaire)null);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs avec questionnaire à rattacher
     * @param identifiantQuestion
     * @param intituleQuestion
     * @param typeQuestion
     * @param scoreMaxQuestion
     * @param questionnaire
     */
    public Question(String identifiantQuestion, String intituleQuestion, QuestionType typeQuestion, int scoreMaxQuestion, Questionnaire questionnaire) {
        super(identifiantQuestion);
        this.intituleQuestion = intituleQuestion;
        log.info("Intitulé question : " + this.intituleQuestion);
        this.typeQuestion = typeQuestion;
        log.info("Type de question : " + this.typeQuestion);
        this.scoreMaxQuestion = scoreMaxQuestion;
        log.info("Score max de la question : " + this.scoreMaxQuestion);
        this.optionsQuestion = new ArrayList<>();
        log.info("Propositions : " + this.optionsQuestion);
        this.questionnaire = questionnaire;
        log.info("Questionnaire : " + this.questionnaire);
        this.nom = "";

        if(this.questionnaire != null && !this.questionnaire.getQuestions().contains(this)) {
            this.questionnaire.getQuestions().add(this);
            log.info("Ajout de la question dans le questionnaire : " + this.questionnaire.getQuestions());
        }
    }

    /**
     * Getter intituleQuestion
     *
     * @return intituleQuestion
     */
    public String getIntituleQuestion() {
        return intituleQuestion;
    }

    /**
     * Setter intituleQuestion
     *
     * @param intituleQuestion to set
     */
    public void setIntituleQuestion(String intituleQuestion) {
        this.intituleQuestion = intituleQuestion;
    }

    /**
     * Getter typeQuestion
     *
     * @return typeQuestion
     */
    public QuestionType getTypeQuestion() {
        return typeQuestion;
    }

    /**
     * Setter typeQuestion
     *
     * @param typeQuestion to set
     */
    public void setTypeQuestion(QuestionType typeQuestion) {
        this.typeQuestion = typeQuestion;
    }

    /**
     * Getter scoreMaxQuestion
     *
     * @return scoreMaxQuestion
     */
    public int getScoreMaxQuestion() {
        return scoreMaxQuestion;
    }

    /**
     * Setter scoreMaxQuestion
     *
     * @param scoreMaxQuestion to set
     */
    public void setScoreMaxQuestion(int scoreMaxQuestion) {
        this.scoreMaxQuestion = scoreMaxQuestion;
    }

    /**
     * Getter optionsQuestion
     *
     * @return optionsQuestion
     */
    public Collection<OptionQuestion> getOptionsQuestion() {
        return optionsQuestion;
    }

    /**
     * Setter optionsQuestion
     *
     * @param optionsQuestion to set
     */
    public void setOptionsQuestion(Collection<OptionQuestion> optionsQuestion) {
        this.optionsQuestion = optionsQuestion;
    }

    /**
     * Getter questionnaire
     *
     * @return questionnaire
     */
    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    /**
     * Setter questionnaire
     *
     * @param questionnaire to set
     */
    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Question question = (Question) o;
        return scoreMaxQuestion == question.scoreMaxQuestion &&
                Objects.equals(intituleQuestion, question.intituleQuestion) &&
                typeQuestion == question.typeQuestion &&
                optionsQuestion.containsAll(question.optionsQuestion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), intituleQuestion, typeQuestion, scoreMaxQuestion, optionsQuestion);
    }

    @Override
    public String toString() {
        return "Question{" +
                "identifiant='" + identifiant + '\'' +
                ", intituleQuestion='" + intituleQuestion + '\'' +
                ", typeQuestion=" + typeQuestion +
                ", scoreMaxQuestion=" + scoreMaxQuestion +
                ", uuid=" + uuid +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }

    public OptionQuestion getOptionQuestion(String id) {
        OptionQuestion optionQuestion = null;

        Optional<OptionQuestion> optionalOptionQuestion = this.optionsQuestion.stream().filter(o -> o.identifiant.equals(id)).findFirst();
        if(optionalOptionQuestion.isPresent())
            optionQuestion = optionalOptionQuestion.get();

        return optionQuestion;
    }

    @Override
    protected String toJSON(List<Class<? extends Model>> clazzFrom, String indentation) {
        String json = "";

        json += indentation + "{\n";

        json += super.toJSON(clazzFrom, indentation);

        json = this.toJSON(clazzFrom, indentation, json, Questionnaire.class, LangUtils.nameOfProperty(this.getClass(), Question::getQuestionnaire), this.getQuestionnaire());
        json = this.toJSON(clazzFrom, indentation, json, OptionQuestion.class, LangUtils.nameOfProperty(this.getClass(), Question::getOptionsQuestion), this.getOptionsQuestion());

        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Question::getIntituleQuestion) + "\":\"" + this.getIntituleQuestion() + "\",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Question::getTypeQuestion) + "\":\"" + this.getTypeQuestion().getNom() + "\",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Question::getScoreMaxQuestion) + "\":" + this.getScoreMaxQuestion() + "\n";

        json += indentation + "}";

        return json;
    }
}