package fr.univangers.esterwebapp.model.modelenum.proposition.converter;

import fr.univangers.esterwebapp.model.modelenum.proposition.NAF;

import javax.persistence.AttributeConverter;

public class ConverterNAF implements AttributeConverter<NAF, String> {

    @Override
    public String convertToDatabaseColumn(NAF naf) {
        if(naf == null) return "";
        else return naf.getCode();
    }

    @Override
    public NAF convertToEntityAttribute(String code) {
        return NAF.getProposition(code);
    }
}
