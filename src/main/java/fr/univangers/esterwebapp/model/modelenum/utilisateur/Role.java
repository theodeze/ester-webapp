package fr.univangers.esterwebapp.model.modelenum.utilisateur;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import fr.univangers.esterwebapp.model.modelenum.proposition.Proposition;
import org.json.JSONArray;
import org.json.JSONObject;

public enum Role implements Proposition {

    ENTREPRISE("ENT", "Entreprise", 0),
    SALARIE("SAL", "Salarié", 0),
    ASSISTANT("ASS", "Assistant", 0),
    MEDECIN("MED", "Médecin", 0),
    ADMIN("ADMIN", "Administrateur", 0),
    INFIRMIER("INF", "Infirmier", 0),
    PREVENTEUR("PREV", "Préventeur", 0);

    private String code;
    private String nom;
    private Integer score;

    Role() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    Role(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static Role getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static Role[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<Role[]>() {}, Role.class);
    }

    public static Role[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<Role[]>() {}, Role.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "Role{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}