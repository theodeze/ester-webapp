package fr.univangers.esterwebapp.model.modelenum.proposition;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import org.json.JSONArray;
import org.json.JSONObject;

public enum Region implements Proposition {

    GUADELOUPE("01", "Guadeloupe", 0),
    MARTINIQUE("02", "Martinique", 0),
    GUYANE("03", "Guyane", 0),
    REUNION("04", "La Réunion", 0),
    MAYOTTE("06", "Mayotte", 0),
    ILE_DE_FRANCE("11", "Ile-de-France", 0),
    CENTRE_VAL_DE_LOIRE("24", "Centre-Val de Loire", 0),
    BOURGOGNE_FRANCHE_COMPTE("27", "Bourgogne-Franche-Compté", 0),
    NORMANDIE("28", "Normandie", 0),
    HAUTS_DE_FRANCE("32", "Hauts-de-France", 0),
    GRAND_EST("44", "Grand Est", 0),
    PAYS_DE_LA_LOIRE("52", "Pays de la loire", 0),
    BRETAGNE("53", "Bretagne", 0),
    NOUVELLE_AQUITAINE("75", "Nouvelle-Aquitaine", 0),
    OCCITANIE("76", "Occitanie", 0),
    AUVERGNE_RHONE_ALPES("84", "Auvergne-Rhône-Alpes", 0),
    PROVENCE_ALPES_COTE_AZUR("93", "Provence-Alpes-Côte d'Azur", 0),
    CORSE("94", "Corse", 0),
    COM("COM", "Collectivités d'Outre-Mer", 0);

    private String code;
    private String nom;
    private Integer score;

    Region() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    Region(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static Region getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static Region[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<Region[]>() {}, Region.class);
    }

    public static Region[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<Region[]>() {}, Region.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "Region{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}