package fr.univangers.esterwebapp.model.modelenum.proposition;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class PCS implements Proposition {

    private static Logger log = Logger.getLogger(PCS.class);

    private String code;
    private String nom;
    private Integer score;
    private static PCS[] pcs = new PCS[0];

    public static PCS[] values() {
        if(pcs.length > 0) return pcs;
        else {
            pcs = readJSONArray();
            if(pcs.length > 0) return pcs;
            else return readCSVArray();
        }
    }

    public PCS() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    public PCS(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static PCS getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static PCS[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<PCS[]>() {}, PCS.class);
    }

    public static PCS[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<PCS[]>() {}, PCS.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "PCS{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}
