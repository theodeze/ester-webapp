package fr.univangers.esterwebapp.model.modelenum.proposition;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import org.json.JSONArray;
import org.json.JSONObject;

public enum Anciennete implements Proposition {

    MOINS_1_ANS("1", "Moins de 1 an", 0),
    ENTRE_1_2_ANS("2", "Entre 1 et 2 ans", 0),
    ENTRE_3_10_ANS("3", "Entre 3 et 10 ans", 0),
    PLUS_10_ANS("4", "Plus de 10 ans", 0);

    private String code;
    private String nom;
    private Integer score;

    Anciennete() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    Anciennete(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static Anciennete getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static Anciennete[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<Anciennete[]>() {}, Anciennete.class);
    }

    public static Anciennete[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<Anciennete[]>() {}, Anciennete.class);
    }
    
    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "Anciennete{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}
