package fr.univangers.esterwebapp.model.modelenum.question;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import fr.univangers.esterwebapp.model.modelenum.proposition.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;

public enum QuestionEnum implements Proposition {

    NUQUE("AQ81", "Nuque/cou 12 mois", QuestionType.CHECKBOX, 0, Choix.values()),
    EPAULE("AQ82", "Epaule/bras 12 mois", QuestionType.CHECKBOX, 0, Choix.values()),
    COUDE("AQ83", "Coude/avant-bras 12 mois", QuestionType.CHECKBOX, 0, Choix.values()),
    MAIN("AQ84", "Main/poignet 12 mois", QuestionType.CHECKBOX, 0, Choix.values()),
    DOIGTS("AQ85", "Doigts 12 mois", QuestionType.CHECKBOX, 0, Choix.values()),
    HAUT_DOS("AQ86", "Haut du dos 12 mois", QuestionType.CHECKBOX, 0, Choix.values()),
    BAS_DOS("AQ87", "Bas du dos 12 mois", QuestionType.CHECKBOX, 0, Choix.values()),
    SYMPTOME_12("UNSYMPMBSUP12", "Au moins un symptome au membre supérieur au cours des 12 derniers mois", QuestionType.CHECKBOX, 0, Choix.values()),
    DUREE_NUQUE("AQ91", "Durée nuque/cou 12 mois", QuestionType.RADIO, 0, Duree.values()),
    DUREE_EPAULE("AQ92", "Durée épaule/bras 12 mois", QuestionType.RADIO, 0, Duree.values()),
    DUREE_COUDE("AQ93", "Durée coude/avant-bras 12 mois", QuestionType.RADIO, 0, Duree.values()),
    DUREE_MAIN("AQ94", "Durée main/poignet 12 mois", QuestionType.RADIO, 0, Duree.values()),
    DUREE_DOIGTS("AQ95", "Durée doigts 12 mois", QuestionType.RADIO, 0, Duree.values()),
    DUREE_HAUT_DOS("AQ96", "Durée haut du dos 12 mois", QuestionType.RADIO, 0, Duree.values()),
    DUREE_BAS_DOS("AQ97", "Durée bas du dos 12 mois", QuestionType.RADIO, 0, Duree.values()),
    SYMPTOME_30("UNSYMPMBSUP30", "Au moins un symptome du membre supérieur au cours des 12 derniers mois dont la durée est supérieure à 30 jours", QuestionType.CHECKBOX, 0, Choix.values()),
    SYMPTOME_QUOT("UNSYMPMBSUPQUOT", "Au moins un symptome symptome du membre supérieur au cours des 12 derniers mois dont la durée est quotidienne", QuestionType.CHECKBOX, 0, Choix.values()),
    SCIATIQUE_BAS_GENOU("AQ101", "Sciatique avec douleurs plus bas que le genou", QuestionType.CHECKBOX, 0, Choix.values()),
    SCIATIQUE_HAUT_GENOU("AQ102", "Sciatique avec douleurs ne dépassant pas le genou", QuestionType.CHECKBOX, 0, Choix.values()),
    LUMBAGO("AQ103", "Lumbago", QuestionType.CHECKBOX, 0, Choix.values()),
    AUTRE_LOMBALGIE("AQ104", "Autre lombalgie", QuestionType.CHECKBOX, 0, Choix.values()),
    INFLUENCER_DEROULEMENT_TRAVAIL("AQ588", "J'ai la possibilité d'influencer le déroulement de mon travail.", QuestionType.RADIO, 2, Decision.values()),
    COLLEGUES_AIDENT_TACHES("AQ5826", "Les collègues avec qui je travaille m'aident à mener mes tâches à bien.", QuestionType.RADIO, 2, Decision.values()),
    ECHELLE_BORG("AQ48", "Comment évaluez-vous l'intensité des efforts physiques de votre travail au cours d'une journée de travail ?", QuestionType.RADIO, 2, EchelleBorg.values()),
    REPETER_ACTIONS("AQ49", "Votre travail nécessite-t-il de répéter les mêmes actions plus de 2 à 4 fois environ par minute ?", QuestionType.RADIO, 2, Contrainte.values()),
    PENCHER("AQ50", "Votre travail nécessite-t-il de vous pencher en avant et/ou sur le côté régulièrement ou de manière prolongée ?", QuestionType.RADIO, 2, Contrainte.values()),
    TRAVAILLER_BRAS_EN_LAIR("AQ573", "Travaillez-vous avec un ou deux bras en l'air (au-dessus des épaules) régulièrement ou de manière prolongée ?", QuestionType.RADIO, 2, Contrainte.values()),
    FLECHIR_COUDE("AQ576", "Fléchissez-vous le(s) coude(s) régulièrement ou de manière prolongée ?", QuestionType.RADIO, 1, Contrainte.values()),
    PRESSER_OBJETS("AQ579", "Pressez ou prenez-vous fermement des objets ou des pièces entre le pouce et l'index ?", QuestionType.RADIO, 2, Contrainte.values());

    private String code;
    private String nom;
    private Integer score;
    private QuestionType questionType;
    private Proposition[] propositions;

    QuestionEnum() {
        this.code = "";
        this.nom = "";
        this.questionType = QuestionType.RADIO;
        this.score = 0;
        this.propositions = new Proposition[0];
    }

    QuestionEnum(String code, String nom, QuestionType questionType, Integer score, Proposition[] propositions) {
        this.code = code;
        this.nom = nom;
        this.questionType = questionType;
        this.score = score;
        this.propositions = propositions;
    }

    public static QuestionEnum getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getNom() {
        return nom;
    }

    @Override
    public Integer getScore() {
        return score;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public Proposition[] getPropositions() {
        return propositions;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static QuestionEnum[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<QuestionEnum[]>() {}, QuestionEnum.class);
    }

    public static QuestionEnum[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<QuestionEnum[]>() {}, QuestionEnum.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "QuestionEnum{" +
                "code='" + code + '\'' +
                ", nom='" + nom + '\'' +
                ", questionType=" + questionType +
                ", score=" + score +
                ", propositions=" + Arrays.toString(propositions) +
                '}';
    }
}