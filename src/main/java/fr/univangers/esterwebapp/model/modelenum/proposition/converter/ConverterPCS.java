package fr.univangers.esterwebapp.model.modelenum.proposition.converter;

import fr.univangers.esterwebapp.model.modelenum.proposition.PCS;

import javax.persistence.AttributeConverter;

public class ConverterPCS implements AttributeConverter<PCS, String> {

    @Override
    public String convertToDatabaseColumn(PCS pcs) {
        if(pcs == null) return "";
        else return pcs.getCode();
    }

    @Override
    public PCS convertToEntityAttribute(String code) {
        return PCS.getProposition(code);
    }
}
