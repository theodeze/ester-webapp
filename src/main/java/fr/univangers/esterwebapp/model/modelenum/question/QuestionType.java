package fr.univangers.esterwebapp.model.modelenum.question;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import fr.univangers.esterwebapp.model.modelenum.proposition.Proposition;
import org.json.JSONArray;
import org.json.JSONObject;

public enum QuestionType implements Proposition {

    SHORTANSWER("SHORTANSWER", "shortAnswer", 0),
    LONGANSWER("LONGANSWER", "longAnswer", 0),
    CHECKBOX("CHECKBOX", "checkbox", 0),
    RADIO("RADIO", "radio", 0),
    PICTURE("PICTURE", "picture", 0),
    VIDEO("VIDEO", "video", 0);

    private String code;
    private String nom;
    private Integer score;

    QuestionType() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    QuestionType(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static QuestionType getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static QuestionType[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<QuestionType[]>() {}, QuestionType.class);
    }

    public static QuestionType[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<QuestionType[]>() {}, QuestionType.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "QuestionType{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}