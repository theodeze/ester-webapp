package fr.univangers.esterwebapp.model.modelenum;

import fr.univangers.esterwebapp.model.modelenum.proposition.Proposition;
import fr.univangers.esterwebapp.util.Util;
import java.util.Arrays;
import java.util.Optional;

public class SearchEnum {

    private SearchEnum() {}

    public static <T extends Proposition> T getProposition(T[] propositions, String code) {
        if(Util.isNullOrEmpty(code)) return null;
        if(propositions == null) return null;
        if(propositions.length == 0) return null;

        Optional<T> optionalProposition = Arrays.stream(propositions).filter(p -> p.getCode().trim().toLowerCase().equalsIgnoreCase(code.trim().toLowerCase())).findFirst();
        if(optionalProposition.isPresent())
            return optionalProposition.get();
        else
            return null;
    }

}
