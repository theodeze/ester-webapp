package fr.univangers.esterwebapp.model.modelenum.proposition;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import org.json.JSONArray;
import org.json.JSONObject;

public enum Profession implements Proposition {

    OUVRIER_SPECIALISE("1", "Ouvrier spécialisé", 0),
    OUVRIER_QUALIFIE("2", "Ouvrier qualifié", 0),
    AGENT_MAITRISE("3", "Agent de maitrise", 0),
    DIRECTEUR("4", "Directeur", 0),
    TECHNICIEN("5","Technicien", 0),
    INSTITUTEUR_ASSISTANT_SOCIAL_INFIRMIER("6", "Instituteur, assistant social, infirmier", 0),
    INGENIEUR_CADRE("7", "Ingénieur ou cadre", 0),
    PROFESSEUR("8", "Professeur", 0),
    EMPLOYE_BUREAU("9", "Employé de bureau", 0),
    AUTRE("10", "Autre", 0);

    private String code;
    private String nom;
    private Integer score;

    Profession() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    Profession(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static Profession getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static Profession[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<Profession[]>() {}, Profession.class);
    }

    public static Profession[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<Profession[]>() {}, Profession.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "Profession{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}
