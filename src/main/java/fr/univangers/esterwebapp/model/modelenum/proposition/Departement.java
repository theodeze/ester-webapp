package fr.univangers.esterwebapp.model.modelenum.proposition;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import org.json.JSONArray;
import org.json.JSONObject;

public enum Departement implements Proposition {

    AIN("01", "Ain", 84),
    AISNE("02", "Aisne", 32),
    ALLIER("03", "Allier", 84),
    ALPES_HAUTE_PROVENCE("04", "Alpes-de-Haute-Provence", 93),
    HAUTES_ALPES("05", "Hautes-Alpes", 93),
    ALPES_MARITIMES("06", "Alpes-Maritimes", 93),
    ARDECHE("07", "Ardèche", 84),
    ARDENNES("08", "Ardennes", 44),
    ARIEGE("09", "Ariège", 76),
    AUBE_44("10", "Aube", 44),
    AUBE_76("11", "Aube", 76),
    AVEYRON("12", "Aveyron", 76),
    BOUCHES_RHONES("13", "Bouches-du-Rhône", 93),
    CALVADOS("14", "Calvados", 28),
    CANTAL("15", "Cantal", 84),
    CHARENTE("16", "Charente", 75),
    CHARENTE_MARITIME("17", "Charente-Maritime", 75),
    CHER("18", "Cher", 24),
    CORREZE("19", "Corrèze", 75),
    COTE_OR("21", "Côte-d'Or", 27),
    COTES_ARMOR("22", "Côtes-d'Armor", 53),
    CREUSE("23", "Creuse", 75),
    DORDOGNE("24", "Dordogne", 75),
    DOUBS("25", "Doubs", 27),
    DROME("26", "Drôme", 84),
    EURE("27", "Eure", 28),
    EURE_ET_LOIR("28", "Eure-et-Loir", 24),
    FINISTERE("29", "Finistère", 53),
    CORSE_SUD("2A", "Corse-du-Sud", 94),
    HAUTE_CORSE("2B", "Haute-Corse", 94),
    GARD("30", "Gard", 76),
    HAUTE_GARONNE("31", "Haute-Garonne", 76),
    GERS("32", "Gers", 76),
    GIRONDE("33", "Gironde", 75),
    HERAULT("34", "Hérault", 76),
    ILLE_ET_VILAINE("35", "Ille-et-Vilaine", 53),
    INDRE("36", "Indre", 24),
    INDRE_ET_LOIRE("37", "Indre-et-Loire", 24),
    ISERE("38", "Isère", 84),
    JURA("39", "Jura", 27),
    LANDES("40", "Landes", 75),
    LOIR_ET_CHER("41", "Loir-et-Cher", 24),
    LOIRE("42", "Loire", 84),
    HAUTE_LOIRE("43", "Haute-Loire", 84),
    LOIRE_ATLANTIQUE("44", "Loire-Atlantique", 52),
    LOIRET("45", "Loiret", 24),
    LOT("46", "Lot", 76),
    LOT_ET_GARONNE("47", "Lot-et-Garonne", 75),
    LOZERE("48", "Lozère", 76),
    MAINE_ET_LOIRE("49", "Maine-et-Loire", 52),
    MANCHE("50", "Manche", 28),
    MARNE("51", "Marne", 44),
    HAUTE_MARNE("52", "Haute-Marne", 44),
    MAYENNE("53", "Mayenne", 52),
    MEURTHE_ET_MOSELLE("54", "Meurthe-et-Moselle", 44),
    MEUSE("55", "Meuse", 44),
    MORBIHAN("56", "Morbihan", 53),
    MOSELLE("57", "Moselle", 44),
    NIEVRE("58", "Nièvre", 27),
    NORD("59", "Nord", 32),
    OISE("60", "Oise", 32),
    ORNE("61", "Orne", 28),
    PAS_DE_CALAIS("62", "Pas-de-Calais", 32),
    PUY_DE_DOME("63", "Puy-de-Dôme", 84),
    PYRENEES_ATLANTIQUES("64", "Pyrénées-Atlantiques", 75),
    HAUTES_PYRENEES("65", "Hautes-Pyrénées", 76),
    PYRENEES_ORIENTALES("66", "Pyrénées-Orientales", 76),
    BAS_RHIN("67", "Bas-Rhin", 44),
    HAUT_RHIN("68", "Haut-Rhin", 44),
    RHONE("69", "Rhône", 84),
    HAUTE_SAONE("70", "Haute-Saône", 27),
    SAONE_ET_LOIRE("71", "Saône-et-Loire", 27),
    SARTHE("72", "Sarthe", 52),
    SAVOIE("73", "Savoie", 84),
    HAUTE_SAVOIE("74", "Haute-Savoie", 84),
    PARIS("75", "Paris", 11),
    SEINE_MARITIME("76", "Seine-Maritime", 28),
    SEINE_ET_MARNE("77", "Seine-et-Marne", 11),
    YVELINES("78", "Yvelines", 11),
    DEUX_SEVRES("79", "Deux-Sèvres", 75),
    SOMME("80", "Somme", 32),
    TARN("81", "Tarn", 76),
    TARN_ET_GARONNE("82", "Tarn-et-Garonne", 76),
    VAR("83", "Var", 93),
    VAUCLUSE("84", "Vaucluse", 93),
    VENDEE("85", "Vendée", 52),
    VIENNE("86", "Vienne", 75),
    HAUTE_VIENNE("87", "Haute-Vienne", 75),
    VOSGES("88", "Vosges", 44),
    YONNE("89", "Yonne", 27),
    TERRITOIRE_DE_BELFORT("90", "Territoire de Belfort", 27),
    ESSONE("91", "Essonne", 11),
    HAUTS_DE_SEINES("92", "Hauts-de-Seine", 11),
    SEINE_SAINT_DENIS("93", "Seine-Saint-Denis", 11),
    VAL_DE_MARNE("94", "Val-de-Marne", 11),
    VAL_OISE("95", "Val-d'Oise", 11),
    GUADELOUPE("971", "Guadeloupe", 1),
    MARTINIQUE("972", "Martinique", 2),
    GUYANE("973", "Guyane", 3),
    REUNION("974", "La réunion", 4),
    SAINT_PIERRE_ET_MIQUELON("975", "Saint-Pierre-et-Miquelon", 0),
    MAYOTTE("976", "Mayotte", 6),
    SAINT_BARTHELEMY("977", "Saint-Barthélemy", 0),
    SAINT_MARTIN("978", "Saint-Martin", 0),
    TERRES_AUSTRALES_ET_ANTARCTIQUES("984", "Terres australes et antarctiques françaises", 0),
    WALLIS_ET_FUTUNA("986", "Wallis et Futuna", 0),
    POLYNESIE_FRANCAISE("987", "Polynésie française", 0),
    NOUVELLE_CALEDONIE("988", "Nouvelle calédonie", 0),
    ILE_DE_CLIPPERTON("989", "Ile de Clipperton", 0);

    private String code;
    private String nom;
    private Integer score;

    Departement() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    Departement(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static Departement getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static Departement[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<Departement[]>() {}, Departement.class);
    }

    public static Departement[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<Departement[]>() {}, Departement.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "Departement{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}