package fr.univangers.esterwebapp.model.modelenum.question;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import fr.univangers.esterwebapp.model.modelenum.proposition.Proposition;
import org.json.JSONArray;
import org.json.JSONObject;

public enum QuestionnaireEnum implements Proposition {

    LOMBALGIE(1, "evalRiskTMS_Lombalgie", "Eval-Risk-TMS : Risque de lombalgie chronique", 0),
    CHRONIQUE(1, "evalRiskTMS_SMS-MSChronique", "Eval-Risk-TMS : Risque de SMS-MS chronique", 0);

    private int version;
    private String code;
    private String nom;
    private Integer score;

    QuestionnaireEnum() {
        this(0, "", "", 0);
    }

    QuestionnaireEnum(int version, String code, String nom, Integer score) {
        this.version = version;
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static QuestionnaireEnum getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    public int getVersion() {
        return version;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getNom() {
        return nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static QuestionnaireEnum[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<QuestionnaireEnum[]>() {}, QuestionnaireEnum.class);
    }

    public static QuestionnaireEnum[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<QuestionnaireEnum[]>() {}, QuestionnaireEnum.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "QuestionnaireEnum{" +
                "version='" + version + '\'' +
                ", code='" + code + '\'' +
                ", nom='" + nom + '\'' +
                '}';
    }
}