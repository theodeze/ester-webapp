package fr.univangers.esterwebapp.model.modelenum.proposition;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import org.json.JSONArray;
import org.json.JSONObject;

public enum Duree implements Proposition {

    MOINS_24_HEURES("1", "Moins de 24 heures", 0),
    ENTRE_1_7_JOURS("2","1 à 7 jours", 0),
    ENTRE_8_30_JOURS("3", "8 à 30 jours", 0),
    PLUS_30_JOURS("4", "Plus de 30 jours", 0),
    PERMANENCE("5", "En permanence", 0);

    private String code;
    private String nom;
    private Integer score;

    Duree() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    Duree(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static Duree getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static Duree[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<Duree[]>() {}, Duree.class);
    }

    public static Duree[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<Duree[]>() {}, Duree.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "Duree{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}
