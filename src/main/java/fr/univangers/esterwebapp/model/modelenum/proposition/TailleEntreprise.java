package fr.univangers.esterwebapp.model.modelenum.proposition;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import org.json.JSONArray;
import org.json.JSONObject;

public enum TailleEntreprise implements Proposition {

    MOINS_10_SALARIES("1", "Moins de 10 salariés", 0),
    ENTRE_10_49_SALARIES("2", "De 10 à 49 salariés", 0),
    ENTRE_50_199_SALARIES("3", "De 50 à 199 salariés", 0),
    PLUS_200_SALARIES("4", "200 salariés et plus", 0);

    private String code;
    private String nom;
    private Integer score;

    TailleEntreprise() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    TailleEntreprise(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static TailleEntreprise getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static TailleEntreprise[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<TailleEntreprise[]>() {}, TailleEntreprise.class);
    }

    public static TailleEntreprise[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<TailleEntreprise[]>() {}, TailleEntreprise.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "TailleEntreprise{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}
