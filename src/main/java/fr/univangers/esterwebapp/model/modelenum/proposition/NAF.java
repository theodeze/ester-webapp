package fr.univangers.esterwebapp.model.modelenum.proposition;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class NAF implements Proposition {

    private static Logger log = Logger.getLogger(NAF.class);

    private String code;
    private String nom;
    private Integer score;
    private static NAF[] naf = new NAF[0];

    public static NAF[] values() {
        if(naf.length > 0) return naf;
        else {
            naf = readJSONArray();
            if(naf.length > 0) return naf;
            else return readCSVArray();
        }
    }

    public NAF() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    public NAF(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static NAF getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static NAF[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<NAF[]>() {}, NAF.class);
    }

    public static NAF[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<NAF[]>() {}, NAF.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "NAF{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}
