package fr.univangers.esterwebapp.model.modelenum.proposition;

import org.json.JSONObject;

public interface Proposition {
    public String getCode();
    public String getNom();
    public Integer getScore();
    public JSONObject toJSONObject();
    public String toCSVObject();
    public boolean writeJSONObject();
    public boolean writeCSVObject();
}
