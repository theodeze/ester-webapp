package fr.univangers.esterwebapp.model.modelenum.proposition;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import org.json.JSONArray;
import org.json.JSONObject;

public enum Decision implements Proposition {

    PAS_DU_TOUT_ACCORD("1", "Pas du tout d'accord", 0),
    PAS_ACCORD("2", "Pas d'accord", 0),
    ACCORD("3", "D'accord", 0),
    TOUT_A_FAIT_ACCORD("4", "Tout à fait d'accord", 0);

    private String code;
    private String nom;
    private Integer score;

    Decision() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    Decision(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static Decision getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static Decision[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<Decision[]>() {}, Decision.class);
    }

    public static Decision[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<Decision[]>() {}, Decision.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "Decision{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}
