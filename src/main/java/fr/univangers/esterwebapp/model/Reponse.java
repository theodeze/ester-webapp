package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.model.modelenum.proposition.Proposition;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import org.apache.log4j.Logger;
import javax.persistence.*;
import java.util.*;

/**
 * Modele qui représente une Reponse à une question d'un questionnaire
 */
@Entity
public class Reponse extends Model {

    /**
     * Log les traitements d'une Reponse
     */
    private static Logger log = Logger.getLogger(Reponse.class);

    /**
     * Les valeurs répondus à la question
     */
    @Column(name = "valeurReponse")
    private String valeurReponse;

    /**
     * Le score de la réponse
     */
    @Column(name = "scoreReponse")
    private int scoreReponse;

    /**
     * Le salarié qui a répondu
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private Salarie repondant;

    /**
     * La question qui a été répondu
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private Question question;

    /**
     * Le résultat de la réponse
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private Resultat resultat;

    /**
     * Constructeur par défaut pour Hibernate
     */
    public Reponse() {
        this("", "", 0);
    }

    /**
     * Constructeur avec une proposition sans rattaché au salarié, resultat et question
     * @param proposition
     */
    public Reponse(Proposition proposition) {
        this(proposition.getCode(), proposition.getNom(), proposition.getScore());
    }

    /**
     * Constructeur avec une proposition rattaché au salarié, resultat et question
     * @param proposition
     * @param repondant
     * @param resultat
     * @param question
     */
    public Reponse(Proposition proposition, Salarie repondant, Resultat resultat, Question question) {
        this(proposition.getCode(), proposition.getNom(), proposition.getScore(), repondant, resultat, question);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs sans salarié, resultat et question rattaché
     * @param identifiant
     * @param valeurReponse
     * @param scoreReponse
     */
    public Reponse(String identifiant, String valeurReponse, int scoreReponse) {
        this(identifiant, valeurReponse, scoreReponse, null, null, null);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs avec salarié, resultat et question rattaché
     * @param identifiant
     * @param valeurReponse
     * @param scoreReponse
     * @param repondant
     * @param resultat
     * @param question
     */
    public Reponse(String identifiant, String valeurReponse, int scoreReponse, Salarie repondant, Resultat resultat, Question question) {
        super(identifiant);
        this.valeurReponse = valeurReponse;
        log.info("Valeur réponse : " + this.valeurReponse);
        this.scoreReponse = scoreReponse;
        log.info("Score de la réponse : " + this.scoreReponse);
        this.repondant = repondant;
        log.info("Répondant : " + this.repondant);
        this.resultat = resultat;
        log.info("Résultat : " + this.resultat);
        this.question = question;
        log.info("Question : " + this.question);

        if(this.resultat != null && !this.resultat.getReponseParQuestion().contains(this)) {
            this.resultat.getReponseParQuestion().add(this);
            log.info("Ajout de la réponse dans la liste du résultat: " + this.resultat.getReponseParQuestion());
        }
    }

    /**
     * Getter valeursReponse
     *
     * @return valeursReponse
     */
    public String getValeurReponse() {
        return valeurReponse;
    }

    /**
     * Setter valeursReponse
     *
     * @param valeursReponse to set
     */
    public void setValeurReponse(String valeurReponse) {
        this.valeurReponse = valeurReponse;
    }

    /**
     * Getter scoreReponse
     *
     * @return scoreReponse
     */
    public int getScoreReponse() {
        return scoreReponse;
    }

    /**
     * Setter scoreReponse
     *
     * @param scoreReponse to set
     */
    public void setScoreReponse(int scoreReponse) {
        this.scoreReponse = scoreReponse;
    }

    /**
     * Getter repondant
     *
     * @return repondant
     */
    public Salarie getRepondant() {
        return repondant;
    }

    /**
     * Setter repondant
     *
     * @param repondant to set
     */
    public void setRepondant(Salarie repondant) {
        this.repondant = repondant;
    }

    /**
     * Getter question
     *
     * @return question
     */
    public Question getQuestion() {
        return question;
    }

    /**
     * Setter question
     *
     * @param question to set
     */
    public void setQuestion(Question question) {
        this.question = question;
    }

    /**
     * Getter resultat
     *
     * @return resultat
     */
    public Resultat getResultat() {
        return resultat;
    }

    /**
     * Setter resultat
     *
     * @param resultat to set
     */
    public void setResultat(Resultat resultat) {
        this.resultat = resultat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Reponse reponse = (Reponse) o;
        return scoreReponse == reponse.scoreReponse &&
                Objects.equals(valeurReponse, reponse.valeurReponse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), valeurReponse, scoreReponse);
    }

    @Override
    public String toString() {
        return "Reponse{" +
                "identifiant='" + identifiant + '\'' +
                ", valeurReponse=" + valeurReponse +
                ", scoreReponse=" + scoreReponse +
                ", uuid=" + uuid +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }

    @Override
    protected String toJSON(List<Class<? extends Model>> clazzFrom, String indentation) {
        String json = "";

        json += indentation + "{\n";

        json += super.toJSON(clazzFrom, indentation);

        json = this.toJSON(clazzFrom, indentation, json, Salarie.class, LangUtils.nameOfProperty(this.getClass(), Reponse::getRepondant), this.getRepondant());
        json = this.toJSON(clazzFrom, indentation, json, Question.class, LangUtils.nameOfProperty(this.getClass(), Reponse::getQuestion), this.getQuestion());
        json = this.toJSON(clazzFrom, indentation, json, Resultat.class, LangUtils.nameOfProperty(this.getClass(), Reponse::getResultat), this.getResultat());

        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Reponse::getScoreReponse) + "\":" + this.getScoreReponse() + ",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Reponse::getValeurReponse) + "\":\"" + this.getValeurReponse() + "\"\n";

        json += indentation + "}";

        return json;
    }
}