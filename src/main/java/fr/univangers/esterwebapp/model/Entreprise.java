package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import org.apache.log4j.Logger;
import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * Modele qui représente une Entreprise qui doit se connecter par email ou id et mot de passe
 */
@Entity
public class Entreprise extends Connexion {

    /**
     * Log les traitements d'une Entreprise
     */
    private static Logger log = Logger.getLogger(Entreprise.class);


    /**
     * Constructeur par défaut pour Hibernate
     */
    public Entreprise() {
        this("", "", "", "", false);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs
     * @param identifiant
     * @param nom
     * @param email
     * @param password
     * @param isFirstConnexion
     */
    public Entreprise(String identifiant, String nom, String email, String password, boolean isFirstConnexion) {
        super(identifiant, email, password, Role.ENTREPRISE, isFirstConnexion);
        this.nom = nom;
        log.info("Nom : " + this.nom);
    }

    /**
     * Getter nom
     * @return nom
     */
    @Override
    public String getNom() {
        return nom;
    }

    /**
     * Setter nom
     * @param nom to set
     */
    @Override
    public void setNom(String nom) {
        this.nom = nom;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Entreprise that = (Entreprise) o;
        return Objects.equals(nom, that.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), nom);
    }

    @Override
    public String toString() {
        return "Entreprise{" +
                "nom='" + nom + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", identifiant='" + identifiant + '\'' +
                ", role=" + role +
                ", firstConnexion=" + firstConnexion +
                ", created =" + created +
                ", updated =" + updated +
                ", uuid=" + uuid +
                '}';
    }

    @Override
    protected String toJSON(List<Class<? extends Model>> clazzFrom, String indentation) {
        String json = "";

        json += indentation + "{\n";

        json += super.toJSON(clazzFrom, indentation);

        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Entreprise::getNom) + "\":\"" + this.getNom() + "\"\n";

        json += indentation + "}";

        return json;
    }
}