package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.util.Util;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import org.apache.log4j.Logger;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;

/**
 * Model abstraite qui factorise les attributs communs à toutes entités non modifiable
 * - Le UUID
 * - La date d'enregistrement du modele afin de récupérer le premier et la derniere entité enregistré
 */
@MappedSuperclass
public abstract class Model {

    /**
     * Log les traitements d'un modele
     */
    private static Logger log = Logger.getLogger(Model.class);

    /**
     * L'identifiant unique jouant de clé primaire coté BD
     */
    @Id
    @Column(name = "uuid", unique = true, updatable = false, nullable = false)
    protected UUID uuid;

    /**
     * L'identifiant unique afin de faciliter la recherche d'un modele par chaine de caractere
     */
    @Column(name="identifiant", nullable = false)
    protected String identifiant;

    /**
     * Le nom unique afin de faciliter la recherche d'un modele par chaine de caractere
     */
    @Column(name="nom")
    protected String nom;

    /**
     * La date de création à la miliseconde près du modele
     */
    @Column(name = "created", unique = true, updatable = false, nullable = false)
    protected Timestamp created;

    /**
     * La date de mise à jour à la miliseconde près du modele
     */
    @Column(name = "updated", unique = true, updatable = false, nullable = false)
    protected Timestamp updated;

    /**
     * Constructeur par défaut pour Hibernate
     */
    public Model() {
        this("");
    }

    /**
     * Constructeur avec argument pour initialiser les attributs
     * @param identifiant
     */
    public Model(String identifiant) {
        //Génération du uuid coté serveur et non coté BD car cela implique de récupérer le uuid d'une entité coté BD
        //Par exemple si on crée une nouvelle entité, alors il faut l'insérer puis le récupérer dans la BD
        //Alors que lorsqu'on crée une entité, on a immédiatement le uuid
        this.uuid = UUID.randomUUID();
        log.info("UUID : " + this.uuid);
        if(Util.isNullOrEmpty(identifiant))
            this.identifiant = this.uuid.toString();
        else
            this.identifiant = identifiant;
        log.info("Identifiant : " + this.identifiant);
    }

    /**
     * A la création on enregistre la date du moment présent
     */
    @PrePersist
    protected void onCreate() {
        //On attend au moins 1 ms entre chaque create
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
        this.updated = this.created = new Timestamp(new Date().getTime());
        log.info("Created : " + this.created);
    }

    /**
     * A la mise à jour on enregistre la date du moment présent
     */
    @PreUpdate
    protected void onUpdate() {
        //On attend au moins 1 ms entre chaque update
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
        this.updated = new Timestamp(new Date().getTime());
        log.info("Updated : " + this.created);
    }

    /**
     * Getter uuid
     *
     * @return uuid
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * Getter created
     *
     * @return created
     */
    public Timestamp getCreated() {
        return created;
    }

    /**
     * Getter updated
     *
     * @return updated
     */
    public Timestamp getUpdated() {
        return updated;
    }

    /**
     * Getter identifiant
     *
     * @return identifiant
     */
    public String getIdentifiant() {
        return identifiant;
    }

    /**
     * Setter identifiant
     *
     * @param identifiant to set
     */
    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Model model = (Model) o;
        return Objects.equals(uuid.toString(), model.uuid.toString()) &&
                Objects.equals(identifiant, model.identifiant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, created, updated, identifiant);
    }

    @Override
    public String toString() {
        return "Model{" +
                "uuid=" + uuid +
                ", identifiant=" + identifiant +
                ", created =" + created +
                ", updated =" + updated +
                '}';
    }

    public String toCSV(String separator) {
        return separator;
    }

    public String toJSONFormat() {
        return this.toJSON().replaceAll("\n", "").replaceAll("\r", "").replaceAll("\t", "").replaceAll("'","\\\\'");
    }

    public String toJSON() {
        return this.toJSON("");
    }

    public String toJSON(String indentation) {
        List<Class<? extends Model>> clazzFrom = new ArrayList<>();
        clazzFrom.add(this.getClass());
        return this.toJSON(clazzFrom, indentation);
    }

    protected String toJSON(List<Class<? extends Model>> clazzFrom, String indentation) {
        String json = "";

        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Model::getUuid) + "\":\"" + this.getUuid().toString() + "\",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Model::getIdentifiant) + "\":\"" + this.getIdentifiant() + "\",\n";

        return json;
    }

    protected String toJSON(List<Class<? extends Model>> clazzFrom, String indentation, String json, Class<? extends Model> clazzForbid, String property, Collection<? extends Model> models) {

        //Evite un stackoverflow
        if(!clazzFrom.contains(clazzForbid)) {
            List<Class<? extends Model>> clazzFromTMP = new ArrayList<>(clazzFrom);
            clazzFromTMP.add(this.getClass());
            json += indentation + "\t\"" + property + "\":\n";
            json += indentation + "\t[\n";

            for(Model model : models) {
                json += model.toJSON(clazzFromTMP, indentation + "\t\t") + ",\n";
            }
            //On enlève la dernière virgule si présence de modele
            if(!models.isEmpty()) json = json.substring(0, json.length() - 2);
            json += "\n";
            json += indentation + "\t],\n";
        }

        return json;
    }

    protected String toJSON(List<Class<? extends Model>> clazzFrom, String indentation, String json, Class<? extends Model> clazzForbid, String property, Model model) {

        if(!clazzFrom.contains(clazzForbid)) {
            List<Class<? extends Model>> clazzFromTMP = new ArrayList<>(clazzFrom);
            clazzFromTMP.add(this.getClass());
            json += indentation + "\t\"" + property + "\":\n";
            if(model != null) {
                json += model.toJSON(clazzFromTMP, indentation + "\t") + ",\n";
            } else {
                json += "{\n";
                json += "},\n";
            }
        }

        return json;
    }
}