package fr.univangers.esterwebapp.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import fr.univangers.esterwebapp.model.modelenum.proposition.Proposition;
import org.apache.log4j.Logger;
import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Entity
public class OptionQuestion extends Model {

    private static Logger log = Logger.getLogger(OptionQuestion.class);

    /**
     * L'intitulé de la proposition
     */
    @Column(name = "valeur")
    private String valeur;

    /**
     * Le score de la proposition
     */
    @Column(name = "score")
    private int score;

    /**
     * La question auquel appartient l'option
     */
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
    private Question question;

    /**
     * Constructeur par défaut pour Hibernate
     */
    public OptionQuestion() {
        this("", "", 0, null);
    }

    /**
     * Constructeur avec une proposition
     * @param proposition
     */
    public OptionQuestion(Proposition proposition) {
        this(proposition.getCode(), proposition.getNom(), proposition.getScore(), null);
    }

    /**
     * Constructeur avec une proposition et une question
     * @param proposition
     * @param question
     */
    public OptionQuestion(Proposition proposition, Question question) {
        this(proposition.getCode(), proposition.getNom(), proposition.getScore(), question);
    }

    /**
     * Constructeur avec argument pour initialiser les attributs
     * @param identifiant
     * @param valeur
     * @param score
     * @param question
     */
    public OptionQuestion(String identifiant, String valeur, int score, Question question) {
        super(identifiant);
        this.valeur = valeur;
        log.info("Valeur : " + valeur);
        this.score = score;
        log.info("Score : " + score);
        this.question = question;
        log.info("Question : " + question);
        this.nom="";

        if(this.question!= null && !this.question.getOptionsQuestion().contains(this)) {
            this.question.getOptionsQuestion().add(this);
            log.info("Ajout de l'option question dans la question : " + this.question.getOptionsQuestion());
        }
    }

    /**
     * Getter valeur
     *
     * @return valeur
     */
    public String getValeur() {
        return valeur;
    }

    /**
     * Setter valeur
     *
     * @param valeur to set
     */
    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    /**
     * Getter score
     *
     * @return score
     */
    public int getScore() {
        return score;
    }

    /**
     * Setter score
     *
     * @param score to set
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * Getter question
     *
     * @return question
     */
    public Question getQuestion() {
        return question;
    }

    /**
     * Setter question
     *
     * @param question to set
     */
    public void setQuestion(Question question) {
        this.question = question;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        OptionQuestion that = (OptionQuestion) o;
        return score == that.score &&
                Objects.equals(valeur, that.valeur);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), valeur, score);
    }

    @Override
    public String toString() {
        return "OptionQuestion{" +
                "valeur='" + valeur + '\'' +
                ", score=" + score +
                ", uuid=" + uuid +
                ", identifiant='" + identifiant + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }

    @Override
    protected String toJSON(List<Class<? extends Model>> clazzFrom, String indentation) {
        String json = "";

        json += indentation + "{\n";

        json += super.toJSON(clazzFrom, indentation);

        json = this.toJSON(clazzFrom, indentation, json, Question.class, LangUtils.nameOfProperty(this.getClass(), OptionQuestion::getQuestion), this.getQuestion());

        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), OptionQuestion::getValeur) + "\":\"" + this.getValeur() + "\",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), OptionQuestion::getScore) + "\":" + this.getScore() + "\n";

        json += indentation + "}";

        return json;
    }
}