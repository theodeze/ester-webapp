package fr.univangers.esterwebapp.model.reference;

import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;

/**
 * Classe qui permet d'exporter les données d'un model en format JSON
 * @param <T> le modele
 */
public class ExportDataFileJSON<T extends Model> extends ExportDataFile<T> {

    private static Logger log = Logger.getLogger(ExportDataFileJSON.class);

    public ExportDataFileJSON(T model) {
        super(model);
    }

    @Override
    public boolean write() {
        this.records = this.model.toJSON();
        log.info("Records JSON : " + this.records);
        return !Util.isNullOrEmpty(this.records);
    }
}
