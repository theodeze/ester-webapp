package fr.univangers.esterwebapp.model.reference;

import org.apache.log4j.Logger;

import javax.servlet.http.Part;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui permet d'importer les données d'un fichier dans la BD
 * @param <T> le modele
 */
public abstract class ImportDataFile {

    private static Logger log = Logger.getLogger(ImportDataFile.class);

    /**
     * Le fichier stocké dans la requete
     */
    protected Part file;

    /**
     * Le stream permettant de lire le fichier
     */
    protected InputStream inputStream;

    /**
     * Contenu du fichier stocké dans un fichier
     */
    protected List<List<String>> records;

    public ImportDataFile(Part file) {
        this.file = file;
        log.info("Fichier : " + this.file);

        this.records = new ArrayList<>();
        try {
            this.inputStream = file.getInputStream();
            log.info("Input Stream : " + this.inputStream);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Méthode abstraite permettant de lire dans un fichier (CSV, XML, JSON, etc.)
     * @return vrai si la lecture à pu se faire. Faux dans le cas contraire
     */
    public abstract boolean read();

    /**
     * Getter records
     *
     * @return records
     */
    public List<List<String>> getRecords() {
        return records;
    }

    /**
     * Setter records
     *
     * @param records to set
     */
    public void setRecords(List<List<String>> records) {
        this.records = records;
    }
}
