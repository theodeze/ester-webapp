package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.model.modelenum.utilisateur.Age;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Sexe;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import org.apache.log4j.Logger;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * Modele qui représente un SalarieRef
 */
@Entity
public class SalarieRef extends Utilisateur {

    /**
     * Log les traitements d'un SalarieRef
     */
    private static Logger log = Logger.getLogger(SalarieRef.class);

    /**
     * Le sexe du salarié : homme ou femme
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "sexe")
    private Sexe sexe;

    /**
     * L'age de salarié avec un interval de 5ans
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "age")
    private Age age;

    /**
     * Le nombre de connexion du salarié
     */
    @Column(name = "nombreConnexion")
    private int nombreConnexion;

    /**
     * La durée de connexion du salarié
     */
    @Column(name = "dureeConnexion")
    private long dureeConnexion;


    /**
     * La section travail
     */
    @Embedded
    private Travail travail;


    /**
     * L'entreprise auquel le salarié est inscrit
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
    private Entreprise entreprise;

    /**
     * L'utilisateur qui a crée le salarié
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
    private UtilisateurEster createurSalarieRef;

    /**
     * L'ensemble des questionnaires répondus
     */
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
    private Collection<Questionnaire> questionnairesRepondus;

    /**
     * L'ensemble des questionnaires non répondus
     */
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REFRESH})
    private Collection<Questionnaire> questionnairesNonRepondus;

    /**
     * Constructeur par défaut pour Hibernate
     */
    public SalarieRef() {
        this("", Sexe.HOMME, Age.ANS_15_19, null);
    }

    /**
     * Constructeur avec paramétres sans entreprise et sans créateur renseigné
     * @param identifiant
     * @param sexe
     * @param age
     * @param travail
     */
    public SalarieRef(String identifiant, Sexe sexe, Age age, Travail travail) {
        this(identifiant, sexe, age,travail,null, null);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs avec entreprise et de créateur renseigné
     * @param identifiant
     * @param sexe
     * @param age
     * @param entreprise
     * @param createur
     * @param travail
     */
    public SalarieRef(String identifiant, Sexe sexe, Age age, Travail travail,
                   Entreprise entreprise, UtilisateurEster createur ) {
        super(identifiant, Role.SALARIE, true);
        this.sexe = sexe;
        log.info("Sexe : " + this.sexe);
        this.age = age;
        log.info("Age : " + this.age);
        this.travail= travail;
        log.info("Travail : " + this.travail);
        this.nombreConnexion = 0;
        this.dureeConnexion = 0;
        this.entreprise = entreprise;
        log.info("Entreprise : " + this.entreprise);
        this.createurSalarieRef = createur;
        log.info("Créateur : " + this.createurSalarieRef);

        this.questionnairesRepondus = new ArrayList<>();
        this.questionnairesNonRepondus = new ArrayList<>();
    }

    /**
     * Getter sexe
     *
     * @return sexe
     */
    public Sexe getSexe() {
        return sexe;
    }

    /**
     * Setter sexe
     *
     * @param sexe to set
     */
    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    /**
     * Getter age
     *
     * @return age
     */
    public Age getAge() {
        return age;
    }

    /**
     * Setter age
     *
     * @param age to set
     */
    public void setAge(Age age) {
        this.age = age;
    }

    public Travail getTravail() {
        return travail;
    }

    public void setTravail(Travail travail) {
        this.travail = travail;
    }


    /**
     * Getter nombreConnexion
     *
     * @return nombreConnexion
     */
    public int getNombreConnexion() {
        return nombreConnexion;
    }

    /**
     * Setter nombreConnexion
     *
     * @param nombreConnexion to set
     */
    public void setNombreConnexion(int nombreConnexion) {
        this.nombreConnexion = nombreConnexion;
    }

    /**
     * Getter dureeConnexion
     *
     * @return dureeConnexion
     */
    public long getDureeConnexion() {
        return dureeConnexion;
    }

    /**
     * Setter dureeConnexion
     *
     * @param dureeConnexion to set
     */
    public void setDureeConnexion(long dureeConnexion) {
        this.dureeConnexion = dureeConnexion;
    }

    /**
     * Getter entreprise
     *
     * @return entreprise
     */
    public Entreprise getEntreprise() {
        return entreprise;
    }

    /**
     * Setter entreprise
     *
     * @param entreprise to set
     */
    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }

    /**
     * Getter createurSalarieRef
     *
     * @return createurSalarieRef
     */
    public UtilisateurEster getCreateurSalarieRef() {
        return createurSalarieRef;
    }

    /**
     * Setter createurSalarieRef
     *
     * @param createurSalarieRef to set
     */
    public void setCreateurSalarieRef(UtilisateurEster createurSalarieRef) {
        this.createurSalarieRef = createurSalarieRef;
    }

    /**
     * Getter questionnairesRepondus
     *
     * @return questionnairesRepondus
     */
    public Collection<Questionnaire> getQuestionnairesRepondus() {
        return questionnairesRepondus;
    }

    /**
     * Setter questionnairesRepondus
     *
     * @param questionnairesRepondus to set
     */
    public void setQuestionnairesRepondus(Collection<Questionnaire> questionnairesRepondus) {
        this.questionnairesRepondus = questionnairesRepondus;
    }

    /**
     * Getter questionnairesNonRepondus
     *
     * @return questionnairesNonRepondus
     */
    public Collection<Questionnaire> getQuestionnairesNonRepondus() {
        return questionnairesNonRepondus;
    }

    /**
     * Setter questionnairesNonRepondus
     *
     * @param questionnairesNonRepondus to set
     */
    public void setQuestionnairesNonRepondus(Collection<Questionnaire> questionnairesNonRepondus) {
        this.questionnairesNonRepondus = questionnairesNonRepondus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        SalarieRef SalarieRef = (SalarieRef) o;
        return  nombreConnexion == SalarieRef.nombreConnexion &&
                dureeConnexion == SalarieRef.dureeConnexion &&
                sexe == SalarieRef.sexe ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), sexe, nombreConnexion, dureeConnexion);
    }

    @Override
    public String toString() {
        return "SalarieRef{" +
                "sexe=" + sexe +
                ", nombreConnexion=" + nombreConnexion +
                ", dureeConnexion=" + dureeConnexion +
                ", role=" + role +
                ", firstConnexion=" + firstConnexion +
                ", uuid=" + uuid +
                ", identifiant='" + identifiant + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }

    @Override
    protected String toJSON(List<Class<? extends Model>> clazzFrom, String indentation) {
        String json = "";

        json += indentation + "{\n";

        json += super.toJSON(clazzFrom, indentation);

        json = this.toJSON(clazzFrom, indentation, json, Entreprise.class, LangUtils.nameOfProperty(this.getClass(), SalarieRef::getEntreprise), this.getEntreprise());
        json = this.toJSON(clazzFrom, indentation, json, UtilisateurEster.class, LangUtils.nameOfProperty(this.getClass(), SalarieRef::getCreateurSalarieRef), this.getCreateurSalarieRef());

        json = this.toJSON(clazzFrom, indentation, json, Questionnaire.class, LangUtils.nameOfProperty(this.getClass(), SalarieRef::getQuestionnairesRepondus), this.getQuestionnairesRepondus());
        json = this.toJSON(clazzFrom, indentation, json, Questionnaire.class, LangUtils.nameOfProperty(this.getClass(), SalarieRef::getQuestionnairesNonRepondus), this.getQuestionnairesNonRepondus());

        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), SalarieRef::getSexe) + "\":\"" + this.getSexe().getNom() + "\",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), SalarieRef::getTravail) + "\":\"" + this.getTravail() + "\",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), SalarieRef::getNombreConnexion) + "\":\"" + this.getNombreConnexion() + "\",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), SalarieRef::getDureeConnexion) + "\":" + this.getDureeConnexion() + "\n";

        json += indentation + "}";

        return json;
    }
}