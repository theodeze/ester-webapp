package fr.univangers.esterwebapp;

import fr.univangers.esterwebapp.service.QuestionnaireService;
import fr.univangers.esterwebapp.model.*;
import fr.univangers.esterwebapp.model.modelenum.question.QuestionType;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Age;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.model.modelenum.proposition.*;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Sexe;
import fr.univangers.esterwebapp.service.ServeurMailService;

import java.util.*;
import java.util.stream.Collectors;

import static fr.univangers.esterwebapp.util.SecurityUtil.createIdentifiant;

// Compte Gmail de l'Administrateur :
//      email = adm.root.ester@gmail.com
//      mot de passe = ESTER49adROOT
//      pour reset de mot de passe temporaire = ESTER49adROOTtempo (rajouter juste "tempo" à la fin)

public class DataEster {

    private DataEster() {}

    public static final String PATH_FILE_CSV_DONNEES_REFERENCES_QUESTIONNAIRE_CHRONIQUE_DEMO = "public/csv/questionnaire/evalRiskTMS_SMS-MSChronique.csv";
    public static final String PATH_FILE_JSON_DONNEES_REFERENCES_QUESTIONNAIRE_CHRONIQUE_DEMO = "public/json/questionnaire/evalRiskTMS_SMS-MSChronique.json";

    public static List<ServeurMail> getListServeursMail() {
        List<ServeurMail> serveurMails = new ArrayList<>();

        String alias = "no-reply@ester.com";
        String motpasse = "Administrateur";
        String host = "smtp.gmail.com";
        int port = 587;

        ServeurMail serveurMail1 =  new ServeurMail("ester.chu.angers@gmail.com", alias, motpasse, host, port);
        ServeurMail serveurMail2 =  new ServeurMail("ester.chu.angers@yahoo.com", alias, motpasse, host, port);

        serveurMails.add(serveurMail1);
        serveurMails.add(serveurMail2);

        return serveurMails;
    }

    /**
     * Liste de mails pour les tests
     * @return Liste de mails pour les tests
     */
    public static List<Mail> getListMail() {
        List<Mail> mails = new ArrayList<>();
        List<ServeurMail> serveurMails = getListServeursMail();

        ServeurMailService.getInstance().create(serveurMails.get(0));
        ServeurMailService.getInstance().create(serveurMails.get(1));
        Mail mail1 = new Mail("Oublie de mot de passe", Arrays.asList("adm.root.ester@gmail.com"), "text/html", "Vous avez oublié votre mot de passe", 1, serveurMails.get(0));
        Mail mail2 = new Mail("Oublie de mot de passe", Arrays.asList("assistant.ester@gmail.com"), "text/html", "Vous avez oublié votre mot de passe", 1, serveurMails.get(1));

        mails.add(mail1);
        mails.add(mail2);

        return mails;
    }

    /**
     * Liste d'utilisateurs ester pour les tests
     * @return Liste d'utilisateurs ester pour les tests
     */
    public static List<UtilisateurEster> getListUtilisateurEster() {
        List<UtilisateurEster> utilisateurEsters = new ArrayList<>();

        //Tous les utilisateurs qui ont "EmailOK" ont une adresse mail valide (déjà crée)
        //Les boites mails utilisés sont GMAIL (@gmail.com), YAHOO (@yahoo.com), OUTLOOK (@outlook.fr) et LAPOSTE (@laposte.net) et FREE (@free.fr)
        //Les autres qui ont "EmailKO" n'ont pas d'adresse mail de créer

        UtilisateurEster assistantTrueESTER13EmailOK = new UtilisateurEster("AssistantTrueESTER13","Assist", "assistanttrue.ester13@laposte.net", "Assistant@True!ESTER13", Role.ASSISTANT, true);
        UtilisateurEster assistantTrueESTER14EmailKO = new UtilisateurEster("AssistantTrueESTER14","Assist", "assistanttrue.ester14@laposte.net", "Assistant@True!ESTER14", Role.ASSISTANT, true);

        UtilisateurEster medecinTrueESTER17EmailOK = new UtilisateurEster("MedecinTrueESTER17","Medecin", "medecintrue.ester17@gmail.com", "Medecin@True!ESTER17", Role.MEDECIN, true);
        UtilisateurEster medecinTrueESTER18EmailKO = new UtilisateurEster("MedecinTrueESTER18","Medecin", "medecintrue.ester18@gmail.com", "Medecin@True!ESTER18", Role.MEDECIN, true);

        UtilisateurEster adminTrueESTER21EmailOK = new UtilisateurEster("AdminTrueESTER21","Admin", "admintrue.ester21@free.fr", "Admin@True!ESTER21", Role.ADMIN, true); //password pour free : "Admin@TrueESTER"
        UtilisateurEster adminTrueESTER22EmailKO = new UtilisateurEster("AdminTrueESTER22","Admin", "admintrue.ester22@free.fr", "Admin@True!ESTER22", Role.ADMIN, true);

        UtilisateurEster infirmierTrueESTER25EmailOK = new UtilisateurEster("InfirmierTrueESTER25","Infirm", "infirmiertrue.ester25@outlook.fr", "Infirmier@True!ESTER25", Role.INFIRMIER, true);
        UtilisateurEster infirmierTrueESTER26EmailKO = new UtilisateurEster("InfirmierTrueESTER26","Infirm", "infirmiertrue.ester26@outlook.fr", "Infirmier@True!ESTER26", Role.INFIRMIER, true);

        UtilisateurEster preventeurTrueESTER29EmailOK = new UtilisateurEster("PreventeurTrueESTER29","Preventeur", "preventeurtrue.ester29@yahoo.com", "Preventeur@True!ESTER29", Role.PREVENTEUR, true);
        UtilisateurEster preventeurTrueESTER30EmailKO = new UtilisateurEster("PreventeurTrueESTER30","Preventeur", "preventeurtrue.ester30@yahoo.com", "Preventeur@True!ESTER30", Role.PREVENTEUR, true);

        utilisateurEsters.add(assistantTrueESTER13EmailOK);
        utilisateurEsters.add(assistantTrueESTER14EmailKO);

        utilisateurEsters.add(medecinTrueESTER17EmailOK);
        utilisateurEsters.add(medecinTrueESTER18EmailKO);

        utilisateurEsters.add(adminTrueESTER21EmailOK);
        utilisateurEsters.add(adminTrueESTER22EmailKO);

        utilisateurEsters.add(infirmierTrueESTER25EmailOK);
        utilisateurEsters.add(infirmierTrueESTER26EmailKO);

        utilisateurEsters.add(preventeurTrueESTER29EmailOK);
        utilisateurEsters.add(preventeurTrueESTER30EmailKO);


        return utilisateurEsters;
    }

    /**
     * Crée un utilisateur admin pour la démo
     * @return Un utilisateur admin pour la démo
     */
    public static UtilisateurEster getUtilisateurAdminDemo() {
        return new UtilisateurEster("AdminESTER","Admin", "adm.root.ester@gmail.com", "AdminESTER49", Role.ADMIN, true);
    }

    /**
     * Crée un utilisateur assistant pour la démo
     * @return Un utilisateur assistant pour la démo
     */
    public static UtilisateurEster getUtilisateurAssistantDemo() {
        return new UtilisateurEster("AssistantESTER","Assis", "assistant.ester@gmail.com", "AssistantESTER49", Role.ASSISTANT, true);
    }

    /**
     * Crée un utilisateur médecin pour la démo
     * @return Un utilisateur médecin pour la démo
     */
    public static UtilisateurEster getUtilisateurMedecinDemo() {
        return new UtilisateurEster("MedecinESTER","Medecin", "medecin.ester@gmail.com", "MedecinESTER49", Role.MEDECIN, true);
    }

    /**
     * Crée un utilisateur infirmier pour la démo
     * @return Un utilisateur infirmier pour la démo
     */
    public static UtilisateurEster getUtilisateurInfirmierDemo() {
        return new UtilisateurEster("InfirmierESTER","Infirm", "infirmier.ester@gmail.com", "InfirmierESTER49", Role.INFIRMIER, true);
    }

    /**
     * Crée un utilisateur préventeur pour la démo
     * @return Un utilisateur préventeur pour la démo
     */
    public static UtilisateurEster getUtilisateurPreventeurDemo() {
        return new UtilisateurEster("PreventeurESTER","Preventeur", "preventeur.ester@gmail.com", "PreventeurESTER49", Role.PREVENTEUR, true);
    }

    /**
     * Liste d'utilisateurs ester pour la démo
     * @return Liste d'utilisateurs ester pour la démo
     */
    public static List<UtilisateurEster> getListUtilisateurEsterDemo() {
        return Arrays.asList(getUtilisateurAdminDemo(), getUtilisateurAssistantDemo(), getUtilisateurMedecinDemo(), getUtilisateurInfirmierDemo(), getUtilisateurPreventeurDemo());
    }

    /**
     * Liste d'entreprise pour les tests
     * @return Liste d'entreprises pour les tests
     */
    public static List<Entreprise> getListEntreprise() {
        List<Entreprise> entreprises = new ArrayList<>();

        Entreprise entrepriseTrueENTREPRISE05EmailOK = new Entreprise("EntrepriseTrueENTREPRISE05", "ENTREPRISE-CINQ", "entreprisetrue.ester05@yahoo.com", "Entreprise@True!ESTER05", true);
        Entreprise entrepriseTrueENTREPRISE06EmailKO = new Entreprise("EntrepriseTrueENTREPRISE06", "ENTREPRISE-SIX", "entreprisetrue.ester06@yahoo.com", "Entreprise@True!ESTER06", true);
        Entreprise entrepriseFalseENTREPRISE07EmailOK = new Entreprise("EntrepriseFalseENTREPRISE07", "ENTREPRISE-SEPT", "entreprisefalse.ester07@yahoo.com", "Entreprise@False!ESTER07", false);
        Entreprise entrepriseFalseENTREPRISE08EmailKO = new Entreprise("EntrepriseFalseENTREPRISE08", "ENTREPRISE-HUIT", "entreprisefalse.ester08@yahoo.com", "Entreprise@False!ESTER08", false);

        entreprises.add(entrepriseTrueENTREPRISE05EmailOK);
        entreprises.add(entrepriseTrueENTREPRISE06EmailKO);
        entreprises.add(entrepriseFalseENTREPRISE07EmailOK);
        entreprises.add(entrepriseFalseENTREPRISE08EmailKO);

        return entreprises;
    }

    /**
     * Crée une entreprise pour la démo
     * @return Une entreprise pour la démo
     */
    public static Entreprise getEntrepriseDemo() {
        return new Entreprise("EntrepriseESTER", "ESTER", "entreprise.ester@gmail.com", "EntrepriseESTER49", true);
    }

    /**
     * Liste de salariés pour les tests
     * @return Liste de salariés pour les tests
     */
    public static List<Salarie> getListSalarie() {
        List<UtilisateurEster> utilisateurEsters = getListUtilisateurEster();
        List<Entreprise> entreprises = getListEntreprise();
        List<Salarie> salaries = new ArrayList<>();

        Travail travail = new Travail(Departement.MAINE_ET_LOIRE, Region.PAYS_DE_LA_LOIRE,
               NAF.values()[0], PCS.values()[0]);

        Salarie salarie01 = new Salarie("SalarieTrueSALARIE01",
                Sexe.HOMME, Age.ANS_15_19, travail, entreprises.get(0), utilisateurEsters.get(0));
        Salarie salarie02 = new Salarie("SalarieTrueSALARIE02",
                Sexe.HOMME, Age.ANS_20_24, travail, entreprises.get(0), utilisateurEsters.get(1));

        salaries.add(salarie01);
        salaries.add(salarie02);

        return salaries;
    }

    /**
     * Liste de salariés pour la démo
     * @return Liste de mails pour les tests
     */
    public static List<Salarie> getListSalarieDemo()  {
        List<Salarie> salaries = new ArrayList<>();
        List<UtilisateurEster> utilisateurEsters = getListUtilisateurEsterDemo();
        Entreprise entreprise = getEntrepriseDemo();

        Travail travail = new Travail(Departement.AIN, Region.AUVERGNE_RHONE_ALPES, NAF.values()[0], PCS.values()[0]);
        int i = 1;

        for(UtilisateurEster utilisateurEster : utilisateurEsters) {
            Salarie homme = new Salarie(createIdentifiant(i), Sexe.HOMME, Age.ANS_30_34, travail, entreprise, utilisateurEster);
            //Salarie femme = new Salarie("SalarieESTERFemme" + i, Sexe.FEMME, Age.ANS_55_59, travail, entreprise, utilisateurEster);

            salaries.add(homme);
            //salaries.add(femme);
            i++;
        }

        return salaries;
    }

    /**
     * Liste de salariés références pour les tests
     * @return Liste de salariés références pour les tests
     */
    public static List<SalarieRef> getListSalarieRef() {
        return getListSalarie().stream()
                .map(s -> new SalarieRef(s.getIdentifiant(), s.getSexe(), s.getAge(),
                        s.getTravail(), s.getEntreprise(), s.getCreateurSalarie()))
                .collect(Collectors.toList());
    }

    /**
     * Liste de questionnaires pour les tests
     * @return Liste de questionnaires pour les tests
     */
    public static List<Questionnaire> getListQuestionnaire() {
        List<Salarie> salaries = getListSalarie();
        List<Questionnaire> questionnaires = new ArrayList<>();

        for(int i=0; i<salaries.size(); i++) {
            Questionnaire questionnaire = new Questionnaire(i, "Q"+i, "Questionnaire "+i, new Date(), new Date(), salaries.get(i).getCreateurSalarie());
            questionnaires.add(questionnaire);
        }

        return questionnaires;
    }

    /**
     * Liste de questionnaires pour la démo
     * @return Liste de questionnaires pour la démo
     */
    public static List<Questionnaire> getListQuestionnairesDemo() {
        List<Questionnaire> questionnaires = new ArrayList<>();
        List<Salarie> salaries = getListSalarieDemo();

        Optional<Salarie> optionalSalarie = salaries.stream().filter(s -> s.getCreateurSalarie().getRole() == Role.ADMIN).findFirst();
        Salarie salarieFind = salaries.get(0);
        if(optionalSalarie.isPresent())
            salarieFind = optionalSalarie.get();

        //Le questionnaire lombalgie se crée à partir du générateur

        UtilisateurEster admin = salarieFind.getCreateurSalarie();

        Questionnaire lombalgie = QuestionnaireService.getInstance().getOrNewQuestionnaireLombalgie();
        Questionnaire chronique = QuestionnaireService.getInstance().getOrNewQuestionnaireChronique();

        lombalgie.setCreateurQuestion(admin);
        chronique.setCreateurQuestion(admin);
        admin.getQuestionnairesCrees().add(lombalgie);
        admin.getQuestionnairesCrees().add(chronique);

        lombalgie.setDernierModificateurQuestion(admin);
        chronique.setDernierModificateurQuestion(admin);

        for(Salarie salarie : salaries) {
            lombalgie.getModificateursQuestion().add(salarie.getCreateurSalarie());
            chronique.getModificateursQuestion().add(salarie.getCreateurSalarie());
            salarie.getCreateurSalarie().getQuestionnairesAcces().add(lombalgie);
            salarie.getCreateurSalarie().getQuestionnairesAcces().add(chronique);

            lombalgie.getSalariesNonRepondus().add(salarie);
            chronique.getSalariesNonRepondus().add(salarie);
            salarie.getQuestionnairesNonRepondus().add(lombalgie);
            salarie.getQuestionnairesNonRepondus().add(chronique);
        }

        questionnaires.add(lombalgie);
        questionnaires.add(chronique);

        return questionnaires;
    }

    /**
     * Liste de questions pour les tests
     * @return Liste de questions pour les tests
     */
    public static List<Question> getListQuestions() {
        List<Questionnaire> questionnaires = getListQuestionnaire();
        List<Question> questions = new ArrayList<>();

        Question question1 = new Question("Q1", "Question 1", QuestionType.CHECKBOX, 2, questionnaires.get(0), Region.values());
        Question question2 = new Question("Q2", "Question 2", QuestionType.CHECKBOX, 2, questionnaires.get(1), Anciennete.values());
        Question question3 = new Question("Q3", "Question 3", QuestionType.CHECKBOX, 2, questionnaires.get(2), Choix.values());
        Question question4 = new Question("Q4", "Question 4", QuestionType.CHECKBOX, 2, questionnaires.get(3), ContratTravail.values());

        Question question5 = new Question("Q5", "Question 5", QuestionType.CHECKBOX, 2, questionnaires.get(4), Decision.values());
        Question question6 = new Question("Q6", "Question 6", QuestionType.CHECKBOX, 2, questionnaires.get(5), Departement.values());
        Question question7 = new Question("Q7", "Question 7", QuestionType.CHECKBOX, 2, questionnaires.get(6), Diplome.values());
        Question question8 = new Question("Q8", "Question 8", QuestionType.CHECKBOX, 2, questionnaires.get(7), Duree.values());

        Question question9 = new Question("Q9", "Question 9", QuestionType.CHECKBOX, 2, questionnaires.get(8), EchelleBorg.values());
        Question question10 = new Question("Q10", "Question 10", QuestionType.CHECKBOX, 2, questionnaires.get(9), Profession.values());
        Question question11 = new Question("Q11", "Question 11", QuestionType.CHECKBOX, 2, questionnaires.get(10), SecteurActivite.values());
        Question question12 = new Question("Q12", "Question 12", QuestionType.CHECKBOX, 2, questionnaires.get(11), Sexe.values());

        Question question13 = new Question("Q13", "Question 13", QuestionType.CHECKBOX, 2, questionnaires.get(12), Contrainte.values());
        Question question14 = new Question("Q14", "Question 14", QuestionType.CHECKBOX, 2, questionnaires.get(13), TailleEntreprise.values());
        Question question15 = new Question("Q15", "Question 15", QuestionType.CHECKBOX, 2, questionnaires.get(14), Anciennete.values());
        Question question16 = new Question("Q16", "Question 16", QuestionType.CHECKBOX, 2, questionnaires.get(15), Choix.values());

        Question question17 = new Question("Q17", "Question 17", QuestionType.CHECKBOX, 2, questionnaires.get(16), ContratTravail.values());
        Question question18 = new Question("Q18", "Question 18", QuestionType.CHECKBOX, 2, questionnaires.get(17), Decision.values());
        Question question19 = new Question("Q19", "Question 19", QuestionType.CHECKBOX, 2, questionnaires.get(18), Diplome.values());
        Question question20 = new Question("Q20", "Question 20", QuestionType.CHECKBOX, 2, questionnaires.get(19), Duree.values());

        questions.add(question1);
        questions.add(question2);
        questions.add(question3);
        questions.add(question4);

        questions.add(question5);
        questions.add(question6);
        questions.add(question7);
        questions.add(question8);

        questions.add(question9);
        questions.add(question10);
        questions.add(question11);
        questions.add(question12);

        questions.add(question13);
        questions.add(question14);
        questions.add(question15);
        questions.add(question16);

        questions.add(question17);
        questions.add(question18);
        questions.add(question19);
        questions.add(question20);

        return questions;
    }

    /**
     * Liste d'options pour les tests
     * @return Liste d'options pour les tests
     */
    public static List<OptionQuestion> getListOptionQuestion() {
        List<Question> questions = getListQuestions();
        List<OptionQuestion> optionQuestions = new ArrayList<>();

        for(Question question : questions) {
            OptionQuestion optionQuestion = new OptionQuestion(question.getIdentifiant(), "", 0, null);
            optionQuestions.add(optionQuestion);
        }

        return optionQuestions;
    }

    /**
     * Liste de résultats pour les tests
     * @return Liste de résultats pour les tests
     */
    public static List<Resultat> getListResultat() {
        List<Question> questions = getListQuestions();
        List<Resultat> resultats = new ArrayList<>();

        for(int i=0; i<questions.size(); i++) {
            //Optional<Salarie> optionalSalarie = questions.get(i).getQuestionnaire().getCreateurQuestion().getSalariesCrees().stream().findFirst();
            Salarie salarie = null;
          //  if(optionalSalarie.isPresent())
           //     salarie = optionalSalarie.get();

            Resultat resultat = new Resultat(questions.get(i).getQuestionnaire().getIdentifiant(), new Date(), new Date(), i, questions.get(i).getQuestionnaire(),
                    questions.get(i).getQuestionnaire().getCreateurQuestion(), salarie);

            resultats.add(resultat);
        }

        return resultats;
    }

    /**
     * Liste de résultats références pour les tests
     * @return Liste de résultats références pour les tests
     */
    public static List<ResultatRef> getListResultatRef() {
        List<SalarieRef> salarieRefs = getListSalarieRef();
        List<Resultat> resultats = getListResultat();
        List<ResultatRef> resultatRefs = new ArrayList<>();

        for(int i=0; i<resultats.size(); i++) {
            Resultat r = resultats.get(i);
            salarieRefs.get(i).setCreateurSalarieRef(r.getPersonneAssociant());
            salarieRefs.get(i).setEntreprise(r.getPersonneRepondant().getEntreprise());
            ResultatRef resultatRef = new ResultatRef(r.getQuestionnaireRepondu().getIdentifiant(), r.getDateLienAuSalarie(), r.getDateReponse(), r.getScoreGlobal(), r.getQuestionnaireRepondu(), r.getPersonneAssociant(), salarieRefs.get(i));
            resultatRefs.add(resultatRef);
        }

        return resultatRefs;
    }

    /**
     * Liste de réponses pour les tests
     * @return Liste de réponses pour les tests
     */
    public static List<Reponse> getListReponse() {
        List<Resultat> resultats = getListResultat();
        List<Reponse> reponses = new ArrayList<>();

        Question question = null;
        Optional<Question> optionalQuestion = null;

        optionalQuestion = resultats.get(0).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse1 = new Reponse(Departement.AIN, resultats.get(0).getPersonneRepondant(), resultats.get(0), question);

        optionalQuestion = resultats.get(1).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse2 = new Reponse(Anciennete.ENTRE_1_2_ANS, resultats.get(1).getPersonneRepondant(), resultats.get(1), question);

        optionalQuestion = resultats.get(2).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse3 = new Reponse(Choix.OUI, resultats.get(2).getPersonneRepondant(), resultats.get(2), question);

        optionalQuestion = resultats.get(3).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse4 = new Reponse(ContratTravail.CDI, resultats.get(3).getPersonneRepondant(), resultats.get(3), question);

        optionalQuestion = resultats.get(4).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse5 = new Reponse(Decision.ACCORD, resultats.get(4).getPersonneRepondant(), resultats.get(4), question);

        optionalQuestion = resultats.get(5).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse6 = new Reponse(Departement.values()[0], resultats.get(5).getPersonneRepondant(), resultats.get(5), question);

        optionalQuestion = resultats.get(6).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse7 = new Reponse(Diplome.BAC, resultats.get(6).getPersonneRepondant(), resultats.get(6), question);

        optionalQuestion = resultats.get(7).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse8 = new Reponse(Duree.ENTRE_1_7_JOURS, resultats.get(7).getPersonneRepondant(), resultats.get(7), question);

        optionalQuestion = resultats.get(8).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse9 = new Reponse(EchelleBorg.values()[0], resultats.get(8).getPersonneRepondant(), resultats.get(8), question);

        optionalQuestion = resultats.get(9).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse10 = new Reponse(Profession.AGENT_MAITRISE, resultats.get(9).getPersonneRepondant(), resultats.get(9), question);

        optionalQuestion = resultats.get(10).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse11 = new Reponse(SecteurActivite.AGRICULTURE, resultats.get(10).getPersonneRepondant(), resultats.get(10), question);

        optionalQuestion = resultats.get(11).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse12 = new Reponse(Sexe.FEMME, resultats.get(11).getPersonneRepondant(), resultats.get(11), question);

        optionalQuestion = resultats.get(12).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse13 = new Reponse(Contrainte.JAMAIS, resultats.get(12).getPersonneRepondant(), resultats.get(12), question);

        optionalQuestion = resultats.get(13).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse14 = new Reponse(TailleEntreprise.ENTRE_10_49_SALARIES, resultats.get(13).getPersonneRepondant(), resultats.get(13), question);

        optionalQuestion = resultats.get(14).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse15 = new Reponse(Anciennete.ENTRE_3_10_ANS, resultats.get(14).getPersonneRepondant(), resultats.get(14), question);

        optionalQuestion = resultats.get(15).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse16 = new Reponse(Choix.NON, resultats.get(15).getPersonneRepondant(), resultats.get(15), question);

        optionalQuestion = resultats.get(16).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse17 = new Reponse(ContratTravail.CDD, resultats.get(16).getPersonneRepondant(), resultats.get(16), question);

        optionalQuestion = resultats.get(17).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse18 = new Reponse(Decision.PAS_ACCORD, resultats.get(17).getPersonneRepondant(), resultats.get(17), question);

        optionalQuestion = resultats.get(18).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse19 = new Reponse(Diplome.CAP_BEP, resultats.get(18).getPersonneRepondant(), resultats.get(18), question);

        optionalQuestion = resultats.get(19).getQuestionnaireRepondu().getQuestions().stream().findFirst();
        if(optionalQuestion.isPresent()) question = optionalQuestion.get();
        Reponse reponse20 = new Reponse(Duree.PLUS_30_JOURS, resultats.get(19).getPersonneRepondant(), resultats.get(19), question);

        reponses.add(reponse1);
        reponses.add(reponse2);
        reponses.add(reponse3);
        reponses.add(reponse4);

        reponses.add(reponse5);
        reponses.add(reponse6);
        reponses.add(reponse7);
        reponses.add(reponse8);

        reponses.add(reponse9);
        reponses.add(reponse10);
        reponses.add(reponse11);
        reponses.add(reponse12);

        reponses.add(reponse13);
        reponses.add(reponse14);
        reponses.add(reponse15);
        reponses.add(reponse16);

        reponses.add(reponse17);
        reponses.add(reponse18);
        reponses.add(reponse19);
        reponses.add(reponse20);

        return reponses;
    }

    /**
     * Liste de réponses références pour les tests
     * @return Liste de réponses références pour les tests
     */
    public static List<ReponseRef> getListReponseRef() {
        List<ResultatRef> resultatRefs = getListResultatRef();
        List<Reponse> reponses = getListReponse();
        List<ReponseRef> reponseRefs = new ArrayList<>();

        for(int i=0; i<reponses.size(); i++) {
            Reponse r = reponses.get(i);
            Question question = null;
            Optional<Question> optionalQuestion = resultatRefs.get(i).getQuestionnaireRepondu().getQuestions().stream().findFirst();
            if(optionalQuestion.isPresent())
                question = optionalQuestion.get();

            ReponseRef reponseRef = new ReponseRef(r.getIdentifiant(), r.getValeurReponse(), r.getScoreReponse(), resultatRefs.get(i).getPersonneRepondant(),
                    resultatRefs.get(i), question);
            reponseRefs.add(reponseRef);
        }

        return reponseRefs;
    }
}