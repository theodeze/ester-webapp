package fr.univangers.esterwebapp.filter;

import fr.univangers.esterwebapp.servlet.Servlet;
import fr.univangers.esterwebapp.model.Utilisateur;
import org.apache.log4j.Logger;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Filtre abstrait qui permet de vérifier si l'utilisateur à le droit d'accéder à la page
 * En effet si un salarié arrive à se connecter alors il doit accéder uniquement à sa ressource, et non
 * aller sur la ressource d'un autre utilisateur comme une entreprise par exemple
 * @param <T> l'utilisateur
 */
public abstract class UtilisateurFilter<T extends Utilisateur> implements Filter {

    /**
     * Log les traitements du filtre utilisateur
     */
    private static Logger log = Logger.getLogger(UtilisateurFilter.class);

    /**
     * Redirection vers la page de connexion
     */
    private static final String ACCES_CONNEXION  = "/connexion";

    /**
     * Le modèle de classe de l'utilisateur
     */
    private Class<T> clazz;

    public UtilisateurFilter(Class<T> clazz)
    {
        this.clazz = clazz;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        // Cast des objets request et response
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        log.info("Cast des objets request : " + req + " et response : " + resp);

        // Récupération de la session depuis la requête
        HttpSession session = request.getSession();
        log.info("Récupération de la session depuis la requête : " + session);

        Utilisateur sessionUser = Servlet.readUserInSession(request);
        //sessionUser.getClass().isAssignableFrom(this.clazz) = sessionUser instanceof T
        if (sessionUser != null && sessionUser.getClass().isAssignableFrom(this.clazz)) {
            // Affichage de la page restreinte
            log.info("Affichage de la page restreinte");
            chain.doFilter(request, response);
        }
        else {
            // Redirection vers la page publique
            log.info("Redirection vers la page publique");
            req.getRequestDispatcher(ACCES_CONNEXION).forward(req, response);
        }
    }
}