package fr.univangers.esterwebapp.filter;

import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.servlet.UtilisateurEsterServlet;

import javax.servlet.annotation.WebFilter;

/**
 * Filtre qui permet de s'assurer que l'utilisateur est bien un personne de l'équipe Ester
 * Si Ester alors il peut se connecter et continuer
 * Sinon il revient sur la page d'accueil
 */
@WebFilter(filterName = "UtilisateurEsterFilter", urlPatterns = {UtilisateurEsterFilter.URL_UTILISATEUR})
public class UtilisateurEsterFilter extends UtilisateurFilter<UtilisateurEster> {

    public static final String URL_UTILISATEUR = UtilisateurEsterServlet.URL_UTILISATEUR + "/*";

    public UtilisateurEsterFilter() {
        super(UtilisateurEster.class);
    }
}