package fr.univangers.esterwebapp.filter;

import fr.univangers.esterwebapp.model.Mail;
import fr.univangers.esterwebapp.service.MailService;
import fr.univangers.esterwebapp.servlet.ConnexionServlet;
import fr.univangers.esterwebapp.servlet.ResetPasswordServlet;
import fr.univangers.esterwebapp.servlet.Servlet;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.UUID;

/**
 * Filtre qui permet de s'assurer que l'utilisateur a bien fait une demande de réinitialisation de mot de passe
 * avant d'accéder à la page
 */
@WebFilter(filterName = "ResetPasswordFilter", urlPatterns = {ResetPasswordFilter.URL_RESET_MOTPASSE})
public class ResetPasswordFilter implements Filter {

    private static Logger log = Logger.getLogger(ResetPasswordFilter.class);

    public static final String URL_RESET_MOTPASSE = ResetPasswordServlet.URL_RESET_MOTPASSE + "/*";

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) {
        // Cast des objets request et response
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        log.info("Cast des objets request : " + req + " et response : " + resp);

        boolean valid = false;

        //Récupération du token
        String token = request.getParameter(ResetPasswordServlet.TOKEN_INPUT);
        request.setAttribute(ResetPasswordServlet.TOKEN_VALUE, token);
        log.info("Token : " + token);

        if(!Util.isNullOrEmpty(token)) {

            //Vérification du token
            UUID uuid = null;
            try {
                uuid = UUID.fromString(token);
                log.info("UUID : " + uuid);
            }
            catch (Exception e) {
                log.error(e.getMessage());
                uuid = null;
            }

            if(uuid != null) {
                //Vérification de la demande de réinitialisation du mot de passe
                Mail mail = MailService.getInstance().readById(uuid);
                log.info("Mail : " + mail);

                if(mail != null) {
                    //Vérification de l'expiration du lien
                    Date today = new Date();

                    if(mail.getDateExpire().compareTo(today) >= 0) {
                        valid = true;
                        String message = "Veuillez saisir un nouveau mot de passe";
                        Servlet.ATT_MSG_SUCCESS.write(message, request);
                    }
                    else {
                        String message = "La demande de réinitialisation de mot de passe est expiré. Veuillez renouveller la demande";
                        Servlet.ATT_MSG_WARNING.write(message, request);
                        MailService.getInstance().delete(mail);
                    }
                }
            }
        }

        if(valid) {
            try {
                // Affichage de la page restreinte
                log.info("Affichage de la page restreinte");
                chain.doFilter(request, response);
            }
            catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        else {
            // Redirection vers la page publique
            log.info("Redirection vers la page publique");
            Servlet.dispatch(request, response, ConnexionServlet.VUE_LOGIN);
        }
    }
}