package fr.univangers.esterwebapp;

import fr.univangers.esterwebapp.model.Question;
import org.apache.log4j.Logger;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

public class ResultatChart {

    /**
     * Log les traitements de ResultatChart
     */
    private static Logger log = Logger.getLogger(ResultatChart.class);

    private static final String RED = "#ff3d00";
    private static final String ORANGE = "#ff9100";
    private static final String YELLOW = "#ffea00";
    private static final String GREEN = "#00e676";
    private static final String BLUE = "#2962ff";

    private static  final String[] tabColors = {RED,ORANGE,YELLOW,GREEN,BLUE};

    private static final String IDEVALRISKTMSCHRONIQUE = "evalRiskTMS_SMS-MSChronique";
    private static final String IDEVALRISKTMSLOMBALGIE = "evalRiskTMS_Lombalgie";

    private static final String SALTSA = "Seuil du facteur de risque (SALTSA)";
    private static final String TJ = "Toujours (>4h/jours)";
    private static final String SO = "Souvent (de 2 à 4h/jour)";
    private static final String RA = "Rarement (moins de 2h/jour)";
    private static final String JA = "Jamais";


    private static final String VS = "D'accord ou tout à fait d'accord vs pas d'accord ou pas du tout d'accord";
    private static final String PT = "Pas du tout d'accord";
    private static final String PA = "Pas d'accord";
    private static final String DA = "D'accord";
    private static final String TA = "Tout à fait d'accord";

    private List<Double> categoriesHisto = new ArrayList<>();
    private List<Double> dataHisto = new ArrayList<>();
    private List<String> colorsHisto = new ArrayList<>();
    private int plotBandFactorHisto = 0;
    private ArrayList<String> reponsesQuestion = new ArrayList<>();


    private List<List<String>> dataCSV = new ArrayList<>();
    private List<List<String>> answers = new ArrayList<>();

    private List<String> categoriesRPE = new ArrayList<>();
    private List<Double> dataRPE = new ArrayList<>();
    private List<String> colorsRPE = new ArrayList<>();
    private String reponseRpe = null;

    public List<Double> getCategoriesHisto() {
        return categoriesHisto;
    }

    public List<Double> getDataHisto() {
        return dataHisto;
    }
    public List<String> getColorsHisto() {
        return colorsHisto;
    }
    public int getPlotBandsFactorHisto() {
        return plotBandFactorHisto;
    }

    public void addHisto(double categorie, double data, String color) {
        categoriesHisto.add(categorie);
        dataHisto.add(data);
        colorsHisto.add(color);
    }

    public List<String> getCategoriesRPE() {
        return categoriesRPE;
    }

    public List<Double> getDataRPE() {
        return dataRPE;
    }
    public List<String> getColorsRPE() {
        return colorsRPE;
    }

    public void addRPE(String categorie, double data, String color) {
        categoriesRPE.add(categorie);
        dataRPE.add(data);
        colorsRPE.add(color);
    }

    public String getReponseRpe() {
        return reponseRpe;
    }

    public void addAnswer(int index, String answer) {
        if(index > answers.size())
            return;
        if(index == answers.size())
            answers.add(new ArrayList<>());
        answers.get(index).add(answer);
    }

    public List<String> getAnswers(int index) {
        if(index >= answers.size())
            return Collections.emptyList();
        return answers.get(index);
    }

    public int sizeDataCSV() {
        return dataCSV.size();
    }

    public String getDataCSV(int index) {
        if(index >= dataCSV.size())
            return null;
        return String.join("\n", dataCSV.get(index));
    }

    public void setTitleDataCSV(int index, String... titles) {
        if(index > dataCSV.size())
            return;
        if(index == dataCSV.size())
            dataCSV.add(new ArrayList<>());
        if(dataCSV.get(index).isEmpty())
            dataCSV.get(index).add(0, String.join(",", titles));
        else
            dataCSV.get(index).set(0, String.join(",", titles));
    }

    public void addDataCSV(int index, int plot, String... datas) {
        if(index > dataCSV.size())
            return;
        if(index == dataCSV.size())
            dataCSV.add(new ArrayList<>());
        List<String> datasPlot = new ArrayList<>(Arrays.asList(datas));
        if(plot == 1) {
            datasPlot.add(2, "0");
            datasPlot.add(4, "0");
            datasPlot.add(6, "1");
        } else if(plot == 2) {
            datasPlot.add(2, "0");
            datasPlot.add(4, "1");
            datasPlot.add(6, "0");
        } else {
            datasPlot.add(2, "1");
            datasPlot.add(4, "0");
            datasPlot.add(6, "0");
        }
        dataCSV.get(index).add(String.join(",", datasPlot));
    }
    public void chartsEvalRiskChronique() {
        setTitleDataCSV(0,"Categorie","La plupart du temps (> 4 heures/jour)",SALTSA,"Souvent (2 à 4 heures/jour)",SALTSA,"Rarement (< 2 heures/jour)",SALTSA,JA);
        addDataCSV(0,3,"Votre travail nécessite-t-il de répêter les mêmes actions plus de 2 à 4 fois environ par minute ?"
                ,"25","25","25","25");
        addDataCSV(0,2,"Travailler avec un ou deux bras en l'air (au-dessus des épaules) régulièrement ou de manière prolongée"
                ,"25","25","25","25");
        addDataCSV(0,2,"Fléchir le(s) coude(s) régulièrement ou de manière prolongée"
                ,"15","35","25","25");
        addDataCSV(0,3,"Presser ou prendre ferment des objets ou des pièces entre le pouce et l'index"
                ,"25","25","35","15");
        setTitleDataCSV(1,"Categorie",PT,VS,PA,VS,DA,VS,TA);
        addDataCSV(1,2,"J'ai la possibilité d'influencer le déroulement de mon travail"
                ,"15","25","45","15");
        addDataCSV(1,2,"Les collègues avec qui je travaille m'aident à mener les tàches à bien"
                ,"15","55","15","15");
    }
    public void chartsEvalRiskLombalgie() {
        setTitleDataCSV(0,"Categorie","Toujours",SALTSA,"Souvent",SALTSA,"Rarement",SALTSA,JA);
        addDataCSV(0,3,"Votre travail nécessite-t-il de se pencher en avant ou sur le côté plus de 2h/jour ?"
                ,"20","25","30","25");
    }
    public ResultatChart() {
        addHisto(0, 9.6, GREEN);
        addHisto(1, 30, GREEN);
        addHisto(2, 7, BLUE);
        addHisto(3, 16, BLUE);
        addHisto(4, 6.5, BLUE);
        addHisto(5, 8, BLUE);
        addHisto(6, 3, BLUE);
        addHisto(7, 3, BLUE);
        addHisto(8, 1, BLUE);
        addHisto(9, 1, BLUE);
        addHisto(10, 1, BLUE);
        addHisto(11, 0.5, BLUE);
        addHisto(12, 0.5, BLUE);
        addHisto(13, 0.5, BLUE);
        addHisto(14, 0.5, BLUE);
        addHisto(15, 0.5, BLUE);

        WebContext ctx = WebContextFactory.get();
        HttpServletRequest request = ctx.getHttpServletRequest();
        String questionnaire = (String) request.getSession(false).getAttribute("questionnaire");
        log.info("Questionnaire resultatChart"+questionnaire);
        Question question = (Question) request.getSession(false).getAttribute("questionRpe");
        Map<String,Double> pourcentagesScore = (Map<String, Double>) request.getSession(false).getAttribute("pourcentagesScore");
        log.info("Map pourcentage Score"+pourcentagesScore);
        Map<String,Double> pourcentageRpe = (Map<String, Double>) request.getSession(false).getAttribute("pourcentagesRPE");
        log.info("Map Pourecentage Rpe"+pourcentageRpe);

        plotBandFactorHisto = (request.getSession(false).getAttribute("score")!= null)?(int)request.getSession(false).getAttribute("score"):0  ;
        reponsesQuestion = (request.getSession(false).getAttribute("reponsesQuestions") == null)?new ArrayList<>(): (ArrayList<String>)request.getSession(false).getAttribute("reponsesQuestions");
        reponseRpe = (String) request.getSession(false).getAttribute("reponseRPE");

        if(questionnaire.compareTo(IDEVALRISKTMSCHRONIQUE) == 0)
            chartsEvalRiskChronique();
        if(questionnaire.compareTo(IDEVALRISKTMSLOMBALGIE) == 0)
            chartsEvalRiskLombalgie();
      /*  //initialisation histo
        Set<Map.Entry<String, Double>> setPourcentScore = pourcentagesScore.entrySet();
        Iterator<Map.Entry<String, Double>> it = setPourcentScore.iterator();
        while(it.hasNext()){
            Map.Entry<String, Double> e = it.next();
            int rnd = new Random().nextInt(tabColors.length);
            addHisto(Double.parseDouble(e.getKey()),e.getValue(),tabColors[rnd]);
        }*/

        //calculer les pourcentages des réponses aux questions
        /*for(Question questiion:questionnaire.getQuestions()) {
            Map<String,Integer> options = questiion.getOptionsQuestion();
            String[] pourecentagesReponse = {};
            String[] valeursOption ={};
            Set<Entry<String, Integer >> setOptions = options.entrySet();
            Iterator<Entry<String, Integer>> iter = setOptions.iterator();
            int i = 0;
            while(iter.hasNext()){
                Entry<String, Integer> e = iter.next();
                valeursOption[i] = e.getKey();
                pourecentagesReponse[i+1] = Integer.toString(ResultatService.getInstance().getPourcentageReponse(questionnaire,questiion,e.getKey()));
                i++;
            }
            pourecentagesReponse[0] = questiion.getIntituleQuestion();
            setTitleDataCSV(0,valeursOption);
            addDataCSV(0,2,pourecentagesReponse);
        }*/

        //Initialisation histo RPE
       /* Set<Entry<String, Double>> setPourcentRpe = pourcentageRpe.entrySet();
        Iterator<Entry<String, Double>> ite = setPourcentRpe.iterator();
        while(ite.hasNext()){
            Entry<String, Double> e = ite.next();
            int rnd = new Random().nextInt(tabColors.length);
            addRPE(e.getKey(),e.getValue(),tabColors[rnd]);
        }*/
        for(int i= 1;i<reponsesQuestion.size() && !reponsesQuestion.isEmpty();i++)
        {
            if(i<5)
                addAnswer(0,reponsesQuestion.get(i));
            else addAnswer(1,reponsesQuestion.get(i));
        }
        addRPE("6 Pas d\'effort du tout", 8.1, GREEN);
        addRPE("7 Extrêmement léger", 5.1, GREEN);
        addRPE("8", 0, GREEN);
        addRPE("9 Très léger", 6.8, GREEN);
        addRPE("10", 1, YELLOW);
        addRPE("11 Léger", 19.5, YELLOW);
        addRPE("12", 5.1, ORANGE);
        addRPE("13 Un peu dur", 24.3, ORANGE);
        addRPE("14", 5.6, ORANGE);
        addRPE("15 Dur", 16.2, ORANGE);
        addRPE("16", 1.8, RED);
        addRPE("17 Très dur", 4.1, RED);
        addRPE("18", 0.5, RED);
        addRPE("19 Extrêment dur", 0.5, RED);
        addRPE("20 Epuissant", 1.5, RED);
    }

}
