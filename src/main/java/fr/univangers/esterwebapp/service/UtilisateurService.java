package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.dao.mongodb.UtilisateurDAO;
import fr.univangers.esterwebapp.model.Utilisateur;
import fr.univangers.esterwebapp.operation.OperationUtilisateur;
import org.apache.log4j.Logger;

public abstract class UtilisateurService<T extends Utilisateur> extends ModelService<T> implements OperationUtilisateur<T> {

    /**
     * Log les traitements d'un utilisateur
     */
    private static Logger log = Logger.getLogger(UtilisateurService.class);

    protected UtilisateurDAO<T> utilisateurDAO;

    protected UtilisateurService(UtilisateurDAO<T> utilisateurDAO) {
        super(utilisateurDAO);
        this.utilisateurDAO = utilisateurDAO;
        log.info(this.utilisateurDAO);
    }

    @Override
    public synchronized T readByEmailOrId(String emailOrId) {
        return this.utilisateurDAO.readByEmailOrId(emailOrId);
    }
}