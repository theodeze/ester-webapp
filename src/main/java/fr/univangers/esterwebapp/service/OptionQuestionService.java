package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.dao.mongodb.OptionQuestionDAO;
import fr.univangers.esterwebapp.model.OptionQuestion;
import org.apache.log4j.Logger;

@Service
public class OptionQuestionService extends ModelService<OptionQuestion> {

    /**
     * Log les traitements du question service
     */
    private static Logger log = Logger.getLogger(OptionQuestionService.class);

    private static OptionQuestionService questionService = null;

    private OptionQuestionService() {
        super(OptionQuestionDAO.getInstance());
    }

    public static synchronized OptionQuestionService getInstance() {
        if(questionService == null)
            questionService = new OptionQuestionService();

        log.info("Get singleton QuestionService : " + questionService);
        return questionService;
    }
}