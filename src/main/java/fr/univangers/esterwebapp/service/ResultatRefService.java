package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.dao.mongodb.ResultatRefDAO;
import fr.univangers.esterwebapp.model.*;
import org.apache.log4j.Logger;
import java.util.Date;

import java.util.*;

@Service
public class ResultatRefService extends ModelService<ResultatRef> {

    /**
     * Log les traitements du résultat ref service
     */
    private static Logger log = Logger.getLogger(ResultatRefService.class);

    private static ResultatRefService resultatRefService = null;

    private ResultatRefService() {
        super(ResultatRefDAO.getInstance());
    }

    public static synchronized ResultatRefService getInstance() {
        if(resultatRefService == null)
            resultatRefService = new ResultatRefService();

        log.info("Get singleton ResultatRefService : " + resultatRefService);
        return resultatRefService;
    }

    public synchronized ResultatRef getOrNewResultatRefService(Questionnaire questionnaire, UtilisateurEster importeur, SalarieRef salarieRef) {
        return new ResultatRef(questionnaire.getIdentifiant(), new Date(), new Date(), 0, questionnaire, importeur, salarieRef);
    }

    public Map<String,Double> pourcentageScore (Questionnaire questionnaire){
        //  Map qui va contenir les pourcentages des scores
        Map<String,Double> pourcentagesScore = new HashMap<>();
        //initialisation des pourecentages à 0
        for (int i = 0;i<=15;i++)
            pourcentagesScore.put(String.valueOf(i),0.0);
        log.info(" Initialistation pourcentages score "+ pourcentagesScore);
        // Map qui va contenir le nombre de résultat avec un même score
        Map<String,Integer> nbScore = new HashMap<>();
        // initialisation à O
        for (int i = 0;i<=15;i++)
            nbScore.put(String.valueOf(i),0);
        Collection<ResultatRef> resultatRefs = questionnaire.getResultatsRef();
        log.info("resultatrefs "+resultatRefs);
        for(ResultatRef resultatRef: resultatRefs) {
            // Pour chaque résultat on met à jour la valeur dans le map
            int score = resultatRef.getScoreGlobal();
            int nb = nbScore.get(String.valueOf(score));
            nb++;
            nbScore.put(String.valueOf(score),nb);
        }
        log.info("scores tab "+nbScore);

        // On parcours le map qui contient le nombre de résultat avec un même score pour calculer les pourcentages

        Set<Map.Entry<String, Integer>> setNbScore = nbScore.entrySet();
        Iterator<Map.Entry<String, Integer>> it = setNbScore.iterator();
        while(it.hasNext()){
            Map.Entry<String, Integer> e = it.next();
            pourcentagesScore.put(e.getKey(),(resultatRefs.isEmpty())?0:(double) e.getValue()/resultatRefs.size());}
        log.info("Pourcentage Score"+pourcentagesScore);
        return pourcentagesScore;
    }

    public Map<String,Double> pourcentageRpe(Questionnaire questionnaire, Question question) {
        //  Map qui va contenir les pourcentages des réponses concernant l'intensité physique
        Map<String,Double> pourcentageRpe = new HashMap<>();
        // Map qui va contenir le nombre de réponse à la question  avec la même  valeur
        Map<String,Integer> nbRep = new HashMap<>();

        //initialisation des pourecentages à 0
        //initiatialisation à 0 du map nbRep
        // On parcours les options de la question
        Collection<OptionQuestion> options = question.getOptionsQuestion();
        for(OptionQuestion optionQuestion:options){
            pourcentageRpe.put(optionQuestion.getIdentifiant(),0.0);
            nbRep.put(optionQuestion.getIdentifiant(),0);
        }


        Collection<ResultatRef> resultatRefs = questionnaire.getResultatsRef();
        for(ResultatRef resultatRef: resultatRefs) {
            for(ReponseRef reponseRef:resultatRef.getReponseParQuestion()) {
                if(reponseRef.getQuestion().equals(question)) {
                    int nb = nbRep.get(reponseRef.getValeurReponse());
                    nb++;
                    nbRep.put(reponseRef.getValeurReponse(),nb);
                }
            }
        }

        // On parcours le map qui contient le nombre de réponse avec une même valeur pour calculer les pourcentages
        Set<Map.Entry<String, Integer>> setNbRep = nbRep.entrySet();
        Iterator<Map.Entry<String, Integer>> it = setNbRep.iterator();
        while(it.hasNext()){
            Map.Entry<String, Integer> e = it.next();
            pourcentageRpe.put(e.getKey(),(resultatRefs.isEmpty())?0:(double)e.getValue()/resultatRefs.size());
        }

        return pourcentageRpe;
    }
}
