package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.operation.OperationExport;
import fr.univangers.esterwebapp.operation.OperationModel;
import fr.univangers.esterwebapp.dao.mongodb.ModelDAO;

import java.util.Collection;
import java.util.UUID;

public abstract class ModelService<T extends Model> implements OperationModel<T>, OperationExport {

    protected ModelDAO<T> modelDAO;

    protected ModelService(ModelDAO<T> dao) {
        this.modelDAO = dao;
    }

    @Override
    public synchronized boolean create(T entity) {
        return this.modelDAO.create(entity);
    }

    public synchronized boolean merge(T entity) {
        return this.modelDAO.merge(entity);
    }

    @Override
    public synchronized Collection<T> read() {
        return this.modelDAO.read();
    }

    @Override
    public synchronized boolean exportJSON(String pathFileJSON) {
        return this.modelDAO.exportJSON(pathFileJSON);
    }

    @Override
    public synchronized boolean exportCSV(String pathFileCSV) {
        return this.modelDAO.exportCSV(pathFileCSV);
    }

    @Override
    public synchronized T readLast() {
        return this.modelDAO.readLast();
    }

    @Override
    public synchronized T readFirst() {
        return this.modelDAO.readFirst();
    }

    @Override
    public synchronized T readById(UUID uuid) {
        return this.modelDAO.readById(uuid);
    }

    @Override
    public synchronized T readById(String id) {
        return this.modelDAO.readById(id);
    }

    @Override
    public synchronized boolean exportJSONById(String id, String pathFileJSON) {
        return this.modelDAO.exportJSONById(id, pathFileJSON);
    }

    @Override
    public synchronized boolean exportCSVById(String id, String pathFileCSV) {
        return this.modelDAO.exportCSVById(id, pathFileCSV);
    }

    @Override
    public synchronized boolean save(T entity) {
        return this.modelDAO.save(entity);
    }

    @Override
    public synchronized boolean update(T entity) {
        return this.modelDAO.update(entity);
    }

    @Override
    public synchronized boolean delete(T entity) {
        return this.modelDAO.delete(entity);
    }

    @Override
    public synchronized boolean delete() {
        return this.modelDAO.delete();
    }

    public synchronized T readByName(String nom) {
        return this.modelDAO.readByName(nom);
    }

}