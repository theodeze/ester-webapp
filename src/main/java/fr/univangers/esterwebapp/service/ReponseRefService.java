package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.dao.mongodb.ReponseRefDAO;
import fr.univangers.esterwebapp.model.modelenum.proposition.Manquant;
import fr.univangers.esterwebapp.model.*;
import org.apache.log4j.Logger;

import java.util.Collection;

@Service
public class ReponseRefService extends ModelService<ReponseRef> {

    /**
     * Log les traitements du réponse service
     */
    private static Logger log = Logger.getLogger(ReponseRefService.class);

    private static ReponseRefService reponseRefService = null;

    private ReponseRefService() {
        super(ReponseRefDAO.getInstance());
    }

    public static synchronized ReponseRefService getInstance() {
        if(reponseRefService == null)
            reponseRefService = new ReponseRefService();

        log.info("Get singleton ReponseRefService : " + reponseRefService);
        return reponseRefService;
    }

    public synchronized ReponseRef getOrNewReponseRef(String codeReponse, ResultatRef resultat, Question question, SalarieRef repondant) {
        OptionQuestion optionQuestion = question.getOptionQuestion(codeReponse);
        if(optionQuestion == null) {
            //Si c'est une réponse manquante
            if(Manquant.getProposition(codeReponse) != null)
                return new ReponseRef(codeReponse, "", 0, repondant, resultat, question);
            else
                return null;
        } else {
            return new ReponseRef(codeReponse, optionQuestion.getValeur(), optionQuestion.getScore(), repondant, resultat, question);
        }
    }

    public Collection<ReponseRef> readByQuestion(String questionUUID) {
       return  ((ReponseRefDAO)modelDAO).readByQuestion(questionUUID);
    }
}
