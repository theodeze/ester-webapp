package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.model.Question;
import fr.univangers.esterwebapp.dao.mongodb.QuestionDAO;
import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.modelenum.question.QuestionEnum;
import org.apache.log4j.Logger;
import java.util.Arrays;
import java.util.Optional;

@Service
public class QuestionService extends ModelService<Question> {

    /**
     * Log les traitements du question service
     */
    private static Logger log = Logger.getLogger(QuestionService.class);

    private static QuestionService questionService = null;

    private QuestionService() {
        super(QuestionDAO.getInstance());
    }

    public static synchronized QuestionService getInstance() {
        if (questionService == null)
            questionService = new QuestionService();

        log.info("Get singleton QuestionService : " + questionService);
        return questionService;
    }

    /**
     * Récupère ou crée une question à partir d'un questionnaire
     *
     * @param codeQuestion
     * @param questionnaire
     * @return
     */
    public synchronized Question getOrNewQuestion(String codeQuestion, Questionnaire questionnaire) {
        //On vérifie si le questionnaire possède déjà la question
        Question question = questionnaire.getQuestion(codeQuestion);
        if (question != null) return question;

        //Sinon on crée la question à partir d'une énumération
        Optional<QuestionEnum> optionalQuestionEnum = Arrays.stream(QuestionEnum.values()).filter(q -> q.getCode().equals(codeQuestion)).findFirst();
        QuestionEnum questionEnum = null;
        if (optionalQuestionEnum.isPresent())
            questionEnum = optionalQuestionEnum.get();

        if (questionEnum == null) return null;

        question = new Question(questionEnum, questionnaire);
        //this.save(question);

        return question;
    }

    public Question readByIntitule(String intitule) {
        return QuestionDAO.getInstance().readByIntitule(intitule);
    }
}
