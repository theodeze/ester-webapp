package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.model.*;
import org.apache.log4j.Logger;
import java.util.List;

public class ImportQuestionThread implements Runnable {

    private static Logger log = Logger.getLogger(ImportQuestionThread.class);

    private List<List<String>> records;
    private List<String> codeEntetes;
    private UtilisateurEster importeur;
    private Questionnaire questionnaire;
    private Question question;
    private int i;

    public ImportQuestionThread(List<List<String>> records, List<String> codeEntetes, UtilisateurEster importeur, Questionnaire questionnaire, Question question, int i) {
        this.records = records;
        this.codeEntetes = codeEntetes;
        this.importeur = importeur;
        this.questionnaire = questionnaire;
        this.question = question;
        this.i = i;
    }

    @Override
    public void run() {
        //Parcours de toutes les réponses pour la question courante dans un thread
        for(int j=1; j<this.records.size(); j++) {
            //On récupère les codes réponses
            List<String> codeReponses = this.records.get(j);
            log.info("Code reponses : " + codeReponses);

            //On crée un salarié référence
            SalarieRef salarieRef = SalarieRefService.getInstance().getOrNewSalarieRef(this.codeEntetes, codeReponses, this.importeur, this.questionnaire);
            log.info("Salarié référence : " + salarieRef);

            //On crée un résultat référence pour cette question associé au salarié
            ResultatRef resultatRef = ResultatRefService.getInstance().getOrNewResultatRefService(this.questionnaire, this.importeur, salarieRef);
            log.info("Résultat référence : " + resultatRef);

            //On récupére la réponse de la question courante
            String codeReponse = codeReponses.get(this.i).trim();
            log.info("Code Reponse : " + codeReponse);

            //On crée un résultat référence
            ReponseRef reponse = ReponseRefService.getInstance().getOrNewReponseRef(codeReponse, resultatRef, this.question, salarieRef);
            log.info("Reponse : " + reponse);
        }
    }
}
