package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.util.SecurityUtil;
import fr.univangers.esterwebapp.dao.mongodb.ConnexionDAO;
import fr.univangers.esterwebapp.model.Connexion;
import fr.univangers.esterwebapp.operation.OperationConnexion;
import org.apache.log4j.Logger;

public abstract class ConnexionService<T extends Connexion> extends UtilisateurService<T> implements OperationConnexion<T> {

    /**
     * Log les traitements du service utilisateur ester
     */
    private static Logger log = Logger.getLogger(ConnexionService.class);

    protected ConnexionDAO<T> connexionDAO;

    protected ConnexionService(ConnexionDAO<T> connexionDAO) {
        super(connexionDAO);
        this.connexionDAO = connexionDAO;
    }

    @Override
    public synchronized T readByEmail(String email) {
        return this.connexionDAO.readByEmail(email);
    }

    @Override
    public synchronized T readByEmailOrId(String emailOrId) {
        return this.connexionDAO.readByEmailOrId(emailOrId);
    }

    /**
     * Mise à jour du mot de passe d'un utilisateur
     * @param emailOrID l'identifiant qui permet de retrouver l'utilisateur
     * @param newPassword le nouveau mot de passe à mettre à jour
     * @return vrai si mise à jour. Faux dans le cas contraire
     */
    public synchronized boolean updatePassword(String emailOrID, String newPassword) {
        T utilisateur = this.readByEmailOrId(emailOrID);
        if(utilisateur == null) return false;

        utilisateur.setPassword(SecurityUtil.chiffrerMotPasse(newPassword));
        return this.save(utilisateur);
    }

    /**
     * Récupére un utilisateur par id ou email si le mot de passe associé est correct
     * @param identifiantOrEmail l'id ou l'email qui permet de retrouver l'utilisateur
     * @param motPasse qui permet de verifier l'authentification de l'utilisateur
     * @return
     */
    public synchronized T connecter(String identifiantOrEmail, String motPasse) {
        T utilisateur = this.readByEmailOrId(identifiantOrEmail);
        log.info("Utilisateur : " + utilisateur);
        if (utilisateur == null) {
            return null;
        }
        log.info("Mot de passe <= 8 : " + motPasse.length());
        if(motPasse.length() <= 8) {
            // Renvoyer Pop-up Erreur de type MDP LENGTH
            return null;
        }
        log.info("Mot de passe " + motPasse + " bien formé : " + verifMdp(motPasse));
        if(!verifMdp(motPasse)) {
            // Renvoyer Pop-up Erreur de type MDP TYPE ERROR, LETTER OR DIGIT MISSING
            return null;
        }
        boolean check = SecurityUtil.verifierMotPasse(motPasse, utilisateur.getPassword());
        log.info("Mot de passe vérifié : " + check);

        return check ? utilisateur : null;
    }

    /**
     * Vérifier si le mot de passe est correctement formé et respecte les formats attendus (présence de chiffre et lettre)
     * @param motPasse à verifier
     * @return vrai si bien formé. Faux dans le cas contraire
     */
    public synchronized boolean verifMdp (String motPasse) {
        boolean verifLettre = false;
        boolean verifChiffre = false;

        for(Character current : motPasse.toCharArray()) {
            // Si le caractère courant est un chiffre, alors on toggle à true la vérification des chiffres
            if(Character.isDigit(current) && !verifChiffre) {
                verifChiffre = true;
            }


            // Si le caractère courant est une lettre, alors on toggle à true la vérification des lettres
            if(Character.isLetter(current) && !verifLettre) {
                verifLettre = true;
            }
        }

        log.info("Mot de passe contient des chiffres : " + verifChiffre);
        log.info("Mot de passe contient des lettres : " + verifLettre);

        // On retourne True si le mdp est valide, False sinon
        return verifChiffre && verifLettre;
    }

}