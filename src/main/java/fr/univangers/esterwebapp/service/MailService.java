package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.dao.mongodb.MailDAO;
import fr.univangers.esterwebapp.model.Mail;
import fr.univangers.esterwebapp.model.ServeurMail;
import fr.univangers.esterwebapp.servlet.ResetPasswordServlet;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Collections;

@Service
public class MailService extends ModelService<Mail> {

    /**
     * Log les traitements du mail service
     */
    private static Logger log = Logger.getLogger(MailService.class);

    private static MailService mailService = null;

    private static final String SUBJECT_REINIT = "ESTER - Réinitialiser le mot de passe";
    private static final String SUBJECT_CREAT = "ESTER - Creation de compte";
    private static final String BODY = "";
    private static final String TYPE_BODY = "text/html; charset=\"UTF-8\";";
    private static final int EXPIRE_HOUR = 1;

    private MailService() {
        super(MailDAO.getInstance());
    }

    public static synchronized MailService getInstance() {
        if(mailService == null)
            mailService = new MailService();

        log.info("Get singleton MailService : " + mailService);
        return mailService;
    }

    public synchronized boolean sendForgotPassword(String email, String baseURL) {
        Collection<ServeurMail> serveurMails = ServeurMailService.getInstance().read();
        if(serveurMails.isEmpty()) return false;

        ServeurMail serveurMail = serveurMails.toArray(new ServeurMail[1])[0];
        log.info("Serveur mail : " + serveurMail);

        Mail mail = new Mail(SUBJECT_REINIT, Collections.singletonList(email), TYPE_BODY, BODY, EXPIRE_HOUR, serveurMail);
        log.info("Mail : " + mail);

        String body = this.getBodyForgotPassword(mail, baseURL);
        mail.setBody(body);
        log.info("Body : " + body);

        if(mail.send()) {
            return this.save(mail);
        } else {
            return false;
        }
    }

    public synchronized boolean sendCreationCompte(String email, String baseURL, String compteCree) {
        Collection<ServeurMail> serveurMails = ServeurMailService.getInstance().read();
        if(serveurMails.isEmpty()) return false;

        ServeurMail serveurMail = serveurMails.toArray(new ServeurMail[1])[0];
        log.info("Serveur mail : " + serveurMail);

        Mail mail = new Mail(SUBJECT_CREAT, Collections.singletonList(email), TYPE_BODY, BODY, EXPIRE_HOUR, serveurMail);
        log.info("Mail : " + mail);

        String body = this.getBodyCreationCompte(mail, baseURL, compteCree);
        mail.setBody(body);
        log.info("Body : " + body);

        if(mail.send()) {
            return this.save(mail);
        } else {
            return false;
        }
    }

    private synchronized String getBodyForgotPassword(Mail mail, String baseURL) {
        String token = mail.getUuid().toString();
        String url = String.format("%s%s?%s=%s", baseURL, ResetPasswordServlet.URL_RESET_MOTPASSE, ResetPasswordServlet.TOKEN_INPUT, token);
        int expireHour = mail.getExpireHour();

        String message = "";
        message += "<!DOCTYPE html>";
        message += "<html lang=\"fr-FR\">";
        message += "<head>";
        message += "<meta charset=\"utf-8\">";
        message += "</head>";
        message += "<body>";
        message += "<p>Bonjour,</p> <br/>";
        message += "<p>Vous avez demandé à réinitialiser votre mot de passe.</p>";
        message += "<p>Veuillez cliquer sur le lien ci-dessous pour poursuivre la démarche :</p> <br/>";
        message += "<p><a href=\""+url+"\">Réinitialiser le mot de passe</a> </p> <br/>";
        message += "<p>Ce lien est valable pendant " + expireHour + " heure(s), après quoi il sera nécessaire d'effectuer une nouvelle demande de réinitialisation de mot de passe.</p>";
        message += "<p>Si vous n'avez pas demandé la réinitialisation de votre mot de passe, veuillez ignorer cet email.</p> <br/>";
        message += "<p>Bien cordialement,</p>";
        message += "<p>L'équipe ESTER</p> <br/>";
        message += "<i>Ce courriel a été envoyé automatiquement, merci de ne pas y répondre.</i>";
        message += "</body>";
        message += "</html>";

        return message;
    }

    private synchronized String getBodyCreationCompte(Mail mail, String baseURL, String compteCree) {
        String token = mail.getUuid().toString();
        String url = baseURL + "?mail=" + token + "&role=" + compteCree;
        int expireHour = mail.getExpireHour();

        String message = "";
        message += "<!DOCTYPE html>";
        message += "<html lang=\"fr-FR\">";
        message += "<head>";
        message += "<meta charset=\"utf-8\">";
        message += "</head>";
        message += "<body>";
        message += "<p>Bonjour,</p> <br/>";
        message += "<p>Nous avons le plaisir de vous informer que votre compte " + compteCree + " est dorénavant disponible.</p>";
        message += "<p>Afin de valider votre inscription veuillez cliquer sur le lien ci-dessous :</p> <br/>";
        message += "<p><a href=\""+ url +"\">Création du compte</a> </p> <br/>";
        message += "<p>Ce lien est valable pendant " + expireHour + " heure(s), après quoi il sera nécessaire de renouvelle la démarche.</p>";
        message += "<p>Si vous n'avez pas demandé la création de votre compte ou que vous n'êtes pas le destinataire de ce message, veuillez ignorer cet email.</p> <br/>";
        message += "<p>Bien cordialement,</p>";
        message += "<p>L'équipe ESTER</p> <br/>";
        message += "<i>Ce courriel a été envoyé automatiquement, merci de ne pas y répondre.</i>";
        message += "</body>";
        message += "</html>";

        return message;
    }
}