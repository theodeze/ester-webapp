package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.Resultat;
import fr.univangers.esterwebapp.dao.mongodb.ResultatDAO;
import fr.univangers.esterwebapp.model.Salarie;
import org.apache.log4j.Logger;

@Service
public class ResultatService extends ModelService<Resultat> {

    /**
     * Log les traitements du résultat service
     */
    private static Logger log = Logger.getLogger(ResultatService.class);

    private static ResultatService resultatService = null;

    private ResultatService() {
        super(ResultatDAO.getInstance());
    }

    public static synchronized ResultatService getInstance() {
        if(resultatService == null)
            resultatService = new ResultatService();

        log.info("Get singleton ResultatService : " + resultatService);
        return resultatService;
    }

    public synchronized Resultat resultatSalarieQuestionnaire(Questionnaire questionnaire, Salarie personneRepondant) {
        return ((ResultatDAO)modelDAO).readByQuestionnaireReponduSalarieRepondant(questionnaire,personneRepondant);
    }

/*    public synchronized int getPourcentageReponse(String identifiantQuestionnaire,String identifiantQuestion, String valeurReponse) {
        int nombreTotal = 0;
        int nombreReponse = 0;
        Collection<Resultat> resultats = questionnaire.getResultats();
        Collection<Reponse> reponses;
        for(Resultat resultat:resultats) {
            reponses = resultat.getReponseParQuestion();
            for(Reponse reponse:reponses) {
                if(reponse.getQuestion() == question) {
                    if(reponse.getValeurReponse().equals(valeurReponse)) {
                        nombreReponse++;
                    }
                    nombreTotal++;
                }
            }
        }
        return nombreTotal == 0 ? 0 : (int)(nombreReponse / nombreTotal * 100);
    }*/
}