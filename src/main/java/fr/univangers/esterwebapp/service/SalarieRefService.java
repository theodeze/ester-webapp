package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.SalarieRef;
import fr.univangers.esterwebapp.dao.mongodb.SalarieRefDAO;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.model.modelenum.proposition.Departement;
import fr.univangers.esterwebapp.model.modelenum.proposition.NAF;
import fr.univangers.esterwebapp.model.modelenum.proposition.PCS;
import fr.univangers.esterwebapp.model.modelenum.proposition.Region;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Age;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.SalarieEnum;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Sexe;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SalarieRefService extends UtilisateurService<SalarieRef> {

    /**
     * Log les traitements du salarie ref service
     */
    private static Logger log = Logger.getLogger(SalarieRefService.class);

    private static SalarieRefService salarieRefService = null;

    private List<SalarieRef> salarieRefs;

    private SalarieRefService() {
        super(SalarieRefDAO.getInstance());
        this.salarieRefs = new ArrayList<>();
    }

    public static synchronized SalarieRefService getInstance() {
        if( salarieRefService == null)
            salarieRefService = new SalarieRefService();

        log.info("Get singleton SalarieRefService : " + salarieRefService);
        return salarieRefService;
    }

    public synchronized SalarieRef getOrNewSalarieRef(List<String> entetes, List<String> data, UtilisateurEster createur, Questionnaire questionnaire) {
        SalarieRef salarieRef = new SalarieRef();

        for(int i = 0; i < entetes.size(); i++) {
            //Si l'entete appartient au salarié
            String codeEntete = entetes.get(i);
            log.info("Code entete : " + codeEntete);

            //On vérifie si l'entete appartient aux données du salarié
            SalarieEnum salarieEnum = SalarieEnum.getProposition(codeEntete);
            log.info("Salarié enum : " + salarieEnum);

            //Si c'est une donnée qui n'appartient pas au salarié au passe au suivant
            if(salarieEnum == null) continue;

            String codeData = data.get(i).trim();
            log.info("Code data : " + codeData);

            //On modifie la donnée dans le salarié référence
            //Attention il faut mettre le switch dans une fonction pour éviter
            //de casser la boucle
            this.setDataSalarieRef(salarieRef, salarieEnum, codeData);
        }

        //On regarde si le salarié est déjà crée
        salarieRef = this.checkSalarieRefExist(salarieRef);

        //On lui associe le questionnaire répondu
        if(!salarieRef.getQuestionnairesRepondus().contains(questionnaire)) {
            salarieRef.getQuestionnairesRepondus().add(questionnaire);
        }
        if(!questionnaire.getSalariesRefRepondus().contains(salarieRef)) {
            questionnaire.getSalariesRefRepondus().add(salarieRef);
        }

        if(!this.salarieRefs.contains(salarieRef)) {
            this.salarieRefs.add(salarieRef);
        }

        log.info("Salarié réf : " + salarieRef);

        return salarieRef;
    }

    private synchronized void setDataSalarieRef(SalarieRef salarieRef, SalarieEnum salarieEnum, String codeData) {
        if(salarieEnum == null) return;
        if(salarieRef == null) return;

        switch(salarieEnum) {
            case SEXE:
                salarieRef.setSexe(Sexe.getProposition(codeData));
                break;
            case AGE:
                Age age = Age.getProposition(codeData);
                salarieRef.setAge(age);
               /* if(age != null) {
                    salarieRef.setNaisInf(age.getMinAge());
                    salarieRef.setNaisSup(age.getMaxAge());
                } else {
                    salarieRef.setNaisInf(0);
                    salarieRef.setNaisSup(0);
                }*/
                break;

            case DEPARTEMENT_ACTIVITE:
                salarieRef.getTravail().setDepartementActivite(Departement.getProposition(codeData));
                break;
            case REGION_ACTIVITE:
                salarieRef.getTravail().setRegionActivite(Region.getProposition(codeData));
                break;

            case CODE_NAF:
                salarieRef.getTravail().setCodeNAF(NAF.getProposition(codeData));
                break;

            case CODE_PCS:
                salarieRef.getTravail().setCodePCS(PCS.getProposition(codeData));
                break;
            case IDENTIFIANT:
                salarieRef.setIdentifiant(codeData);
                break;
            default:
        }
    }

    private synchronized SalarieRef checkSalarieRefExist(SalarieRef salarieRef) {
        Optional<SalarieRef> optionalSalarieRef = this.salarieRefs.stream().filter(s -> s.equals(salarieRef)).findFirst();
        if(optionalSalarieRef.isPresent())
            return optionalSalarieRef.get();
        else return salarieRef;
    }
}