package fr.univangers.esterwebapp.service;

import org.apache.log4j.Logger;
import org.passay.CharacterData;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

import static org.passay.IllegalRegexRule.ERROR_CODE;

public class PasswordGeneratorService {

    /**
     * Log les traitements du générateur de mot de passe
     */
    private static Logger log = Logger.getLogger(PasswordGeneratorService.class);

    private static PasswordGeneratorService passwordGeneratorService = null;

    private PasswordGeneratorService() { }

    public static synchronized PasswordGeneratorService getInstance() {
        if(passwordGeneratorService == null)
            passwordGeneratorService = new PasswordGeneratorService();

        log.info("Get singleton PasswordGeneratorService : " + passwordGeneratorService);
        return passwordGeneratorService;
    }

    public synchronized String generatePassword() {
        Random random = null;
        try {
            random = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            return "";
        }

        PasswordGenerator gen = new PasswordGenerator();
        CharacterData caracteresMinuscule = EnglishCharacterData.LowerCase;
        CharacterRule regleMinuscule = new CharacterRule(caracteresMinuscule);
        regleMinuscule.setNumberOfCharacters(1);

        CharacterData caracteresMajuscule = EnglishCharacterData.UpperCase;
        CharacterRule regleMajuscule = new CharacterRule(caracteresMajuscule);
        regleMajuscule.setNumberOfCharacters(1);

        CharacterData chiffres = EnglishCharacterData.Digit;
        CharacterRule regleChiffre = new CharacterRule(chiffres);
        regleChiffre.setNumberOfCharacters(1);

        CharacterData caracteresSpeciaux = new CharacterData() {
            public String getErrorCode() {
                return ERROR_CODE;
            }

            public String getCharacters() {
                return "^$*.[]{}()?-\"!@#%&/\\,><:;|_~";
            }
        };
        CharacterRule regleCaracSpeciaux = new CharacterRule(caracteresSpeciaux);
        regleCaracSpeciaux.setNumberOfCharacters(1);

        String password = gen.generatePassword(8 +random.nextInt(20-8),regleCaracSpeciaux, regleMinuscule,
                regleMajuscule, regleChiffre);
        log.info("Password : " + password);

        return shuffle(password.toCharArray());
    }

    private static String shuffle(char[] characters) {
        for (int i = 0; i < characters.length; i++) {
            int randomIndex = (int)(Math.random() * characters.length);
            char temp = characters[i];
            characters[i] = characters[randomIndex];
            characters[randomIndex] = temp;
        }
        return new String(characters);
    }
}