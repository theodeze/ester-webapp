package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.dao.mongodb.SalarieDAO;
import fr.univangers.esterwebapp.model.Salarie;
import org.apache.log4j.Logger;

@Service
public class SalarieService extends UtilisateurService<Salarie> {

    /**
     * Log les traitements du salarie service
     */
    private static Logger log = Logger.getLogger(SalarieService.class);

    private static SalarieService salarieService = null;

    private SalarieService() {
        super(SalarieDAO.getInstance());
    }

    public static synchronized SalarieService getInstance() {
        if( salarieService == null)
            salarieService = new SalarieService();

        log.info("Get singleton SalarieService : " + salarieService);
        return salarieService;
    }
}