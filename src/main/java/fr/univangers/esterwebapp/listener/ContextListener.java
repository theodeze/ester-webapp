package fr.univangers.esterwebapp.listener;

import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.service.ServeurMailService;
import fr.univangers.esterwebapp.util.HibernateUtil;
import org.apache.log4j.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.nio.charset.StandardCharsets;

/**
 * Listener qui permet
 * - D'initialiser l'encodage des caracteres en UTF8
 * - Paramétrer la session à 24h d'utilisation. Au delà suppression
 * - Enregistrer le serveur de mail si non fait
 * - D'initialiser la base de données prod
 * https://www.deadcoderising.com/execute-code-on-webapp-startup-and-shutdown-using-servletcontextlistener/
 */
@WebListener
public class ContextListener implements ServletContextListener {

    private static final Logger log = Logger.getLogger(ContextListener.class);

    /**
     * 24h en minutes = 1440
     */
    private static final int VINGT_QUATRE_HEURES_IN_MINUTES = 1440;

    private static ServletContext servletContext = null;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        servletContext = sce.getServletContext();
        ConverterEnum.FILE_PATH = servletContext.getRealPath("/public");
        log.info("Path : " + ConverterEnum.FILE_PATH);

        log.info("Encodage des caractères en utf8 pour les réponses et les requetes");
        servletContext.setRequestCharacterEncoding(StandardCharsets.UTF_8.name());
        servletContext.setResponseCharacterEncoding(StandardCharsets.UTF_8.name());

        log.info("Session limité à 24h");
        servletContext.setSessionTimeout(VINGT_QUATRE_HEURES_IN_MINUTES);

        log.info("Connection avec la base de données prod");
        HibernateUtil.openProd();

        log.info("Enregistrement du serveur de mail");
        ServeurMailService.getInstance().getOrNewServeurMailESTER();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("Déconnection avec la base de données");
        HibernateUtil.close();
    }

    /**
     * Getter servletContext
     *
     * @return servletContext
     */
    public static ServletContext getServletContext() {
        return servletContext;
    }
}