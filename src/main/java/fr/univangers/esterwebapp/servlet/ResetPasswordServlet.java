package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.model.Mail;
import fr.univangers.esterwebapp.service.MailService;
import fr.univangers.esterwebapp.util.Util;
import fr.univangers.esterwebapp.model.Utilisateur;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@WebServlet(name = "ResetPasswordServlet", urlPatterns = {ResetPasswordServlet.URL_RESET_MOTPASSE})
public class ResetPasswordServlet extends PasswordServlet {

    private static Logger log = Logger.getLogger(ResetPasswordServlet.class);

    public static final String URL_RESET_MOTPASSE = "/reset_password";
    public static final String VUE_RESET_MOTPASSE = "/jsp/reset_password.jsp";

    public static final String TOKEN_INPUT = "InputToken";
    public static final String TOKEN_VALUE = "ValueToken";
    public static final String NEW_MOTPASSE_INPUT = "InputNewPassword";
    public static final String CONFIRM_MOTPASSE_INPUT = "InputConfirmPassword";
    public static final String NEW_MOTPASSE_VALUE = "ValueNewPassword";
    public static final String CONFIRM_MOTPASSE_VALUE = "ValueConfirmPassword";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.info("Do post ResetPasswordServlet");

        String vue = VUE_RESET_MOTPASSE;

        String token = request.getParameter(TOKEN_INPUT);
        request.setAttribute(TOKEN_VALUE, token);
        log.info("Token : " + token);

        //Si le token existe
        if(!Util.isNullOrEmpty(token)) {

            UUID uuid = null;
            try {
                uuid = UUID.fromString(token);
                log.info("UUID : " + uuid);
            }
            catch (Exception e) {
              log.error(e.getMessage());
              uuid = null;
            }

            //Si le token est bien un uuid
            if(uuid != null) {

                Mail mail = MailService.getInstance().readById(uuid);
                log.info("Mail : " + mail);

                //Si il y a bien une demande de réinitialisation
                if(mail != null) {

                    String newPassword = request.getParameter(NEW_MOTPASSE_INPUT);
                    request.setAttribute(NEW_MOTPASSE_VALUE, newPassword);
                    log.info("Nouveau password : " + newPassword);

                    String confirmPassword = request.getParameter(CONFIRM_MOTPASSE_INPUT);
                    request.setAttribute(CONFIRM_MOTPASSE_VALUE, confirmPassword);
                    log.info("Confirmation password : " + confirmPassword);

                    //Les mots de passe sont bien égaux
                    if(newPassword.equals(confirmPassword)) {

                        Utilisateur user = this.connecter(mail.getIdentifiant(), confirmPassword);
                        log.info("User retrouvé : " + user);

                        //Ce mot de passe n'est pas déjà associé à l'utilisateur
                        if(user == null) {

                            boolean update = this.updatePassword(mail.getIdentifiant(), newPassword);
                            log.info("Mise à jour du mot de passe " + update);

                            //La mise à jour est bel et bien enregistré
                            if(update) {
                                String message = "Mot de passe modifié";
                                Servlet.ATT_MSG_SUCCESS.write(message, request);
                                MailService.getInstance().delete(mail);
                                //On redirige l'utilisateur vers la page de connexion
                                request.removeAttribute(TOKEN_VALUE);
                                request.removeAttribute(TOKEN_INPUT);
                                request.getSession().invalidate();
                                vue = "index.jsp";
                            }
                            else {
                                String message = "Problème lors de la mise à jour du mot de passe. Veuillez réessayer plus tard.";
                                Servlet.ATT_MSG_ERROR.write(message, request);
                            }
                        }
                        else {
                            String message = "Ce mot passe est déjà associé. Veuillez saisir un nouveau mot de passe.";
                            Servlet.ATT_MSG_WARNING.write(message, request);
                        }
                    }
                    else {
                        String message = "Les mots de passe ne sont pas identiques.";
                        Servlet.ATT_MSG_WARNING.write(message, request);
                    }
                }
                else {
                    String message = "Un problème est survenu. Veuillez réessayer plus tard.";
                    Servlet.ATT_MSG_ERROR.write(message, request);
                }
            }
        }

        Servlet.dispatch(request, response, vue);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get ResetPasswordServlet");
        Servlet.dispatch(request, response, VUE_RESET_MOTPASSE);
    }
}