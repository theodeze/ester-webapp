package fr.univangers.esterwebapp.servlet.Questionnaires;

import fr.univangers.esterwebapp.service.SalarieService;
import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.Salarie;
import fr.univangers.esterwebapp.service.QuestionnaireService;
import fr.univangers.esterwebapp.servlet.Servlet;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

public class GiveQuestionnaireToSalarieServlet extends HttpServlet {

    /**
     * Logger pour tracer les traitements de la servlet
     */
    private static Logger log = Logger.getLogger(GiveQuestionnaireToSalarieServlet.class);

    public static final String URL_GIVE_QUESTIONNAIRE_TO_SALARIE = "giveQuestionnaireToSalarie";
    public static final String VUE_GIVE_QUESTIONNAIRE_TO_SALARIE = "/jsp/Questionnaries/giveQuestionnaireToSalarie.jsp";

    public static final String SALARIES = "salaries";
    public static final String SALARIE_SELECT = "SalarieSelect";

    public static final String QUESTIONNAIRES = "questionnaires";
    public static final String QUESTIONNAIRE_SELECT = "QuestionnaireSelect";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        log.info("Do post GiveQuestionnaireToSalarieServlet");

        String idSalarie = request.getParameter(SALARIE_SELECT);
        log.info("ID salarié : " + idSalarie);

        if(!Util.isNullOrEmpty(idSalarie)) {

            String idQuestionnaire = request.getParameter(QUESTIONNAIRE_SELECT);
            log.info("ID questionnaire : " + idQuestionnaire);

            if(!Util.isNullOrEmpty(idQuestionnaire)) {

                Salarie salarie = SalarieService.getInstance().readById(idSalarie);
                log.info("Salarié select : " + salarie);
                Questionnaire questionnaire = QuestionnaireService.getInstance().readById(idQuestionnaire);
                log.info("Questionnaire select : " + questionnaire);

                if(salarie != null && questionnaire != null) {
                    salarie.getQuestionnairesNonRepondus().add(questionnaire);
                    questionnaire.getSalariesNonRepondus().add(salarie);

                    boolean saveQuestionnaire = QuestionnaireService.getInstance().save(questionnaire);
                    log.info("Save questionnaire : " + saveQuestionnaire);
                    boolean saveSalarie = SalarieService.getInstance().save(salarie);
                    log.info("Save salarié : " + saveSalarie);

                    if(saveQuestionnaire && saveSalarie) {
                        Servlet.ATT_MSG_SUCCESS.write("Questionnaire attribué avec succès", request);
                    } else {
                        Servlet.ATT_MSG_ERROR.write("Problème lors de l'attribution du questionnaire", request);

                    }
                    request.removeAttribute(QUESTIONNAIRES);
                    request.removeAttribute(SALARIE_SELECT);
                    request.removeAttribute(QUESTIONNAIRE_SELECT);
                    Collection<Salarie> salaries = SalarieService.getInstance().read();
                    log.info("List salariés : " + salaries);
                    request.setAttribute(SALARIES, salaries);
                }
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get GiveQuestionnaireToSalarieServlet");

        //On récupère les salariés
        Collection<Salarie> salaries = SalarieService.getInstance().read();
        log.info("List salariés : " + salaries);
        request.setAttribute(SALARIES, salaries);

        //Si le salarié est sélectionné alors on supprime les questionnaires
        //que le salarié possède déjà pour eviter les doublons
        String idSalarie = request.getParameter(SALARIE_SELECT);
        log.info("ID salarié : " + idSalarie);
        if(!Util.isNullOrEmpty(idSalarie)) {

            //On recupère les questionnaires
            Collection<Questionnaire> questionnaires = QuestionnaireService.getInstance().read();
            log.info("List questionnaires : " + questionnaires);

            Salarie salarie = SalarieService.getInstance().readById(idSalarie);
            log.info("Salarié select : " + salarie);
            if(salarie != null) {
                questionnaires.removeIf(q -> q.getSalariesNonRepondus().contains(salarie));
                questionnaires.removeIf(q -> q.getSalariesRepondus().contains(salarie));
            }

            if(!questionnaires.isEmpty()){
                request.setAttribute(QUESTIONNAIRES, questionnaires);
            }
            else {
                Servlet.ATT_MSG_WARNING.write("Plus aucun questionnaire à attribuer pour ce salarié", request);
            }
        }
    }
}