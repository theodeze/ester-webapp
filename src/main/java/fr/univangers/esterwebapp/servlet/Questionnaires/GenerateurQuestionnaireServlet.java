package fr.univangers.esterwebapp.servlet.Questionnaires;

import fr.univangers.esterwebapp.formulaire.Questionnaire.GenerateQuestionnaireForm;
import fr.univangers.esterwebapp.formulaire.Questionnaire.QuestionnaireJSON;
import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.service.QuestionnaireService;
import fr.univangers.esterwebapp.service.UtilisateurEsterService;
import fr.univangers.esterwebapp.servlet.Servlet;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.*;

@WebServlet(name = "GenerateurQuestionnaireServlet", urlPatterns = {"/"+GenerateurQuestionnaireServlet.URL_GENQUESTIONNAIRE})
public class GenerateurQuestionnaireServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(GenerateurQuestionnaireServlet.class);

    public static final String URL_GENQUESTIONNAIRE = "genererQuestionnaire";
    public static final String VUE_GENQUESTIONNAIRE = "/jsp/Questionnaries/questionnaryForm.jsp";
    public static final String EVAL_RISK_TMS_LOMBALGIE = "Eval-Risk-TMS : Risque de lombalgie chronique";
    public static final String EVAL_RISK_TMS = "Eval-Risk-TMS : Risque de SMS-MS chronique";

    private static GenerateQuestionnaireForm form  = new GenerateQuestionnaireForm();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.info("Do post Generateur Questionnaire");

        try {

            String nameQuestionToDelete = request.getParameterValues("nameToDelete") != null ? request.getParameterValues("nameToDelete")[0] : "";
            log.info("Nom à supprimer : "+nameQuestionToDelete);

            if(!nameQuestionToDelete.isEmpty()&&!nameQuestionToDelete.equals(EVAL_RISK_TMS_LOMBALGIE)&&!nameQuestionToDelete.equals(EVAL_RISK_TMS)) {
                form.deleteQuestionnary(request, response, nameQuestionToDelete);
            }

            String myQuestionnary = request.getParameterValues("json")!=null? request.getParameterValues("json")[0]:"";
            log.info("Liste questionnaires json : " + myQuestionnary);
            if(myQuestionnary != null) {
                JSONObject questionnaireIntermediaire = new JSONObject(myQuestionnary);
                String nomQuestionnaire = questionnaireIntermediaire.getString("nom");

                String createur = null;
                Date dateCreation = null;
                UtilisateurEster user = Servlet.readUserInSession(request);
                boolean access = false;

                if(QuestionnaireService.getInstance().readByName(nomQuestionnaire) != null) {
                    if (user.getRole()==Role.ADMIN || user.getQuestionnairesAcces().contains(QuestionnaireService.getInstance().readByName(nomQuestionnaire)) || user.getQuestionnairesCrees().contains(QuestionnaireService.getInstance().readByName(nomQuestionnaire))) {
                        access = true;
                    }
                }
                else {
                    access = true;
                }

                if(access) {
                    if (!myQuestionnary.isEmpty() && !nomQuestionnaire.equals(EVAL_RISK_TMS_LOMBALGIE) && !nomQuestionnaire.equals(EVAL_RISK_TMS)) {
                      Questionnaire questionnaireToDelete = QuestionnaireService.getInstance().readByName(nomQuestionnaire);
                      if (questionnaireToDelete != null) {
                          createur = questionnaireToDelete.getCreateurQuestion().getIdentifiant();
                          dateCreation = questionnaireToDelete.getDateSoumission();
                          QuestionnaireService.getInstance().delete(questionnaireToDelete);
                          updateUser(request);
                      }
                      form.createOrUpdateQuestionnary(request, response, myQuestionnary, createur, dateCreation);
                  }
              }
              else{
                    String errorAccess ="Ce questionnaire existe déjà en base et vous n'êtes pas habilité à le modifier";
                    Servlet.ATT_MSG_ERROR.write(errorAccess, request);
                    response.setStatus(400);
                    response.setContentType("text/plaintext");
                    response.getWriter().write(errorAccess);
              }
            }

            updateUser(request);
        } catch (Exception e) {
            log.info(e.getMessage(), e);
            Servlet.ATT_MSG_ERROR.write("Problème de lien avec la Collection Questionnaire", request);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        log.info("Do get Generateur Questionnaire");
        UtilisateurEster membreESTER = Servlet.readUserInSession(request);

        String whatIsNeeded = request.getParameterValues("needed") != null ? request.getParameterValues("needed")[0]:"" ;
        log.info("Needed : "+whatIsNeeded);

        if (whatIsNeeded.equals("AllOfOne")) {

        String nameQuestionToGET = request.getParameterValues("text")[0];
        log.info("Name Got "+nameQuestionToGET);


            Questionnaire questionnaireToSet = QuestionnaireService.getInstance().readByName(nameQuestionToGET);
            log.info("Questionnaire To Set " + questionnaireToSet);

            if (questionnaireToSet != null) {
                QuestionnaireJSON questionnaireJSON = new QuestionnaireJSON(questionnaireToSet.getNom(), questionnaireToSet.getVersion(), questionnaireToSet.getCreateurQuestion().getEmail(),
                        questionnaireToSet.getDateDerniereModif(), questionnaireToSet.getDateSoumission(), questionnaireToSet.getDernierModificateurQuestion().getEmail());

                questionnaireJSON.initQuestionnaireJSON(questionnaireToSet, membreESTER);

                JSONObject jsonQuestionnaire = new JSONObject(questionnaireJSON);
                log.info("json Questionnaire : " + jsonQuestionnaire);
                String saveString = jsonQuestionnaire.toString();
                response.setContentType("text/json");
                response.getWriter().write(saveString);
                response.setStatus(200);
            } else {
                response.setStatus(400);
            }
        }

        else if (whatIsNeeded.equals("AllOfName")) {
            List<String> listNameQuestionnaire = new ArrayList<>();
            List<Questionnaire> listQuestionnaire = new ArrayList<>(QuestionnaireService.getInstance().read());
            log.info("List of Questionnaries : " + listQuestionnaire);

            for (int indexQuestionnaire = listQuestionnaire.size()-1; indexQuestionnaire != -1; --indexQuestionnaire) {
                String questionnaireString = listQuestionnaire.get(indexQuestionnaire).getNom();
                List<UtilisateurEster> listDroits = new ArrayList<>(listQuestionnaire.get(indexQuestionnaire).getModificateursQuestion());
                for(UtilisateurEster utilisateurEster : listDroits)
                {
                    if(membreESTER.getRole()== Role.ADMIN || membreESTER.getEmail().equals(utilisateurEster.getEmail())) {
                        listNameQuestionnaire.add(questionnaireString);
                        break;
                    }
                }
            }
            if(!listNameQuestionnaire.isEmpty()) {
                JSONArray jsonListNameQuestionnaire = new JSONArray(listNameQuestionnaire);
                log.info("json nameList : " + jsonListNameQuestionnaire);

                    String saveString = jsonListNameQuestionnaire.toString();
                    response.setContentType("text/json");
                    response.getWriter().write(saveString);
                    response.setStatus(200);
                }
            else {
                    response.setStatus(400);
            }
        }
        updateUser(request);
    }

    private void updateUser(HttpServletRequest request) {
        UtilisateurEster membreESTER = Servlet.readUserInSession(request);
        membreESTER = UtilisateurEsterService.getInstance().readById(membreESTER.getUuid());
        Servlet.writeUserInSession(membreESTER,false, request);
    }
}
