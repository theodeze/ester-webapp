package fr.univangers.esterwebapp.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "ResultatServlet", urlPatterns = {MentionsLegalesServlet.URL_RESULTAT})
public class MentionsLegalesServlet extends HttpServlet {

    public static final String URL_RESULTAT = "/mentionsLegales";
    public static final String VUE_RESULTAT = "/jsp/MentionsLegales.jsp";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        this.doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        Servlet.dispatch(request, response, VUE_RESULTAT);
    }
}
