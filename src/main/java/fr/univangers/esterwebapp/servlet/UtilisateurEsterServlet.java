package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.servlet.CreateAccounts.CreateAccountEntrepriseServlet;
import fr.univangers.esterwebapp.servlet.CreateAccounts.CreateAccountSalarieServlet;
import fr.univangers.esterwebapp.servlet.CreateAccounts.CreateAccountUserServlet;
import fr.univangers.esterwebapp.servlet.Questionnaires.GiveQuestionnaireToSalarieServlet;
import fr.univangers.esterwebapp.servlet.reference.ImportExportDataReferenceServlet;
import fr.univangers.esterwebapp.servlet.Questionnaires.AfficheQuestionnaireESTER;
import fr.univangers.esterwebapp.servlet.Questionnaires.GenerateurQuestionnaireServlet;
import org.apache.log4j.Logger;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "UtilisateurEsterServlet", urlPatterns = {UtilisateurEsterServlet.URL_UTILISATEUR})
@MultipartConfig
public class UtilisateurEsterServlet extends UtilisateurServlet {

    private static Logger log = Logger.getLogger(UtilisateurEsterServlet.class);

    public static final String URL_UTILISATEUR = "/utilisateur";
    public static final String VUE_UTILISATEUR = "/jsp/utilisateur.jsp";

    public UtilisateurEsterServlet() {
        super();
        //Créer un compte salarié
        this.servlets.put(CreateAccountSalarieServlet.URL_CREATE_ACCOUNT_SALARIE, new CreateAccountSalarieServlet());
        //Créer un compte utilisateur
        this.servlets.put(CreateAccountUserServlet.URL_CREATE_ACCOUNT_USER, new CreateAccountUserServlet());
        //Créer un compte entreprise
        this.servlets.put(CreateAccountEntrepriseServlet.URL_CREATE_ACCOUNT_ENTREPRISE, new CreateAccountEntrepriseServlet());
        //Générer un questionnaire
        this.servlets.put(GenerateurQuestionnaireServlet.URL_GENQUESTIONNAIRE, new GenerateurQuestionnaireServlet());
        //Administrer des questionnaires
        this.servlets.put(GiveQuestionnaireToSalarieServlet.URL_GIVE_QUESTIONNAIRE_TO_SALARIE, new GiveQuestionnaireToSalarieServlet());
        //Afficher des questionnaires
        this.servlets.put(AfficheQuestionnaireESTER.URL_AFFICHE_QUESTIONNAIRE_ESTER, new AfficheQuestionnaireESTER());
        //Modifier les paramétres de compte des autres
        this.servlets.put(ParametresConnexionServlet.URL_PARAMETRES_CONNEXION_AUTRES_UTILISATEURS, new ParametresConnexionServlet());
        //Voir les résultats
        this.servlets.put(ResultatServlet.URL_RESULTAT, new ResultatServlet());
        //Modifier le serveur de mail
        this.servlets.put(ServeurMailServlet.URL_SERVEUR_MAIL, new ServeurMailServlet());
        //Importer, Exporter les données de références
        this.servlets.put(ImportExportDataReferenceServlet.URL_IMPORT_EXPORT_DATA, new ImportExportDataReferenceServlet());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post Utilisateur Ester");
        super.doPost(request, response);
        Servlet.dispatch(request, response, VUE_UTILISATEUR);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get Utilisateur Ester");
        super.doGet(request, response);
        Servlet.dispatch(request, response, VUE_UTILISATEUR);
    }
}