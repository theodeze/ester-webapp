package fr.univangers.esterwebapp.servlet.reference;

import fr.univangers.esterwebapp.formulaire.reference.DataReferenceForm;
import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.service.QuestionnaireService;
import fr.univangers.esterwebapp.servlet.Servlet;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

public class ImportExportDataReferenceServlet extends HttpServlet {

    /**
     * Logger pour tracer les traitements de la servlet
     */
    private static Logger log = Logger.getLogger(ImportExportDataReferenceServlet.class);

    /**
     * URL pour configurer le serveur de mail
     */
    public static final String URL_IMPORT_EXPORT_DATA = "importExportData";

    /**
     * La vue pour configurer le serveur de mail
     */
    public static final String VUE_IMPORT_EXPORT_DATA = "/jsp/importExportData.jsp";

    /**
     * La liste des questionnaires
     */
    public static final String QUESTIONNAIRES = "Questionnaires";

    /**
     * Nom du champ questionnaire dans le formulaire (mappage entre la back et le front)
     */
    public static final String QUESTIONNAIRE_SELECT = "questionnaireSelect";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post ImportExportData");

        boolean valid = true;

        //L'id du questionnaire
        String idQuestionnaire = request.getParameter(QUESTIONNAIRE_SELECT);
        log.info("Identifiant questionnaire : " + idQuestionnaire);
        if(Util.isNullOrEmpty(idQuestionnaire)) {
            valid = false;
        }

        Questionnaire questionnaire = QuestionnaireService.getInstance().readById(idQuestionnaire);
        log.info("Questionnaire : " + questionnaire);
        if(questionnaire == null) {
            valid = false;
        }

        if(valid) {
            DataReferenceForm<Questionnaire> dataReferenceForm = new DataReferenceForm<>(request, questionnaire);
            DataReferenceServlet<Questionnaire> dataReferenceServlet = dataReferenceForm.createModelFromRequest();

            if(dataReferenceServlet != null) {
                dataReferenceServlet.execute(request, response);
            } else {
                Servlet.ATT_MSG_ERROR.write("Opération inconnue", request);
            }
        }

        this.doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get ImportExportData");

        //On enregistre les questionnaires dans la requete
        Collection<Questionnaire> questionnaires = QuestionnaireService.getInstance().read();
        request.setAttribute(QUESTIONNAIRES, questionnaires);
    }
}