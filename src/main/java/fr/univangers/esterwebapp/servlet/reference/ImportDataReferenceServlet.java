package fr.univangers.esterwebapp.servlet.reference;

import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.model.reference.ImportDataFile;
import fr.univangers.esterwebapp.formulaire.reference.ImportDataReferenceForm;
import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.service.QuestionnaireService;
import fr.univangers.esterwebapp.servlet.Servlet;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ImportDataReferenceServlet<T extends Model> extends DataReferenceServlet<T> {

    private static Logger log = Logger.getLogger(ImportDataReferenceServlet.class);

    public ImportDataReferenceServlet(T model) {
        super(model);
    }

    @Override
    public boolean execute(HttpServletRequest request, HttpServletResponse response) {
        ImportDataReferenceForm importDataReferenceForm = new ImportDataReferenceForm(request);
        ImportDataFile importDataFile = importDataReferenceForm.createModelFromRequest();

        if(importDataFile != null) {
            boolean read = importDataFile.read();
            log.info("Is read : " + read);
            if(read) {
                UtilisateurEster importeur = Servlet.readUserInSession(request);
                boolean importData = QuestionnaireService.getInstance().importDataFromFile(importDataFile.getRecords(), (Questionnaire) this.model, importeur);
                if(importData) {
                    Servlet.ATT_MSG_SUCCESS.write("Toutes les données ont été importées", request);
                    return true;
                } else {
                    Servlet.ATT_MSG_ERROR.write("Erreur lors de l'import des données", request);
                    return false;
                }
            } else {
                Servlet.ATT_MSG_ERROR.write("Erreur lors de la lecture du fichier", request);
                return false;
            }
        } else {
            Servlet.ATT_MSG_ERROR.write("Le format du fichier n'est pas reconnu. Uniquement JSON ou CSV", request);
            return false;
        }
    }
}
