package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class UtilisateurServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(UtilisateurServlet.class);

    protected Map<String, HttpServlet> servlets;

    public UtilisateurServlet() {
        this.servlets = new HashMap<>();
        //Tout utilisateur peut modifier ses paramétres
        this.servlets.put(ParametresConnexionServlet.URL_PARAMETRES_CONNEXION, new ParametresConnexionServlet());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post Utilisateur");
        this.doService(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get Utilisateur");
        this.doService(request, response);
    }

    private void doService(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do service Utilisateur");
        String page = request.getParameter(Servlet.PARAM_PAGE);
        log.info("Page : " + page);
        log.info("servlets keys "+this.servlets.toString());
        if(!Util.isNullOrEmpty(page) && this.servlets.containsKey(page)) {
            try {
                this.servlets.get(page).service(request, response);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        else {
            log.info(page + " Not found !");
        }
    }
}