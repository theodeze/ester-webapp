package fr.univangers.esterwebapp.servlet.CreateAccounts;

import fr.univangers.esterwebapp.dao.mongodb.EntrepriseDAO;
import fr.univangers.esterwebapp.model.Entreprise;
import fr.univangers.esterwebapp.model.Mail;
import fr.univangers.esterwebapp.service.MailService;
import fr.univangers.esterwebapp.servlet.ConnexionServlet;
import fr.univangers.esterwebapp.servlet.PasswordServlet;
import fr.univangers.esterwebapp.servlet.Servlet;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@WebServlet(name = "NewAccountEntrepriseServlet", urlPatterns = {NewAccountEntrepriseServlet.URL_NEW_ACCOUNT_ENTREPRISE})
public class NewAccountEntrepriseServlet extends PasswordServlet {

    private static Logger log = Logger.getLogger(String.valueOf(CreateAccountUserServlet.class));

    public static final String URL_NEW_ACCOUNT_ENTREPRISE = "/newAccountEntreprise";
    public static final String VUE_NEW_ACCOUNT_ENTREPRISE = "/jsp/CreateAccounts/newAccountEntreprise.jsp";

    public static final String NAME_INPUT = "nameInput";
    public static final String TOKEN_INPUT = "mail";
    public static final String TOKEN_VALUE = "ValueToken";
    public static final String NEW_MOTPASSE_INPUT = "InputNewPassword";
    public static final String CONFIRM_MOTPASSE_INPUT = "InputConfirmPassword";


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post CreationUtilisateur");

        String vue = VUE_NEW_ACCOUNT_ENTREPRISE;

        String token = request.getParameter(TOKEN_INPUT);
        request.setAttribute(TOKEN_VALUE, token);
        log.info("Token : " + token);

        //Si le token existe
        if(!Util.isNullOrEmpty(token)) {

            UUID uuid = null;
            try {
                uuid = UUID.fromString(token);
                log.info("UUID : " + uuid);
            } catch (Exception e) {
                log.error(e.getMessage());
                uuid = null;
            }

            //Si le token est bien un uuid
            if (uuid != null) {

                Mail mail = MailService.getInstance().readById(uuid);
                log.info("Mail : " + mail);

                //Si il y a bien une demande de réinitialisation
                if (mail != null) {

                    String nameInput = request.getParameter(NAME_INPUT);
                    log.info("Name Entreprise : " + nameInput);

                    String newPassword = request.getParameter(NEW_MOTPASSE_INPUT);
                    //log.info("Password : " + newPassword);

                    String confirmPassword = request.getParameter(CONFIRM_MOTPASSE_INPUT);
                    //log.info("Confirmation password : " + confirmPassword);

                    String email = mail.getIdentifiant();

                    //Les mots de passe sont bien égaux
                    if (newPassword.equals(confirmPassword)) {

                        Entreprise entreprise = new Entreprise(nameInput, nameInput, email, newPassword,true);
                        log.info("Entreprise : " + entreprise);

                        boolean boo = EntrepriseDAO.getInstance().create(entreprise);

                        if (boo) {
                            String message = "Création du compte réussie";
                            Servlet.ATT_MSG_SUCCESS.write(message, request);
                            //On redirige l'entreprise vers la page de connexion
                            vue = ConnexionServlet.VUE_LOGIN;
                        } else {
                            String message = "Problème lors de la création du compte. Veuillez réessayer plus tard.";
                            Servlet.ATT_MSG_ERROR.write(message, request);
                        }
                    } else {
                        String message = "Les mots de passe ne sont pas identiques.";
                        Servlet.ATT_MSG_WARNING.write(message, request);
                    }
                }
            }
        }
        Servlet.dispatch(request, response, vue);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get ResetPasswordServlet");
        Servlet.dispatch(request, response, VUE_NEW_ACCOUNT_ENTREPRISE);
    }
}
