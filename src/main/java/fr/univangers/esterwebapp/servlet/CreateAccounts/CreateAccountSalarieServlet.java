package fr.univangers.esterwebapp.servlet.CreateAccounts;

import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.service.SalarieService;
import fr.univangers.esterwebapp.model.Salarie;
import fr.univangers.esterwebapp.servlet.Servlet;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static fr.univangers.esterwebapp.util.SecurityUtil.createIdentifiant;

public class CreateAccountSalarieServlet extends HttpServlet {

    /**
     * Logger pour tracer les traitements de la servlet
     */
    private static Logger log = Logger.getLogger(CreateAccountSalarieServlet.class);

    public static final String URL_CREATE_ACCOUNT_SALARIE = "createAccountSalarie";
    public static final String VUE_CREATE_ACCOUNT_SALARIE = "/jsp/CreateAccounts/createAccountSalarie.jsp";

    public static final String LISTE_SALARIES = "liste_salaries";

    private static final long MILLISECONDES_TO_HEURES = 3600000; // 1000 * 60 * 60

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post CreateAccountSalarieServlet");

        UtilisateurEster user = Servlet.readUserInSession(request);

        String nouvID = createIdentifiant();
        boolean saveSalarie = false;

        if(nouvID!= null) {
            Salarie nouvSalarie = new Salarie();
            nouvSalarie.setIdentifiant(nouvID);
            nouvSalarie.setCreateurSalarie(user);
            saveSalarie = SalarieService.getInstance().merge(nouvSalarie);
            log.info("Salarié créé : " + nouvSalarie);
        }
        if(saveSalarie) {
            Servlet.ATT_MSG_SUCCESS.write("Compte salarié créé ! Identifiant", request);
            setListSalaries(request);
        }else
            Servlet.ATT_MSG_ERROR.write("Echec dans la création d'un nouveau compte salarié", request);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get CreateAccountSalarieServlet");

        setListSalaries(request);
    }

    private void setListSalaries(HttpServletRequest request)
    {
        List<Salarie> salaries = new ArrayList(SalarieService.getInstance().read());
        Date today = new Date();
        log.debug("Date du jour " + today);

        UtilisateurEster user = Servlet.readUserInSession(request);
        log.info("User Id : "+user.getIdentifiant());

        //Il faut supprimer les salaries créés il y a plus de 24h
        salaries.removeIf(s -> {
            //On convertit le timestamp en date
            Date dateCreatedSalarie = new Date(s.getCreated().getTime());
            log.debug("Date de création : " + dateCreatedSalarie);

            long diff = today.getTime() - dateCreatedSalarie.getTime();
            diff = diff / MILLISECONDES_TO_HEURES;
            log.debug("Création il y a " + diff + " heures");

            return diff >= 24;
        });

        List<Salarie> salarieListCopy = new ArrayList<>();
        for(int index = salaries.size()-1; index != -1; --index){
            if(user.getRole()== Role.ADMIN || user.getIdentifiant().equals(salaries.get(index).getCreateurSalarie().getIdentifiant()))
            salarieListCopy.add(salaries.get(index));
        }

        log.info("Liste des salariés à afficher : " + salarieListCopy);
        request.setAttribute(LISTE_SALARIES, salarieListCopy);
    }
}