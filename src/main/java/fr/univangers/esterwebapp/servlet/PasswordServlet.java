package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.model.Connexion;
import fr.univangers.esterwebapp.service.ConnexionService;
import fr.univangers.esterwebapp.service.ManagerService;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;

public abstract class PasswordServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(PasswordServlet.class);

    protected Connexion getUtilisateur(String email) {
        for(ConnexionService<? extends Connexion> utilisateurConnexionService : ManagerService.getInstance().getUtilisateursConnexionService()) {
            Connexion user = utilisateurConnexionService.readByEmail(email);
            log.info("User : " + user);
            if(user != null) return user;
        }

        return null;
    }

    protected Connexion connecter(String email, String password) {
        for(ConnexionService<? extends Connexion> utilisateurConnexionService : ManagerService.getInstance().getUtilisateursConnexionService()) {
            Connexion user = utilisateurConnexionService.connecter(email, password);
            log.info("User : " + user);
            if(user != null) return user;
        }

        return null;
    }

    protected boolean updatePassword(String email, String password) {
        for(ConnexionService<? extends Connexion> utilisateurConnexionService : ManagerService.getInstance().getUtilisateursConnexionService()) {
            boolean update = utilisateurConnexionService.updatePassword(email, password);
            if(update) return true;
        }

        return false;
    }
}