package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.servlet.Questionnaires.AfficheQuestionnaireESTER;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "EntrepriseServlet", urlPatterns = {EntrepriseServlet.URL_ENTREPRISE})
public class EntrepriseServlet extends UtilisateurServlet {

    private static Logger log = Logger.getLogger(EntrepriseServlet.class);

    public static final String URL_ENTREPRISE = "/entreprise";
    public static final String VUE_ENTREPRISE = "/jsp/entreprise.jsp";

    public EntrepriseServlet() {
        super();
        this.servlets.put(AfficheQuestionnaireESTER.URL_AFFICHE_QUESTIONNAIRE_ESTER, new AfficheQuestionnaireESTER());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post Entreprise");
        super.doPost(request, response);
        Servlet.dispatch(request, response, VUE_ENTREPRISE);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get Entreprise");
        super.doGet(request, response);
        Servlet.dispatch(request, response, VUE_ENTREPRISE);
    }
}
