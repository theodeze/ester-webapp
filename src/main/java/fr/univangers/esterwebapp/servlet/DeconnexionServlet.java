package fr.univangers.esterwebapp.servlet;

import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "DeconnexionServlet", urlPatterns = {DeconnexionServlet.URL_LOGOUT})
public class DeconnexionServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(DeconnexionServlet.class);

    public static final String URL_LOGOUT = "/deconnexion";
    public static final String VUE_LOGOUT = "/index.jsp";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post Deconnexion");
        this.doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get Deconnexion");

        //Supprime la session, ce qui déconnecte l'utilisateur
        HttpSession session = request.getSession(false);
        if(session != null) session.invalidate();

        Servlet.dispatch(request, response, VUE_LOGOUT);
    }
}