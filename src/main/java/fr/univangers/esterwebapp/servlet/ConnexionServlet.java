package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.model.Utilisateur;
import fr.univangers.esterwebapp.service.EntrepriseService;
import fr.univangers.esterwebapp.service.SalarieService;
import fr.univangers.esterwebapp.service.UtilisateurEsterService;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ConnexionServlet", urlPatterns = {ConnexionServlet.URL_LOGIN})
public class ConnexionServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(ConnexionServlet.class);

    public static final String URL_LOGIN = "/connexion";
    public static final String VUE_LOGIN = "/jsp/login.jsp";

    public static final String TYPE_INPUT = "TypeInput";
    public static final String TYPE_VALUE_UTILISATEUR = "TypeUtilisateur";
    public static final String TYPE_VALUE_SALARIE = "TypeSalarie";
    public static final String TYPE_VALUE_ENTREPRISE = "TypeEntreprise";
    public static final String IDENTIFIANT_INPUT = "IDInput";
    public static final String IDENTIFIANT_VALUE = "IDValue";
    public static final String MOTPASSE_INPUT = "PasswordInput";
    public static final String MOTPASSE_VALUE = "PasswordValue";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post Connexion");

        //Préconditions sur le formulaire soumis

        String type = request.getParameter(TYPE_INPUT);
        log.info("Type : " + type);
        if(Util.isNullOrEmpty(type)) {
            Servlet.ATT_MSG_ERROR.write("Problème lors de la connexion. Rééesayer ultérieurement", request);
        }

        String identifiantOrEmail = request.getParameter(IDENTIFIANT_INPUT);
        request.setAttribute(IDENTIFIANT_VALUE, identifiantOrEmail);
        log.info("Identifiant : " + identifiantOrEmail);
        if(Util.isNullOrEmpty(identifiantOrEmail)) {
            Servlet.ATT_MSG_WARNING.write("Vous devez renseigner un identifiant !", request);
        }

        String motPasse = request.getParameter(MOTPASSE_INPUT);
        request.setAttribute(MOTPASSE_VALUE, motPasse);
        log.info("Mot de passe : " + motPasse);
        if(motPasse != null && motPasse.equals("")) {
            Servlet.ATT_MSG_WARNING.write("Vous devez renseigner un mot de passe !", request);
        }

        //Tentative de connexion de l'utilisateur

        Utilisateur utilisateur = null;

        if(type.equals(TYPE_VALUE_SALARIE)) {
            utilisateur = SalarieService.getInstance().readById(identifiantOrEmail);
        } else if(type.equals(TYPE_VALUE_ENTREPRISE)) {
            utilisateur =  EntrepriseService.getInstance().connecter(identifiantOrEmail, motPasse);
        } else if (type.equals(TYPE_VALUE_UTILISATEUR)) {
            utilisateur =  UtilisateurEsterService.getInstance().connecter(identifiantOrEmail, motPasse);
        }

        log.info("Utilisateur : " + utilisateur);

        if (utilisateur != null) {
            Servlet.ATT_MSG_SUCCESS.write("Vous êtes connecté.", request);
        } else {
            Servlet.ATT_MSG_WARNING.write("Votre identifiant ou mot de passe est incorrect.", request);
        }

        //On enregistre l'utilisateur dans la session
        Servlet.writeUserInSession(utilisateur, true, request);
        Servlet.dispatch(request, response, VUE_LOGIN);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get Connexion");
        Servlet.dispatch(request, response, VUE_LOGIN);
    }
}