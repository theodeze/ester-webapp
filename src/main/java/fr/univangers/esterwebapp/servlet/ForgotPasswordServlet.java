package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.model.Connexion;
import fr.univangers.esterwebapp.model.Mail;
import fr.univangers.esterwebapp.service.MailService;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ForgotPasswordServlet", urlPatterns = {ForgotPasswordServlet.URL_FORGOT_MOTPASSE})
public class ForgotPasswordServlet extends PasswordServlet {

    private static Logger log = Logger.getLogger(ForgotPasswordServlet.class);

    public static final String URL_FORGOT_MOTPASSE = "/forgot_password";
    public static final String VUE_FORGOT_MOTPASSE = "/jsp/forgot_password.jsp";

    public static final String EMAIL_INPUT = "EmailInput";
    public static final String EMAIL_VALUE = "EmailValue";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post ForgotPasswordServlet");

        String email = request.getParameter(EMAIL_INPUT);
        request.setAttribute(EMAIL_VALUE, email);
        log.info("Email : " + email);

        Connexion user = this.getUtilisateur(email);
        log.info("User : " + user);

        //Si l'utilisateur est inscrit
        if(user != null) {

            //Si le mail n'est pas déjà envoyé
            if(MailService.getInstance().readById(user.getEmail()) != null) {
                MailService.getInstance().delete(MailService.getInstance().readById(user.getEmail()));
            }
                //On construit l'url pour l'utilisateur afin qu'il réinitialise le mot de passe en cliquant sur un lien
                String url = request.getRequestURL().toString();
                url = url.substring(0, url.length() - URL_FORGOT_MOTPASSE.length());
                boolean send = MailService.getInstance().sendForgotPassword(user.getEmail(), url);

                //Si le mail est bel et bien envoyé
                if(send) {
                    log.info("Mail envoyé à " + user.getEmail());
                    String message = "Un mail a été envoyé à " + user.getEmail();
                    Servlet.ATT_MSG_SUCCESS.write(message, request);
                }
                else {
                    log.error("Problème survenu lors de l'envoi du mail à " + user.getEmail());
                    String message = "Un problème est survenu lors de l'envoi du mail à " + user.getEmail() + ". Veuillez réessayer plus tard.";
                    Servlet.ATT_MSG_ERROR.write(message, request);
                }
        }
        else {
            log.warn("Email non reconnu : " + email);
            String message = "L'adresse mail " + email + " n'est pas reconnu";
            Servlet.ATT_MSG_WARNING.write(message, request);
        }

        Servlet.dispatch(request, response, VUE_FORGOT_MOTPASSE);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get ForgotPasswordServlet");
        Servlet.dispatch(request, response, VUE_FORGOT_MOTPASSE);
    }
}