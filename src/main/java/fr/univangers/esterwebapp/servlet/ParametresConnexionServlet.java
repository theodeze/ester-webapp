package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.formulaire.SalarieForm;
import fr.univangers.esterwebapp.formulaire.connexion.ParametresConnexionCollectionForm;
import fr.univangers.esterwebapp.formulaire.connexion.ParametresConnexionModelForm;
import fr.univangers.esterwebapp.model.Utilisateur;
import fr.univangers.esterwebapp.service.ManagerService;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

public class ParametresConnexionServlet extends HttpServlet {

    /**
     * Logger pour tracer les traitements de la servlet
     */
    private static Logger log = Logger.getLogger(ParametresConnexionServlet.class);

    /**
     * URL pour modifier les paramètres de connexion de l'utilisateur enregistré dans la session
     */
    public static final String URL_PARAMETRES_CONNEXION = "parametresConnexion";

    /**
     * URL pour modifier les paramètres de connexion d'un autre utilisateur
     */
    public static final String URL_PARAMETRES_CONNEXION_AUTRES_UTILISATEURS = "parametresConnexionAutresUtilisateurs";

    /**
     * La vue pour modifier les paramétres de connexion d'un utilisateur (enregistré dans la session ou autre sélectionné dans un select)
     */
    public static final String VUE_PARAMETRES_CONNEXION = "/jsp/parametresConnexion.jsp";

    /**
     * La vue pour modifier les paramétres de connexion d'un utilisateur (enregistré dans la session ou autre sélectionné dans un select)
     */
    public static final String VUE_PARAMETRES_CONNEXION_AUTRES_UTILISATEURS = "/jsp/parametresConnexionAutresUsers.jsp";

    /**
     * L'utilisateur sélectionné pour modifier ses paramétres de connexion
     */
    public static final String UTILISATEUR = "utilisateur";

    /**
     * Liste de tous les utilisateurs ester dont on peut modifier leurs paramétres de connexion
     */
    public static final String UTILISATEURS = "utilisateurs";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post ParametresConnexion");

        this.doGet(request, response);

        String probleme = "Problème lors de la modification";

        //On récupère la page
        String page = request.getParameter(Servlet.PARAM_PAGE);
        log.info("Page : " + page);

        //Si la page est identifié
        if (!Util.isNullOrEmpty(page)) {

            //On récupère l'utilisateur
            Utilisateur utilisateur = (Utilisateur) request.getAttribute(UTILISATEUR);
            log.info("Utilisateur : " + utilisateur);

            if(utilisateur != null) {

                //On met à jour l'utilisateur
                ParametresConnexionModelForm parametresConnexionModelForm = new ParametresConnexionModelForm(request);
                boolean updateUser = parametresConnexionModelForm.updateModelFromRequest(utilisateur);
                log.info("Update user : " + updateUser);

                //Si toutes les préconditions sont validés alors on peut mettre à jour l'utilisateur
                if(updateUser) {

                    //On met à jour l'utilisateur dans la BD
                    updateUser = ManagerService.getInstance().updateUtilisateur(utilisateur);

                    if(updateUser) {
                        Servlet.ATT_MSG_SUCCESS.write("Paramétre de connexion modifié", request);
                        request.setAttribute(UTILISATEUR, utilisateur);

                        //Si c'est l'utilisateur lui-meme alors il faut réecrire l'utilisateur dans la session
                        if(page.equals(ParametresConnexionServlet.URL_PARAMETRES_CONNEXION)) {
                            Servlet.writeUserInSession(utilisateur, false, request);
                        }
                    } else {
                        Servlet.ATT_MSG_WARNING.write(probleme, request);
                    }
                } else {
                    Servlet.ATT_MSG_WARNING.write(probleme, request);
                }
            } else {
                Servlet.ATT_MSG_WARNING.write(probleme, request);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get ParametresConnexion");

        //On récupère l'utilisateur courant
        Utilisateur utilisateurSession = Servlet.readUserInSession(request);
        log.info("Utilisateur session : " + utilisateurSession);

        //On récupère la page
        String page = request.getParameter(Servlet.PARAM_PAGE);
        log.info("Page : " + page);

        if(utilisateurSession != null && !Util.isNullOrEmpty(page)) {
            if(page.equals(ParametresConnexionServlet.URL_PARAMETRES_CONNEXION)) {
                request.setAttribute(UTILISATEUR, utilisateurSession);
            } else if(page.equals(ParametresConnexionServlet.URL_PARAMETRES_CONNEXION_AUTRES_UTILISATEURS)) {
                this.doGetParametresConnexionAutresUtilisateurs(request, utilisateurSession);
            }
        }
    }

    protected void doGetParametresConnexionAutresUtilisateurs(HttpServletRequest request, Utilisateur utilisateur) {
        //Collection des utilisateurs
        ParametresConnexionCollectionForm parametresConnexionForm = new ParametresConnexionCollectionForm(request);
        Collection<Utilisateur> utilisateurs = parametresConnexionForm.createModelFromRequest();
        log.info("Collection utilisateurs : " + utilisateurs);

        if(!utilisateurs.isEmpty()) {
            //On supprime l'utilisateur courant dans les listes car pas d'interet de se modifier lui meme
            //S'il veut modifier ses propres paramétres un autre lien permet de le faire
            utilisateurs.removeIf(u -> u.getIdentifiant().equals(utilisateur.getIdentifiant()));

            //Si c'est un médecin, alors on supprime tous les utilisateurs admin car les médecins ne sont pas habiletés à modifier leurs paramètres
            if(utilisateur.getRole() == Role.MEDECIN) {
                utilisateurs.removeIf(u -> u.getRole() == Role.ADMIN);
            }

            //On enregistre la liste dans la requete
            request.setAttribute(UTILISATEURS, utilisateurs);
        }

        //Sélection d'un utilisateur à partir de la collection
        ParametresConnexionModelForm parametresConnexionModelForm = new ParametresConnexionModelForm(request);
        Utilisateur user = parametresConnexionModelForm.createModelFromRequest();
        log.info("Model utilisateur : " + user);

        if(user != null) {
            if(user.getRole() == Role.SALARIE){
                SalarieForm salarieForm = new SalarieForm(request);
                salarieForm.getEntreprise();
            }
            //On met l'utilisateur dans la requete
            request.setAttribute(UTILISATEUR, user);
        }
    }
}