<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.servlet.ForgotPasswordServlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>
<c:set var="EmailInput" value="${ForgotPasswordServlet.EMAIL_INPUT}" scope="request"/>

<!DOCTYPE html>
<html lang="fr-FR" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<c:url value="/public/img/ua.png"/>">

    <title>Mot de passe oublié - ESTER</title>
    <link rel="stylesheet" href="<c:url value="/public/css/global_style.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/menu.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/libraries/bootstrap.min.css"/>">
    <script src="<c:url value="/public/js/libraries/jquery.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/popper.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/bootstrap.min.js"/>"></script>
</head>

<body class="d-flex flex-column h-100">

<c:import url="/jsp/UtilitiesToImport/navbar.jsp"/>

<main>
    <div class="container p-2 m_shadow" style="background-color: white; margin-top: 10px;">
        <div class="card-body center_block">
        <c:if test="${not empty Message && Message}">
            <c:import url="${Servlet.VUE_ALERT}"/>
        </c:if>

        <div class="row justify-content-center p-2">
            <div class="col-md-auto">
                <h1 class="font-weight-normal text-center">Mot de passe oublié</h1>
            </div>
        </div>

        <div class="row justify-content-center">
            <div style="text-align: initial;">
            <form method="post">
                <div class="mb-4">

                    <div class="form-group">
                        <label for="${EmailInput}">Veuillez saisir votre email</label>
                        <input type="email" name="${EmailInput}" id="${EmailInput}" placeholder="nom@exemple.com" value="${empty EmailValue ? '' : EmailValue}" class="form-control"  required>
                    </div>

                    <button class="btn btn-md-auto btn-primary btn-block" type="submit">VALIDER</button>
                </div>
            </form>
            </div>
        </div>
        </div>
    </div>
</main>

<c:import url="/jsp/UtilitiesToImport/footer.jsp"/>

</body>
</html>