<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.servlet.reference.ImportExportDataReferenceServlet"%>
<%@page import="fr.univangers.esterwebapp.formulaire.reference.DataReferenceForm"%>
<%@page import="fr.univangers.esterwebapp.formulaire.reference.ImportDataReferenceForm"%>
<%@page import="fr.univangers.esterwebapp.formulaire.reference.ExportDataReferenceForm"%>
<%@page import="fr.univangers.esterwebapp.model.modelenum.proposition.FileFormat"%>

<div class="row m-2" id="container">
    <div class="col justify-content-center">
        <div class="row justify-content-center">
            <div class="col-md-auto">
                <h4 class="font-weight-normal text-center">Importer ou exporter des données de références</h4>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <form method="post" enctype="multipart/form-data">

                    <div class="form-group row">
                        <label for="${ImportExportDataReferenceServlet.QUESTIONNAIRE_SELECT}">Sélectionner un questionnaire</label>
                        <select id="${ImportExportDataReferenceServlet.QUESTIONNAIRE_SELECT}" name="${ImportExportDataReferenceServlet.QUESTIONNAIRE_SELECT}" class="custom-select">
                            <c:forEach items="${Questionnaires}" var="questionnaire">
                                <option value="${questionnaire.identifiant}">${questionnaire.nom}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3">Voulez-vous importer ou exporter des données de références du questionnaire ?</label>
                        <div class="col-sm-9">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="${DataReferenceForm.IMPORT_EXPORT_INPUT}" id="${DataReferenceForm.EXPORT}" value="${DataReferenceForm.EXPORT}">
                                <label class="form-check-label" for="${DataReferenceForm.EXPORT}">${DataReferenceForm.EXPORT}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="${DataReferenceForm.IMPORT_EXPORT_INPUT}" id="${DataReferenceForm.IMPORT}" value="${DataReferenceForm.IMPORT}">
                                <label class="form-check-label" for="${DataReferenceForm.IMPORT}">${DataReferenceForm.IMPORT}</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row" id="fileExportDiv" style="display: none;">
                        <label for="fileExportInput">Sélectionner un format de sortie</label>
                        <select id="fileExportInput" name="${ExportDataReferenceForm.FORMAT_FILE_SELECT}" class="custom-select">
                            <c:forEach items="${FileFormat.values()}" var="format">
                                <option value="${format.code}">${format.code}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <div class="form-group row" id="fileImportDiv" style="display: none;">
                        <div class="custom-file">
                            <label for="fileImportInput" class="custom-file-label">Sélectionner un fichier à importer</label>
                            <input id="fileImportInput" name="${ImportDataReferenceForm.FILE_INPUT}" type="file" class="custom-file-input" lang="fr" accept="${FileFormat.displayAcceptFormat()}">
                        </div>
                    </div>

                    <div class="from-group row">
                        <div class="col-md-auto">
                            <button type="submit" class="btn btn-primary" id="load">Confirmer</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <!-- Modal Spinner -->
    <div class="modal fade" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="loadMeLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="loader"></div>
                    <div class="loader-txt">
                        <p>Veuillez patienter ...</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    //Ajouter le nom du fichier dans le input file
    $("#fileImportInput").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    //Afficher une animation de chargement
    $("#load").on("click", function(e) {
        $("#loadMe").modal({
            backdrop: "static", //remove ability to close modal with click
            keyboard: false, //remove option to close with keyboard
            show: true //Display loader!
        });
         setTimeout(function() {
             $("#loadMe").modal("hide");
         }, 3500);
    });
    document.getElementById("${DataReferenceForm.EXPORT}").addEventListener('click', function () {
        document.getElementById("fileImportDiv").style.display = "none";
        document.getElementById("fileExportDiv").style.display = "block";
    });
    document.getElementById("${DataReferenceForm.IMPORT}").addEventListener('click', function () {
        document.getElementById("fileExportDiv").style.display = "none";
        document.getElementById("fileImportDiv").style.display = "block";
    });
</script>