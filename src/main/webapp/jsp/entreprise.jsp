<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.EntrepriseServlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.ParametresConnexionServlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.ResultatServlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.Questionnaires.AfficheQuestionnaireESTER"%>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>

    <meta charset="UTF-8">
    <link rel="icon" href="<c:url value="/public/img/ua.png"/>">

    <title>Page entreprise - ESTER</title>

    <link rel="stylesheet" href="<c:url value="/public/css/libraries/bootstrap.min.css"/>">
    <script src="<c:url value="/public/js/libraries/jquery.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/popper.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/bootstrap.min.js"/>"></script>

    <link rel="stylesheet" href="<c:url value="/public/css/global_style.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/menu.css"/>">

</head>
<body class="d-flex flex-column min-vh-100">

<c:import url="/jsp/UtilitiesToImport/navbar.jsp"/>

<main>
    <div class="container-fluid  min-vh-100">
        <div class="row menu_gauche">

            <div class="col-md-2 menu_gauche" style="background: #158CBA;">
                <ul style="color:white; padding: 15px;">

                    <li>
                        <a style="color:white;" href="<c:url value="${EntrepriseServlet.URL_ENTREPRISE}?page=${ParametresConnexionServlet.URL_PARAMETRES_CONNEXION}"/>">Modifier paramètres de connexion</a>
                    </li>
                    <!--
                    <li>
                        <a style="color:white;" href="<c:url value="${ResultatServlet.URL_RESULTAT}"/>">Résultats globaux</a>
                    </li>

                    <li>
                        <a style="color:white;" href="<c:url value="/resultat"/>">Résultats par poste</a>
                    </li>
                    -->
                    <li>

                    <a style="color:white;"  href="<c:url value="${EntrepriseServlet.URL_ENTREPRISE}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${AfficheQuestionnaireESTER.URL_AFFICHE_QUESTIONNAIRE_ESTER}"/>
                                            </c:url>">Afficher les questionnaires</a>
                    </li>

                </ul>
            </div>

                <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container" >
                    <div class="card-body center_block">
                        <c:if test="${not empty Message && Message}">
                            <c:import url="${Servlet.VUE_ALERT}"/>
                        </c:if>

                        <h1>Entreprise</h1>
                        <p>Bienvenue sur la partie Entreprise du Projet ESTER</p>
                        <div style="text-align: initial;">
                            <c:if test='${param.page == ParametresConnexionServlet.URL_PARAMETRES_CONNEXION}'>
                                <c:import url="${ParametresConnexionServlet.VUE_PARAMETRES_CONNEXION}"/>
                            </c:if>
                            <!-- Afficher un questionnaire -->
                            <c:if test='${not empty param[Servlet.PARAM_PAGE] && param[Servlet.PARAM_PAGE] == AfficheQuestionnaireESTER.URL_AFFICHE_QUESTIONNAIRE_ESTER}'>
                                <c:import url="${AfficheQuestionnaireESTER.VUE_AFFICHE_QUESTIONNAIRE_ESTER}"/>
                            </c:if>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</main>

<c:import url="/jsp/UtilitiesToImport/footer.jsp"/>

</body>
</html>