<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.model.modelenum.utilisateur.Role"%>
<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.ParametresConnexionServlet"%>
<%@page import="fr.univangers.esterwebapp.formulaire.connexion.ParametresConnexionCollectionForm" %>
<%@page import="fr.univangers.esterwebapp.formulaire.connexion.ParametresConnexionModelForm"%>

<h3>Modification des paramètres de compte d'un autre utilisateur</h3>

<!--Sélection d'un type utilisateur-->
<form method="get">

    <c:if test='${not empty ParametresConnexionServlet.URL_PARAMETRES_CONNEXION_AUTRES_UTILISATEURS}'>
        <input type="hidden" name="${Servlet.PARAM_PAGE}" value="${ParametresConnexionServlet.URL_PARAMETRES_CONNEXION_AUTRES_UTILISATEURS}" />
    </c:if>

    <div class="form-group row">
        <div class="col-sm-2">
            <div class="col-md-auto">
                <label>Type utilisateur</label>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="col-md-auto">
                <c:forEach items="${Role.values()}" var="role">
                    <c:if test="${role.code != 'ADMIN' && Utilisateur.role == Role.MEDECIN}">
                        <div class="form-check form-check-inline">
                            <c:choose>
                                <c:when test='${not empty param[ParametresConnexionCollectionForm.TYPE_UTILISATEUR]}'>
                                    <input class="form-check-input" type="radio" name="${ParametresConnexionCollectionForm.TYPE_UTILISATEUR}" id="${role.code}" value="${role.code}"
                                        ${role.code == param[ParametresConnexionCollectionForm.TYPE_UTILISATEUR] ? 'checked' : ''}>
                                </c:when>
                                <c:otherwise>
                                    <input class="form-check-input" type="radio" name="${ParametresConnexionCollectionForm.TYPE_UTILISATEUR}" id="${role.code}" value="${role.code}">
                                </c:otherwise>
                            </c:choose>
                            <label class="form-check-label" for="${role.code}">${role.nom}</label>
                        </div>
                    </c:if>
                    <c:if test="${Utilisateur.role == Role.ADMIN}">
                        <div class="form-check form-check-inline">
                            <c:choose>
                                <c:when test='${not empty param[ParametresConnexionCollectionForm.TYPE_UTILISATEUR]}'>
                                    <input class="form-check-input" type="radio" name="${ParametresConnexionCollectionForm.TYPE_UTILISATEUR}" id="${role.code}" value="${role.code}"
                                        ${role.code == param[ParametresConnexionCollectionForm.TYPE_UTILISATEUR] ? 'checked' : ''}>
                                </c:when>
                                <c:otherwise>
                                    <input class="form-check-input" type="radio" name="${ParametresConnexionCollectionForm.TYPE_UTILISATEUR}" id="${role.code}" value="${role.code}">
                                </c:otherwise>
                            </c:choose>
                            <label class="form-check-label" for="${role.code}">${role.nom}</label>
                        </div>
                    </c:if>
                </c:forEach>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="col-md-auto">
                <button class="btn btn-block btn-primary" type="submit">Valider</button>
            </div>
        </div>
    </div>

</form>

<!--Choix d'un utilisateur dans la liste-->
<c:if test='${not empty utilisateurs}'>
    <form method="get">

        <c:if test='${not empty ParametresConnexionServlet.URL_PARAMETRES_CONNEXION_AUTRES_UTILISATEURS}'>
            <input type="hidden" name="${Servlet.PARAM_PAGE}" value="${ParametresConnexionServlet.URL_PARAMETRES_CONNEXION_AUTRES_UTILISATEURS}" />
        </c:if>
        <c:if test='${not empty param[ParametresConnexionCollectionForm.TYPE_UTILISATEUR]}'>
            <input type="hidden" name="${ParametresConnexionCollectionForm.TYPE_UTILISATEUR}" value="${param[ParametresConnexionCollectionForm.TYPE_UTILISATEUR]}" />
        </c:if>

        <div class="form-group row">
            <div class="col-sm-2">
                <div class="col-md-auto">
                    <label for="${ParametresConnexionModelForm.UTILISATEUR_SELECT}">Membres ESTER</label>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="col-md-auto">
                    <div class="form-group">
                        <select name="${ParametresConnexionModelForm.UTILISATEUR_SELECT}" class="custom-select">
                            <c:forEach items="${utilisateurs}" var="utilisateur">
                                <option value="${utilisateur.identifiant}">${utilisateur.identifiant}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="col-md-auto">
                    <button class="btn btn-block btn-primary" type="submit">Modifier ces paramètres</button>
                </div>
            </div>
        </div>
    </form>
</c:if>

<!--Modification des paramètres de compte de l'utilisateur choisi dans le select-->
<c:if test='${not empty param[ParametresConnexionModelForm.UTILISATEUR_SELECT]}'>
    <c:import url="${ParametresConnexionServlet.VUE_PARAMETRES_CONNEXION}"/>
</c:if>