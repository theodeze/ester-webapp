<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.model.modelenum.utilisateur.Role"%>
<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.UtilisateurEsterServlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.ParametresConnexionServlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.CreateAccounts.CreateAccountUserServlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.CreateAccounts.CreateAccountEntrepriseServlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.ServeurMailServlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.Questionnaires.GenerateurQuestionnaireServlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.Questionnaires.AfficheQuestionnaireESTER"%>
<%@page import="fr.univangers.esterwebapp.servlet.CreateAccounts.CreateAccountSalarieServlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.Questionnaires.GiveQuestionnaireToSalarieServlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.reference.ImportExportDataReferenceServlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.ResultatServlet"%>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<c:url value="/public/img/ua.png"/>">

    <title> Page membre - ESTER </title>

    <link rel="stylesheet" href="<c:url value="/public/css/libraries/bootstrap.min.css"/>">
    <script src="<c:url value="/public/js/libraries/jquery.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/popper.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/public/js/ownFiles/populate_select.js"/>"></script>
    <script src="<c:url value="/public/js/ownFiles/check_password.js"/>"></script>

    <link rel="stylesheet" href="<c:url value="/public/css/global_style.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/menu.css"/>">

</head>

<body class="d-flex flex-column min-vh-100">

<c:import url="/jsp/UtilitiesToImport/navbar.jsp"/>

<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-2 menu_gauche"  style="background: #158CBA;">
                <ul style="color:white; padding:15px;">

                    <c:if test="${Utilisateur.role == Role.PREVENTEUR}">

                        <li class="dropdown">
                            <a style="color:white;" class="dropdown-toggle" href="#" id="gestionSalariesMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Gestion des salariés
                            </a>
                            <div class="dropdown-menu" aria-labelledby="gestionSalariesMenu">
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${CreateAccountSalarieServlet.URL_CREATE_ACCOUNT_SALARIE}"/>
                                            </c:url>">Crée un compte salarié</a>
                            </div>
                        </li>
                        <li class="dropdown">
                            <a style="color:white;" class="dropdown-toggle" href="#" id="gestionQuestionnaireMenuPreventeur" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Gestion des questionnaires</a>

                            <div class="dropdown-menu" aria-labelledby="gestionQuestionnaireMenuPreventeur">
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                    <c:param name="${Servlet.PARAM_PAGE}" value="${GiveQuestionnaireToSalarieServlet.URL_GIVE_QUESTIONNAIRE_TO_SALARIE}"/>
                                                </c:url>">Administrer les questionnaires</a>
                                <a class="dropdown-item"  href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                    <c:param name="${Servlet.PARAM_PAGE}" value="${AfficheQuestionnaireESTER.URL_AFFICHE_QUESTIONNAIRE_ESTER}"/>
                                                </c:url>">Afficher les questionnaires</a>
                            </div>
                        </li>

                    </c:if>

                    <c:if test="${Utilisateur.role == Role.INFIRMIER || Utilisateur.role == Role.ASSISTANT}">

                        <!-- Connection en tant que Infirmier/Assistant -->

                        <li class="dropdown">
                            <a style="color:white;" class="dropdown-toggle" href="#" id="gestionQuestionnaireMenuInfAss" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Gestion des questionnaires</a>

                            <div class="dropdown-menu" aria-labelledby="gestionQuestionnaireMenuInfAss">
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${GiveQuestionnaireToSalarieServlet.URL_GIVE_QUESTIONNAIRE_TO_SALARIE}"/>
                                            </c:url>">Administrer les questionnaires</a>
                                <a class="dropdown-item"  href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${AfficheQuestionnaireESTER.URL_AFFICHE_QUESTIONNAIRE_ESTER}"/>
                                            </c:url>">Afficher les questionnaires</a>
                            </div>
                        </li>

                    </c:if>


                    <c:if test="${Utilisateur.role == Role.ADMIN}">

                        <!-- Connection en tant que Administrateur -->

                        <li>
                            <a class="dropdown-toggle" href="#" id="creationComptes" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:white;">
                                Création de compte
                            </a>
                            <div class="dropdown-menu" aria-labelledby="creationComptes">
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${CreateAccountUserServlet.URL_CREATE_ACCOUNT_USER}"/>
                                            </c:url>">Membre ESTER</a>

                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${CreateAccountEntrepriseServlet.URL_CREATE_ACCOUNT_ENTREPRISE}"/>
                                            </c:url>">Entreprise</a>

                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${CreateAccountSalarieServlet.URL_CREATE_ACCOUNT_SALARIE}"/>
                                            </c:url>">Salarié</a>
                            </div>
                        </li>

                        <li>
                            <a class="dropdown-toggle" href="#" id="gestionComptesMenuAdmin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:white;">
                                Modification de compte
                            </a>

                            <div class="dropdown-menu" aria-labelledby="gestionComptesMenuAdmin">
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${ParametresConnexionServlet.URL_PARAMETRES_CONNEXION_AUTRES_UTILISATEURS}"/>
                                            </c:url>">Modifier les comptes des autres utilisateurs</a>
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${ParametresConnexionServlet.URL_PARAMETRES_CONNEXION}"/>
                                            </c:url>">Modifier votre compte</a>
                            </div>
                        </li>

                        <li class="dropdown">
                            <a style="color:white;" class="dropdown-toggle" href="#" id="gestionQuestionnaireMenuAdmin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Gestion des questionnaires</a>

                            <div class="dropdown-menu" aria-labelledby="gestionQuestionnaireMenuAdmin">
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${GenerateurQuestionnaireServlet.URL_GENQUESTIONNAIRE}"/>
                                            </c:url>">Créer un questionnaire</a>
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${GiveQuestionnaireToSalarieServlet.URL_GIVE_QUESTIONNAIRE_TO_SALARIE}"/>
                                            </c:url>">Administrer les questionnaires</a>
                                <a class="dropdown-item"  href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${AfficheQuestionnaireESTER.URL_AFFICHE_QUESTIONNAIRE_ESTER}"/>
                                            </c:url>">Afficher les questionnaires</a>
                            </div>
                        </li>

                        <li>
                            <a style="color:white;" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${ImportExportDataReferenceServlet.URL_IMPORT_EXPORT_DATA}"/>
                                            </c:url>">Importer ou exporter des données</a>
                        </li>

                        <li>
                            <a style="color:white;" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${ServeurMailServlet.URL_SERVEUR_MAIL}"/>
                                            </c:url>">Configurer serveur mail</a>
                        </li>

                    </c:if>

                    <c:if test="${Utilisateur.role == Role.MEDECIN}">

                        <!-- Connection en tant que Médecin -->

                        <li>
                            <a class="dropdown-toggle" href="#" id="creationComptesMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:white;">
                                Création de compte
                            </a>
                            <div class="dropdown-menu" aria-labelledby="gestionComptesMenu">
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${CreateAccountUserServlet.URL_CREATE_ACCOUNT_USER}"/>
                                            </c:url>">Membre ESTER</a>
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${CreateAccountEntrepriseServlet.URL_CREATE_ACCOUNT_ENTREPRISE}"/>
                                            </c:url>">Entreprise</a>
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${CreateAccountSalarieServlet.URL_CREATE_ACCOUNT_SALARIE}"/>
                                            </c:url>">Salarié</a>
                            </div>
                        </li>

                        <li>
                            <a class="dropdown-toggle" href="#" id="gestionComptesMenuMedecin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:white;">
                                Modification de comptes
                            </a>

                            <div class="dropdown-menu" aria-labelledby="gestionComptesMenuMedecin">
                                <a class="dropdown-item"  href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${ParametresConnexionServlet.URL_PARAMETRES_CONNEXION_AUTRES_UTILISATEURS}"/>
                                            </c:url>">Modifier les comptes des autres utilisateurs</a>
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${ParametresConnexionServlet.URL_PARAMETRES_CONNEXION}"/>
                                            </c:url>">Modifier votre compte</a>
                            </div>
                        </li>

                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" id="gestionQuestionnaireMenuMedecin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:white;">Gestion des questionnaires</a>

                            <div class="dropdown-menu" aria-labelledby="gestionQuestionnaireMenuMedecin">
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${GenerateurQuestionnaireServlet.URL_GENQUESTIONNAIRE}"/>
                                            </c:url>">Créer un questionnaire</a>
                                <a class="dropdown-item" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${GiveQuestionnaireToSalarieServlet.URL_GIVE_QUESTIONNAIRE_TO_SALARIE}"/>
                                            </c:url>">Administrer les questionnaires</a>
                                <a class="dropdown-item"  href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${AfficheQuestionnaireESTER.URL_AFFICHE_QUESTIONNAIRE_ESTER}"/>
                                            </c:url>">Afficher les questionnaires</a>

                            </div>
                        </li>

                        <li>
                            <a style="color:white;" href="<c:url value="${UtilisateurEsterServlet.URL_UTILISATEUR}">
                                                <c:param name="${Servlet.PARAM_PAGE}" value="${ImportExportDataReferenceServlet.URL_IMPORT_EXPORT_DATA}"/>
                                            </c:url>">Importer ou exporter des données</a>
                        </li>

                    </c:if>

                    <!--On affiche les statistiques pour tous le monde
                    <li>
                        <a style="color:white;" href="<c:url value="/resultat"/>">Afficher les statistiques</a>
                    </li>
                    -->
                </ul>
            </div>

                <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container">
                    <div class="card-body center_block">
                        <c:if test="${not empty Message && Message}">
                            <c:import url="${Servlet.VUE_ALERT}"/>
                        </c:if>

                        <h1>${Utilisateur.role.nom}</h1>
                        <p>Bienvenue sur la partie ${Utilisateur.role.nom} du Projet ESTER</p>
                        <div style="text-align: initial;">
                            <!-- Générer un questionnaire -->
                            <c:if test='${not empty param[Servlet.PARAM_PAGE] && param[Servlet.PARAM_PAGE] == GenerateurQuestionnaireServlet.URL_GENQUESTIONNAIRE}'>
                                <c:import url="${GenerateurQuestionnaireServlet.VUE_GENQUESTIONNAIRE}"/>
                            </c:if>

                            <!-- Création d'un compte utilisateur -->
                            <c:if test='${not empty param[Servlet.PARAM_PAGE] && param[Servlet.PARAM_PAGE] == CreateAccountUserServlet.URL_CREATE_ACCOUNT_USER}'>
                                <c:import url="${CreateAccountUserServlet.VUE_CREATE_ACCOUNT_USER}"/>
                            </c:if>

                            <!-- Création d'un compte salarié -->
                            <c:if test='${not empty param[Servlet.PARAM_PAGE] && param[Servlet.PARAM_PAGE] == CreateAccountSalarieServlet.URL_CREATE_ACCOUNT_SALARIE}'>
                                <c:import url="${CreateAccountSalarieServlet.VUE_CREATE_ACCOUNT_SALARIE}"/>
                            </c:if>

                            <!-- Création d'un compte entreprise ?? -->

                            <c:if test='${not empty param[Servlet.PARAM_PAGE] && param[Servlet.PARAM_PAGE] == CreateAccountEntrepriseServlet.URL_CREATE_ACCOUNT_ENTREPRISE}'>
                                <c:import url="${CreateAccountEntrepriseServlet.VUE_CREATE_ACCOUNT_ENTREPRISE}"/>
                            </c:if>


                            <!-- Configuration du serveur Mail -->
                            <c:if test='${not empty param[Servlet.PARAM_PAGE] && param[Servlet.PARAM_PAGE] == ServeurMailServlet.URL_SERVEUR_MAIL}'>
                                <c:import url="${ServeurMailServlet.VUE_SERVEUR_MAIL}"/>
                            </c:if>

                            <!--Modifier les paramétres de connexion des autres utilisateurs-->
                            <c:if test='${not empty param[Servlet.PARAM_PAGE] && param[Servlet.PARAM_PAGE] == ParametresConnexionServlet.URL_PARAMETRES_CONNEXION_AUTRES_UTILISATEURS}'>
                                <c:import url="${ParametresConnexionServlet.VUE_PARAMETRES_CONNEXION_AUTRES_UTILISATEURS}"/>
                            </c:if>

                            <!--Modifier les paramétres de connexion de l'utilisateur courant-->
                            <c:if test='${not empty param[Servlet.PARAM_PAGE] && param[Servlet.PARAM_PAGE] == ParametresConnexionServlet.URL_PARAMETRES_CONNEXION}'>
                                <c:import url="${ParametresConnexionServlet.VUE_PARAMETRES_CONNEXION}"/>
                            </c:if>

                            <!--Importer/exporter des données-->
                            <c:if test='${not empty param[Servlet.PARAM_PAGE] && param[Servlet.PARAM_PAGE] == ImportExportDataReferenceServlet.URL_IMPORT_EXPORT_DATA}'>
                                <c:import url="${ImportExportDataReferenceServlet.VUE_IMPORT_EXPORT_DATA}"/>
                            </c:if>

                            <!-- Attribuer un questionnaire à un Salarié -->
                            <c:if test='${not empty param[Servlet.PARAM_PAGE] && param[Servlet.PARAM_PAGE] == GiveQuestionnaireToSalarieServlet.URL_GIVE_QUESTIONNAIRE_TO_SALARIE}'>
                                <c:import url="${GiveQuestionnaireToSalarieServlet.VUE_GIVE_QUESTIONNAIRE_TO_SALARIE}"/>
                            </c:if>

                            <!-- Afficher un questionnaire -->
                            <c:if test='${not empty param[Servlet.PARAM_PAGE] && param[Servlet.PARAM_PAGE] == AfficheQuestionnaireESTER.URL_AFFICHE_QUESTIONNAIRE_ESTER}'>
                                <c:import url="${AfficheQuestionnaireESTER.VUE_AFFICHE_QUESTIONNAIRE_ESTER}"/>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/jsp/UtilitiesToImport/footer.jsp"/>

</body>
</html>