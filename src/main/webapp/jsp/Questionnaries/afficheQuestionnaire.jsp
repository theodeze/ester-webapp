<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.Questionnaires.QuestionnaireServlet"%>

<c:set var="QuestionnairesSelect" value="${QuestionnaireServlet.QUESTIONNAIRES_SELECT}" scope="request"/>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Questionnaire - ESTER</title>

    <link rel="icon" href="<c:url value="/public/img/ua.png"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/libraries/bootstrap.min.css"/>">

    <script src="<c:url value="/public/js/libraries/jquery.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/popper.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/public/js/ownFiles/afficheQuestionnaire.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/public/css/global_style.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/menu.css"/>">

</head>

<body class="d-flex flex-column min-vh-100">

<c:import url="/jsp/UtilitiesToImport/navbar.jsp"/>

<main>
    <c:choose>
        <c:when test="${not empty Questionnaire}">
            <div class="container-fluid min-vh-100">
        </c:when>
        <c:otherwise>
            <div class="container-fluid min-vh-100">
        </c:otherwise>
    </c:choose>

        <div class="row menu_gauche">

            <!-- Menu questionnaire -->
            <div class="col-md-2 menu_gauche" style="background: #158CBA;">
                <ul style="color:white; padding:15px;">
                    <li>
                        <a style="color:white;" href="<c:url value="/salarie?page=modifierProfil"/>">Modifier mon profil</a>
                    </li>
                    <li>
                        <a style="color:white;" href="<c:url value="/salarie/questionnaire"/>">Voir les questionnaires</a>
                    </li>
                    <!--
                    <li>
                        <a style="color:white;" href="<c:url value="/resultat"/>">Voir les statistiques</a>
                    </li>
                    -->
                </ul>
            </div>


            <div class="col">
                <div class="container-fluid mt-3 m_shadow" style="background-color: white" id="container">
                    <div class="card-body center_block">
                    <c:if test="${not empty Message && Message}">
                        <c:import url="${Servlet.VUE_ALERT}"/>
                    </c:if>

                    <h1>Questionnaire</h1>

                    <!-- Tous les questionnaires non répondus du salarié -->
                     <div style="text-align: initial;">

                     <c:if test="${empty Questionnaire && not empty Utilisateur.questionnairesNonRepondus}">
                    <div id="listQuestionnaire">
                        <form method="get">
                            <div class="form-group row">
                                <div class="col-md-auto justify-content-center d-flex align-content-center pt-2">
                                    <label for="${QuestionnairesSelect}">Liste des Questionnaires : </label>
                                </div>
                                <div class="col-md-auto justify-content-center d-flex align-content-center pt-2">
                                    <select id="${QuestionnairesSelect}" name="${QuestionnairesSelect}" class="custom-select">
                                        <c:forEach items="${Utilisateur.questionnairesNonRepondus}" var="questionnaire">
                                            <option value="${questionnaire.nom}">${questionnaire.nom}</option>
                                        </c:forEach>
                                    </select>
                                </div>

                                <div class="col-md-auto justify-content-center d-flex align-content-center pt-2">
                                    <button class="btn btn-primary btn-md" type="submit">Choisir</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    </c:if>
                         <c:if test="${empty Utilisateur.questionnairesNonRepondus}">
                             <div class="col-md-auto justify-content-center d-flex align-content-center pt-2">
                                 <p><u>Vous n'avez pas de questionnaires disponible pour le moment !</u></p>
                             </div>
                         </c:if>

                    <c:if test="${not empty Questionnaire}">

                        <!-- Questionnaire sélectionné -->
                        <div class="row m-2 justify-content-center">
                            <div class="col justify-content-center">
                                <div class="row justify-content-center">
                                    <div class="col-md-auto my-3" id="titreQuestionnaire">
                                    </div>
                                </div>

                                <div class="row justify-content-center">
                                    <div class="col md-auto justify-content-center">
                                        <form method="post" id="FormQuestionnaire">
                                            <!-- Questions -->
                                            <div id="containerQuestionnaire" class="container-fluid row d-flex justify-content-center">
                                            </div>

                                            <!-- Boutons -->
                                            <div class="from-group">
                                                <div class="col-md-auto">
                                                    <div class="row mt-2 d-flex justify-content-center">
                                                        <button class="col-md-2 btn btn-md btn-primary d-flex mt-1 mr-1 ml-2 justify-content-center" id="validateAction" type="button">Valider </button>
                                                        <button class="col-md-2 btn btn-md btn-warning d-flex mt-1 mr-1 ml-2 justify-content-center" type="reset" id="resetAction">Reset</button>
                                                        <a class="col-md-2 btn btn-md btn-danger d-flex mt-1 mr-1 ml-2 justify-content-center" href="<c:url value="/salarie/questionnaire"/>" id="cancelAction">Annuler</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <script>
                            affiche('${Questionnaire}', '${QuestionnaireServlet.QUESTION_INPUT}',
                                '${QuestionnaireServlet.REPONSE_INPUT}', '${QuestionnaireServlet.NB_QUESTIONS_INPUT}');
                        </script>

                    </c:if>
                     </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/jsp/UtilitiesToImport/footer.jsp"/>
<c:import url="/jsp/Questionnaries/modalsQuestionnaries.jsp"/>

</body>

</html>