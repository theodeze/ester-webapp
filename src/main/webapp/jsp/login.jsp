<%@page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.servlet.ConnexionServlet"%>
<%@page import="fr.univangers.esterwebapp.model.modelenum.utilisateur.Role"%>
<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.ForgotPasswordServlet"%>

<c:set var="IDInput" value="${ConnexionServlet.IDENTIFIANT_INPUT}" scope="request"/>
<c:set var="PasswordInput" value="${ConnexionServlet.MOTPASSE_INPUT}" scope="request"/>
<c:set var="TypeInput" value="${ConnexionServlet.TYPE_INPUT}" scope="request"/>
<c:set var="TypeUtilisateur" value="${ConnexionServlet.TYPE_VALUE_UTILISATEUR}" scope="request"/>
<c:set var="TypeSalarie" value="${ConnexionServlet.TYPE_VALUE_SALARIE}" scope="request"/>
<c:set var="TypeEntreprise" value="${ConnexionServlet.TYPE_VALUE_ENTREPRISE}" scope="request"/>

<!DOCTYPE html>
<html lang="fr-FR" class="h-100">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/public/img/ua.png"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/global_style.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/menu_login.css"/>">

    <title>Connexion - ESTER</title>

    <link rel="stylesheet" href="<c:url value="/public/css/libraries/bootstrap.min.css"/>">

    <script src="<c:url value="/public/js/libraries/jquery.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/popper.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/bootstrap.min.js"/>"></script>
    <script>
        function showDiv(name) {
            document.getElementById('divSalarie').style.display = "none";
            document.getElementById('divEntreprise').style.display = "none";
            document.getElementById('divUtilisateur').style.display = "none";
            document.getElementById(name).style.display = "block";
        }

        function timer(lien) {
            window.setTimeout(function () {
                document.location.href="<%=request.getContextPath()%>/" + lien;
            }, 1000);
        }
    </script>

</head>
<body class="d-flex flex-column h-100">

<c:import url="/jsp/UtilitiesToImport/navbar.jsp"/>

<main>
    <div class="container p-2" id="container">

        <c:if test="${not empty Success}">
            <c:if test="${not empty Utilisateur && Utilisateur.role != Role.ENTREPRISE && Utilisateur.role != Role.SALARIE}">
                <script>
                    timer("utilisateur");
                </script>
            </c:if>
            <c:if test="${not empty Utilisateur && Utilisateur.role == Role.ENTREPRISE}">
                <script>
                    timer("entreprise");
                </script>
            </c:if>
            <c:if test="${not empty Utilisateur && Utilisateur.role == Role.SALARIE}">
                <script>
                    timer("salarie");
                </script>
            </c:if>
        </c:if>

        <c:if test="${not empty Message && Message}">
            <c:import url="${Servlet.VUE_ALERT}"/>
        </c:if>

        <div class="row justify-content-center p-2">
            <div class="col-md-auto">
                <h1 class="font-weight-normal text-center">Connexion</h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <button class="btnDiv" id="btnSalarie" onclick="showDiv('divSalarie');">Salarié</button>
            <button class="btnDiv" id="btnEntreprise" onclick="showDiv('divEntreprise');">Entreprise</button>
            <button class="btnDiv" id="btnUtilisateur" onclick="showDiv('divUtilisateur');">Membre Ester</button>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-auto my-1 mx-3">
                <form id="divSalarie" style="display: none;" class="form-signin m_shadow" method="post">
                    <div class="form-group">
                        <label for="${IDInput}">Identifiant</label>
                        <input type="text" id="${IDInput}" name="${IDInput}" class="form-control" value="${empty IDValue ? '' : IDValue}" required>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="col-md-auto">
                            <input type="hidden" name="${TypeInput}" value="${TypeSalarie}" />
                            <button class="btn btn-block btn-primary" type="submit">SE CONNECTER</button>
                        </div>
                    </div>
                </form>

                <form id="divEntreprise" style="display:none;" class="form-signin m_shadow" method="post">
                    <div class="form-group">
                        <label for="${IDInput}">Adresse email ou identifiant</label>
                        <input type="text" id="${IDInput}" name="${IDInput}" class="form-control" value="${empty IDValue ? '' : IDValue}" required>
                    </div>
                    <div class="form-group">
                        <label for="PasswordInputEnt">Mot de passe</label>
                        <input type="password" id="PasswordInputEnt" name="${PasswordInput}" class="form-control" value="${empty PasswordValue ? '' : PasswordValue}" required>
                        <div class="row justify-content-center">
                            <a href="<c:url value="${ForgotPasswordServlet.URL_FORGOT_MOTPASSE}"/>">Mot de passe oublié ?</a>
                        </div>
                    </div>

                    <div class="row justify-content-md-center">
                        <div class="col-md-auto">
                            <input type="hidden" name="${TypeInput}" value="${TypeEntreprise}" />
                            <button id="submitEntreprise" class="btn btn-block btn-primary" type="submit" title="MDP : minimum 8 caractères, au moins un chiffre et une lettre">SE CONNECTER</button>
                        </div>
                    </div>
                </form>

                <form id="divUtilisateur" style="display:none;" class="form-signin m_shadow " method="post">
                    <div class="form-group">
                        <label for="${IDInput}">Adresse email ou identifiant</label>
                        <input type="text" id="${IDInput}" name="${IDInput}" class="form-control" value="${empty IDValue ? '' : IDValue}" required>
                    </div>
                    <div class="form-group">
                        <label for="PasswordInputUtil">Mot de passe</label>
                        <input type="password" id="PasswordInputUtil" name="${PasswordInput}" class="form-control" value="${empty PasswordValue ? '' : PasswordValue}" required>
                        <div class="row justify-content-center">
                            <a href="<c:url value="${ForgotPasswordServlet.URL_FORGOT_MOTPASSE}"/>">Mot de passe oublié ?</a>
                        </div>
                    </div>

                    <div class="row justify-content-md-center">
                        <div class="col-md-auto">
                            <input type="hidden" name="${TypeInput}" value="${TypeUtilisateur}" />
                            <button id="submitUtilisateur" class="btn btn-block btn-primary" type="submit">SE CONNECTER</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</main>

<c:import url="/jsp/UtilitiesToImport/footer.jsp"/>
</body>
</html>