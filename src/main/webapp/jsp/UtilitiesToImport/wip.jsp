<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="fr-FR" class="h-100">
<head>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<c:url value="/public/img/ua.png"/>">

    <title>En cours de Développement...</title>

    <link rel="stylesheet" href="<c:url value="/public/css/default.css"/>">

    <link rel="stylesheet" href="<c:url value="/public/css/libraries/bootstrap.min.css"/>">
    <script src="<c:url value="/public/js/libraries/jquery.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/popper.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/bootstrap.min.js"/>"></script>

</head>
<body>

<c:import url="/jsp/UtilitiesToImport/navbar.jsp"/>


<div class="card">
    <div class="card-header">
        <h5 class="card-title" style='font-weight:bold'>En Développement...</h5>
    </div>
    <div id="histo-div" class="card-body">
        <div class="row justify-content-center">
            <div class="col-md-4 justify-content-center">
                <p style="font-size: 150%;">Désolé, cette page n'est pas encore accessible...</p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-2 justify-content-center">
                <img src="<c:url value="/public/img/worker.png"/>" alt="Women Worker" width="250">
            </div>
        </div>
    </div>
</div>



</body>
</html>