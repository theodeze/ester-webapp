<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<c:url value="/public/img/ua.png"/>">

    <link rel="stylesheet" href="<c:url value="/public/css/global_style.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/menu.css"/>">

    <title>ESTER - Mentions Légales</title>

    <link rel="stylesheet" href="<c:url value="/public/css/libraries/bootstrap.min.css"/>">
    <script src="<c:url value="/public/js/libraries/jquery.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/popper.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/bootstrap.min.js"/>"></script>

</head>
<body class="d-flex flex-column min-vh-100">

<header>
    <c:import url="/jsp/UtilitiesToImport/navbar.jsp"/>
</header>

<main>
    <div class="container p-2 m_shadow" style="background-color: white;margin-top: 10px;">
        <div class="card-body center_block">

        <div class="row justify-content-center align-items-center mb-2">
            <div class="col col-lg-3" style="text-align:center;"><img src="<c:url value="/public/img/irset.png"/>" alt="Logo-ister"></div>
            <div class="col col-lg-3" style="text-align:center;"><img src="<c:url value="/public/img/ua_h.png"/>" alt="Logo-Université-Angers"></div>
        </div>

        <div class="row p-2">
            <div class="col-md-12 container-fluid">
                <h2><b>Mentions Légales</b></h2>

                <h3>Données enregistrées</h3>

                <c:import url="/jsp/UtilitiesToImport/DataAccordeon.jsp"/>
            </div>
        </div>
        </div>
    </div>
</main>
<c:import url="/jsp/UtilitiesToImport/footer.jsp"/>

</body>

<c:import url="/jsp/UtilitiesToImport/modalFirstConnexion.jsp"/>

</html>