<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<footer class="footer mt-auto py-3">
    <div class="container">
        <div class="row justify-content-around p-3">
            <div class="col-md-4" style="text-align:center;"><a href="<c:url value="/jsp/UtilitiesToImport/MentionsLegales.jsp"/>">Mentions Légales</a></div>
            <div class="col-md-4" style="text-align:center;"><a href="<c:url value="http://ester.univ-angers.fr/fr/acces-directs/presentation.html"/>">Projet ESTER</a></div>
            <div class="col-md-4" style="text-align:center;"><a href="<c:url value="http://ester.univ-angers.fr/fr/index.html"/>">Contacts</a></div>
        </div>
    </div>
</footer>