<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="fr.univangers.esterwebapp.model.modelenum.utilisateur.Role"%>

<!-- Menu en header de la page -->

<header class="m_shadow">
<nav class="navbar navbar-expand-lg navbar-dark bg-primary m_border">

    <ul class="navbar-nav mr-auto">
        <!-- Lien Accueil -->
        <li class="nav-item active">
            <a class="nav-link" href="<c:url value="/"/>">Accueil
                <span class="sr-only">(current)</span></a>
        </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
        <c:if test="${empty Utilisateur}">
            <div class="btn-group" role="group">
                <a class="btn btn-light" href="<c:url value="/connexion"/>">SE CONNECTER</a>
            </div>
        </c:if>
        <c:if test="${!empty Utilisateur}">
            <div class="btn-group" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <c:out value="${Utilisateur.identifiant}"/>
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                    <c:if test="${Utilisateur.role == Role.ENTREPRISE}">
                        <a class="dropdown-item" href="<c:url value="/entreprise"/>">Entreprise</a>
                    </c:if>

                    <c:if test="${Utilisateur.role == Role.SALARIE}">
                        <a class="dropdown-item" href="<c:url value="/salarie"/>">Salarie</a>
                        <a class="dropdown-item" href="<c:url value="/salarie/questionnaire"/>">Liste des Questionnaires</a>
                    </c:if>

                    <c:if test="${Utilisateur.role != Role.ENTREPRISE && Utilisateur.role != Role.SALARIE}">

                        <c:if test="${Utilisateur.role == Role.ASSISTANT}">
                            <a class="dropdown-item" href="<c:url value="/utilisateur"/>">Assistant</a></c:if>

                        <c:if test="${Utilisateur.role == Role.MEDECIN}">
                            <a class="dropdown-item" href="<c:url value="/utilisateur"/>">Médecin</a></c:if>

                        <c:if test="${Utilisateur.role == Role.ADMIN}">
                            <a class="dropdown-item" href="<c:url value="/utilisateur"/>">Administrateur</a></c:if>

                        <c:if test="${Utilisateur.role == Role.INFIRMIER}">
                            <a class="dropdown-item" href="<c:url value="/utilisateur"/>">Infirmier</a></c:if>

                        <c:if test="${Utilisateur.role == Role.PREVENTEUR}">
                            <a class="dropdown-item" href="<c:url value="/utilisateur"/>">Préventeur</a></c:if>

                        <c:if test="${Utilisateur.role == Role.ADMIN || Utilisateur.role == Role.MEDECIN}">
                            <a class="dropdown-item" href="<c:url value="/utilisateur?page=genererQuestionnaire"/>">Générateur de Questionnaires</a>
                            <a class="dropdown-item" href="<c:url value="/utilisateur?page=parametresConnexionAutresUtilisateurs"/>">Modifier les comptes des autres utilisateurs</a>
                        </c:if>

                        <a class="dropdown-item" href="<c:url value="/utilisateur?page=parametresConnexion"/>">Modifier votre compte</a>
                        <a class="dropdown-item" href="<c:url value="/utilisateur?page=afficheQuestionnaireESTER"/>">Liste de Questionnaires</a>

                    </c:if>

                    <a class="dropdown-item" href="<c:url value="/deconnexion"/>">Déconnexion</a>
                </div>
            </div>

        </c:if>
    </form>
</nav>
    <c:import url="/jsp/UtilitiesToImport/modalFirstConnexion.jsp"/>
</header>