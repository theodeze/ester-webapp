<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.ServeurMailServlet"%>

<div class="container-fluid row">
    <form id="configurationServeurMail" class="form-signin" method="post">

        <div class="form-label-group">
            <h3>Configuration serveur mail : </h3>
        </div>

        <c:if test='${not empty param[Servlet.PARAM_PAGE]}'>
            <input type="hidden" name="${Servlet.PARAM_PAGE}" value="${param[Servlet.PARAM_PAGE]}" />
        </c:if>

        <input type="hidden" name="${ServeurMailServlet.IDENTIFIANT_INPUT}" value="${serveurMail.identifiant}" />

        <div class="form-row">
            <div class="form-label-group">
                <label for="${ServeurMailServlet.MAIL_INPUT}">Email</label>
                <input type="email" id="${ServeurMailServlet.MAIL_INPUT}" name="${ServeurMailServlet.MAIL_INPUT}" value="${serveurMail.identifiant}" class="form-control" required>
            </div>
        </div>

        <div class="form-row">
            <div class="form-label-group">
                <label for="${ServeurMailServlet.ALIAS_INPUT}">Alias</label>
                <input type="email" id="${ServeurMailServlet.ALIAS_INPUT}" name="${ServeurMailServlet.ALIAS_INPUT}" value="${serveurMail.alias}" class="form-control" required>
            </div>
        </div>

        <div class="form-row">
            <div class="form-label-group">
                <label for="${ServeurMailServlet.MOTPASSE_INPUT}">Mot de passe</label>
                <input type="password" id="${ServeurMailServlet.MOTPASSE_INPUT}" name="${ServeurMailServlet.MOTPASSE_INPUT}" value="${serveurMail.password}" class="form-control" required>
            </div>
        </div>

        <div class="form-row">
            <div class="form-label-group">
                <label for="${ServeurMailServlet.HOST_INPUT}">Host</label>
                <input type="text" id="${ServeurMailServlet.HOST_INPUT}" name="${ServeurMailServlet.HOST_INPUT}" value="${serveurMail.host}" class="form-control" required>
            </div>
        </div>

        <div class="form-row">
            <div class="form-label-group">
                <label for="${ServeurMailServlet.PORT_INPUT}">Port</label>
                <input type="number" id="${ServeurMailServlet.PORT_INPUT}" name="${ServeurMailServlet.PORT_INPUT}" value="${serveurMail.port}" class="form-control"  required>
            </div>
        </div>

        <div class="form-group row justify-content-center d-flex align-content-center pt-2">
            <div class="col-md-auto">
                <button class="btn btn-block btn-primary" type="submit">Valider</button>
            </div>
            <div class="col-md-auto">
                <button class="btn btn-block btn-danger" type="reset">Annuler</button>
            </div>
        </div>

    </form>
</div>