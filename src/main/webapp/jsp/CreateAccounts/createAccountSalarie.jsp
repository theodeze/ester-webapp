<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.servlet.CreateAccounts.CreateAccountSalarieServlet"%>

<div class="row pt-2 container-fluid">
    <div class="row m-2 col-md-12">
        <h5>Création d'un compte Membre ESTER</h5>
    </div>
    <div class="row m-2 col-md-12" id="container_formCreateAccount">

        <form class="col-md-auto" id="saisi" method="post">
            <input class="btn btn-primary" type="submit" value="Génerer code patient "/>
        </form>
    </div>

    <!-- Liste des ID des salariés créés dans les denrières 24h -->
    <div class="row m-2 col-md-12">
        <h5 class="mt-4">Salariés créés dans les dernières 24h :</h5>
    </div>

    <div class="row m-2 pl-3 col-md-12">
        <table class="table table-striped" name="${CreateAccountSalarieServlet.LISTE_SALARIES}">
            <tr><th>Identifiant salarié</th><th> Date de création</th></tr>
            <c:forEach items="${liste_salaries}" var="salarie">
                <tr><td>${salarie.identifiant}</td><td> ${salarie.created}</td></tr>
            </c:forEach>
        </table>
    </div>
</div>



