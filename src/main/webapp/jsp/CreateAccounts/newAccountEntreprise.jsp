<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.CreateAccounts.NewAccountEntrepriseServlet"%>

<c:set var="nameInput" value="${NewAccountEntrepriseServlet.NAME_INPUT}" scope="request"/>
<c:set var="NewPasswordInput" value="${NewAccountEntrepriseServlet.NEW_MOTPASSE_INPUT}" scope="request"/>
<c:set var="ConfirmPasswordInput" value="${NewAccountEntrepriseServlet.CONFIRM_MOTPASSE_INPUT}" scope="request"/>
<c:set var="ValueToken" value="${NewAccountEntrepriseServlet.TOKEN_VALUE}" scope="request"/>
<c:set var="mail" value="${NewAccountEntrepriseServlet.TOKEN_INPUT}" scope="request"/>


<!DOCTYPE html>
<html lang="fr-FR" class="h-100">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<c:url value="/public/img/ua.png"/>">

    <title>Creation de l'entreprise - ESTER</title>

    <link rel="stylesheet" href="<c:url value="/public/css/libraries/bootstrap.min.css"/>">
    <script src="<c:url value="/public/js/libraries/jquery.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/popper.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/public/js/ownFiles/check_password_format.js"/>"></script>
    <script src="<c:url value="/public/js/ownFiles/check_password.js"/>"></script>

</head>

<body class="d-flex flex-column h-100">

<c:import url="/jsp/UtilitiesToImport/navbar.jsp"/>

<main>
    <div class="container-fluid p-2" id="container">
        <div class="row justify-content-center">
            <div class="text-center m-2">

                <c:if test="${not empty Message && Message}">
                    <c:import url="${Servlet.VUE_ALERT}"/>
                </c:if>

                <div class="row justify-content-center">
                    <div class="col-md-auto">
                        <h1 class="font-weight-normal text-center">Création du compte</h1>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-md-auto my-1 mx-3">
                        <form class="form-signin" method="post">

                            <div class="form-group">
                                <label for="${nameInput}">Saisir un nom</label>
                                <input type="text" name="${nameInput}" id="${nameInput}" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="${NewPasswordInput}">Saisir un mot de passe</label>
                                <input type="password" name="${NewPasswordInput}" id="${NewPasswordInput}" class="form-control" value="${empty NewPasswordValue ? '' : NewPasswordValue}"  onkeyup="checkFormat('${ConfirmPasswordInput}')" required>
                            </div>
                            <div class="form-group">
                                <label for="${ConfirmPasswordInput}">Confirmer le mot de passe</label>
                                <input type="password" name="${ConfirmPasswordInput}" id="${ConfirmPasswordInput}" class="form-control" value="${empty ConfirmPasswordValue ? '' : ConfirmPasswordValue}"  onkeyup="checkFormat('${ConfirmPasswordInput}')" required>
                            </div>
                            <div class="row justify-content-md-center">
                                    <button class="col-md-5 btn btn-primary mr-2" type="submit" disabled="true" id="submitPasswordReset" title="MDP : minimum 8 caractères, au moins un chiffre et une lettre">Valider</button>
                                    <button class="col-md-5 btn btn-danger" type="reset" id="ResetPasswordReset">Annuler</button>
                            </div>
                            <input type="hidden" name="${TokenInput}" value="${TokenValue}" />
                            <div class="row justify-content-md-center">
                                <div class="col-md-auto">
                                    <span id="wrongFormatPasswordText" hidden> Votre mot de passe doit faire au moins 8 caractères, au moins un chiffre et une lettre</span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/jsp/UtilitiesToImport/footer.jsp"/>

</body>
</html>