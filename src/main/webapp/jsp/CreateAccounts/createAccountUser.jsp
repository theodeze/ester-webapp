<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.model.Utilisateur"%>
<%@page import="fr.univangers.esterwebapp.model.modelenum.utilisateur.Role"%>
<%@page import="fr.univangers.esterwebapp.servlet.CreateAccounts.CreateAccountUserServlet"%>

<div class="row pt-2">
    <div class="row m-2 col-md-auto">
        <h5>Création d'un compte Membre ESTER</h5>
    </div>
    <div class="row m-2 container-fluid" id="container_formCreateAccount">
        <div class="col-md-auto d-flex justify-content-center">
            <form method="post">
                <div class="form-group row">
                    <label class="col-form-label" for="${CreateAccountUserServlet.COMPTE_SELECT}">Choisissez un type de compte : </label>
                    <div class="col-md-auto">
                        <select id="${CreateAccountUserServlet.COMPTE_SELECT}" name="${CreateAccountUserServlet.COMPTE_SELECT}" class="custom-select">
                            <c:if test="${Utilisateur.role == Role.ADMIN}">
                                <option value="Administrateur">Administrateur</option>
                            </c:if>
                            <c:if test="${Utilisateur.role == Role.MEDECIN || Utilisateur.role == Role.ADMIN}">
                                <option value="Médecin">Médecin</option>
                                <option value="Infirmier">Infirmier</option>
                                <option value="Préventeur">Préventeur</option>
                                <option value="Assistant">Assistant</option>
                            </c:if>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-auto">
                        <label for="${CreateAccountUserServlet.EMAIL_INPUT}">Email à entrer : </label>
                    </div>
                    <div class="col-md-auto">
                        <input type="email" name="${CreateAccountUserServlet.EMAIL_INPUT}" id="${CreateAccountUserServlet.EMAIL_INPUT}" placeholder="nom@exemple.com" class="form-control" required/>
                    </div>
                </div>
                <div class="form-group row justify-content-center d-flex align-content-center">
                    <div class="col-md-auto">
                        <button class="btn btn-block btn-primary" type="submit">Valider</button>
                    </div>
                    <div class="col-md-auto">
                        <button class="btn btn-block btn-danger" type="reset">Annuler</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

