    $(document).ready(function () {

        var questionnaryListSession = typeof $.session.get('questionnaryListSession') == 'string' ? JSON.parse($.session.get('questionnaryListSession')) : new Array();

        questionnaryListSession.forEach(function (item, key) {
            var dateToTest = new Date();
            var tempDate = new Date(item.currentDate);
            tempDate.setHours(tempDate.getHours()+24);
            if(tempDate.getDate() < dateToTest.getDate()){
                questionnaryListSession.splice(key);
            }
        });

        var questionnaryListDataBase = new Array();
        var currentQuestionnary = {};
        var savedQuestionnary = {};
        var actionForm;
        var currentQuestionnaryKey;
        var typeQuestionnaryWhereSave;
        var widthWindow = $(window).width();
        var heightWindow = $(window).height();
        $('#popupAddQuestion').hide();
        $('.onFormShowed').hide();

        var EnteteQuestions = function(numQuestion, order) {
            return "<div class=\"row question questionCard mr-1 mb-1\">\n" +
                "<div class='col-md-10'>\n" +
                "<div class=\"row m-2\">\n" +
                "<div class=\"col-md-1\">" +
                "<span class='questionnaryTitle'>" + numQuestion + ".&nbsp;</span>" +
                "</div>" +
                "<div class=\"col-md-1 d-flex ml-auto mr-2 row\">\n" +
                "<a class=\"col-md-12 pullRight questionIcon moveUpQuestion\" id=\"moveUpQuestion-" + order + "\" title=\"Monter la question\"><i class=\"fas fa-angle-up fa-3x d-flex justify-content-center\"></i> </a>\n" +
                "<a class=\"col-md-12 pullRight questionIcon moveDownQuestion\" id=\"moveDownQuestion-" + order + "\" title=\"Descendre la question\"><i class=\"fas fa-angle-down fa-3x d-flex justify-content-center\"></i> </a>\n" +
                "</div>\n" +
                "<a class=\"col-md-1 ml-2 d-flex pullRight questionIcon deleteQuestion mt-1\" id=\"deleteQuestion-" + order + "\" title=\"Supprimer la question\">&nbsp;<i class=\"fas fa-trash fa-3x d-flex justify-content-center align-content-center\"></i></a>\n" +
                "</div>\n" ;
        }


        // Selection dans le menu : création, modification, duplication ...
        $('#selectQuestionnaryAction').change(function () {
            switch ($(this).val()) {
                case "1" :
                    actionForm = 'create';
                    currentQuestionnary = {
                        nom: '',
                        version: 1,
                        droitsDeModification: [],
                        questionsCollection: [],
                        currentDate: new Date()
                    };
                    savedQuestionnary = new Array();

                    $('#questionnaryNameContainer').empty();
                    $('#displayedQuestionnaryQuestionList').empty();
                    $('.onFormDisabled').prop("disabled", true);
                    $('.onFormShowed').show();
                    $('#popupAddQuestion').show();
                    $('#popupSharePrivilege').prop("disabled", false);
                    $('#questionnaryNameContainer').append(
                        '<div class="col-md-8">' +
                        '   <input class="form-control" type="text" id="questionnaryNameInput" placeholder="Nom de votre questionnaire" required/>' +
                        '</div>'
                    );
                    $("#selectQuestionnaryAction").val("0");
                    refreshUsersRights();
                    break;

                case "2" :
                    actionForm = 'duplicate';
                    currentQuestionnary = {
                        nom: JSON.parse(JSON.stringify(currentQuestionnary.nom)) + "_Copie",
                        version: 1,
                        droitsDeModification: [],
                        questionsCollection: JSON.parse(JSON.stringify(currentQuestionnary.questionsCollection)),
                        currentDate: new Date()
                    };
                    savedQuestionnary = JSON.parse(JSON.stringify(currentQuestionnary));
                    UpdateQuestionnary();
                    $("#selectQuestionnaryAction").val("0");
                    // TODO Dupliquer questionnaire
                    break;

                case "3" :
                    actionForm = 'update';
                    savedQuestionnary = JSON.parse(JSON.stringify(currentQuestionnary));
                    currentQuestionnary.version += 1;
                    currentQuestionnary.currentDate=new Date();
                    UpdateQuestionnary();
                    $("#selectQuestionnaryAction").val("0");
                    // TODO Modifier questionnaire
                    break;

                case "4" :
                    actionForm = 'delete';
                    $("#selectQuestionnaryAction").val("0");
                    $('#popupSharePrivilege').prop("disabled", false);
                    $('#DeletingQuestionnary').modal('show');
                    break;
            }
        });

        // Validation de suppression d'un questionnaire
        $(document).on('click', '#ValidateDeleting', function () {
            if(typeQuestionnaryWhereSave == 'questionInDataBase') {
                $('#WaitedDemand').find('#typeDemand').html("Suppresion du questionnaire dans la base de données...");
                $("#WaitedDemand").modal("show");
                $.ajax({
                    method: "POST",
                    url: "genererQuestionnaire",
                    data: {
                        nameToDelete: currentQuestionnary.nom
                    },
                    dataType: 'json',
                    success: function () {
                        $("#WaitedDemand").modal("hide");

                        for (var index = 0; index < questionnaryListDataBase.length; ++index) {
                            if (questionnaryListDataBase[index] == currentQuestionnary.nom) {
                                questionnaryListDataBase.splice(index, 1);
                                break;
                            }
                        }
                        $('#MsgSuccess').html("La suppression du questionnaire dans la base de données s'est effectuée avec succès.");
                        $('#BDDSuccess').modal('show');

                        window.setTimeout(function () {
                            document.location.reload();
                        }, 1500);

                    },
                    error: function () {
                        $('#MsgErreur').html("Une erreur s'est produite lors de la suppression de votre questionnaire. Veuillez réessayer ultérieurement.");
                        $('#ErreurSaisie').modal('show');
                        $("#WaitedDemand").modal("hide");
                        window.setTimeout(function () {
                            $("#ErreurSaisie").modal("hide");
                        }, 1500);
                    }
                });
            }
            else {
                clearSessionQuestionnaries();
            }
            DisableGeneratorButtons();
            refreshQuestionnaryList();
        });


        $('#questionShortAnswer').on("click", function () {
            var order = currentQuestionnary.questionsCollection.length;

            currentQuestionnary.questionsCollection.push({
                typeQuestion: 'shortAnswer',
                displayedType: 'Question à réponse rédigée courte',
                intituleQuestion: '',
                scoreMaxQuestion: 0,
                optionsQuestion: []
            });

            var numQuestion = order + 1;
            $('#displayedQuestionnaryQuestionList').append(
                EnteteQuestions(numQuestion,order) +

                "<div class='row'>" +
                "<div class='col-md-12'>" +
                "<input type=\"text\" class=\"form-control libelleQuestion\" id=\"libelleQuestion-" + order + "\" placeholder=\"Intitulé de la question\" required/>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>"
            );
            FixIconSize();

            $('#questionModal').modal('hide');
        });

        $('#questionLongAnswer').on("click", function () {
            var order = currentQuestionnary.questionsCollection.length;
            currentQuestionnary.questionsCollection.push({
                typeQuestion: 'longAnswer',
                displayedType: 'Question à réponse rédigée longue',
                intituleQuestion: '',
                scoreMaxQuestion: 0,
                optionsQuestion: []
            });

            var numQuestion = order + 1;
            $('#displayedQuestionnaryQuestionList').append(
                EnteteQuestions(numQuestion,order) +

                "<div class='row'>" +
                "<div class='col-md-12'>" +
                "<textarea cols='10' rows='4' type=\"text\" class=\"form-control libelleQuestion\" id=\"libelleQuestion-" + order + "\" placeholder=\"Intitulé de la question\" required/>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>\n" +
                "</div>"
            );

            FixIconSize();
            $('#questionModal').modal('hide');
        });

        $('#questionRadio').on("click", function () {
            var order = currentQuestionnary.questionsCollection.length;

            currentQuestionnary.questionsCollection.push({
                typeQuestion: 'radio',
                displayedType: 'Question à réponse unique prédéfinie',
                intituleQuestion: '',
                scoreMaxQuestion: 0,
                optionsQuestion: []
            });
            var numQuestion = order + 1;

            $('#displayedQuestionnaryQuestionList').append(
                EnteteQuestions(numQuestion,order)+

                "<div class=\"row pb-1\">\n" +
                "<div class='col-md-8'>" +
                "<input type=\"text\" class=\"form-control libelleQuestion\" id=\"libelleQuestion-" + order + "\" placeholder=\"Intitulé de la question\" required/>\n" +
                "</div>" +
                "<div class='col-md-4'>" +
                "<input type=\"number\" class=\"form-control baremQuestion\" disabled id=\"baremQuestion-" + order + "\" placeholder=\"Score maximal de la question\" value='0' required/>\n" +
                "</div>" +
                "</div>\n" +
                "<div class=\"row m-2\">\n" +
                "<a class=\"col-md-2 pt-2 pullRight addAnswer\" id='addAnswer-" + order + "' title=\"Ajouter une proposition de réponse\">&nbsp;<i class=\"fas fa-plus fa-lg\"></i></a>" +
                "</div>\n" +
                "<div class=\"pt-2\" id='answerContainer-" + order + "'>" +
                "</div>\n" +
                "</div>\n" +
                "</div>"
            );

            FixIconSize();

            $('#questionModal').modal('hide');
        });

        $('#questionPicture').on("click", function () {
            currentQuestionnary.questionsCollection.push({
                typeQuestion: 'questionPicture',
                displayedType: 'Image',
                intituleQuestion: '',
                scoreMaxQuestion: 0
            });

            $('#questionModal').modal('hide');
        });

        $('#questionVideo').on("click", function () {
            currentQuestionnary.questionsCollection.push({
                typeQuestion: 'questionVideo',
                displayedType: 'Vidéo',
                intituleQuestion: '',
                scoreMaxQuestion: 0,
                optionsQuestion: []
            });

            $('#questionModal').modal('hide');
        });

        $(document).on('keyup', '.baremAnswer', function () {
            var questionOrder = $(this).attr('id').split('-')[1];
            var totalBarem = 0;
            currentQuestionnary.questionsCollection[questionOrder].optionsQuestion[$(this).attr('id').split('-')[2]].score = parseInt($(this).val());
            currentQuestionnary.questionsCollection[questionOrder].optionsQuestion.forEach(function (thisAnswer) {
                if (thisAnswer.score > totalBarem)
                    totalBarem = thisAnswer.score;
            });

            currentQuestionnary.questionsCollection[questionOrder].scoreMaxQuestion = totalBarem;
            $('#baremQuestion-' + questionOrder).val(totalBarem);
        });

        $(document).on('change', '.libelleQuestion', function () {
            currentQuestionnary.questionsCollection[$(this).attr('id').split('-')[1]].intituleQuestion = $(this).val();
        });

        $(document).on('click', '.deleteAnswer', function () {
            var orderToDelete = parseInt($(this).attr('id').split('-')[2]);
            var questionOrder = parseInt($(this).attr('id').split('-')[1]);
            for (var key in currentQuestionnary.questionsCollection[questionOrder].optionsQuestion) {
                if (currentQuestionnary.questionsCollection[questionOrder].optionsQuestion.hasOwnProperty(key)) {
                    if (key >= orderToDelete && key != currentQuestionnary.questionsCollection[questionOrder].optionsQuestion.length - 1) {
                        var newOrder = parseInt(key) + 1;
                        currentQuestionnary.questionsCollection[questionOrder].optionsQuestion[key] = currentQuestionnary.questionsCollection[questionOrder].optionsQuestion[newOrder];
                    }
                    if (key == currentQuestionnary.questionsCollection[questionOrder].optionsQuestion.length - 1) {
                        currentQuestionnary.questionsCollection[questionOrder].optionsQuestion.pop();
                    }
                }
            }
            refreshAnswerList(questionOrder);
        });

        $(document).on('click', '.addAnswer', function () {
            var order = $(this).attr('id').split('-')[1];
            currentQuestionnary.questionsCollection[$(this).attr('id').split('-')[1]].optionsQuestion.push(
                {
                    option: '',
                    score: 0
                }
            );

            var numAnswer = (currentQuestionnary.questionsCollection[$(this).attr('id').split('-')[1]].optionsQuestion.length) - 1;
            $('#answerContainer-' + $(this).attr('id').split('-')[1]).append(
                "<div class=\"row m-2\">\n" +
                "<div class=\"col-md-7\">\n" +
                "<input type=\"text\" class='libelleAnswer form-control' placeholder=\"Intitulé de la réponse\" id='libelleAnswer-" + order + "-" + numAnswer + "' required/>\n" +
                "</div>\n" +
                "<div class=\"col-md-3\">\n" +
                "<input type=\"text\" class='baremAnswer form-control' placeholder=\"Score de la réponse\" id='baremAnswer-" + order + "-" + numAnswer + "' required/>\n" +
                "</div>\n" +
                "<div class=\"col-md-2\">\n" +
                "<a class=\"d-flex questionIcon deleteAnswer pullLeft mt-1\" id=\"deleteAnswer-" + order + "-" + numAnswer + "\" title=\"Supprimer la réponse\">&nbsp;<i class=\"fas fa-trash fa-lg\"></i></a>\n" +
                "</div>\n" +
                "</div>\n"
            );
            FixIconSize();
        });

        var displayQuestionnaryList = function () {
            var questionnaryContainer = $('#questionnaryListContainerSession');
            questionnaryListSession.forEach(function (item, key) {
                questionnaryContainer.append(
                    "<div id=" + key + " class=\"questionnary row pb-1\">\n" +
                    "<div class=\"col-md-12 canBeSelected QuestionnaryListElement questionInSession questionnaryCard\">\n" +
                    "<span class=\"questionnaryTitle\">" + item.nom + "</span>\n" +
                    "</div>\n" +
                    "</div>"
                );
            });

            questionnaryContainer = $('#questionnaryListContainerDataBase');
            questionnaryListDataBase.forEach(function (item, key) {
                questionnaryContainer.append(
                    "<div id=" + item+"_"+key + " class=\"questionnary row pb-1\">\n" +
                    "<div class=\"col-md-12 canBeSelected QuestionnaryListElement questionInDataBase questionnaryCard\">\n" +
                    "<span class=\"questionnaryTitle\">" + item + "</span>\n" +
                    "</div>\n" +
                    "</div>"
                );
            });
        };

        var refreshAnswerList = function (questionOrder) {
            currentQuestionnary.questionsCollection[questionOrder].optionsQuestion.forEach(function (value, index) {
                index++;
            })
            $('#answerContainer-' + questionOrder).empty();

            $('#answerContainer-' + questionOrder).append(
            );
            var maxScore = 0;
            currentQuestionnary.questionsCollection[questionOrder].optionsQuestion.forEach(function (thisAnswer, order) {
                $('#answerContainer-' + questionOrder).append(
                    "<div class=\"row m-2\">\n" +
                    "<div class=\"col-md-7\">\n" +
                    "<input type=\"text\" class='libelleAnswer form-control' placeholder=\"Intitulé de la réponse\" id='libelleAnswer-" + questionOrder + "-" + order + "' value=\"" + thisAnswer.option + "\" required/>\n" +
                    "</div>\n" +
                    "<div class=\"col-md-3\">\n" +
                    "<input type=\"text\" class='baremAnswer form-control' placeholder=\"Score de la réponse\" id='baremAnswer-" + questionOrder + "-" + order + "' value=\"" + thisAnswer.score.toString() + "\" required/>\n" +
                    "</div>\n" +
                    "<div class=\"col-md-2\">\n" +
                    "<a class=\"d-flex questionIcon deleteAnswer pullLeft mt-1\" id=\"deleteAnswer-" + questionOrder + "-" + order + "\" title=\"Supprimer la réponse\">&nbsp;<i class=\"fas fa-trash fa-lg\"></i></a>\n" +
                    "</div>\n" +
                    "</div>\n"
                );
                if (thisAnswer.score > maxScore) {
                    maxScore = thisAnswer.score;
                }
                FixIconSize();
            });

            $('#baremQuestion-' + questionOrder).val(maxScore);
        }

        var refreshQuestionnaryQuestionList = function () {
            $('#displayedQuestionnaryQuestionList').empty();

            if (currentQuestionnary.questionsCollection == undefined) {
                currentQuestionnary.questionsCollection = new Array();
            }

            if (currentQuestionnary.questionsCollection != undefined) {
                currentQuestionnary.questionsCollection.forEach(function (thisQuestion, order) {
                    var numQuestion = order + 1;
                    switch (thisQuestion.typeQuestion) {
                        case 'shortAnswer' :
                            $('#displayedQuestionnaryQuestionList').append(
                                EnteteQuestions(numQuestion,order) +

                                "<div class='row'>" +
                                "<div class='col-md-12'>" +
                                "<input type=\"text\" class=\"form-control libelleQuestion\" id=\"libelleQuestion-" + order + "\" placeholder=\"Intitulé de la question\" value=\"" + thisQuestion.intituleQuestion + "\" required/>\n" +
                                "</div>\n" +
                                "</div>\n" +
                                "</div>\n" +
                                "</div>"
                            );
                            FixIconSize();
                            break;
                        case 'longAnswer' :
                            $('#displayedQuestionnaryQuestionList').append(
                                EnteteQuestions(numQuestion,order) +

                                "<div class='row'>" +
                                "<div class='col-md-12'>" +
                                "<textarea cols='10' rows='4' type=\"text\" class=\"form-control libelleQuestion\" id=\"libelleQuestion-" + order + "\" placeholder=\"Intitulé de la question\" required> " + thisQuestion.intituleQuestion + "</textarea> " +
                                "</div>\n" +
                                "</div>\n" +
                                "</div>\n" +
                                "</div>"
                            );
                            FixIconSize();
                            break;
                        case 'radio' :
                            $('#displayedQuestionnaryQuestionList').append(
                                EnteteQuestions(numQuestion,order) +

                                "<div class=\"row\">\n" +
                                "<div class='col-md-9'>" +
                                "<input type=\"text\" class=\"form-control libelleQuestion\" id=\"libelleQuestion-" + order + "\" placeholder=\"Intitulé de la question\" value=\"" + thisQuestion.intituleQuestion + "\" required/>\n" +
                                "</div>" +
                                "<div class='col-md-3'>" +
                                "<input type=\"number\" class=\"form-control baremQuestion\" disabled id=\"baremQuestion-" + order + "\" placeholder=\"Score maximal de la question\" value=\"" + thisQuestion.scoreMaxQuestion + "\" required/>\n" +
                                "</div>" +
                                "</div>\n" +
                                "<div class=\"row m-2\">\n" +
                                "<a class=\"col-md-2 pullRight addAnswer\" id='addAnswer-" + order + "' title=\"Ajouter une proposition de réponse\">&nbsp;<i class=\"fas fa-plus fa-lg\"></i></a>" +
                                "</div>\n" +
                                "<div id='answerContainer-" + order + "'>" +
                                "</div>\n" +
                                "</div>\n" +
                                "</div>"
                            );
                            FixIconSize();
                            refreshAnswerList(order);
                            break;

                    }
                });
            }
        };


        $(document).on('click', '.moveDownQuestion', function () {
            var order = parseInt($(this).attr('id').split('-')[1]);
            var nextOrder = order + 1;
            if (order != currentQuestionnary.questionsCollection.length - 1 && currentQuestionnary.questionsCollection.length != 1) {
                var switchSave = currentQuestionnary.questionsCollection[nextOrder];
                currentQuestionnary.questionsCollection[nextOrder] = currentQuestionnary.questionsCollection[order];
                currentQuestionnary.questionsCollection[order] = switchSave;
                refreshQuestionnaryQuestionList();
            } else {
                $('#MsgErreur').html("Vous essayez de descendre une question en dernière position.");
                $('#ErreurSaisie').modal('show');
            }
        });

        $(document).on('click', '.moveUpQuestion', function () {
            var order = parseInt($(this).attr('id').split('-')[1]);
            var nextOrder = order - 1;
            if (order != 0 && currentQuestionnary.questionsCollection.length != 1) {
                var switchSave = currentQuestionnary.questionsCollection[nextOrder];
                currentQuestionnary.questionsCollection[nextOrder] = currentQuestionnary.questionsCollection[order];
                currentQuestionnary.questionsCollection[order] = switchSave;
                refreshQuestionnaryQuestionList();
            } else {
                $('#MsgErreur').html('Vous essayez de monter une question en première position.');
                $('#ErreurSaisie').modal('show');
            }
        });



        $(document).on('click', '.deleteQuestion', function () {
            functionToDelete($(this),currentQuestionnary.questionsCollection);
            refreshQuestionnaryQuestionList();
        });


        $(document).on('change', '.libelleAnswer', function () {
            if (currentQuestionnary.questionsCollection[$(this).attr('id').split('-')[1]].optionsQuestion[$(this).attr('id').split('-')[2]].option != undefined)
                currentQuestionnary.questionsCollection[$(this).attr('id').split('-')[1]].optionsQuestion[$(this).attr('id').split('-')[2]].option = $(this).val();
        });

        $(document).on('change', '#questionnaryNameInput', function () {
            currentQuestionnary.nom = $(this).val();
        });

        var getQuestionnaryInDB = function (jsonQuestionnaire){
            currentQuestionnary= {
                nom: JSON.parse(JSON.stringify(jsonQuestionnaire.nom)),
                version:JSON.parse(JSON.stringify(jsonQuestionnaire.version)),
                droitsDeModification: JSON.parse(JSON.stringify(jsonQuestionnaire.emailModificateurs)),
                questionsCollection :[],
                currentDate:new Date()
            };
            var questionsList =  jsonQuestionnaire.questionJSONS;
            questionsList.forEach(function (item, key) {
                currentQuestionnary.questionsCollection[key]={
                    typeQuestion:JSON.parse(JSON.stringify(item.typeQuestion.nom)),
                    intituleQuestion: JSON.parse(JSON.stringify(item.intitule)),
                    scoreMaxQuestion: JSON.parse(JSON.stringify(item.scoreMax)),
                    optionsQuestion : []
                };
                switch ( currentQuestionnary.questionsCollection[key].typeQuestion) {
                    case 'radio':
                        currentQuestionnary.questionsCollection[key].displayedType = "Question à réponse unique prédéfinie";
                        break;
                    case 'shortAnswer' :
                        currentQuestionnary.questionsCollection[key].displayedType = "Question à réponse rédigée courte";
                        break;
                    case 'longAnswer':
                        currentQuestionnary.questionsCollection[key].displayedType = "Question à réponse rédigée longue";
                        break;
                }

                item.optionsQuestions.forEach(function (subItem, subKey) {
                    currentQuestionnary.questionsCollection[key].optionsQuestion[subKey]={
                        option: JSON.parse(JSON.stringify(subItem.valeur)),
                        score: JSON.parse(JSON.stringify(subItem.score))
                    }
                });
            });
        };

        var displayQuestionnaryBeforeModification = function(currentQuestionnaryKey, displayedQuestionnaryQuestionList){
            displayedQuestionnaryQuestionList.empty();

            $('#selectQuestionnaryAction option[value="2"]').removeAttr('disabled'); // On réactive la duplication
            if(currentQuestionnary.nom !=="Eval-Risk-TMS : Risque de lombalgie chronique"&&currentQuestionnary.nom!=="Eval-Risk-TMS : Risque de SMS-MS chronique"){
                $('#selectQuestionnaryAction option[value="3"]').removeAttr('disabled'); // On réactive la modification
                $('#selectQuestionnaryAction option[value="4"]').removeAttr('disabled'); // On réactive la suppression
            }
            

            $('#questionnaryNameContainer').append(
                '<div class="titleCard questionnaryTitle" style="text-align: center;"> ' +
                '<span style="text-align: center;">' + currentQuestionnary.nom + '</span>' +
                ' </div>'
            );

            if (currentQuestionnary.questionsCollection != undefined) {
                currentQuestionnary.questionsCollection.forEach(function (item, key) {
                    var questionNum = key + 1;
                    switch (item.typeQuestion) {
                        case 'shortAnswer' :
                        case 'longAnswer' :
                            displayedQuestionnaryQuestionList.append("\n" +
                                "<div id=" + key + " class=\"questionCard mb-1\">" +
                                "<div id=" + key + " class=\"row question\">\n" +
                                "<div class='col-md-12'>" +
                                "<span class='questionnaryTitle'>" + questionNum + '. ' + item.intituleQuestion + "</span>\n" +
                                "<span class='pullRight'>" + item.displayedType + "</span>\n" +
                                "</div>" +
                                "</div>" +
                                "</div>"
                            );
                            break;

                        case 'radio' :
                            displayedQuestionnaryQuestionList.append("\n" +
                                "<div id=" + key + " class=\"questionCard mb-1\">" +
                                "<div id=" + key + " class=\"row question\">\n" +
                                "<div class='col-md-6'>" +
                                "<span class='questionnaryTitle'>" + questionNum + '. ' + item.intituleQuestion + "</span>\n" +
                                "</div>" +
                                "<div class='col-md-6'>" +
                                "<span class='questionnaryTitle'>" + item.scoreMaxQuestion + "</span>\n" +
                                "<span class='pullRight'>" + item.displayedType + "</span>\n" +
                                "</div>" +
                                "</div>" +
                                "</div>"
                            );
                            item.optionsQuestion.forEach(function (thisAnswer) {
                                $("#" + key + ".questionCard").append(
                                    "<div class=\"row answerCard m-2\">\n" +
                                    "<div class=\"col-md-5\">\n" +
                                    "<span> Intitulé de réponse : " + thisAnswer.option + " </span>\n" +
                                    "</div>\n" +
                                    "<div class=\"col-md-3\">\n" +
                                    "<span> Point(s) attribué(s) : " + thisAnswer.score + "</span>\n" +
                                    "</div>\n" +
                                    "</div>\n"
                                );
                            });
                            break;
                    }
                });
            }
        };


        $(document).on('click', '.questionnary', function () {
            currentQuestionnaryKey = $(this).attr('id');
            var displayedQuestionnaryQuestionList = $('#displayedQuestionnaryQuestionList');

            if ($(this).children('div').hasClass('questionInSession')) {
                currentQuestionnary = questionnaryListSession[currentQuestionnaryKey];
                currentQuestionnary.version-=1;     // pour garder la même version après modification (seulement les fichiers en local)
                displayQuestionnaryBeforeModification(currentQuestionnaryKey, displayedQuestionnaryQuestionList); // Afin que les modifications se fasse à partir du nouveau questionnaire courant et non un ancien
                typeQuestionnaryWhereSave = 'questionInSession';
                $('#questionnaryListModal').modal('hide');
            }
            else if ($(this).children('div').hasClass('questionInDataBase')) {
                $('#WaitedDemand').find('#typeDemand').html("Récupération du questionnaire depuis la base de données en cours...");
                $('#questionnaryListModal').modal('hide');
                $("#WaitedDemand").modal("show");
                $.ajax({
                    method: "GET",
                    url: "genererQuestionnaire",
                    data:{
                        needed : "AllOfOne",
                        text : $(this).children('div').children('span').text(),
                    },
                    cache:false,
                    dataType: 'json',
                    success: function (jsonQuestionnaire, status) {
                        getQuestionnaryInDB(jsonQuestionnaire);
                        displayQuestionnaryBeforeModification(currentQuestionnaryKey, displayedQuestionnaryQuestionList); // Afin que les modifications se fasse à partir du nouveau questionnaire courant et non un ancien
                        $("#WaitedDemand").modal("hide");
                        typeQuestionnaryWhereSave = 'questionInDataBase';
                    },
                    error: function () {
                        $('#MsgErreur').html("Il y a eu un souci lors de la récupération du questionnaire. Veuillez réessayer ultérieurement");
                        $('#ErreurSaisie').modal('show');
                        $("#WaitedDemand").modal("hide");
                        window.setTimeout(function () {
                            $("#ErreurSaisie").modal("hide");
                        }, 1500);
                    }
                });
            }
        });

        $('#popupQuestionnaryList').on("click", function () {
            $('#questionnaryListModal').modal('show');
        });

        $('#popupSharePrivilege').on("click", function () {
            refreshUsersRights();
            $('#privilegeModal').modal('show');
        });

        $('#popupAddQuestion').on("click", function () {
            $('#questionModal').modal('show');
        });

        $('#cancelAction').on("click", function () {
            $("#selectQuestionnaryAction").val("0");
            if (savedQuestionnary !== []) {
                currentQuestionnary = JSON.parse(JSON.stringify(savedQuestionnary));
                UpdateQuestionnary();
            }
        });

        $('#resetAction').on("click", function () {
            currentQuestionnary = {};
            DisableGeneratorButtons();
            refreshUsersRights();
        });

        $('#saveTempo').on("click",function () {
            if (actionForm == 'create' || actionForm == 'duplicate' || actionForm == 'update') {
                for(var index = 0 ; index < questionnaryListSession.length; ++index){
                    if(questionnaryListSession[index].nom == currentQuestionnary.nom ){
                        questionnaryListSession.splice(index,1);
                    }
                }
                questionnaryListSession.push(currentQuestionnary);
            }

            $.session.set('questionnaryListSession', JSON.stringify(questionnaryListSession));
            refreshQuestionnaryList();
        });

        $('#validateAction').on("click", function () {
            var bool = true;
            $('.containerInsideQuestionnary').find('input').each(function () {
                if ($(this).val() === undefined || $(this).val() === '') {
                    $('#MsgErreur').html("Navré mais il vous reste des <u>champs vides</u>. Remplisser les balises vides avant de le valider.");
                    bool = false;
                }
            });

            if(bool) {
                $('.containerInsideQuestionnary').find('textarea').each(function () {
                    if ($(this).val() === undefined || $(this).val() === '') {
                        $('#MsgErreur').html("Navré mais il vous reste des <u>champs vides</u>. Remplisser les balises vides avant de le valider.");
                        bool = false;
                    }
                });
            }

            if (actionForm != 'update' && bool === true) {
                questionnaryListDataBase.forEach(function (item, key) {
                    if (item === $('#questionnaryNameInput').val()) {
                        $('#MsgErreur').html("Navré mais le <u> nom saisi</u> est déjà pris. Changez le nom de votre questionnaire avant de le valider.");
                        bool = false;
                    }
                });
            }

            if (bool == true) {

                $('#WaitedDemand').find('#typeDemand').html("Envoie du nouveau questionnaire vers la Base de données en cours...");
                $("#WaitedDemand").modal("show");
                $.ajax({
                    method: "POST",
                    url: "genererQuestionnaire",
                    data: {
                        json: JSON.stringify(currentQuestionnary)
                    },
                    dataType: 'json',
                    success: function () {
                        $("#WaitedDemand").modal("hide");
                        $('#MsgSuccess').html("L'enregistrement du questionnaire dans la base de données s'est effectué avec succès.");
                        $('#BDDSuccess').modal('show');
                        $('#displayedQuestionnaryQuestionList').empty();
                        clearSessionQuestionnaries();
                        DisableGeneratorButtons();
                        refreshQuestionnaryList();

                        window.setTimeout(function () {
                            document.location.reload();
                        }, 1500);
                    },
                    error: function () {
                        $('#MsgErreur').html("Une erreur s'est produite lors de l'ajout de votre questionnaire dans la base de données. Ceci peut être dû à plusieurs facteurs : \n " +
                            "<ul>" +
                            "<li> Un questionnaire existe déjà en base de données avec le même nom et vous n'avez pas les droits d'accès suffisant pour le modifier. Pour y remédier, vous pouvez " +
                            "essayer avec un autre nom ou contacter un administrateur afin de lui demander de plus amples informations ;</li>" +
                            "<li> Une erreur de transfert avec la base de données est survenu. Pour y remédier, vous pouvez réessayer ultérieurement.</li>" +
                            "</ul>");
                        $('#ErreurSaisie').modal('show');
                        $("#WaitedDemand").modal("hide");
                        window.setTimeout(function () {
                            $("#ErreurSaisie").modal("hide");
                        }, 1500);
                    }
                });
            } else {
                $('#ErreurSaisie').modal('show');
                window.setTimeout(function () {
                    $("#ErreurSaisie").modal("hide");
                }, 1500);
            }
        });

        $('#ajoutDroitsEmailButton').on("click", function () {
            // TODO Ajout des vérifications sur le serveur
            refreshUsersRights();
            if (currentQuestionnary.droitsDeModification == undefined) {
                currentQuestionnary.droitsDeModification = [];
            }
            if ($('#ajoutDroitsEmail').val() != undefined && $('#ajoutDroitsEmail').is(':valid') && jQuery.inArray($('#ajoutDroitsEmail').val(), currentQuestionnary.droitsDeModification) === -1) {
                currentQuestionnary.droitsDeModification.push($('#ajoutDroitsEmail').val());
                refreshUsersRights();
            }
        });

        $(document).on('click', '.deleteUser', function () {
            functionToDelete($(this), currentQuestionnary.droitsDeModification);
            refreshUsersRights();
        });

        $(document).on('click', '.QuestionnaryListElement', function () {
            DisableGeneratorButtons();
        });

        var clearSessionQuestionnaries = function () {
            $.session.clear();
            if (currentQuestionnary != undefined)
                for (var index = 0; index < questionnaryListSession.length; ++index) {
                    if (questionnaryListSession[index].nom == currentQuestionnary.nom) {
                        questionnaryListSession.splice(index, 1);
                        break;
                    }
                }
            $.session.set('questionnaryListSession', JSON.stringify(questionnaryListSession));
        }

        var DisableGeneratorButtons = function () {
            $('#questionnaryNameContainer').empty();
            $('#displayedQuestionnaryQuestionList').empty();
            $('.onFormDisabled').prop("disabled", false);
            $('#popupSharePrivilege').prop("disabled", true);
            $('.onFormShowed').hide();
            $('#popupAddQuestion').hide();
            $('#selectQuestionnaryAction option[value="2"]').attr('disabled', 'disabled'); // On désactive le duplicate
            $('#selectQuestionnaryAction option[value="3"]').attr('disabled', 'disabled'); // On désactive l'update
            $('#selectQuestionnaryAction option[value="4"]').attr('disabled', 'disabled'); // On désactive le delete
            $("#selectQuestionnaryAction").val("0");
        }

        var refreshQuestionnaryList = function () {
            $('#questionnaryListContainerSession').empty();
            $('#questionnaryListContainerDataBase').empty();
            displayQuestionnaryList();
        }


        var UpdateQuestionnary = function () {
            $('#questionnaryNameContainer').empty();
            $('.onFormShowed').show();
            $('#popupAddQuestion').show();
            $('#popupSharePrivilege').prop("disabled", false);
            $('#questionnaryNameContainer').append(
                '<div class="col-md-8">' +
                '   <input class="form-control" type="text" id="questionnaryNameInput" placeholder="Nom de votre questionnaire" value="' + currentQuestionnary.nom + '" required/>' +
                '</div>'
            );
            refreshQuestionnaryQuestionList();
        }

        var FixIconSize = function() {
            if (widthWindow < 1000 || heightWindow < 1000) {
                $('.fa-3x').addClass("fa-2x", "ToFixSize").removeClass('fa-3x');
            } else {
                $('.ToFixSize').addClass("fa-3x").removeClass('fa-2x', 'ToFixSize');
            }
        }

        var refreshUsersRights = function () {
            $('.UsersRightsList').empty();
            var index = 0;

            if (currentQuestionnary.droitsDeModification == undefined) {
                currentQuestionnary.droitsDeModification = new Array();
            }

            if (currentQuestionnary.droitsDeModification != undefined) {
                currentQuestionnary.droitsDeModification.forEach(function (thisUser) {
                    $('.UsersRightsList').append(
                        '<div class=\"row p-1 d-flex justify-content-center\">\n' +
                        '<div class=\"col-md-8 bg-light d-flex justify-content-center pl-1 pr-1 pt-1 rounded\">' + thisUser + '</div> \n' +
                        '<div class=\"col-md-auto d-flex justify-content-center\"><button id=\"deleteUserTag-' + index + '\" class=\"btn btn-outline-danger deleteUser\">X</button></div> \n' +
                        '</div>'
                    );
                    index++;
                });
                if (currentQuestionnary.droitsDeModification.length > 0) {
                    $('.UsersRightsList').addClass('questionCard');
                } else {
                    $('.UsersRightsList').removeClass('questionCard');
                }
            }
        }

        var functionToDelete = function(element, tableau) {
            var userToDelete = parseInt(element.attr('id').split('-')[1]);
            for (var key in tableau) {
                if (tableau.hasOwnProperty(key)) {
                    if (key >= userToDelete && key != tableau.length - 1) {
                        var newOrder = parseInt(key) + 1;
                        tableau[key] = tableau[newOrder];
                    }
                    if (key == tableau.length - 1) {
                        tableau.pop();
                    }
                }
            }
        };

        FixIconSize();      // Application de la fonction

            $.ajax({
                method: "GET",
                url: "genererQuestionnaire",
                cache:false,
                data:{
                    needed : "AllOfName",
                },
                dataType: 'json',
                success: function (json, status) {
                   questionnaryListDataBase = json;
                    displayQuestionnaryList();
                },
                error: function () {
                    $('#MsgErreur').html("Une erreur s'est produite lors de la récupération des questionnaires dans la base de données. Veuillez recharger votre page ou essayer ultérieurement.");
                    $('#ErreurSaisie').modal('show');
                    window.setTimeout(function () {
                        $("#ErreurSaisie").modal("hide");
                    }, 1500);
                }
            });

    });