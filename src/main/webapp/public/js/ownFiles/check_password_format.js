function checkFormat(ID_mdp){
    var ValueMP = $("#"+ID_mdp).val();
    var checkNumber = false;
    var checkLetter = false;
    var RegexpLetter = /^[a-zA-Z]$/;

    for(var current in ValueMP) {

        // Si il y a un chiffre, alors on toggle le boolean checkNumber à true
        if(!checkNumber && !!parseInt(ValueMP[current], 10)) {
            checkNumber = true;
        }


        // Si il y a une lettre, alors on toggle le boolean checkLetter à true
        if(!checkLetter && RegexpLetter.test(ValueMP[current])) {
            checkLetter = true;
        }
    }

    // Si le mot de passe est valide, alors on enable le bouton submit
    if(checkLetter && checkNumber && ValueMP.length >= 8) {
        $("#submitPasswordReset").prop('disabled',false);
    	$("#wrongFormatPasswordText").hide();
    } else {
        $("#submitPasswordReset").prop('disabled',true);
        $("#wrongFormatPasswordText").show();
    }

}
